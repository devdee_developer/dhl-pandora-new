myApp.controller("exportController", [
	"$scope",
	"Upload",
	"$filter",
	"baseService",
	"exportApiService",
	function ($scope, Upload, $filter, baseService, exportApiService) {
		$scope.CreateModel = {};
		$scope.modelSearch = {};
		$scope.downloaddaily = {};
		$scope.url = {};

		$scope.modelDeviceList = [];
		//page system
		$scope.listPageSize = baseService.getListPageSize();
		$scope.TempPageSize = {};
		$scope.TempPageIndex = {};
		$scope.PageSize = baseService.setPageSize(20);
		$scope.totalPage = 1; //init;
		$scope.totalRecords = 0;
		$scope.PageIndex = 1;
		$scope.SortColumn = baseService.setSortColumn("id");
		$scope.SortOrder = baseService.setSortOrder("asc");

		$scope.listdaily = [];
		$scope.TempdailyIndex = {};
		$scope.TempSearchdailyIndex = {};

		$scope.tempWeekIndex = {};
		$scope.tempYearIndex = {};
		$scope.tempMonthIndex = {};
		// $scope.listYear = [{ Year: 2021 }, { Year: 2022 },];

		$scope.listCheckStatus = [
			{ CheckStatus: "All" },
			{ CheckStatus: "Yes" },
			{ CheckStatus: "No" },
		];
		$scope.tempCheckStatusIndex = {};

		$scope.tempQuarterIndex = {};

		$scope.model = {};

		$scope.order = {};

		$scope.isView = false;
		$scope.file = {};

		$scope.file_Pandora_Dashboard = {};
		$scope.fileshipment = {};

		$scope.sort = function (e) {
			baseService.sort(e);
			$scope.SortColumn = baseService.getSortColumn();
			$scope.SortOrder = baseService.getSortOrder();
			$scope.reload();
		};

		$scope.getFirstPage = function () {
			$scope.PageIndex = baseService.getFirstPage();
			$scope.reload();
		};
		$scope.getBackPage = function () {
			$scope.PageIndex = baseService.getBackPage();
			$scope.reload();
		};
		$scope.getNextPage = function () {
			$scope.PageIndex = baseService.getNextPage();
			$scope.reload();
		};
		$scope.getLastPage = function () {
			$scope.PageIndex = baseService.getLastPage();
			$scope.reload();
		};
		$scope.searchByPage = function () {
			$scope.PageIndex = baseService.setPageIndex(
				$scope.TempPageIndex.selected.PageIndex
			);
			$scope.reload();
		};
		$scope.setPageSize = function (data) {
			$scope.PageSize = baseService.setPageSize(
				$scope.TempPageSize.selected.Value
			);
		};
		$scope.loadByPageSize = function () {
			$scope.PageIndex = baseService.setPageIndex(1);
			$scope.setPageSize();
			$scope.reload();
		};
		//page system

		$scope.ShowDevice = function () {
			$scope.onInit();
			$(".exportdaily").show();
			$(".DisplayDevice").show();
		};
		// $scope.resetModel = function () {
		// 	// $scope.CreateModel = { id: 0 };
		// 	// $scope.file_Pandora_Dashboard = "";
		// 	// $scope.fileshipment = "";
		// }
		$scope.resetSearch = function () {
			// console.log($scope.listWeek[0].week);
			$scope.modelSearch = {
				Year: "",
				Month: "",
				week: "",
			};

			$scope.tempWeekIndex.selected = $scope.listWeek[0];
			// $scope.tempYearIndex.selected = $scope.listYear[0];
			$scope.tempMonthIndex.selected = $scope.listMonth[0];
			$scope.tempCheckStatusIndex.selected = $scope.listCheckStatus[0];
		};

		$scope.onInit = function () {
			$(".require").hide();
			// $scope.resetModel();
			$scope.reload();
			// $scope.resetSearch();
		};

		$scope.reload = function () {
			// $scope.listYear();

			$scope.getWeek();
			$scope.getYear();
			$scope.getMonth();
			$scope.getQuarter();

			$scope.getCheckStatus();
		};

		$scope.validatecheck = function () {
			var bResult = true;
			$(".require").hide();
			// if ($scope.file_pnd3.name == undefined) {
			// 	$(".CreateModel_file_pnd3").show();
			// 	bResult = false;
			// }
			return bResult;
		};

		$scope.upload = function () {
			var bValid = $scope.validatecheck();
			var file_Pandora_Dashboard = $scope.file_Pandora_Dashboard.name;
			// var resapple = file_pnd3.substr(file_pnd3.length - 5, file_pnd3.length);
			// var fileshipment = $scope.fileshipment.name;
			// var resshipment = fileshipment.substr(fileshipment.length - 5, fileshipment.length);

			if (true == bValid) {
				baseService.showOverlay();
				var url = get_base_url("Import/upload_file");
				// console.log(url)
				console.log($scope.file_Pandora_Dashboard);
				Upload.upload({
					url: url,
					data: {
						file_Pandora_Dashboard: $scope.file_Pandora_Dashboard,
					},
				}).then(function (resp) {
					// console.log(resp);
					if (resp.status == 200) {
						baseService.hideOverlay();
						$scope.ShowDevice();
						$scope.file_Pandora_Dashboard = undefined;
						baseService.showMessage("Save success");

						// if (resp.data.code == 100) {
						// 	var download_url = get_base_url('');
						// 	var file = download_url + 'upload/' + resp.data.filename;
						// 	window.open(file, '_bank');
						// }
						if (resp.data.status == true) {
							baseService.showMessage("Save success");
						} else {
							baseService.showMessage(resp.data.message);
						}
					} else {
						baseService.showMessage("Can not upload file");
					}
				});
			}
		};

		$scope.onDownload = function (item) {
			// console.log(item.file_name);
			var download_url = get_base_url("");
			// console.log(download_url);
			var file = download_url + "upload/export_excel/" + item.file_name;
			// console.log(file);
			window.open(file, "_blank");
		};

		$scope.onDeleteTagClick = function (item) {
			exportApiService.deleteImport(item, function (result) {
				console.log(result);
				if (result.status === true) {
					baseService.showMessage(result.ShowMessage);
					$scope.reload();
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.Exportexcel = function () {
			console.log($scope.tempYearIndex);

			if ($scope.modelSearch.start_date != undefined) {
				$scope.modelSearch.week = "";
				$scope.modelSearch.Year = "";
			} else {
				if ($scope.tempWeekIndex.selected != undefined) {
					$scope.modelSearch.week = $scope.tempWeekIndex.selected.week_number;
				}
				if ($scope.tempYearIndex.selected != undefined) {
					$scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
				}
			}

			console.log($scope.modelSearch.start_date);

			console.log($scope.modelSearch);

			exportApiService.Exportexcel($scope.modelSearch, function (result) {
				if (result.status === true) {
					var download_url = get_base_url("");
					// console.log(download_url);
					var file = download_url + "upload/pivot/" + result.message;
					console.log(file);
					window.open(file, "_blank");
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.ExportexcelMonthly = function () {
			console.log($scope.tempYearIndex);

			if ($scope.modelSearch.start_date != undefined) {
				$scope.modelSearch.Month = "";
				$scope.modelSearch.Year = "";
			} else {
				if ($scope.tempMonthIndex.selected != undefined) {
					$scope.modelSearch.Month = $scope.tempMonthIndex.selected.shipmonth;
				}
				if ($scope.tempYearIndex.selected != undefined) {
					$scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
				}
			}

			console.log($scope.modelSearch.start_date);

			console.log($scope.modelSearch);

			exportApiService.ExportexcelMonthly(
				$scope.modelSearch,
				function (result) {
					if (result.status === true) {
						var download_url = get_base_url("");
						// console.log(download_url);
						var file = download_url + "upload/pivot/" + result.message;
						console.log(file);
						window.open(file, "_blank");
					} else {
						baseService.showMessage(result.message);
					}
				}
			);
		};

		$scope.ExportexcelDaily = function () {
			// console.log($scope.tempQuarterIndex.selected);

			// if ($scope.modelSearch.start_date != undefined) {
			// 	$scope.modelSearch.week = "";
			// 	$scope.modelSearch.Year = "";
			// } else {
				if ($scope.tempWeekIndex.selected != undefined) {
					$scope.modelSearch.week = $scope.tempWeekIndex.selected.week_number;
				}
				if ($scope.tempYearIndex.selected != undefined) {
					$scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
				}
				if ($scope.tempQuarterIndex.selected != undefined) {
					$scope.modelSearch.QuarterCode =
						$scope.tempQuarterIndex.selected.QuarterCode;
				}
			// } 
			

			console.log($scope.tempQuarterIndex.selected);

			// console.log($scope.modelSearch);

			exportApiService.ExportexcelDaily($scope.modelSearch, function (result) {
				if (result.status === true) {
					var download_url = get_base_url("");
					// console.log(download_url);
					var file = download_url + "upload/pivot/" + result.message;
					console.log(file);
					window.open(file, "_blank");
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.ExportexcelStatus = function () {
			// console.log($scope.model)

			if ($scope.tempCheckStatusIndex.selected != undefined) {
				$scope.model.CheckStatusCode =
					$scope.tempCheckStatusIndex.selected.CheckStatusCode;
			}

			exportApiService.ExportexcelStatus($scope.model, function (result) {
				if (result.status === true) {
					var download_url = get_base_url("upload/export_excel/");
					console.log(download_url);
					var file = download_url + result.message;
					console.log(file);
					window.open(file, "_blank");
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.ExportWaitCheckpoint = function () {
			if ($scope.modelSearch.start_date != undefined) {
				$scope.modelSearch.week = "";
				$scope.modelSearch.Year = "";
			} else {
				if ($scope.tempWeekIndex.selected != undefined) {
					$scope.modelSearch.week = $scope.tempWeekIndex.selected.week;
				}
				if ($scope.tempYearIndex.selected != undefined) {
					$scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
				}
			}
			console.log($scope.modelSearch);

			exportApiService.ExportWaitCheckpoint(
				$scope.modelSearch,
				function (result) {
					if (result.status === true) {
						var download_url = get_base_url("");
						// console.log(download_url);
						var file = download_url + "upload/export_excel/" + result.message;
						window.open(file, "_blank");
					} else {
						baseService.showMessage(result.message);
					}
				}
			);
		};

		$scope.getWeek = function (item) {
			exportApiService.getComboBox($scope.modelSearch, function (result) {
				// console.log(result);
				if (result.status === true) {
					$scope.listWeek = result.message;
					// console.log($scope.listWeek[0]["week_number"]);
					if ($scope.listWeek.length > 0) {
						if (item == undefined) {
							$scope.tempWeekIndex.selected = $scope.listWeek[0];
							// console.log($scope.tempWeekIndex.selected);
						} else {
							$scope.listWeek.forEach(function (entry, index) {
								// console.log(entry.week,"-" ,item.week)
								if (entry.week_number === item.week_number) {
									console.log(entry.week_number);
									$scope.tempWeekIndex.selected = entry;
								}
							});
							console.log("edit");
						}
					} else {
						$scope.tempWeekIndex.selected = undefined;
						console.log($scope.tempWeekIndex.selected);
					}
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.getYear = function (item) {
			exportApiService.getYearComboBox($scope.modelSearch, function (result) {
				// console.log(result);
				if (result.status === true) {
					$scope.listYear = result.message;
					// console.log($scope.listYear);
					if ($scope.listYear.length > 0) {
						if (item == undefined) {
							$scope.tempYearIndex.selected = $scope.listYear[0];
							// console.log($scope.tempYearIndex.selected);
						} else {
							$scope.listYear.forEach(function (entry, index) {
								if (entry.Year === item.Year) {
									$scope.tempYearIndex.selected = entry;
								}
							});
							console.log("edit");
						}
					} else {
						$scope.tempYearIndex.selected = undefined;
					}
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		// $scope.minDateQ1 = "2021-12-31";
		// $scope.maxDateQ1 = "2022-03-31";

		// $scope.minDateQ2 = "2022-03-31";
		// $scope.maxDateQ2 = "2022-06-30";

		// $scope.minDateQ3 = "2022-06-30";
		// $scope.maxDateQ3 = "2022-09-30";

		// $scope.minDateQ4 = "2022-09-30";
		// $scope.maxDateQ4 = "2022-12-31";
		

		$scope.getQuarter = function () {

			

			$scope.modelSearch.start_date = ""
			$scope.modelSearch.end_date = ""

			$scope.listQuarter = [
				{ QuarterCode: "q1", Quarterlist: "1" },
				{ QuarterCode: "q2", Quarterlist: "2" },
				{ QuarterCode: "q3", Quarterlist: "3" },
				{ QuarterCode: "q4", Quarterlist: "4" },
			];

			//select calendar
			$scope.year = $scope.tempYearIndex.selected.Year;

			if ($scope.tempQuarterIndex.selected != undefined) {
				if ($scope.tempQuarterIndex.selected["Quarterlist"] == 1) {
					$scope.yearMinus = $scope.tempYearIndex.selected.Year - 1;

					$scope.minDate = $scope.yearMinus + "-12-31";
					$scope.maxDate = $scope.year + "-03-31";
				} else if ($scope.tempQuarterIndex.selected["Quarterlist"] == 2) {
					$scope.minDate = $scope.year + "-03-31";
					$scope.maxDate = $scope.year + "-06-30";
				} else if ($scope.tempQuarterIndex.selected["Quarterlist"] == 3) {
					$scope.minDate = $scope.year + "-06-30";
					$scope.maxDate = $scope.year + "-09-30";
				} else if ($scope.tempQuarterIndex.selected["Quarterlist"] == 4) {
					$scope.minDate = $scope.year + "-09-30";
					$scope.maxDate = $scope.year + "-12-31";
				}
			} else {
			}

			//currentQuarter Quarter ปัจจุบัน
			//checkCurrentQuarter ตัวแปรที่จะส่งไปบอกว่าเป็น Quarter ไหม

			// $scope.date = new Date();
			// $scope.currentYear = $scope.date.getFullYear();
			// $scope.currentMonth = $scope.date.getMonth() + 1;

			// if ($scope.currentMonth == 1 || $scope.currentMonth == 2 || $scope.currentMonth == 3) {
			// 	$scope.currentQuarter = "1"
			// } else if ($scope.currentMonth == 4 || $scope.currentMonth == 5 || $scope.currentMonth == 6) {
			// 	$scope.currentQuarter = "2"
			// } else if ($scope.currentMonth == 7 || $scope.currentMonth == 8 || $scope.currentMonth == 9) {
			// 	$scope.currentQuarter = "3"
			// } else if ($scope.currentMonth == 10 || $scope.currentMonth == 11 || $scope.currentMonth == 12) {
			// 	$scope.currentQuarter = "4"
			// } 
			

			// console.log($scope.tempQuarterIndex.selected["Quarterlist"])
			// console.log($scope.currentQuarter)

			// 	if ($scope.tempQuarterIndex.selected["Quarterlist"] != $scope.currentQuarter) {
			// 		$scope.checkCurrentQuarter = "1"
			// 	} else{
			// 		$scope.checkCurrentQuarter = "0"
			// 	}


			// $scope.tempQuarterIndex = {};
			// if (item == undefined) {
			// 	$scope.tempCheckStatusIndex.selected = $scope.listCheckStatus[1];
			// 	console.log($scope.tempCheckStatusIndex.selected);
			// }
		};

		$scope.getMonth = function (item) {
			exportApiService.getMonthComboBox($scope.modelSearch, function (result) {
				// console.log(result);
				if (result.status === true) {
					$scope.listMonth = result.message;
					// console.log($scope.listMonth);
					if ($scope.listMonth.length > 0) {
						if (item == undefined) {
							$scope.tempMonthIndex.selected = $scope.listMonth[0];
							console.log("add");
						} else {
							$scope.listMonth.forEach(function (entry, index) {
								if (entry.Month === item.Month) {
									$scope.tempMonthIndex.selected = entry;
								}
							});
							console.log("edit");
						}
					} else {
						$scope.tempMonthIndex.selected = undefined;
					}
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.getCheckStatus = function (item) {
			console.log(item);
			$scope.listCheckStatus = [
				{ CheckStatusCode: "", CheckStatuslist: "All" },
				{
					CheckStatusCode: "N",
					CheckStatuslist: "Wrong status ( Ontime->Over Due)",
				},
			];

			$scope.tempCheckStatusIndex = {};
			if (item == undefined) {
				$scope.tempCheckStatusIndex.selected = $scope.listCheckStatus[1];
				console.log($scope.tempCheckStatusIndex.selected);
			}
		};
	},
]);

myApp.controller("slaAuNzController", [
	"$scope",
	"Upload",
	"$filter",
	"baseService",
	"slaAuNzApiService",
	function ($scope, Upload, $filter, baseService, slaAuNzApiService) {
		$scope.modelDeviceList = [];
		$scope.CreateModel = {};
		$scope.modelSearch = {};

		$scope.file_Pandora_SlaAu = {};
		//page system
		$scope.listPageSize = baseService.getListPageSize();
		$scope.TempPageSize = {};
		$scope.TempPageIndex = {};
		$scope.PageSize = baseService.setPageSize(20);
		$scope.totalPage = 1; //init;
		$scope.totalRecords = 0;
		// $scope.PageIndex = 1;
		$scope.PageIndex = baseService.setPageIndex(1);
		$scope.SortColumn = baseService.setSortColumn("Id");
		$scope.SortOrder = baseService.setSortOrder("asc");

		$scope.TempServicecenterIndex = {};
		$scope.listServicecenter = [
			{ Svc: "APD" },
			{ Svc: "CXM" },
			{ Svc: "DOM" },
			{ Svc: "EGW" },
			{ Svc: "GDR" },
			{ Svc: "HTY" },
			{ Svc: "HUB" },
			{ Svc: "LZB" },
			{ Svc: "NBK" },
			{ Svc: "NKR" },
			{ Svc: "PCB" },
			{ Svc: "PGT" },
			{ Svc: "RMT" },
			{ Svc: "TAK" },
			{ Svc: "TZB" },
			{ Svc: "LZN" },
			{ Svc: "ZVB" },
		];

		$scope.isView = false;

		$scope.sort = function (e) {
			baseService.sort(e);
			$scope.SortColumn = baseService.getSortColumn();
			$scope.SortOrder = baseService.getSortOrder();
			$scope.reload();
		};

		$scope.column = "destination_city";
		$scope.reverse = false;

		// called on header click
		$scope.sortColumn = function (col) {
			$scope.column = col;
			if ($scope.reverse) {
				$scope.reverse = false;
				$scope.reverseclass = "arrow-up";
			} else {
				$scope.reverse = true;
				$scope.reverseclass = "arrow-down";
			}
		};

		// remove and change class
		$scope.sortClass = function (col) {
			if ($scope.column == col) {
				if ($scope.reverse) {
					return "arrow-down";
				} else {
					return "arrow-up";
				}
			} else {
				return "";
			}
		};

		$scope.getFirstPage = function () {
			$scope.PageIndex = baseService.getFirstPage();
			$scope.reload();
		};
		$scope.getBackPage = function () {
			$scope.PageIndex = baseService.getBackPage();
			$scope.reload();
		};
		$scope.getNextPage = function () {
			$scope.PageIndex = baseService.getNextPage();
			$scope.reload();
		};
		$scope.getLastPage = function () {
			$scope.PageIndex = baseService.getLastPage();
			$scope.reload();
		};
		$scope.searchByPage = function () {
			$scope.PageIndex = baseService.setPageIndex(
				$scope.TempPageIndex.selected.PageIndex
			);
			$scope.reload();
		};
		$scope.setPageSize = function (data) {
			$scope.PageSize = baseService.setPageSize(
				$scope.TempPageSize.selected.Value
			);
		};
		$scope.loadByPageSize = function () {
			$scope.PageIndex = baseService.setPageIndex(1);
			$scope.setPageSize();
			$scope.reload();
		};
		//page system

		$scope.ShowDevice = function () {
			$(".DisplayDevice").show();
			$(".SearchDevice").hide();
			$(".addDevice").hide();
			$scope.reload();
		};

		$scope.ShowSearch = function () {
			$(".DisplayDevice").hide();
			$(".SearchDevice").show();
			$(".addDevice").hide();
		};

		$scope.LoadSearch = function () {
			$scope.ShowDevice();
		};

		$scope.AddNewDevice = function () {
			$scope.resetModel();
			$(".require").hide();
			$(".DisplayDevice").hide();
			$(".SearchDevice").hide();
			$(".addDevice").show();
		};

		$scope.onEditTagClick = function (item) {
			$scope.AddNewDevice();
			$scope.loadEditData(item);
		};

		$scope.loadEditData = function (item) {
			console.log(item);
			$scope.CreateModel = angular.copy(item);

			$scope.listServicecenter.forEach(function (entry, index) {
				if (item.DHL_Svc === entry.Svc)
					$scope.TempServicecenterIndex.selected = entry;
			});
		};

		$scope.resetModel = function () {
			$scope.CreateModel = {
				Id: 0,
				destination_city: "",
				destination_postcode: "",
				destination_svc_area_code: "",
				destination_country_code: "",
				lane: "",
				delivery_provider: "",
				gateway: "",
				t_t_sla: "",
				mon: "",
				tue: "",
				wed: "",
				thu: "",
				fri: "",
				sat: "",
				t_t_pandemic: "",
				product: "",
				type: "",
			};
			// Store: ""

			$scope.file_Pandora_SlaAu = "";
		};

		$scope.resetSearch = function () {
			$scope.modelSearch = {
				// "Store": "",
				destination_city: "",
				destination_postcode: "",
				destination_svc_area_code: "",
				destination_country_code: "",
				lane: "",
				delivery_provider: "",
				gateway: "",
				t_t_sla: "",
				"1_mon": "",
				"2_tue": "",
				"3_wed": "",
				"4_thu": "",
				"5_fri": "",
				"6_sat": "",
				t_t_pandemic: "",
				product: "",
				type: "",
			};
			$scope.LoadSearch();
		};

		////////////////////////////////////////////////////////////////////////////////////////
		// Event
		$scope.onInit = function () {
			$scope.resetModel();
			$scope.resetSearch();

			$scope.listPageSize.forEach(function (entry, index) {
				if (0 === index) $scope.TempPageSize.selected = entry;
			});

			//$scope.reload();
		};

		$scope.reload = function () {
			slaAuNzApiService.ListSlaAuNz($scope.modelSearch, function (results) {
				var result = results.data;
				if (result.status === true) {
					$scope.totalPage = result.toTalPage;
					$scope.listPageIndex = baseService.getListPage(result.toTalPage);
					$scope.listPageIndex.forEach(function (entry, index) {
						if ($scope.PageIndex === entry.Value)
							$scope.TempPageIndex.selected = entry;
					});
					$scope.modelDeviceList = result.message;
					$scope.modelDeviceList.forEach(function (entry) {
						entry.mon = entry["1_mon"];
						entry.tue = entry["2_tue"];
						entry.wed = entry["3_wed"];
						entry.thu = entry["4_thu"];
						entry.fri = entry["5_fri"];
						entry.sat = entry["6_sat"];
						// console.log(entry.mon);
					});

					console.log($scope.modelDeviceList);
					$scope.totalRecords = result.totalRecords;
				} else {
				}
			});
		};

		$scope.onDeleteTagClick = function (item) {
			slaAuNzApiService.DeleteSlaAuNz({ id: item.id }, function (result) {
				if (result.status === true) {
					$scope.reload();
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.validatecheck = function () {
			var bResult = true;
			$(".require").hide();

			// if ($scope.CreateModel.Store == "") {
			//     $(".CreateModel_Store").show();
			//     bResult = false;
			// }
			if ($scope.CreateModel.destination_city == "") {
				$(".CreateModel_destination_city").show();
				bResult = false;
			}

			if ($scope.CreateModel.destination_postcode == "") {
				$(".CreateModel_destination_postcode").show();
				bResult = false;
			}

			if ($scope.CreateModel.destination_svc_area_code == "") {
				$(".CreateModel_destination_svc_area_code").show();
				bResult = false;
			}

			if ($scope.CreateModel.destination_country_code == "") {
				$(".CreateModel_destination_country_code").show();
				bResult = false;
			}

			if ($scope.CreateModel.lane == "") {
				$(".CreateModel_lane").show();
				bResult = false;
			}

			if ($scope.CreateModel.delivery_provider == "") {
				$(".CreateModel_delivery_provider").show();
				bResult = false;
			}

			if ($scope.CreateModel.gateway == "") {
				$(".CreateModel_gateway").show();
				bResult = false;
			}

			if ($scope.CreateModel.t_t_sla == "") {
				$(".CreateModel_t_t_sla").show();
				bResult = false;
			}

			if ($scope.CreateModel.mon == "") {
				$(".CreateModel_mon").show();
				bResult = false;
			}

			if ($scope.CreateModel.tue == "") {
				$(".CreateModel_tue").show();
				bResult = false;
			}

			if ($scope.CreateModel.wed == "") {
				$(".CreateModel_wed").show();
				bResult = false;
			}

			if ($scope.CreateModel.thu == "") {
				$(".CreateModel_thu").show();
				bResult = false;
			}

			if ($scope.CreateModel.fri == "") {
				$(".CreateModel_fri").show();
				bResult = false;
			}

			if ($scope.CreateModel.sat == "") {
				$(".CreateModel_sat").show();
				bResult = false;
			}

			if ($scope.CreateModel.t_t_pandemic == "") {
				$(".CreateModel_t_t_pandemic").show();
				bResult = false;
			}

			if ($scope.CreateModel.product == "") {
				$(".CreateModel_product").show();
				bResult = false;
			}

			if ($scope.CreateModel.type == "") {
				$(".CreateModel_type").show();
				bResult = false;
			}

			return bResult;
		};

		$scope.onSaveTagClick = function () {
			var bValid = $scope.validatecheck();

			if (true == bValid) {
				// console.log($scope.CreateModel);
				// $scope.CreateModel.DHL_Svc = $scope.TempServicecenterIndex.selected.Svc;
				slaAuNzApiService.SaveSlaAuNz($scope.CreateModel, function (result) {
					if (result.status == true) {
						baseService.showMessage(result.message);
						$scope.ShowDevice();
					} else {
						baseService.showMessage(result.message);
					}
				});
			}
		};

		$scope.upload = function () {
			// console.log($scope.file_Pandora_SlaAuNz);
			var bValid = $scope.validatecheck();
			if ($scope.file_Pandora_SlaAuNz != undefined) {
				var file_Pandora_SlaAuNz = $scope.file_Pandora_SlaAuNz.name;
			}
			// console.log(file_Pandora_SlaAuNz);
			// var resapple = file_pnd3.substr(file_pnd3.length - 5, file_pnd3.length);
			// var fileshipment = $scope.fileshipment.name;
			// var resshipment = fileshipment.substr(fileshipment.length - 5, fileshipment.length);

			if (true == bValid) {
				baseService.showOverlay();
				var url = get_base_url("SlaAuNz/upload_file");
				// console.log(file_Pandora_SlaAuNz)
				console.log($scope.file_Pandora_SlaAuNz);
				Upload.upload({
					url: url,
					data: {
						file_Pandora_SlaAuNz: $scope.file_Pandora_SlaAuNz,
					},
				}).then(function (resp) {
					console.log(resp);
					if (resp.status == 200) {
						baseService.hideOverlay();
						$scope.ShowDevice();
						$scope.file_Pandora_SlaAuNz = undefined;
						baseService.showMessage("Save success");

						// if (resp.data.code == 100) {
						// 	var download_url = get_base_url('');
						// 	var file = download_url + 'upload/' + resp.data.filename;
						// 	window.open(file, '_bank');
						// }
						if (resp.data.status == true) {
							baseService.showMessage("Save success");
						} else {
							baseService.showMessage(resp.data.message);
						}
					} else {
						baseService.showMessage("Can not upload file");
					}
				});
			}
		};

		$scope.validatecheck = function () {
			var bResult = true;
			$(".require").hide();
			// if ($scope.file_pnd3.name == undefined) {
			// 	$(".CreateModel_file_pnd3").show();
			// 	bResult = false;
			// }
			return bResult;
		};

		$scope.Exportexcel = function () {
			// if($scope.tempWeekIndex.selected != undefined){
			//     $scope.modelSearch.week = $scope.tempWeekIndex.selected.week;
			// }
			// if($scope.tempYearIndex.selected != undefined){
			//     $scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
			// }
			// console.log($scope.modelSearch);

			slaAuNzApiService.Exportexcel($scope.modelSearch, function (result) {
				if (result.status === true) {
					var download_url = get_base_url("");
					// console.log(download_url);
					var file = download_url + "upload/export_excel/" + result.message;
					window.open(file, "_blank");
				} else {
					baseService.showMessage(result.message);
				}
			});
		};
	},
]);

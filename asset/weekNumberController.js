myApp.controller("weekNumberController", [
	"$scope",
	"Upload",
	"$filter",
	"baseService",
	"weekNumberApiService",
	function ($scope, Upload, $filter, baseService, weekNumberApiService) {
		$scope.modelDeviceList = [];
		$scope.CreateModel = {};
		$scope.modelSearch = {};

		$scope.file_Pandora_Week = {};
		//page system
		$scope.listPageSize = baseService.getListPageSize();
		$scope.TempPageSize = {};
		$scope.TempPageIndex = {};
		$scope.PageSize = baseService.setPageSize(20);
		$scope.totalPage = 1; //init;
		$scope.totalRecords = 0;
		// $scope.PageIndex = 1;
		$scope.PageIndex = baseService.setPageIndex(1);
		$scope.SortColumn = baseService.setSortColumn("Id");
		$scope.SortOrder = baseService.setSortOrder("asc");

		$scope.TempServicecenterIndex = {};

		$scope.isView = false;

		$scope.sort = function (e) {
			baseService.sort(e);
			$scope.SortColumn = baseService.getSortColumn();
			$scope.SortOrder = baseService.getSortOrder();
			$scope.reload();
		};

		$scope.column = "week";
		$scope.reverse = false;

		// called on header click
		$scope.sortColumn = function (col) {
			$scope.column = col;
			if ($scope.reverse) {
				$scope.reverse = false;
				$scope.reverseclass = "arrow-up";
			} else {
				$scope.reverse = true;
				$scope.reverseclass = "arrow-down";
			}
		};

		// remove and change class
		$scope.sortClass = function (col) {
			if ($scope.column == col) {
				if ($scope.reverse) {
					return "arrow-down";
				} else {
					return "arrow-up";
				}
			} else {
				return "";
			}
		};

		$scope.getFirstPage = function () {
			$scope.PageIndex = baseService.getFirstPage();
			$scope.reload();
		};
		$scope.getBackPage = function () {
			$scope.PageIndex = baseService.getBackPage();
			$scope.reload();
		};
		$scope.getNextPage = function () {
			$scope.PageIndex = baseService.getNextPage();
			$scope.reload();
		};
		$scope.getLastPage = function () {
			$scope.PageIndex = baseService.getLastPage();
			$scope.reload();
		};
		$scope.searchByPage = function () {
			$scope.PageIndex = baseService.setPageIndex(
				$scope.TempPageIndex.selected.PageIndex
			);
			$scope.reload();
		};
		$scope.setPageSize = function (data) {
			$scope.PageSize = baseService.setPageSize(
				$scope.TempPageSize.selected.Value
			);
		};
		$scope.loadByPageSize = function () {
			$scope.PageIndex = baseService.setPageIndex(1);
			$scope.setPageSize();
			$scope.reload();
		};
		//page system

		$scope.ShowDevice = function () {
			$(".DisplayDevice").show();
			$(".SearchDevice").hide();
			$(".addDevice").hide();
			$scope.reload();
		};

		$scope.ShowSearch = function () {
			$(".DisplayDevice").hide();
			$(".SearchDevice").show();
			$(".addDevice").hide();
		};

		$scope.LoadSearch = function () {
			$scope.ShowDevice();
		};

		$scope.AddNewDevice = function () {
			$scope.resetModel();
			$(".require").hide();
			$(".DisplayDevice").hide();
			$(".SearchDevice").hide();
			$(".addDevice").show();
		};

		$scope.onEditTagClick = function (item) {
			$scope.AddNewDevice();
			$scope.loadEditData(item);
		};

		$scope.loadEditData = function (item) {
			console.log(item);
			$scope.CreateModel = angular.copy(item);
		};

		$scope.resetModel = function () {
			$scope.CreateModel = { id: 0, week: "", start: "", end: "" };
			$scope.file_Pandora_Week = "";
		};

		$scope.resetSearch = function () {
			$scope.modelSearch = {
				// "Store": "",
				week: "",
				start: "",
				end: "",
			};
			$scope.LoadSearch();
		};

		////////////////////////////////////////////////////////////////////////////////////////
		// Event
		$scope.onInit = function () {
			$scope.resetModel();
			$scope.resetSearch();

			$scope.listPageSize.forEach(function (entry, index) {
				if (0 === index) $scope.TempPageSize.selected = entry;
			});

			//$scope.reload();
		};

		$scope.reload = function () {
			weekNumberApiService.ListWeekNumber(
				$scope.modelSearch,
				function (results) {
					var result = results.data;
					if (result.status === true) {
						$scope.totalPage = result.toTalPage;
						$scope.listPageIndex = baseService.getListPage(result.toTalPage);
						$scope.listPageIndex.forEach(function (entry, index) {
							if ($scope.PageIndex === entry.Value)
								$scope.TempPageIndex.selected = entry;
						});
						$scope.modelDeviceList = result.message;
						$scope.totalRecords = result.totalRecords;
					} else {
					}
				}
			);
		};

		$scope.upload = function () {
			// console.log(Upload);
			// var bValid = $scope.validatecheck();
			if ($scope.file_Pandora_Week != undefined) {
				var file_Pandora_Week = $scope.file_Pandora_Week.name;
			}
			// var resapple = file_pnd3.substr(file_pnd3.length - 5, file_pnd3.length);
			// var fileshipment = $scope.fileshipment.name;
			// var resshipment = fileshipment.substr(fileshipment.length - 5, fileshipment.length);

			baseService.showOverlay();
			var url = get_base_url("WeekNumber/upload_file");
			// console.log(url)
			console.log($scope.file_Pandora_Week);
			Upload.upload({
				url: url,
				data: {
					file_Pandora_Week: $scope.file_Pandora_Week,
				},
			}).then(function (resp) {
				// console.log(resp);
				if (resp.status == 200) {
					baseService.hideOverlay();
					$scope.ShowDevice();
					$scope.file_Pandora_Week = undefined;
					baseService.showMessage("Save success");

					// if (resp.data.code == 100) {
					// 	var download_url = get_base_url('');
					// 	var file = download_url + 'upload/' + resp.data.filename;
					// 	window.open(file, '_bank');
					// }
					if (resp.data.status == true) {
						baseService.showMessage("Save success");
					} else {
						baseService.showMessage(resp.data.message);
					}
				} else {
					baseService.showMessage("Can not upload file");
				}
			});
		};

		$scope.onDeleteTagClick = function (item) {
			weekNumberApiService.DeleteWeekNumber({ id: item.id }, function (result) {
				if (result.status === true) {
					$scope.reload();
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.validatecheck = function () {
			var bResult = true;
			$(".require").hide();

			// if ($scope.CreateModel.Store == "") {
			//     $(".CreateModel_Store").show();
			//     bResult = false;
			// }
			if ($scope.CreateModel.week == "") {
				$(".CreateModel_week").show();
				bResult = false;
			}

			if ($scope.CreateModel.start == "") {
				$(".CreateModel_start").show();
				bResult = false;
			}

			if ($scope.CreateModel.end == "") {
				$(".CreateModel_end").show();
				bResult = false;
			}

			return bResult;
		};

		$scope.onSaveTagClick = function () {
			var bValid = $scope.validatecheck();

			if (true == bValid) {
				//console.log($scope.CriteriaModel);
				// $scope.CreateModel.Id = 0;
				// $scope.CreateModel.DHL_Svc = $scope.TempServicecenterIndex.selected.Svc;
				weekNumberApiService.SaveWeekNumber(
					$scope.CreateModel,
					function (result) {
						if (result.status == true) {
							baseService.showMessage(result.message);
							$scope.ShowDevice();
						} else {
							baseService.showMessage(result.message);
						}
					}
				);
			}
		};

		$scope.Exportexcel = function () {
			// if($scope.tempWeekIndex.selected != undefined){
			//     $scope.modelSearch.week = $scope.tempWeekIndex.selected.week;
			// }
			// if($scope.tempYearIndex.selected != undefined){
			//     $scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
			// }
			// console.log($scope.modelSearch);

			weekNumberApiService.Exportexcel($scope.modelSearch, function (result) {
				if (result.status === true) {
					var download_url = get_base_url("");
					// console.log(download_url);
					var file = download_url + "upload/export_excel/" + result.message;
					window.open(file, "_blank");
				} else {
					baseService.showMessage(result.message);
				}
			});
		};
	},
]);

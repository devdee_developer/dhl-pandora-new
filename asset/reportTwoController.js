myApp.controller('reportTwoController', ['$scope', '$filter', 'baseService', 'reportTwoApiService', function ($scope, $filter, baseService, reportTwoApiService) {

    $scope.modelDeviceList = [];
    $scope.CreateModel = {};
    $scope.modelSearch = {};

    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('id');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.listDatalastCheckPiont = [];

    $scope.tempQuaterIndex = {};
	$scope.tempYearIndex = {};

    $scope.listQuater = [{ Quater: "Q1" }, { Quater: "Q2" }, { Quater: "Q3" }, { Quater: "Q4" },];

    $scope.TempSddStatusIndex = {};
    $scope.listSddStatus = [{ Status: "All" }, { Status: "Overdue & Due Today" }, { Status: "Future Shipments" },];

    $scope.DataSearchSddStatus = {};

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e);
        $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () {
        $scope.PageIndex = baseService.getFirstPage();
        $scope.reload();
    }
    $scope.getBackPage = function () {
        $scope.PageIndex = baseService.getBackPage();
        $scope.reload();
    }
    $scope.getNextPage = function () {
        $scope.PageIndex = baseService.getNextPage();
        $scope.reload();
    }
    $scope.getLastPage = function () {
        $scope.PageIndex = baseService.getLastPage();
        $scope.reload();
    }
    $scope.searchByPage = function () {
        $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex);
        $scope.reload();
    }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () {
        $scope.PageIndex = baseService.setPageIndex(1);
        $scope.setPageSize();
        $scope.reload();
    }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide();
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();

    }

    $scope.AddNewDevice = function () {
        $scope.resetModel();
        $(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide();
        $(".addDevice").show();

    }


    $scope.onEditTagClick = function (item) {
        $scope.AddNewDevice();
        $scope.loadEditData(item);

    }

    $scope.loadEditData = function (item) {
        $scope.CreateModel = angular.copy(item);
        console.log($scope.CreateModel)
    }

    $scope.resetModel = function () {

        $scope.CreateModel = { id: 0, reason_code: "", code_description: "", check_point: "", remark: "" };
    }


    $scope.resetSearch = function () {
        $scope.modelSearch = {
            "Quater": "",
            "Year": "",
        };

        $scope.tempQuaterIndex.selected = $scope.listQuater[0];
		$scope.tempYearIndex.selected =  $scope.listYear[0];
        // $scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () {
        $(".require").hide();
        $scope.tempQuaterIndex.selected = $scope.listQuater[0];
        // console.log($scope.listQuater);
        // $scope.resetModel();
        // $scope.resetSearch();
        $scope.reload();


        // $scope.listPageSize.forEach(function (entry, index) {
        //     if (0 === index)
        //         $scope.TempPageSize.selected = entry;
        // });

        //$scope.reload();
    }

//     $scope.getQuater = function (item) {

//         reportTwoApiService.getComboBox($scope.modelSearch, function (result) {
//           console.log(result)
//           if (result.status === true) {
//               $scope.listQuater = result.message;
//               console.log(item)
//               if($scope.listQuater.length>0){
              
//                   if(item==undefined){
//                       $scope.tempQuaterIndex.selected = $scope.listQuater[0];
//                       console.log("add")
//                   }else{
//                       $scope.listQuater.forEach(function (entry, index) {
//                           // console.log(entry.Quater,"-" ,item.Quater)
//                           if (entry.Quater === item.Quater) {
//                               $scope.tempQuaterIndex.selected = entry;
//                           }
//                       });
//                       console.log("edit")
                      
//                   }
//               }else{
//                   $scope.tempQuaterIndex.selected=undefined;
                      
//               }
              
              
//           } else {
//               baseService.showMessage(result.message);
//           }
//       });
//   }

  $scope.getYear = function (item) {

    reportTwoApiService.getYearComboBox($scope.modelSearch, function (result) {
        console.log(result)
        if (result.status === true) {
            $scope.listYear = result.message;
            console.log($scope.listYear )
            if($scope.listYear.length>0){

                if(item==undefined){
                    $scope.tempYearIndex.selected = $scope.listYear[0];
                    console.log("add")
                }else{
                    $scope.listYear.forEach(function (entry, index) {
                       
                        if (entry.Year === item.Year) {
                            $scope.tempYearIndex.selected = entry;
                        }
                    });
                    console.log("edit")
                    
                }
            }else{
                $scope.tempYearIndex.selected=undefined;
                    
            }
            
        } else {
            baseService.showMessage(result.message);
        }
    });
    }


    $scope.onTimePerformance = function(ontime, order){

        // $scope.modelDeviceList = data;
        console.log(ontime);
        $scope.label1 = [];
        $scope.data1 = [];
        $scope.data2 = [];
        $scope.percent = [];
        $scope.totalLabel = [];

        $scope.title = "ONTIME PERFORMANCE: " + ontime[0]['Quater'];

        for(var i = 0; i < ontime.length;i++){
            $scope.label1.push(ontime[i]['shipmonth']);
            $scope.data1.push(ontime[i]['OnTimeStatus']);
            $scope.data2.push(order[i]['OnTimeStatus']);
        }
        console.log($scope.data1);
        console.log($scope.data2);

        for(var i = 0; i < 3;i++){
            $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.data2[i]));
            // $scope.totalLabel.push($scope.total[i]);
        }

        $scope.label1 = [...new Set($scope.label1)];
        console.log(ontime);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('onTimePerformance');
        // console.log(window.myChart);
        if (window.myChart) window.myChart.destroy();
        window.myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [$scope.label1[2], $scope.label1[1], $scope.label1[0]],
                datasets: [{
                    type: 'line',
                    label: 'On time',
                    data: [$scope.percent[2], $scope.percent[1], $scope.percent[0]],
                    fill: false,
                        borderColor: 'rgb(75,192,192)',
                        tension: 0.1,
                },{
                    type: 'bar',
                    label: 'On time',
                    data: [$scope.percent[2], $scope.percent[1], $scope.percent[0]],
                    backgroundColor: [
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: $scope.title
                }
            }
        });
    }

    $scope.onTimePerformanceQuarterly = function(ontime, order, ontimebefore, orderbefore){

        // $scope.modelDeviceList = data;
        // console.log(data);
        $scope.label1 = [];
        $scope.data1 = [];
        $scope.data2 = [];
        $scope.data3 = [];
        $scope.data4 = [];
        $scope.percent = [];
        $scope.percentprev = [];
        $scope.totalLabel = [];

        $scope.title = "ONTIME PERFORMANCE QUARTERLY: " + ontime[0]['Quater'];

        console.log(ontimebefore);

        for(var i = 0; i < ontime.length;i++){
            $scope.label1[1] = ontime[i]['Quater'];
            $scope.data1.push(ontime[i]['OnTimeStatus']);
            $scope.data2.push(order[i]['OnTimeStatus']);
        }

        for(var i = 0; i < ontimebefore.length;i++){
            $scope.label1[0] = ontimebefore[i]['Quater'];
            $scope.data3.push(ontimebefore[i]['OnTimeStatus']);
            $scope.data4.push(orderbefore[i]['OnTimeStatus']);
        }

        
        

        for(var i = 0; i < 1;i++){
            $scope.data1[i] = parseInt($scope.data1[i]) + parseInt($scope.data1[i+1]) + parseInt($scope.data1[i+2]);
            $scope.data2[i] = parseInt($scope.data2[i]) + parseInt($scope.data2[i+1]) + parseInt($scope.data2[i+2]);
            $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.data2[i]));
            // $scope.totalLabel.push($scope.total[i]);
        }

        console.log($scope.data1);
        console.log($scope.data2);
        console.log($scope.percent);
        

        for(var i = 0; i < 1;i++){
            $scope.percentprev[i] = Math.round((parseInt($scope.data3[i]) * 100) / parseInt($scope.data4[i]));
            // $scope.totalLabel.push($scope.total[i]);
        }

        $scope.label1 = [...new Set($scope.label1)];
        console.log(ontime);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('onTimePerformanceQuarterly');
        if (window.quarterChart) window.quarterChart.destroy();
        window.quarterChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: $scope.label1,
                datasets: [{
                    type: 'line',
                    label: 'On time',
                    data: [$scope.percentprev, $scope.percent],
                    fill: false,
                        borderColor: 'rgb(75,192,192)',
                        tension: 0.1,
                },{
                    type: 'bar',
                    label: 'On time',
                    data: [$scope.percentprev, $scope.percent],
                    backgroundColor: [
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: $scope.title
                }
            }
        });
    }

    $scope.dhlSupport = function(data, order){
        $scope.label1 = [];
        $scope.data1 = [];
        $scope.percent = [];
        $scope.percent2 = 0;

        console.log(data);
        for(var i = 0; i < data.length;i++){
            $scope.label1.push(data[i]['factor_incident']);
            // $scope.label2.push(data[i]['lane']);
            $scope.data1.push(data[i]['CountWaybillNumber']);
        }

        for(var i = 0; i < order.length;i++){
            $scope.percent2 += parseInt(order[i]['OnTimeStatus']);
        }

        // console.log($scope.error);
        // console.log(order);

        $scope.label1 = [...new Set($scope.label1)];

        for(var i = 0; i < $scope.label1.length;i++){
            $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.percent2));
            // $scope.totalLabel.push($scope.total[i]);
        }
        
        console.log($scope.label1);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('dhlSupport');
        if (window.dhlChart) window.dhlChart.destroy();
        window.dhlChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: $scope.label1,
                datasets: [{
                    label: '%',
                    data: $scope.percent,
                    backgroundColor: [
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',

                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                        barThickness: 40,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: "DHL SUPPORT",
                }
            }
        });
    }

    $scope.customerSupport = function(data, order){
        $scope.label1 = [];
        $scope.data1 = [];
        $scope.percent = [];
        $scope.percent2 = 0;

        console.log(data);
        for(var i = 0; i < data.length;i++){
            $scope.label1.push(data[i]['factor_incident']);
            // $scope.label2.push(data[i]['lane']);
            $scope.data1.push(data[i]['CountWaybillNumber']);
            // $scope.data2.push(data[i]['CountProduct']);


        }

        for(var i = 0; i < order.length;i++){
            $scope.percent2 += parseInt(order[i]['OnTimeStatus']);
        }

        // console.log($scope.error);
        console.log($scope.data2);

        $scope.label1 = [...new Set($scope.label1)];

        for(var i = 0; i < $scope.label1.length;i++){
            $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.percent2));
            // $scope.totalLabel.push($scope.total[i]);
        }

        // for(var i = 2; i >= 0;i--){
        //     $scope.total[i] = parseInt($scope.data1[i]) + parseInt($scope.data1[i + 3]) + parseInt($scope.data1[i + 6]);
        // }
        
        console.log($scope.percent);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('customerSupport');
        if (window.customerChart) window.customerChart.destroy();
        window.customerChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: $scope.label1,
                datasets: [{
                    label: '%',
                    data: $scope.percent,
                    backgroundColor: [
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                        barThickness: 40,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: "CUSTOMER SUPPORT",
                },
            }
        });
    }

    $scope.uncontrollable = function(data, order){
        $scope.label1 = [];
        $scope.data1 = [];
        $scope.percent = [];
        $scope.percent2 = 0;


        console.log(data);
        for(var i = 0; i < data.length;i++){
            $scope.label1.push(data[i]['factor_incident']);
            // $scope.label2.push(data[i]['lane']);
            $scope.data1.push(data[i]['CountWaybillNumber']);
            // $scope.data2.push(data[i]['CountProduct']);
        }

        for(var i = 0; i < order.length;i++){
            $scope.percent2 += parseInt(order[i]['OnTimeStatus']);
        }

        // console.log($scope.error);
        // console.log(order);

        $scope.label1 = [...new Set($scope.label1)];

        for(var i = 0; i < $scope.label1.length;i++){
            $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.percent2));
            // $scope.totalLabel.push($scope.total[i]);
        }

        // for(var i = 2; i >= 0;i--){
        //     $scope.total[i] = parseInt($scope.data1[i]) + parseInt($scope.data1[i + 3]) + parseInt($scope.data1[i + 6]);
        // }
        
        console.log($scope.percent);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('uncontrollable');
        if (window.uncontrollChart) window.uncontrollChart.destroy();
        window.uncontrollChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: $scope.label1,
                datasets: [{
                    label: '%',
                    data: $scope.percent,
                    backgroundColor: [
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        
                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                        barThickness: 40,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: "UNCONTROLLABLE",
                }
            }
        });
    }

    $scope.getChart = function () {

        $scope.dataontime = [];
        $scope.dataorder = [];
        $scope.dataDHLsupport = [];
        $scope.dataCustomersupport = [];
        $scope.dataUncontrollable = [];
        $scope.dataFactor = [];
        $scope.dataPreviousQuarter = [];
        $scope.dataPreviousOrder = [];

        if($scope.tempQuaterIndex.selected != undefined){
            $scope.modelSearch.Quater = $scope.tempQuaterIndex.selected.Quater;
        }
        if($scope.tempYearIndex.selected != undefined){
            $scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
        }

        if($scope.modelSearch.Quater == "Q1"){
            $scope.modelSearch.QuaterPrev = "Q4";
            $scope.modelSearch.YearPrev = parseInt($scope.modelSearch.Year) - 1;
        }
        else if($scope.modelSearch.Quater == "Q2"){
            $scope.modelSearch.QuaterPrev = "Q1";
            $scope.modelSearch.YearPrev = $scope.modelSearch.Year;
        }
        else if($scope.modelSearch.Quater == "Q3"){
            $scope.modelSearch.QuaterPrev = "Q2";
            $scope.modelSearch.YearPrev = $scope.modelSearch.Year;
        }
        else if($scope.modelSearch.Quater == "Q4"){
            $scope.modelSearch.QuaterPrev = "Q3";
            $scope.modelSearch.YearPrev = $scope.modelSearch.Year;
        }

        $scope.modelSearch.ontimestatus = "On time";
        $scope.modelSearch.dhlsupport = "DHL Support";
        $scope.modelSearch.customersupport = "Customer Support";
        $scope.modelSearch.uncontrollable = "Uncontrollable";

        reportTwoApiService.getOntimeChart($scope.modelSearch, function (result) {
            console.log(result);
            
            if (result.status === true) {
                $scope.dataontime = result.message.ontime;
                $scope.dataorder = result.message.order;
                $scope.dataDHLsupport = result.message.dhlsupport;
                $scope.dataCustomersupport = result.message.customersupport;
                $scope.dataUncontrollable = result.message.uncontrollable;
                $scope.dataFactor = result.message.factordata;
                $scope.dataPreviousQuarter = result.message.previousquarter;
                $scope.dataPreviousOrder = result.message.previousorder;
                $scope.onTimePerformance($scope.dataontime, $scope.dataorder);
                $scope.onTimePerformanceQuarterly($scope.dataontime, $scope.dataorder, $scope.dataPreviousQuarter, $scope.dataPreviousOrder);
                $scope.dhlSupport($scope.dataDHLsupport, $scope.dataorder);
                $scope.customerSupport($scope.dataCustomersupport, $scope.dataorder);
                $scope.uncontrollable($scope.dataUncontrollable, $scope.dataorder);

                console.log($scope.dataPreviousQuarter);
                console.log($scope.dataPreviousOrder);

                $scope.modelDeviceList.month = {
                        "month1": $scope.dataontime[2]['shipmonth'],
                        "month2": $scope.dataontime[1]['shipmonth'],
                        "month3": $scope.dataontime[0]['shipmonth'],
                };
                
                $scope.labeldhl = [];
                $scope.labelcustomer = [];
                $scope.labeluncontrollable = [];
                $scope.data1 = [];
                $scope.data2 = [];
                $scope.datadhl = [];
                $scope.datacustomer = [];
                $scope.datauncontrollable = [];
                $scope.percent = [];
                $scope.order = [];
                $scope.percentdhl = [];
                $scope.percentcustomer = [];
                $scope.percentuncontrollable = [];
                $scope.avg;

                for(var i = 0; i < $scope.dataDHLsupport.length;i++){
                    $scope.labeldhl.push($scope.dataDHLsupport[i]['factor_incident']);
                    $scope.datadhl.push($scope.dataDHLsupport[i]['CountWaybillNumber']);
                    // $scope.label2.push(data[i]['lane']);
                }

                for(var i = 0; i < $scope.dataCustomersupport.length;i++){
                    $scope.labelcustomer.push($scope.dataCustomersupport[i]['factor_incident']);
                    $scope.datacustomer.push($scope.dataCustomersupport[i]['CountWaybillNumber']);
                    // $scope.label2.push(data[i]['lane']);
                }

                for(var i = 0; i < $scope.dataUncontrollable.length;i++){
                    $scope.labeluncontrollable.push($scope.dataUncontrollable[i]['factor_incident']);
                    $scope.datauncontrollable.push($scope.dataUncontrollable[i]['CountWaybillNumber']);
                    // $scope.label2.push(data[i]['lane']);
                }

                for(var i = 0; i < $scope.dataFactor.length;i++){
                    $scope.order += parseInt($scope.dataorder[i]['OnTimeStatus']);
                }

                for(var i = 0; i < $scope.labeldhl.length;i++){
                    $scope.percentdhl[i] = Math.round((parseInt($scope.datadhl[i]) * 100) / parseInt($scope.percent2));
                    // $scope.totalLabel.push($scope.total[i]);
                }

                for(var i = 0; i < $scope.labelcustomer.length;i++){
                    $scope.percentcustomer[i] = Math.round((parseInt($scope.datacustomer[i]) * 100) / parseInt($scope.percent2));
                    // $scope.totalLabel.push($scope.total[i]);
                }

                for(var i = 0; i < $scope.labeluncontrollable.length;i++){
                    $scope.percentuncontrollable[i] = Math.round((parseInt($scope.datauncontrollable[i]) * 100) / parseInt($scope.percent2));
                    // $scope.totalLabel.push($scope.total[i]);
                }

                // $scope.test['value'] = $scope.percentdhl;

                // console.log($scope.percentdhl);
                // console.log($scope.dataDHLsupport['factorince']);
                

                // $scope.title = ontime[0]['Quater']+" "+ontime[0]['shipmonth']+"-"+ontime[2]['shipmonth'];

                for(var i = 0; i < $scope.dataontime.length;i++){
                    $scope.data1.push($scope.dataontime[i]['OnTimeStatus']);
                    $scope.data2.push($scope.dataorder[i]['OnTimeStatus']);
                }

                for(var i = 0; i < 3;i++){
                    $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.data2[i]));
                    // $scope.totalLabel.push($scope.total[i]);
                }

                $scope.avg = Math.round(($scope.percent[0] + $scope.percent[1] + $scope.percent[2]) / 3);
                $scope.modelDeviceList.ontime = {
                        "mon1": $scope.percent[0],
                        "mon2": $scope.percent[1],
                        "mon3": $scope.percent[2],
                };

                $scope.modelDeviceList.avg = $scope.avg;
            //     $scope.modelDeviceList.dox = {
            //         "ProductName": $scope.dataproduct[3]['ProductName'],
            //             "mon1": new Intl.NumberFormat().format($scope.dataproduct[3]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.dataproduct[4]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.dataproduct[5]['CountWaybill']),
            //     };
            //     $scope.modelDeviceList.wpx = {
            //         "ProductName": $scope.dataproduct[6]['ProductName'],
            //             "mon1": new Intl.NumberFormat().format($scope.dataproduct[6]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.dataproduct[7]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.dataproduct[8]['CountWaybill']),
            //     };

            //     console.log(new Intl.NumberFormat().format($scope.totalLabel));
            //     $scope.modelDeviceList.total = {
            //             "mon1": new Intl.NumberFormat().format($scope.totalLabel[2]),
            //             "mon2": new Intl.NumberFormat().format($scope.totalLabel[1]),
            //             "mon3": new Intl.NumberFormat().format($scope.totalLabel[0]),
            //     };




            //     $scope.modelDeviceList.pac = {
            //         "lane": $scope.datalane[0]['lane'],
            //             "mon1": new Intl.NumberFormat().format($scope.datalane[0]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.datalane[1]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.datalane[2]['CountWaybill']),
            //     };
            //     $scope.modelDeviceList.pjc = {
            //         "lane": $scope.datalane[3]['lane'],
            //             "mon1": new Intl.NumberFormat().format($scope.datalane[3]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.datalane[4]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.datalane[5]['CountWaybill']),
            //     };
            //     $scope.modelDeviceList.roa = {
            //         "lane": $scope.datalane[6]['lane'],
            //             "mon1": new Intl.NumberFormat().format($scope.datalane[6]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.datalane[7]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.datalane[8]['CountWaybill']),
            //     };


            } else {

            }
        }) 

    };

    $scope.reload = function () {

        // $scope.getQuater();
        // $scope.tempQuaterIndex.selected = $scope.listQuater[0];
	    $scope.getYear();
        // let SddStatus;

        // if($scope.TempSddStatusIndex.selected.Status == "All"){
        //     SddStatus = "'Due Today','Overdue','Future Shipments'";
        // }else if($scope.TempSddStatusIndex.selected.Status == "Overdue & Due Today"){
        //     SddStatus = "'Due Today','Overdue'";
        // }else{
        //     SddStatus = "'Future Shipments'";
        // }

        // $scope.DataSearchSddStatus = {
        //     "SddStatus" : SddStatus,
        //     "customer" : "apple",
        // }

        // reportTwoApiService.getPercentStatus($scope.DataSearchSddStatus, function (results) {
        //     var result = results.data;
        //     // console.log(result)
        //     if (result.status === true) {

        //         $scope.totalPage = result.toTalPage;
        //         $scope.listPageIndex = baseService.getListPage(result.toTalPage);
        //         $scope.listPageIndex.forEach(function (entry, index) {
        //             if ($scope.PageIndex === entry.Value)
        //                 $scope.TempPageIndex.selected = entry;
        //         });

        //         $scope.totalRecords = result.totalRecords;
        //         // $scope.modelDeviceList = result.message[0];
        //         $scope.modelDeviceList = result.message;
        //         $scope.listDataGraphPieBytoDay = result.listDataGraphPieBytoDay;
        //         $scope.listDataGraphBarBytoDay = result.listDataGraphBarBytoDay;
        //         $scope.serviceCenterList = result.listDataGraphPieByServiceCenter;
        //         $scope.TotalOnprogress = parseFloat($scope.modelDeviceList[0].num_of_wc) + parseFloat($scope.modelDeviceList[0].num_of_fd);
        //         $scope.SddView = result.sddview;
        //         $scope.last_update = result.last_update;


        //         if ($scope.listDataGraphPieBytoDay.length > 0) {
        //             $scope.createPieStackBytoDay();

        //         }
        //         if ($scope.listDataGraphBarBytoDay.length > 0) {
        //             $scope.createBarStackBytoDay();

        //         }
        //         if ($scope.serviceCenterList.length > 0) {
        //             $scope.createPieStackByServiceCenter();
        //             $scope.createBarStackByServiceCenter();

        //         }


        //         // console.log($scope.listDatalastCheckPiont);
        //     } else {

        //     }
        // })
    }

    $scope.onDeleteTagClick = function (item) {
        codeAppleAndDhlApiService.deleteCodeAppleAndDhl({ id: item.id }, function (result) {
            if (result.status === true) {
                $scope.reload();
            } else {
                baseService.showMessage(result.message);
            }
        });

    }

    $scope.validatecheck = function () {
        var bResult = true;
        $(".require").hide();

        if ($scope.CreateModel.reason_code == "") {
            $(".CreateModel_reason_code").show();
            bResult = false;
        }
        // if ($scope.CreateModel.code_description == "") {
        // 	$(".CreateModel_code_description").show();
        // 	bResult = false;
        // }
        // if ($scope.CreateModel.check_point == "") {
        // 	$(".CreateModel_check_point").show();
        // 	bResult = false;
        // }
        // if ($scope.CreateModel.remark == "") {
        // 	$(".CreateModel_remark").show();
        // 	bResult = false;
        // }


        return bResult;
    }

    $scope.onSaveTagClick = function () {

        var bValid = $scope.validatecheck();
        console.log($scope.CreateModel);
        if (true == bValid) {
            // console.log($scope.CreateModel);
            codeAppleAndDhlApiService.savecodeAppleAndDhl($scope.CreateModel, function (result) {
                if (result.status == true) {
                    $scope.ShowDevice();
                } else {
                    baseService.showMessage(result.message);
                }
            });

        }
    }

    $scope.createBarStackBytoDay = function () {

        // console.log($scope.listDataGraphBarBytoDay);
        var ok = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'OK'
        })
        var rt = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'RT'
        })
        var wc = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'WC'
        })
        var fd = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'FD'
        })
        var exception = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'Exception'
        })
        var other = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'Other'
        })
        // console.log(ok[0].percent,ok[0].count_total)
        $scope.DataCompletionOK = [ok[0].count_total, 0, 0, 0];
        $scope.DataCompletionRT = [rt[0].count_total, 0, 0, 0];
        $scope.DataOnprogressWC = [0, wc[0].count_total, 0, 0];
        $scope.DataOnprogressFD = [0, fd[0].count_total, 0, 0];
        $scope.DataException = [0, 0, exception[0].count_total, 0];
        $scope.DataOther = [0, 0, 0, other[0].count_total];
        $scope.DataGraphlabel = [
            "Completion",
            "On progress",
            "Exception",
            "Other",
        ];
        // console.log($scope.DataCompletionOK);
        var barChartData = {
            labels: $scope.DataGraphlabel,
            datasets: [{
                label: "OK",
                backgroundColor: "#DAF8B7",
                data: $scope.DataCompletionOK,
                stack: "Stack 0",
            },
            {
                label: "RT",
                backgroundColor: "#99f3bd",
                data: $scope.DataCompletionRT,
                stack: "Stack 1",
            },
            {
                label: "WC",
                backgroundColor: "#ffbb91",
                data: $scope.DataOnprogressWC,
                stack: "Stack 0",
            },
            {
                label: "FD",
                backgroundColor: "#fcdab7",
                data: $scope.DataOnprogressFD,
                stack: "Stack 1",
            },
            {
                label: "Exception",
                backgroundColor: "#FAB3B3",
                data: $scope.DataException,
                stack: "Stack 0",
            },
            {
                label: "Other",
                backgroundColor: "#D6D6D5",
                data: $scope.DataOther,
                stack: "Stack 0",
            },
            ],
        };
        var ctx = document.getElementById("GengraphBarBytoDay").getContext("2d");
        if (window.myBar1) window.myBar1.destroy();
        window.myBar1 = new Chart(ctx, {
            type: "bar",
            data: barChartData,
            options: {
                responsive: true,
                legend: {
                    position: "bottom",
                },
                title: {
                    display: true,
                    // text: "To Day",
                    position: "top",
                    fontSize: 15,
                    padding: 40,
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                    },],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            min: 0,
                            callback: function (value) {
                                return value
                                    .toString()
                                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                            },
                        },
                    },],
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value !== 0 ?
                                value.toLocaleString( /* ... */) :
                                ''
                        },
                        anchor: 'end',
                        align: 'end',
                        color: '#888'
                    }
                }
            }
        });
    };



    // $scope.createBarStackBytoDay = function () {
    //     $scope.DatalastCheckPiont = [];
    //     $scope.DatalastCheckPiontlabel = [];
    //     // console.log($scope.listDatalastCheckPiont.length);
    //     if ($scope.listDatalastCheckPiont.length > 0) {
    //         for (var i = 0; i < $scope.listDatalastCheckPiont.length; i++) {
    //             $scope.DatalastCheckPiont.push(
    //                 parseFloat($scope.listDatalastCheckPiont[i]["count_total"])
    //             );
    //             $scope.DatalastCheckPiontlabel.push($scope.listDatalastCheckPiont[i]["latest_checkpiont"]);
    //         }
    //         var barChartData1 = {
    //             labels: $scope.DatalastCheckPiontlabel,
    //             datasets: [{
    //                 label: "label",
    //                 backgroundColor: ["#61F52A", "#15C606", "#DDF019", "#E6FC02", "#F30C0C", "#C9C8C8"],
    //                 data: $scope.DatalastCheckPiont,
    //                 borderWidth: 1,
    //                 borderColor: "#FFFFFF",
    //             },],
    //         };
    //         var ctxProduct = document
    //             .getElementById("canvas")
    //             .getContext("2d");
    //         if (window.myBar1) window.myBar1.destroy();
    //         window.myBar1 = new Chart(ctxProduct, {
    //             type: "bar",
    //             data: barChartData1,
    //             options: {
    //                 title: {
    //                     display: true,
    //                     text: "Apple reportTwo",
    //                     fontSize: 15,
    //                     padding: 20,
    //                 },
    //                 legend: {
    //                     display: true,
    //                     position: "bottom",
    //                 },
    //                 responsive: true,
    //                 scales: {
    //                     xAxes: [{

    //                         stacked: true,
    //                         maxBarThickness: 50,
    //                         ticks: {
    //                             fontSize: 10,
    //                             autoSkip: false,
    //                             maxRotation: 90,
    //                             minRotation: 90
    //                         },
    //                     },],
    //                     yAxes: [{

    //                         scaleLabel: {
    //                             display: true,
    //                             labelString: "",
    //                         },
    //                         stacked: true,
    //                         ticks: {
    //                             min: 0
    //                         },
    //                     },],
    //                 },
    //                 plugins: {
    //                     datalabels: {
    //                         formatter: (value, Tripdate) => {
    //                             const total =
    //                                 Tripdate.chart.$totalizer.totals[Tripdate.dataIndex];
    //                             return total.toLocaleString(/* ... */);
    //                         },
    //                         align: "end",
    //                         anchor: "end",
    //                         display: function (Tripdate) {
    //                             return (
    //                                 Tripdate.datasetIndex === Tripdate.chart.$totalizer.utmost
    //                             );
    //                         }
    //                     }
    //                 }
    //             },
    //         });
    //     }
    // };


    $scope.createPieStackBytoDay = function () {
        // $scope.DataGenGraphPieByToDay = $scope.listDataGraphPieBytoDay.map(status => status.count_total)
        // $scope.DataGenGraphPieByToDaylabel = $scope.listDataGraphPieBytoDay.map(status => status.latest_checkpiont)
        // console.log($scope.listDataGraphPieBytoDay);

        $scope.DataGenGraphPieByToDay = [];
        $scope.DataGenGraphPieByToDaylabel = [];
        $scope.DataGenGraphPieByToDayShowValue = [];
        var colorsPieStackBytoDays  = ["#B7F8AD", "#FBE3C4", "#FAB3B3", "#D6D6D5"];
        var colorPieStackBytoDay  = [];
        for (var i = 0; i < $scope.listDataGraphPieBytoDay.length; i++) {
            if (
                $scope.listDataGraphPieBytoDay[i]["percent"] != null &&
                $scope.listDataGraphPieBytoDay[i]["percent"] != "0.00" &&
                $scope.listDataGraphPieBytoDay[i]["percent"] != undefined
            ) {
                $scope.DataGenGraphPieByToDaylabel.push($scope.listDataGraphPieBytoDay[i]["latest_checkpiont"]);
                $scope.DataGenGraphPieByToDay.push($scope.listDataGraphPieBytoDay[i]["percent"]);
                $scope.DataGenGraphPieByToDayShowValue.push($scope.listDataGraphPieBytoDay[i]["latest_checkpiont"] + " " + $scope.listDataGraphPieBytoDay[i]["percent"] + " %");
                colorPieStackBytoDay.push(colorsPieStackBytoDays[i]);
            } else {

            }
        }
        // $scope.DataGenGraphPieByToDaylabel = ["Completion", "On Progress", "Exception", "Other"];
        // $scope.DataGenGraphPieByToDay = ["580", "7", "9", "600"];

        // console.log($scope.DataGenGraphPieByToDay);
        // $scope.listDatalastCheckPiont.map(status =>
        //     $scope.DataGenGraphPieByTotal.push(status.count_total)
        // )

        var pieChartData1 = {
            labels: $scope.DataGenGraphPieByToDaylabel,
            datasets: [{
                label: "label",
                backgroundColor: colorPieStackBytoDay,
                data: $scope.DataGenGraphPieByToDay,
                borderWidth: 1,
                borderColor: "#FFFFFF",
            },],
        };
        var ctxProduct = document
            .getElementById("GenGraphPieByToday")
            .getContext("2d");
        if (window.myPie1) window.myPie1.destroy();
        window.myPie1 = new Chart(ctxProduct, {
            type: "pie",
            data: pieChartData1,
            options: {
                title: {
                    display: true,
                    // text: "",
                    fontSize: 15,
                    padding: 20,
                },
                legend: {
                    display: true,
                    position: "right",
                    labels: {
                        // fontColor : 'reg(255, 99, 132)',
                        padding: 20

                    }
                },

                responsive: true,
                scales: {
                    xAxes: [{
                        display: false,
                        stacked: true,
                        maxBarThickness: 50,
                        ticks: {
                            fontSize: 10,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                    },],
                    yAxes: [{
                        display: false,
                        scaleLabel: {
                            display: true,
                            labelString: "",
                        },
                        stacked: true,
                        ticks: {
                            min: 0
                        },
                    },],
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 20,
                        bottom: 55,
                    },
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                    // outlabels: {
                    //     color: 'black',
                    // },
                    outlabels: {
                        display: true,
                        text: $scope.DataGenGraphPieByToDayShowValue,
                        // borderWidth: 2,
                        // borderColor: "#000000",
                        // backgroundColor: "#FFFFFF",
                        lineWidth: 2,
                        padding: 3,
                        textAlign: "center",
                        stretch: 20,
                        font: {
                            resizable: true,
                            minSize: 12,
                            maxSize: 18,
                        },
                        color: 'black',
                        // lineColor: "#000000",
                        // valuePrecision: 1,
                        // percentPrecision: 2,
                    }

                }
            },
        });
        // }
    };


    $scope.createPieStackByServiceCenter = function () {

        // $scope.serviceCenterList = [];
        // $scope.DataGenGraphPieByServiceCenter = [];
        // $scope.DataGenGraphPieByServiceCenterlabel = [];
        // $scope.DataGenGraphBarByCostcenter = ['45', '39', '50', '29', '42', '28', '30', '22', '53', '32', '27', '31', '52'];

        // for (var i = 0; i < $scope.serviceCenterList.length; i++) {
        //     // console.log($scope.serviceCenterList[i]);
        //     $scope.DataGenGraphPieByServiceCenter.push($scope.serviceCenterList[i].percent);
        //     $scope.DataGenGraphPieByServiceCenterlabel.push($scope.serviceCenterList[i].service_center);
        // }
        // console.log($scope.serviceCenterList)
        $scope.DataGenGraphPieByServiceCenter = [];
        $scope.DataGenGraphPieByServiceCenterlabel = [];
        $scope.DataGenGraphPieByServiceCenterShowValue = [];
        var colors = ["#3AFF00", "#F0FF00", "#FA5454", "#FFBB28", "#F9F382", "#21C105", "#03FF72", "#03FFCA", "#F59FFF", "#C1D7F9", "#FB9AD5", "#97F9D2", "#A0EFFC", "#D6D6D5"];
        var color = [];
        for (var i = 0; i < $scope.serviceCenterList.length; i++) {
            if (
                $scope.serviceCenterList[i]["percent"] != null &&
                $scope.serviceCenterList[i]["percent"] != "0.00" &&
                $scope.serviceCenterList[i]["percent"] != undefined
            ) {
                $scope.DataGenGraphPieByServiceCenterlabel.push($scope.serviceCenterList[i]["service_center"]);
                $scope.DataGenGraphPieByServiceCenter.push($scope.serviceCenterList[i]["percent"]);
                $scope.DataGenGraphPieByServiceCenterShowValue.push($scope.serviceCenterList[i]["service_center"] + " " + $scope.serviceCenterList[i]["percent"] + " %");
                color.push(colors[i]);
            } else {

            }
        }

        // console.log($scope.DataGenGraphPieByServiceCenterShowValue);

        // if ($scope.listDatalastCheckPiont.length > 0) {
        // 	for (var i = 0; i < $scope.listDatalastCheckPiont.length; i++) {
        // 		$scope.DatalastCheckPiont.push(
        // 			parseFloat($scope.listDatalastCheckPiont[i]["count_total"])
        // 		);
        // 		$scope.DatalastCheckPiontlabel.push($scope.listDatalastCheckPiont[i]["latest_checkpiont"]);
        // 	}
        var pieChartData2 = {
            labels: $scope.DataGenGraphPieByServiceCenterlabel,
            datasets: [{
                label: "label",
                backgroundColor: color,
                data: $scope.DataGenGraphPieByServiceCenter,
                borderWidth: 1,
                borderColor: "#FFFFFF",
            },],
            // hiddenSlices: [1, 3],

        };
        var ctxProduct = document
            .getElementById("GenGraphPieByServiceCenter")
            .getContext("2d");
        if (window.myPie2) window.myPie2.destroy();
        window.myPie2 = new Chart(ctxProduct, {
            type: "pie",
            data: pieChartData2,
            options: {
                title: {
                    display: true,
                    // text: "By Servicecenter",
                    // position: "top",
                    fontSize: 15,
                    // margin: 10,
                    padding: 10,
                },
                legend: {
                    display: true,
                    position: "right",
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        display: false,
                        stacked: true,
                        maxBarThickness: 50,
                        ticks: {
                            fontSize: 10,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                    },],
                    yAxes: [{
                        display: false,
                        scaleLabel: {
                            display: true,
                            labelString: "",
                        },
                        stacked: true,
                        ticks: {
                            min: 0
                        },
                    },],
                },
                // plugins: {
                // 	datalabels: {
                // 		formatter: (value, Tripdate) => {
                // 			const total =
                // 				Tripdate.chart.$totalizer.totals[Tripdate.dataIndex];
                // 			return total.toLocaleString(/* ... */);
                // 		},
                // 		align: "end",
                // 		anchor: "end",
                // 		display: function (Tripdate) {
                // 			return (
                // 				Tripdate.datasetIndex === Tripdate.chart.$totalizer.utmost
                // 			);
                // 		}
                // 	}
                // }
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                        // anchor: 'end',
                        // align: 'end',
                        color: '#888'
                    },
                    datalabels: {
                        display: false,
                    },
                    // outlabels: {
                    //     color: 'black',
                    //     stretch: 20,
                    // },
                    outlabels: {
                        display: true,
                        text: $scope.DataGenGraphPieByServiceCenterShowValue,
                        // borderWidth: 2,
                        // borderColor: "#000000",
                        // backgroundColor: "#FFFFFF",
                        lineWidth: 2,
                        padding: 3,
                        textAlign: "center",
                        stretch: 20,
                        font: {
                            resizable: true,
                            minSize: 12,
                            maxSize: 18,
                        },
                        color: 'black',
                        // lineColor: "#000000",
                        // valuePrecision: 1,
                        // percentPrecision: 2,
                    },
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 20,
                        bottom: 55,
                    },
                },
            },
        });

        // }
    };


    $scope.createBarStackByServiceCenter = function () {
        $scope.DataGenGraphBarByCostcenter = [];
        $scope.DataGenGraphBarByCostcenterlabel = [];
        var color = ["#3AFF00", "#F0FF00", "#FA5454", "#FFBB28", "#F9F382", "#21C105", "#03FF72", "#03FFCA", "#F59FFF", "#C1D7F9", "#FB9AD5", "#97F9D2", "#A0EFFC", "#D6D6D5"];
        // console.log($scope.listDatalastCheckPiont.length);
        var data = [];

        var barChartData2 = {
            labels: [],
            datasets: data
        };
        for (var i = 0; i < $scope.serviceCenterList.length; i++) {


            var value = [null, null, null, null, null, null, null, null, null, null, null, null, null];
            value[i] = $scope.serviceCenterList[i].count_total;
            // console.log(value);
            barChartData2.labels.push($scope.serviceCenterList[i].service_center)
            data.push({
                label: $scope.serviceCenterList[i].service_center,
                backgroundColor: color[i],
                data: value,
                borderWidth: 1,
                borderColor: "#FFFFFF",
            });
        }

        // if ($scope.listDatalastCheckPiont.length > 0) {
        // 	for (var i = 0; i < $scope.listDatalastCheckPiont.length; i++) {
        // 		$scope.DatalastCheckPiont.push(
        // 			parseFloat($scope.listDatalastCheckPiont[i]["count_total"])
        // 		);
        // 		$scope.DatalastCheckPiontlabel.push($scope.listDatalastCheckPiont[i]["latest_checkpiont"]);
        // 	}

        var ctxProduct = document
            .getElementById("GenGraphBarByServiceCenter")
            .getContext("2d");
        if (window.myBar2) window.myBar2.destroy();
        window.myBar2 = new Chart(ctxProduct, {
            type: "bar",
            data: barChartData2,
            options: {
                title: {
                    display: true,
                    // text: "By Costcenter",
                    fontSize: 15,
                    padding: 20,
                },
                legend: {
                    display: true,
                    position: "bottom",
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                        maxBarThickness: 50,
                        ticks: {
                            fontSize: 10,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                    },],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "",
                        },
                        stacked: true,
                        ticks: {
                            min: 0
                        },
                    },],
                },
                // plugins: {
                // 	datalabels: {
                // 		formatter: (value, Tripdate) => {
                // 			const total =
                // 				Tripdate.chart.$totalizer.totals[Tripdate.dataIndex];
                // 			return total.toLocaleString(/* ... */);
                // 		},
                // 		align: "end",
                // 		anchor: "end",
                // 		display: function (Tripdate) {
                // 			return (
                // 				Tripdate.datasetIndex === Tripdate.chart.$totalizer.utmost
                // 			);
                // 		}
                // 	}
                // }
            },
        });
        // }
    };

    $scope.Exportreport = function () {
        // console.log($scope.tempYearIndex);

        if($scope.tempQuaterIndex.selected != undefined){
            $scope.modelSearch.Quater = $scope.tempQuaterIndex.selected.Quater;
        }
        if($scope.tempYearIndex.selected != undefined){
            $scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
        }

        // if($scope.modelSearch.start_date != undefined){
        //     $scope.modelSearch.week = "";
        //     $scope.modelSearch.Year = "";
        // } else {
        //     if($scope.tempWeekIndex.selected != undefined){
        //     $scope.modelSearch.week = $scope.tempWeekIndex.selected.week;
        //     }
        //     if($scope.tempYearIndex.selected != undefined){
        //         $scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
        //     }
        // }

        // console.log($scope.modelSearch.start_date);
        
        // console.log($scope.modelSearch);

        reportTwoApiService.Exportreport($scope.modelSearch, function (result) {
            if (result.status === true) {
                var download_url = get_base_url('');
                // console.log(download_url);
                var file = download_url + 'upload/export_excel/' + result.message;
                window.open(file, '_blank');
            } else {
                baseService.showMessage(result.message);
            }
        })
    }


    $scope.ExportDowloadAllShipment = function (item, type) {
        // console.log(item);
        reportTwoApiService.ExportDowloadAllShipment({ data: item, type: type }, function (result) {
            if (result.status === true) {
                var download_url = get_base_url('');
                var file = download_url + 'upload/export_excel/' + result.message;
                window.open(file, '_blank');
            } else {
                baseService.showMessage(result.message);
            }
        })
    }







}]);
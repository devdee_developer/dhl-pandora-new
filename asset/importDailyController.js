myApp.controller("importDailyController", [
	"$scope",
	"Upload",
	"$filter",
	"baseService",
	"importDailyApiService",
	"reportOneApiService",
	function (
		$scope,
		Upload,
		$filter,
		baseService,
		importDailyApiService,
		reportOneApiService
	) {
		$scope.CreateModel = {};
		$scope.modelSearch = {};
		$scope.downloaddaily = {};
		$scope.url = {};

		$scope.modelDeviceList = [];
		//page system
		$scope.listPageSize = baseService.getListPageSize();
		$scope.TempPageSize = {};
		$scope.TempPageIndex = {};
		$scope.PageSize = baseService.setPageSize(20);
		$scope.totalPage = 1; //init;
		$scope.totalRecords = 0;
		$scope.PageIndex = 1;
		$scope.SortColumn = baseService.setSortColumn("id");
		$scope.SortOrder = baseService.setSortOrder("asc");

		$scope.listdaily = [];
		$scope.TempdailyIndex = {};
		$scope.TempSearchdailyIndex = {};

		$scope.order = {};

		$scope.isView = false;
		$scope.file = {};

		$scope.file_Pandora_Dashboard = {};
		$scope.fileshipment = {};

		$scope.sort = function (e) {
			baseService.sort(e);
			$scope.SortColumn = baseService.getSortColumn();
			$scope.SortOrder = baseService.getSortOrder();
			$scope.reload();
		};

		$scope.getFirstPage = function () {
			$scope.PageIndex = baseService.getFirstPage();
			$scope.reload();
		};
		$scope.getBackPage = function () {
			$scope.PageIndex = baseService.getBackPage();
			$scope.reload();
		};
		$scope.getNextPage = function () {
			$scope.PageIndex = baseService.getNextPage();
			$scope.reload();
		};
		$scope.getLastPage = function () {
			$scope.PageIndex = baseService.getLastPage();
			$scope.reload();
		};
		$scope.searchByPage = function () {
			$scope.PageIndex = baseService.setPageIndex(
				$scope.TempPageIndex.selected.PageIndex
			);
			$scope.reload();
		};
		$scope.setPageSize = function (data) {
			$scope.PageSize = baseService.setPageSize(
				$scope.TempPageSize.selected.Value
			);
		};
		$scope.loadByPageSize = function () {
			$scope.PageIndex = baseService.setPageIndex(1);
			$scope.setPageSize();
			$scope.reload();
		};
		//page system

		$scope.ShowDevice = function () {
			$scope.onInit();
			$(".importdaily").show();
			$(".DisplayDevice").show();
		};
		$scope.resetModel = function () {
			$scope.CreateModel = { id: 0 };
			$scope.file_Pandora_Dashboard = "";
			$scope.fileshipment = "";
		};

		$scope.onInit = function () {
			$(".require").hide();
			$scope.resetModel();
			$scope.reload();
		};

		$scope.reload = function () {
			importDailyApiService.listdaily($scope.modelSearch, function (results) {
				var result = results.data;
				if (result.status === true) {
					$scope.totalPage = result.toTalPage;
					$scope.listPageIndex = baseService.getListPage(result.toTalPage);
					$scope.listPageIndex.forEach(function (entry, index) {
						if ($scope.PageIndex === entry.Value)
							$scope.TempPageIndex.selected = entry;
					});
					$scope.listPageSize.forEach(function (entry, index) {
						if (entry.Value == $scope.PageSize)
							$scope.TempPageSize.selected = entry;
					});

					$scope.totalRecords = result.totalRecords;
					$scope.modelDeviceList = result.message;
				} else {
				}
			});
		};
		$scope.validatecheck = function () {
			var bResult = true;
			$(".require").hide();
			// if ($scope.file_pnd3.name == undefined) {
			// 	$(".CreateModel_file_pnd3").show();
			// 	bResult = false;
			// }
			return bResult;
		};

		$scope.upload = function () {
			console.log($scope.file_Pandora_Dashboard.name)
			
			var bValid = $scope.validatecheck();
			var file_Pandora_Dashboard = $scope.file_Pandora_Dashboard.name;
			// var resapple = file_pnd3.substr(file_pnd3.length - 5, file_pnd3.length);
			// var fileshipment = $scope.fileshipment.name;
			// var resshipment = fileshipment.substr(fileshipment.length - 5, fileshipment.length);

			if (true == bValid) {
				baseService.showOverlay();
				var url = get_base_url("importDaily/upload_file");
				// console.log(url)
				console.log($scope.file_Pandora_Dashboard);
				Upload.upload({
					url: url,
					data: {
						file_Pandora_Dashboard: $scope.file_Pandora_Dashboard,
					},
				}).then(function (resp) {
					console.log(resp);
					if (resp.status == 200) {
						baseService.hideOverlay();
						$scope.ShowDevice();
						$scope.file_Pandora_Dashboard = undefined;
						baseService.showMessage("Save success");

						// if (resp.data.code == 100) {
						// 	var download_url = get_base_url('');
						// 	var file = download_url + 'upload/' + resp.data.filename;
						// 	window.open(file, '_bank');
						// }
						if (resp.data.status == true) {
							baseService.showMessage("Save success");
						} else {
							baseService.showMessage(resp.data.message);
						}
					} else {
						baseService.showMessage("Can not upload file");
					}
				});
			}
		};

		$scope.onDownload = function (item) {
			// console.log(item.file_name);
			var download_url = get_base_url("");
			// console.log(download_url);
			var file = download_url + "upload/import_excel/" + item.file_name;
			// console.log(file);
			window.open(file, "_blank");
		};

		$scope.download = function (item) {
			// console.log(item.file_name);
			var download_url = get_base_url("");
			// console.log(download_url);
			var file = download_url + "upload/Pandora_Template_excel.xlsx";
			// console.log(file);
			window.open(file, "_blank");
		};

		$scope.onDeleteTagClick = function (item) {
			importDailyApiService.deleteImport(item, function (result) {
				console.log(result);
				if (result.status === true) {
					baseService.showMessage(result.ShowMessage);
					$scope.reload();
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.Exportexcel = function (item) {
			console.log(item);
			importDailyApiService.Exportexcel(item, function (result) {
				if (result.status === true) {
					var download_url = get_base_url("");
					var file = download_url + "upload/export_excel/" + result.message;
					window.open(file, "_blank");
				} else {
					baseService.showMessage(result.message);
				}
			});
		};

		$scope.backup = function (item) {
			reportOneApiService.backup(item, function (result) {
				console.log(result);
				if (result.status === true) {
					console.log(result.message);
					baseService.showMessage(result.message);
					// $scope.reload();
				} else {
					baseService.showMessage(result.message);
				}
			});
		};
	},
]);

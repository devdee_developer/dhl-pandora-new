myApp.controller('reportThreeController', ['$scope', '$filter', 'baseService', 'reportThreeApiService', function ($scope, $filter, baseService, reportThreeApiService) {

    $scope.modelDeviceList = [];
    $scope.CreateModel = {};
    $scope.modelSearch = {};

    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('id');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.listDatalastCheckPiont = [];

    $scope.tempQuaterIndex = {};
	$scope.tempYearIndex = {};

    $scope.listQuater = [{ Quater: "Q1" }, { Quater: "Q2" }, { Quater: "Q3" }, { Quater: "Q4" },];

    $scope.TempSddStatusIndex = {};
    $scope.listSddStatus = [{ Status: "All" }, { Status: "Overdue & Due Today" }, { Status: "Future Shipments" },];

    $scope.DataSearchSddStatus = {};

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e);
        $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () {
        $scope.PageIndex = baseService.getFirstPage();
        $scope.reload();
    }
    $scope.getBackPage = function () {
        $scope.PageIndex = baseService.getBackPage();
        $scope.reload();
    }
    $scope.getNextPage = function () {
        $scope.PageIndex = baseService.getNextPage();
        $scope.reload();
    }
    $scope.getLastPage = function () {
        $scope.PageIndex = baseService.getLastPage();
        $scope.reload();
    }
    $scope.searchByPage = function () {
        $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex);
        $scope.reload();
    }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () {
        $scope.PageIndex = baseService.setPageIndex(1);
        $scope.setPageSize();
        $scope.reload();
    }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide();
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();

    }

    $scope.AddNewDevice = function () {
        $scope.resetModel();
        $(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide();
        $(".addDevice").show();

    }


    $scope.onEditTagClick = function (item) {
        $scope.AddNewDevice();
        $scope.loadEditData(item);

    }

    $scope.loadEditData = function (item) {
        $scope.CreateModel = angular.copy(item);
        console.log($scope.CreateModel)
    }

    $scope.resetModel = function () {

        $scope.CreateModel = { id: 0, reason_code: "", code_description: "", check_point: "", remark: "" };
    }


    $scope.resetSearch = function () {
        $scope.modelSearch = {
            "Quater": "",
            "Year": "",
        };

        $scope.tempQuaterIndex.selected = $scope.listQuater[0];
		$scope.tempYearIndex.selected =  $scope.listYear[0];
        // $scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () {
        $(".require").hide();
        $scope.tempQuaterIndex.selected = $scope.listQuater[0];
        // console.log($scope.listQuater);
        // $scope.resetModel();
        // $scope.resetSearch();
        $scope.reload();


        // $scope.listPageSize.forEach(function (entry, index) {
        //     if (0 === index)
        //         $scope.TempPageSize.selected = entry;
        // });

        //$scope.reload();
    }

//     $scope.getQuater = function (item) {

//         reportThreeApiService.getComboBox($scope.modelSearch, function (result) {
//           console.log(result)
//           if (result.status === true) {
//               $scope.listQuater = result.message;
//               console.log(item)
//               if($scope.listQuater.length>0){
              
//                   if(item==undefined){
//                       $scope.tempQuaterIndex.selected = $scope.listQuater[0];
//                       console.log("add")
//                   }else{
//                       $scope.listQuater.forEach(function (entry, index) {
//                           // console.log(entry.Quater,"-" ,item.Quater)
//                           if (entry.Quater === item.Quater) {
//                               $scope.tempQuaterIndex.selected = entry;
//                           }
//                       });
//                       console.log("edit")
                      
//                   }
//               }else{
//                   $scope.tempQuaterIndex.selected=undefined;
                      
//               }
              
              
//           } else {
//               baseService.showMessage(result.message);
//           }
//       });
//   }

  $scope.getYear = function (item) {

    reportThreeApiService.getYearComboBox($scope.modelSearch, function (result) {
        console.log(result)
        if (result.status === true) {
            $scope.listYear = result.message;
            console.log($scope.listYear )
            if($scope.listYear.length>0){

                if(item==undefined){
                    $scope.tempYearIndex.selected = $scope.listYear[0];
                    console.log("add")
                }else{
                    $scope.listYear.forEach(function (entry, index) {
                       
                        if (entry.Year === item.Year) {
                            $scope.tempYearIndex.selected = entry;
                        }
                    });
                    console.log("edit")
                    
                }
            }else{
                $scope.tempYearIndex.selected=undefined;
                    
            }
            
        } else {
            baseService.showMessage(result.message);
        }
    });
    }


    $scope.AuNzPerformance = function(customer, dhl, uncontroll, ontime, order){

        // $scope.modelDeviceList = data;
        console.log(dhl);
        $scope.label1 = [];
        $scope.data1 = [];
        $scope.data2 = [];
        $scope.order = [];
        $scope.percentontime = [];
        $scope.dhl = [];
        $scope.customer = [];
        $scope.uncontrollable = [];
        $scope.percent = [];
        $scope.totalLabel = [];

        $scope.title = "AU NZ PERFORMANCE";

        console.log(order);

        for(var i = 0; i < customer.length;i++){
            $scope.customer.push(customer[i]['CountWaybillNumber']);
            $scope.dhl.push(dhl[i]['CountWaybillNumber']);
        }

        for(var i = 0; i < order.length;i++){
            $scope.label1.push(order[i]['Gateway']);;
        }

        for(var i = 0; i < uncontroll.length;i++){
            $scope.uncontrollable.push(uncontroll[i]['CountWaybillNumber']);
        }

        for(var i = 0; i < ontime.length;i++){
            $scope.data2.push(ontime[i]['CountWaybillNumber']);
        }

        for(var i = 0; i < order.length;i++){
            $scope.order.push(order[i]['CountWaybillNumber']);
        }
        // console.log($scope.data1);
        // console.log($scope.data2);

        for(var i = 0; i < ontime.length;i++){
            $scope.percentontime[i] = Math.round((parseInt($scope.data2[i]) * 100) / parseInt($scope.order[i]));
            // $scope.totalLabel.push($scope.total[i]);
        }
        for(var i = 0; i < $scope.customer.length;i++){
            $scope.customer[i] = Math.round((parseInt($scope.customer[i]) * 100) / parseInt($scope.order[i]));
            // $scope.totalLabel.push($scope.total[i]);
        }
        for(var i = 0; i < $scope.dhl.length;i++){
            $scope.dhl[i] = Math.round((parseInt($scope.dhl[i]) * 100) / parseInt($scope.order[i]));
            // $scope.totalLabel.push($scope.total[i]);
        }
        for(var i = 0; i < $scope.uncontrollable.length;i++){
            $scope.uncontrollable[i] = Math.round((parseInt($scope.uncontrollable[i]) * 100) / parseInt($scope.order[i]));
            // $scope.totalLabel.push($scope.total[i]);
        }

        $scope.label1 = [...new Set($scope.label1)];
        // console.log(data);
        console.log($scope.data1);
        console.log($scope.label1);
        console.log($scope.data2);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('onTimePerformance');
        if (window.AuNzPerformance) window.AuNzPerformance.destroy();
        window.AuNzPerformance = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: $scope.label1,
                datasets: [{
                    label: 'Ontime',
                    data: $scope.percentontime,
                    backgroundColor: [
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                    ],
                    borderWidth: 1
                },{
                    label: 'Customer Support',
                    data: $scope.customer,
                    backgroundColor: [
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                    ],
                    borderWidth: 1
                },{
                    label: 'DHL Support',
                    data: $scope.dhl,
                    backgroundColor: [
                        'rgb(255,205,86)',
                        'rgb(255,205,86)',
                        'rgb(255,205,86)',
                        'rgb(255,205,86)',
                        'rgb(255,205,86)',
                        'rgb(255,205,86)',
                    ],
                    borderWidth: 1
                },{
                    label: 'Uncontrollable',
                    data: $scope.uncontrollable,
                    backgroundColor: [
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: $scope.title
                }
            }
        });
    }

    $scope.AuNzPerformanceQuarterly = function(ontime, order, prevontime, prevorder){

        // $scope.modelDeviceList = data;
        // console.log(data);
        $scope.label1 = [];
        $scope.dataprev = [];
        $scope.data2 = [];
        $scope.order = [];
        $scope.orderprev = [];
        $scope.percentontime = [];
        $scope.percentontimeprev = [];
        $scope.percent = 0;
        $scope.percentprev = 0;
        $scope.totalLabel = [];

        $scope.title = "Average AU NZ On Time";

        console.log(order);

        for(var i = 0; i < ontime.length;i++){
            $scope.label1[1] = ontime[i]['Quater'];
            $scope.data2.push(ontime[i]['CountWaybillNumber']);
        }

        for(var i = 0; i < prevontime.length;i++){
            $scope.label1[0] = prevontime[i]['Quater'];
            $scope.dataprev.push(prevontime[i]['CountWaybillNumber']);
        }

        for(var i = 0; i < order.length;i++){
            $scope.order.push(order[i]['CountWaybillNumber']);
        }

        for(var i = 0; i < prevorder.length;i++){
            $scope.orderprev.push(prevorder[i]['CountWaybillNumber']);
        }
        // console.log($scope.data1);
        // console.log($scope.data2);

        for(var i = 0; i < prevontime.length;i++){
            $scope.percentontimeprev[i] = Math.round((parseInt($scope.dataprev[i]) * 100) / parseInt($scope.orderprev[i]));
            // $scope.totalLabel.push($scope.total[i]);
        }
        for(var i = 0; i < ontime.length;i++){
            $scope.percentontime[i] = Math.round((parseInt($scope.data2[i]) * 100) / parseInt($scope.order[i]));
            // $scope.totalLabel.push($scope.total[i]);
        }
        for(var i = 0; i < $scope.percentontime.length;i++){
            $scope.percent += $scope.percentontime[i];
            // $scope.totalLabel.push($scope.total[i]);
        }
        for(var i = 0; i < $scope.percentontimeprev.length;i++){
            $scope.percentprev += $scope.percentontimeprev[i];
            // $scope.totalLabel.push($scope.total[i]);
        }

        $scope.percent = Math.round($scope.percent / 5);
        $scope.percentprev = Math.round($scope.percentprev / 5);

        $scope.label1 = [...new Set($scope.label1)];
        // console.log(data);
        console.log($scope.percent);
        console.log($scope.percentprev);
        console.log($scope.label1);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('onTimePerformanceQuarterly');
        if (window.AuNzPerformanceQuarterly) window.AuNzPerformanceQuarterly.destroy();
        window.AuNzPerformanceQuarterly = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: $scope.label1,
                datasets: [{
                    type: 'line',
                    label: 'On time',
                    data: [$scope.percentprev, $scope.percent],
                    fill: false,
                        borderColor: 'rgb(75,192,192)',
                        tension: 0.1,
                },{
                    type: 'bar',
                    label: 'On time',
                    data: [$scope.percentprev, $scope.percent],
                    backgroundColor: [
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                        'rgb(255,159,64)',
                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: $scope.title
                }
            }
        });
    }

    $scope.dhlSupport = function(data, order){
        $scope.label1 = [];
        $scope.data1 = [];
        $scope.percent = [];
        $scope.percent2 = 0;

        console.log(data);
        for(var i = 0; i < data.length;i++){
            $scope.label1.push(data[i]['factor_incident']);
            // $scope.label2.push(data[i]['lane']);
            $scope.data1.push(data[i]['CountWaybillNumber']);
        }

        for(var i = 0; i < order.length;i++){
            $scope.percent2 += parseInt(order[i]['OnTimeStatus']);
        }

        // console.log($scope.error);
        // console.log(order);

        $scope.label1 = [...new Set($scope.label1)];

        for(var i = 0; i < $scope.label1.length;i++){
            $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.percent2));
            // $scope.totalLabel.push($scope.total[i]);
        }
        
        console.log($scope.label1);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('dhlSupport');
        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: $scope.label1,
                datasets: [{
                    label: '%',
                    data: $scope.percent,
                    backgroundColor: [
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',
                        'rgb(120, 185, 255)',

                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                        barThickness: 40,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: "DHL SUPPORT",
                }
            }
        });
    }

    $scope.customerSupport = function(data, order){
        $scope.label1 = [];
        $scope.data1 = [];
        $scope.percent = [];
        $scope.percent2 = 0;

        console.log(data);
        for(var i = 0; i < data.length;i++){
            $scope.label1.push(data[i]['factor_incident']);
            // $scope.label2.push(data[i]['lane']);
            $scope.data1.push(data[i]['CountWaybillNumber']);
            // $scope.data2.push(data[i]['CountProduct']);


        }

        for(var i = 0; i < order.length;i++){
            $scope.percent2 += parseInt(order[i]['OnTimeStatus']);
        }

        // console.log($scope.error);
        console.log($scope.data2);

        $scope.label1 = [...new Set($scope.label1)];

        for(var i = 0; i < $scope.label1.length;i++){
            $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.percent2));
            // $scope.totalLabel.push($scope.total[i]);
        }

        // for(var i = 2; i >= 0;i--){
        //     $scope.total[i] = parseInt($scope.data1[i]) + parseInt($scope.data1[i + 3]) + parseInt($scope.data1[i + 6]);
        // }
        
        console.log($scope.percent);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('customerSupport');
        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: $scope.label1,
                datasets: [{
                    label: '%',
                    data: $scope.percent,
                    backgroundColor: [
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                        'rgb(75,192,192)',
                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                        barThickness: 40,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: "CUSTOMER SUPPORT",
                },
            }
        });
    }

    $scope.uncontrollable = function(data, order){
        $scope.label1 = [];
        $scope.data1 = [];
        $scope.percent = [];
        $scope.percent2 = 0;


        console.log(data);
        for(var i = 0; i < data.length;i++){
            $scope.label1.push(data[i]['factor_incident']);
            // $scope.label2.push(data[i]['lane']);
            $scope.data1.push(data[i]['CountWaybillNumber']);
            // $scope.data2.push(data[i]['CountProduct']);
        }

        for(var i = 0; i < order.length;i++){
            $scope.percent2 += parseInt(order[i]['OnTimeStatus']);
        }

        // console.log($scope.error);
        // console.log(order);

        $scope.label1 = [...new Set($scope.label1)];

        for(var i = 0; i < $scope.label1.length;i++){
            $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.percent2));
            // $scope.totalLabel.push($scope.total[i]);
        }

        // for(var i = 2; i >= 0;i--){
        //     $scope.total[i] = parseInt($scope.data1[i]) + parseInt($scope.data1[i + 3]) + parseInt($scope.data1[i + 6]);
        // }
        
        console.log($scope.percent);

        // console.log($scope.label1);
        // console.log(parseInt($scope.data1));
        // console.log($scope.total);
        // console.log($scope.data1[2]);
        // console.log($scope.data1[1]);
        // console.log("a;dklsfj;lkasjd");


        var ctx = document.getElementById('uncontrollable');
        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: $scope.label1,
                datasets: [{
                    label: '%',
                    data: $scope.percent,
                    backgroundColor: [
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        'rgb(255,99,132)',
                        
                    ],
                    borderWidth: 1
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                        barThickness: 40,
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                    },
                },
                title: {
                    display: true,
                    text: "UNCONTROLLABLE",
                }
            }
        });
    }

    $scope.getChart = function () {

        $scope.dataontime = [];
        $scope.dataorder = [];
        $scope.dataPreviousOrder = [];
        $scope.dataAuNzPerformance = [];
        $scope.dataAuNzOntime = [];
        $scope.dataAuNzPreviousOntime = [];
        $scope.dataCustomersupport = [];
        $scope.dataDHLsupport = [];
        $scope.dataUncontrollable = [];
        $scope.dataFactor = [];
        $scope.dataOverThreetoFourDays = [];
        $scope.dataOverFivetoSixDays = [];
        $scope.dataOverSevenDays = [];
        $scope.dataTotal = [];

        if($scope.tempQuaterIndex.selected != undefined){
            $scope.modelSearch.Quater = $scope.tempQuaterIndex.selected.Quater;
        }
        if($scope.tempYearIndex.selected != undefined){
            $scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
        }

        if($scope.modelSearch.Quater == "Q1"){
            $scope.modelSearch.QuaterPrev = "Q4";
            $scope.modelSearch.YearPrev = parseInt($scope.modelSearch.Year) - 1;
        }
        else if($scope.modelSearch.Quater == "Q2"){
            $scope.modelSearch.QuaterPrev = "Q1";
            $scope.modelSearch.YearPrev = $scope.modelSearch.Year;
        }
        else if($scope.modelSearch.Quater == "Q3"){
            $scope.modelSearch.QuaterPrev = "Q2";
            $scope.modelSearch.YearPrev = $scope.modelSearch.Year;
        }
        else if($scope.modelSearch.Quater == "Q4"){
            $scope.modelSearch.QuaterPrev = "Q3";
            $scope.modelSearch.YearPrev = $scope.modelSearch.Year;
        }

        $scope.modelSearch.ontimestatus = "On time";
        $scope.modelSearch.customer = "Customer Support";
        $scope.modelSearch.dhl = "DHL Support";
        $scope.modelSearch.uncontroll = "Uncontrollable";
        $scope.modelSearch.au = "AU";
        $scope.modelSearch.nz = "NZ";
        $scope.modelSearch.overonetotwoday = "Over 1-2 days";
        $scope.modelSearch.overthreetofourday = "Over 3-4 days";
        $scope.modelSearch.overfivetosixday = "Over 5-6 days";
        $scope.modelSearch.oversevenday = "Over 7 days";
        reportThreeApiService.getOntimeChart($scope.modelSearch, function (result) {
            console.log(result);
            
            if (result.status === true) {
                $scope.dataontime = result.message.ontime;
                $scope.dataorder = result.message.aunzorder;
                $scope.dataPreviousOrder = result.message.aunzorderprev;
                $scope.dataAuNzPerformance = result.message.aunzperformance;
                $scope.dataAuNzOntime = result.message.aunzperontime;
                $scope.dataAuNzPreviousOntime = result.message.aunzperontimeprev;
                $scope.dataCustomersupport = result.message.customer;
                $scope.dataDHLsupport = result.message.dhl;
                $scope.dataUncontrollable = result.message.uncontrollable;
                $scope.dataFactor = result.message.factordata;
                $scope.dataOverOnetoTwoDays = result.message.overslaonetotwodays;
                $scope.dataOverThreetoFourDays = result.message.overslathreetofourdays;
                $scope.dataOverFivetoSixDays = result.message.overslafivetosixdays;
                $scope.dataOverSevenDays = result.message.overslasevendays;
                $scope.dataTotal = result.message.overslatotal;
                $scope.dataAllFactor = result.message.allfactor;
                $scope.AuNzPerformance($scope.dataCustomersupport, $scope.dataDHLsupport, $scope.dataUncontrollable, $scope.dataAuNzOntime, $scope.dataorder);
                $scope.AuNzPerformanceQuarterly($scope.dataAuNzOntime, $scope.dataorder, $scope.dataAuNzPreviousOntime, $scope.dataPreviousOrder);
                // $scope.dhlSupport($scope.dataDHLsupport, $scope.dataorder);
                // $scope.customerSupport($scope.dataCustomersupport, $scope.dataorder);
                // $scope.uncontrollable($scope.dataUncontrollable, $scope.dataorder);

                // $scope.modelDeviceList.month = {
                //         "month1": $scope.dataontime[2]['shipmonth'],
                //         "month2": $scope.dataontime[1]['shipmonth'],
                //         "month3": $scope.dataontime[0]['shipmonth'],
                // };

                
                
                $scope.data1 = [];
                $scope.data2 = [];
                $scope.datadhl = [];
                $scope.datacustomer = [];
                $scope.datauncontrollable = [];
                $scope.dataontime = [];
                $scope.percent = [];
                $scope.order = [];
                $scope.percentdhl = [];
                $scope.percentcustomer = [];
                $scope.percentuncontrollable = [];
                $scope.percentontime = [];
                $scope.lateflight = [];
                $scope.forcemajeure = [];
                $scope.airlinetech = [];
                $scope.agreeddelivery = [];
                $scope.onforward = [];
                $scope.other = [];
                $scope.lateflight2 = [];
                $scope.forcemajeure2 = [];
                $scope.airlinetech2 = [];
                $scope.agreeddelivery2 = [];
                $scope.onforward2 = [];
                $scope.other2 = [];
                $scope.lateflight3 = [];
                $scope.forcemajeure3 = [];
                $scope.agreeddelivery3 = [];
                $scope.onforward3 = [];
                $scope.other3 = [];
                $scope.agreeddelivery4 = [];
                $scope.other4 = [];
                $scope.allfactor = [];
                $scope.total = [];
                $scope.avg;

                for(var i = 0; i < $scope.dataDHLsupport.length;i++){
                    $scope.datadhl.push($scope.dataDHLsupport[i]['CountWaybillNumber']);
                    
                    // $scope.label2.push(data[i]['lane']);
                }
                for(var i = 0; i < $scope.dataAllFactor.length;i++){
                    $scope.allfactor.push($scope.dataAllFactor[i]['Countwaybill']);
                    
                    // $scope.label2.push(data[i]['lane']);
                }

                for(var i = 0; i < $scope.dataTotal.length;i++){
                    $scope.total.push($scope.dataTotal[i]['Countwaybill']);
                    
                    // $scope.label2.push(data[i]['lane']);
                }

                for(var i = 0; i < 5; i++)
                {
                    $scope.other[i] = 0;
                    $scope.other2[i] = 0;
                    $scope.other3[i] = 0;
                    $scope.other4[i] = 0;
                    $scope.airlinetech2[i] = 0;
                }

                //Over 1-2 days
                console.log($scope.dataOverOnetoTwoDays);

                for(var i = 0; i < $scope.dataOverOnetoTwoDays.length;i++){
                    if($scope.dataOverOnetoTwoDays[i]['factor_incident'] == "Late flight connection")
                    {
                        $scope.lateflight[0] = $scope.dataOverOnetoTwoDays[i]["AKL"];
                        $scope.lateflight[1] = $scope.dataOverOnetoTwoDays[i]["BNE"];
                        $scope.lateflight[2] = $scope.dataOverOnetoTwoDays[i]["MEL"];
                        $scope.lateflight[3] = $scope.dataOverOnetoTwoDays[i]["PER"];
                        $scope.lateflight[4] = $scope.dataOverOnetoTwoDays[i]["SYD"];
                    }
                    else if($scope.dataOverOnetoTwoDays[i]['factor_incident'] == "Force majeure")
                    {
                        $scope.forcemajeure[0] = $scope.dataOverOnetoTwoDays[i]["AKL"];
                        $scope.forcemajeure[1] = $scope.dataOverOnetoTwoDays[i]["BNE"];
                        $scope.forcemajeure[2] = $scope.dataOverOnetoTwoDays[i]["MEL"];
                        $scope.forcemajeure[3] = $scope.dataOverOnetoTwoDays[i]["PER"];
                        $scope.forcemajeure[4] = $scope.dataOverOnetoTwoDays[i]["SYD"];
                    }
                    else if($scope.dataOverOnetoTwoDays[i]['factor_incident'] == "Airline technicle")
                    {
                        $scope.airlinetech[0] = $scope.dataOverOnetoTwoDays[i]["AKL"];
                        $scope.airlinetech[1] = $scope.dataOverOnetoTwoDays[i]["BNE"];
                        $scope.airlinetech[2] = $scope.dataOverOnetoTwoDays[i]["MEL"];
                        $scope.airlinetech[3] = $scope.dataOverOnetoTwoDays[i]["PER"];
                        $scope.airlinetech[4] = $scope.dataOverOnetoTwoDays[i]["SYD"];
                    }
                    else if($scope.dataOverOnetoTwoDays[i]['factor_incident'] == "Agreed delivery by consignee")
                    {
                        $scope.agreeddelivery[0] = $scope.dataOverOnetoTwoDays[i]["AKL"];
                        $scope.agreeddelivery[1] = $scope.dataOverOnetoTwoDays[i]["BNE"];
                        $scope.agreeddelivery[2] = $scope.dataOverOnetoTwoDays[i]["MEL"];
                        $scope.agreeddelivery[3] = $scope.dataOverOnetoTwoDays[i]["PER"];
                        $scope.agreeddelivery[4] = $scope.dataOverOnetoTwoDays[i]["SYD"];
                    }
                    else if($scope.dataOverOnetoTwoDays[i]['factor_incident'] == "On forwarding")
                    {
                        $scope.onforward[0] = $scope.dataOverOnetoTwoDays[i]["AKL"];
                        $scope.onforward[1] = $scope.dataOverOnetoTwoDays[i]["BNE"];
                        $scope.onforward[2] = $scope.dataOverOnetoTwoDays[i]["MEL"];
                        $scope.onforward[3] = $scope.dataOverOnetoTwoDays[i]["PER"];
                        $scope.onforward[4] = $scope.dataOverOnetoTwoDays[i]["SYD"];
                    }
                    else
                    {
                        $scope.other[0] += parseInt($scope.dataOverOnetoTwoDays[i]["AKL"]);
                        $scope.other[1] += parseInt($scope.dataOverOnetoTwoDays[i]["BNE"]);
                        $scope.other[2] += parseInt($scope.dataOverOnetoTwoDays[i]["MEL"]);
                        $scope.other[3] += parseInt($scope.dataOverOnetoTwoDays[i]["PER"]);
                        $scope.other[4] += parseInt($scope.dataOverOnetoTwoDays[i]["SYD"]);
                    }
                    
                    
                    // $scope.label2.push(data[i]['lane']);
                }
                console.log($scope.lateflight);
                

                for(var i = 0; i < 5; i++)
                {
                    $scope.lateflight[i] = Math.round((parseInt($scope.lateflight[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.forcemajeure[i] = Math.round((parseInt($scope.forcemajeure[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.airlinetech[i] = Math.round((parseInt($scope.airlinetech[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.agreeddelivery[i] = Math.round((parseInt($scope.agreeddelivery[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.onforward[i] = Math.round((parseInt($scope.onforward[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.other[i] = Math.round((parseInt($scope.other[i]) * 100) / parseInt($scope.allfactor[i]));
                }

                //Over 3-4 days

                for(var i = 0; i < $scope.dataOverThreetoFourDays.length;i++){
                    if($scope.dataOverThreetoFourDays[i]['factor_incident'] == "Late flight connection")
                    {
                        $scope.lateflight2[0] = $scope.dataOverThreetoFourDays[i]["AKL"];
                        $scope.lateflight2[1] = $scope.dataOverThreetoFourDays[i]["BNE"];
                        $scope.lateflight2[2] = $scope.dataOverThreetoFourDays[i]["MEL"];
                        $scope.lateflight2[3] = $scope.dataOverThreetoFourDays[i]["PER"];
                        $scope.lateflight2[4] = $scope.dataOverThreetoFourDays[i]["SYD"];
                    }
                    else if($scope.dataOverThreetoFourDays[i]['factor_incident'] == "Force majeure")
                    {
                        $scope.forcemajeure2[0] = $scope.dataOverThreetoFourDays[i]["AKL"];
                        $scope.forcemajeure2[1] = $scope.dataOverThreetoFourDays[i]["BNE"];
                        $scope.forcemajeure2[2] = $scope.dataOverThreetoFourDays[i]["MEL"];
                        $scope.forcemajeure2[3] = $scope.dataOverThreetoFourDays[i]["PER"];
                        $scope.forcemajeure2[4] = $scope.dataOverThreetoFourDays[i]["SYD"];
                    }
                    else if($scope.dataOverThreetoFourDays[i]['factor_incident'] == "Airline technicle")
                    {
                        $scope.airlinetech2[0] = $scope.dataOverThreetoFourDays[i]["AKL"];
                        $scope.airlinetech2[1] = $scope.dataOverThreetoFourDays[i]["BNE"];
                        $scope.airlinetech2[2] = $scope.dataOverThreetoFourDays[i]["MEL"];
                        $scope.airlinetech2[3] = $scope.dataOverThreetoFourDays[i]["PER"];
                        $scope.airlinetech2[4] = $scope.dataOverThreetoFourDays[i]["SYD"];
                    }
                    else if($scope.dataOverThreetoFourDays[i]['factor_incident'] == "Agreed delivery by consignee")
                    {
                        $scope.agreeddelivery2[0] = $scope.dataOverThreetoFourDays[i]["AKL"];
                        $scope.agreeddelivery2[1] = $scope.dataOverThreetoFourDays[i]["BNE"];
                        $scope.agreeddelivery2[2] = $scope.dataOverThreetoFourDays[i]["MEL"];
                        $scope.agreeddelivery2[3] = $scope.dataOverThreetoFourDays[i]["PER"];
                        $scope.agreeddelivery2[4] = $scope.dataOverThreetoFourDays[i]["SYD"];
                    }
                    else if($scope.dataOverThreetoFourDays[i]['factor_incident'] == "On forwarding")
                    {
                        $scope.onforward2[0] = $scope.dataOverThreetoFourDays[i]["AKL"];
                        $scope.onforward2[1] = $scope.dataOverThreetoFourDays[i]["BNE"];
                        $scope.onforward2[2] = $scope.dataOverThreetoFourDays[i]["MEL"];
                        $scope.onforward2[3] = $scope.dataOverThreetoFourDays[i]["PER"];
                        $scope.onforward2[4] = $scope.dataOverThreetoFourDays[i]["SYD"];
                    }
                    else
                    {
                        $scope.other2[0] += parseInt($scope.dataOverThreetoFourDays[i]["AKL"]);
                        $scope.other2[1] += parseInt($scope.dataOverThreetoFourDays[i]["BNE"]);
                        $scope.other2[2] += parseInt($scope.dataOverThreetoFourDays[i]["MEL"]);
                        $scope.other2[3] += parseInt($scope.dataOverThreetoFourDays[i]["PER"]);
                        $scope.other2[4] += parseInt($scope.dataOverThreetoFourDays[i]["SYD"]);
                    }
                    
                    
                    // $scope.label2.push(data[i]['lane']);
                }

                for(var i = 0; i < 5; i++)
                {
                    $scope.lateflight2[i] = Math.round((parseInt($scope.lateflight2[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.forcemajeure2[i] = Math.round((parseInt($scope.forcemajeure2[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.airlinetech2[i] = Math.round((parseInt($scope.airlinetech2[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.agreeddelivery2[i] = Math.round((parseInt($scope.agreeddelivery2[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.onforward2[i] = Math.round((parseInt($scope.onforward2[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.other2[i] = Math.round((parseInt($scope.other2[i]) * 100) / parseInt($scope.allfactor[i]));
                }

                // Over 5-6 days

                for(var i = 0; i < $scope.dataOverFivetoSixDays.length;i++){
                    if($scope.dataOverFivetoSixDays[i]['factor_incident'] == "Late flight connection")
                    {
                        $scope.lateflight3[0] = $scope.dataOverFivetoSixDays[i]["AKL"];
                        $scope.lateflight3[1] = $scope.dataOverFivetoSixDays[i]["BNE"];
                        $scope.lateflight3[2] = $scope.dataOverFivetoSixDays[i]["MEL"];
                        $scope.lateflight3[3] = $scope.dataOverFivetoSixDays[i]["PER"];
                        $scope.lateflight3[4] = $scope.dataOverFivetoSixDays[i]["SYD"];
                    }
                    else if($scope.dataOverFivetoSixDays[i]['factor_incident'] == "Force majeure")
                    {
                        $scope.forcemajeure3[0] = $scope.dataOverFivetoSixDays[i]["AKL"];
                        $scope.forcemajeure3[1] = $scope.dataOverFivetoSixDays[i]["BNE"];
                        $scope.forcemajeure3[2] = $scope.dataOverFivetoSixDays[i]["MEL"];
                        $scope.forcemajeure3[3] = $scope.dataOverFivetoSixDays[i]["PER"];
                        $scope.forcemajeure3[4] = $scope.dataOverFivetoSixDays[i]["SYD"];
                    }
                    else if($scope.dataOverFivetoSixDays[i]['factor_incident'] == "Agreed delivery by consignee")
                    {
                        $scope.agreeddelivery3[0] = $scope.dataOverFivetoSixDays[i]["AKL"];
                        $scope.agreeddelivery3[1] = $scope.dataOverFivetoSixDays[i]["BNE"];
                        $scope.agreeddelivery3[2] = $scope.dataOverFivetoSixDays[i]["MEL"];
                        $scope.agreeddelivery3[3] = $scope.dataOverFivetoSixDays[i]["PER"];
                        $scope.agreeddelivery3[4] = $scope.dataOverFivetoSixDays[i]["SYD"];
                    }
                    else if($scope.dataOverFivetoSixDays[i]['factor_incident'] == "On forwarding")
                    {
                        $scope.onforward3[0] = $scope.dataOverFivetoSixDays[i]["AKL"];
                        $scope.onforward3[1] = $scope.dataOverFivetoSixDays[i]["BNE"];
                        $scope.onforward3[2] = $scope.dataOverFivetoSixDays[i]["MEL"];
                        $scope.onforward3[3] = $scope.dataOverFivetoSixDays[i]["PER"];
                        $scope.onforward3[4] = $scope.dataOverFivetoSixDays[i]["SYD"];
                    }
                    else
                    {
                        $scope.other3[0] += parseInt($scope.dataOverFivetoSixDays[i]["AKL"]);
                        $scope.other3[1] += parseInt($scope.dataOverFivetoSixDays[i]["BNE"]);
                        $scope.other3[2] += parseInt($scope.dataOverFivetoSixDays[i]["MEL"]);
                        $scope.other3[3] += parseInt($scope.dataOverFivetoSixDays[i]["PER"]);
                        $scope.other3[4] += parseInt($scope.dataOverFivetoSixDays[i]["SYD"]);
                    }
                    
                    
                    // $scope.label2.push(data[i]['lane']);
                }
                

                for(var i = 0; i < 5; i++)
                {
                    $scope.lateflight3[i] = Math.round((parseInt($scope.lateflight3[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.forcemajeure3[i] = Math.round((parseInt($scope.forcemajeure3[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.agreeddelivery3[i] = Math.round((parseInt($scope.agreeddelivery3[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.onforward3[i] = Math.round((parseInt($scope.onforward3[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.other3[i] = Math.round((parseInt($scope.other3[i]) * 100) / parseInt($scope.allfactor[i]));
                }

                // Over 7 days

                console.log($scope.dataOverSevenDays);

                for(var i = 0; i < $scope.dataOverSevenDays.length;i++){

                    if($scope.dataOverSevenDays[i]['factor_incident'] == "Agreed delivery by consignee")
                    {
                        $scope.agreeddelivery4[0] = $scope.dataOverSevenDays[i]["AKL"];
                        $scope.agreeddelivery4[1] = $scope.dataOverSevenDays[i]["BNE"];
                        $scope.agreeddelivery4[2] = $scope.dataOverSevenDays[i]["MEL"];
                        $scope.agreeddelivery4[3] = $scope.dataOverSevenDays[i]["PER"];
                        $scope.agreeddelivery4[4] = $scope.dataOverSevenDays[i]["SYD"];
                    }
                    else
                    {
                        $scope.other4[0] += parseInt($scope.dataOverSevenDays[i]["AKL"]);
                        $scope.other4[1] += parseInt($scope.dataOverSevenDays[i]["BNE"]);
                        $scope.other4[2] += parseInt($scope.dataOverSevenDays[i]["MEL"]);
                        $scope.other4[3] += parseInt($scope.dataOverSevenDays[i]["PER"]);
                        $scope.other4[4] += parseInt($scope.dataOverSevenDays[i]["SYD"]);
                    }
                    
                    
                    // $scope.label2.push(data[i]['lane']);
                }
                

                for(var i = 0; i < 5; i++)
                {
                    $scope.agreeddelivery4[i] = Math.round((parseInt($scope.agreeddelivery4[i]) * 100) / parseInt($scope.allfactor[i]));
                    $scope.other4[i] = Math.round((parseInt($scope.other4[i]) * 100) / parseInt($scope.allfactor[i]));
                }

                // Total

                for(var i = 0; i < 5; i++)
                {
                    $scope.total[i] = Math.round((parseInt($scope.total[i]) * 100) / parseInt($scope.allfactor[i]));
                }
                

                for(var i = 0; i < $scope.dataCustomersupport.length;i++){
                    $scope.datacustomer.push($scope.dataCustomersupport[i]['CountWaybillNumber']);
                    // $scope.label2.push(data[i]['lane']);
                }

                for(var i = 0; i < $scope.dataUncontrollable.length;i++){
                    $scope.datauncontrollable.push($scope.dataUncontrollable[i]['CountWaybillNumber']);
                    // $scope.label2.push(data[i]['lane']);
                }

                for(var i = 0; i < $scope.dataAuNzOntime.length;i++){
                    $scope.dataontime.push($scope.dataAuNzOntime[i]['CountWaybillNumber']);
                    // $scope.label2.push(data[i]['lane']);
                }

                for(var i = 0; i < $scope.dataorder.length;i++){
                    $scope.order[i] = parseInt($scope.dataorder[i]['CountWaybillNumber']);
                }

                for(var i = 0; i < $scope.datadhl.length;i++){
                    $scope.percentdhl[i] = Math.round((parseInt($scope.datadhl[i]) * 100) / parseInt($scope.order[i]));
                    // $scope.totalLabel.push($scope.total[i]);
                }

                for(var i = 0; i < $scope.datacustomer.length;i++){
                    $scope.percentcustomer[i] = Math.round((parseInt($scope.datacustomer[i]) * 100) / parseInt($scope.order[i]));
                    // $scope.totalLabel.push($scope.total[i]);
                }

                for(var i = 0; i < $scope.datauncontrollable.length;i++){
                    $scope.percentuncontrollable[i] = Math.round((parseInt($scope.datauncontrollable[i]) * 100) / parseInt($scope.order[i]));
                    // $scope.totalLabel.push($scope.total[i]);
                }

                for(var i = 0; i < $scope.datauncontrollable.length;i++){
                    $scope.percentuncontrollable[i] = Math.round((parseInt($scope.datauncontrollable[i]) * 100) / parseInt($scope.order[i]));
                    // $scope.totalLabel.push($scope.total[i]);
                }

                for(var i = 0; i < $scope.dataontime.length;i++){
                    $scope.percentontime[i] = Math.round((parseInt($scope.dataontime[i]) * 100) / parseInt($scope.order[i]));
                    // $scope.totalLabel.push($scope.total[i]);
                }

                // $scope.test['value'] = $scope.percentdhl;

                // console.log($scope.percentdhl);
                // console.log($scope.dataDHLsupport['factorince']);
                

                // $scope.title = ontime[0]['Quater']+" "+ontime[0]['shipmonth']+"-"+ontime[2]['shipmonth'];

                // for(var i = 0; i < $scope.dataontime.length;i++){
                //     $scope.data1.push($scope.dataontime[i]['OnTimeStatus']);
                //     $scope.data2.push($scope.dataorder[i]['OnTimeStatus']);
                // }

                // for(var i = 0; i < 3;i++){
                //     $scope.percent[i] = Math.round((parseInt($scope.data1[i]) * 100) / parseInt($scope.data2[i]));
                //     // $scope.totalLabel.push($scope.total[i]);
                // }

                // $scope.avg = Math.round(($scope.percent[0] + $scope.percent[1] + $scope.percent[2]) / 3);
                // $scope.modelDeviceList.ontime = {
                //         "mon1": $scope.percent[0],
                //         "mon2": $scope.percent[1],
                //         "mon3": $scope.percent[2],
                // };

                // $scope.modelDeviceList.avg = $scope.avg;
            //     $scope.modelDeviceList.dox = {
            //         "ProductName": $scope.dataproduct[3]['ProductName'],
            //             "mon1": new Intl.NumberFormat().format($scope.dataproduct[3]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.dataproduct[4]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.dataproduct[5]['CountWaybill']),
            //     };
            //     $scope.modelDeviceList.wpx = {
            //         "ProductName": $scope.dataproduct[6]['ProductName'],
            //             "mon1": new Intl.NumberFormat().format($scope.dataproduct[6]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.dataproduct[7]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.dataproduct[8]['CountWaybill']),
            //     };

            //     console.log(new Intl.NumberFormat().format($scope.totalLabel));
            //     $scope.modelDeviceList.total = {
            //             "mon1": new Intl.NumberFormat().format($scope.totalLabel[2]),
            //             "mon2": new Intl.NumberFormat().format($scope.totalLabel[1]),
            //             "mon3": new Intl.NumberFormat().format($scope.totalLabel[0]),
            //     };




            //     $scope.modelDeviceList.pac = {
            //         "lane": $scope.datalane[0]['lane'],
            //             "mon1": new Intl.NumberFormat().format($scope.datalane[0]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.datalane[1]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.datalane[2]['CountWaybill']),
            //     };
            //     $scope.modelDeviceList.pjc = {
            //         "lane": $scope.datalane[3]['lane'],
            //             "mon1": new Intl.NumberFormat().format($scope.datalane[3]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.datalane[4]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.datalane[5]['CountWaybill']),
            //     };
            //     $scope.modelDeviceList.roa = {
            //         "lane": $scope.datalane[6]['lane'],
            //             "mon1": new Intl.NumberFormat().format($scope.datalane[6]['CountWaybill']),
            //             "mon2": new Intl.NumberFormat().format($scope.datalane[7]['CountWaybill']),
            //             "mon3": new Intl.NumberFormat().format($scope.datalane[8]['CountWaybill']),
            //     };


            } else {

            }
        }) 

    };

    $scope.reload = function () {

        // $scope.getQuater();
        // $scope.tempQuaterIndex.selected = $scope.listQuater[0];
	    $scope.getYear();
        // let SddStatus;

        // if($scope.TempSddStatusIndex.selected.Status == "All"){
        //     SddStatus = "'Due Today','Overdue','Future Shipments'";
        // }else if($scope.TempSddStatusIndex.selected.Status == "Overdue & Due Today"){
        //     SddStatus = "'Due Today','Overdue'";
        // }else{
        //     SddStatus = "'Future Shipments'";
        // }

        // $scope.DataSearchSddStatus = {
        //     "SddStatus" : SddStatus,
        //     "customer" : "apple",
        // }

        // reportThreeApiService.getPercentStatus($scope.DataSearchSddStatus, function (results) {
        //     var result = results.data;
        //     // console.log(result)
        //     if (result.status === true) {

        //         $scope.totalPage = result.toTalPage;
        //         $scope.listPageIndex = baseService.getListPage(result.toTalPage);
        //         $scope.listPageIndex.forEach(function (entry, index) {
        //             if ($scope.PageIndex === entry.Value)
        //                 $scope.TempPageIndex.selected = entry;
        //         });

        //         $scope.totalRecords = result.totalRecords;
        //         // $scope.modelDeviceList = result.message[0];
        //         $scope.modelDeviceList = result.message;
        //         $scope.listDataGraphPieBytoDay = result.listDataGraphPieBytoDay;
        //         $scope.listDataGraphBarBytoDay = result.listDataGraphBarBytoDay;
        //         $scope.serviceCenterList = result.listDataGraphPieByServiceCenter;
        //         $scope.TotalOnprogress = parseFloat($scope.modelDeviceList[0].num_of_wc) + parseFloat($scope.modelDeviceList[0].num_of_fd);
        //         $scope.SddView = result.sddview;
        //         $scope.last_update = result.last_update;


        //         if ($scope.listDataGraphPieBytoDay.length > 0) {
        //             $scope.createPieStackBytoDay();

        //         }
        //         if ($scope.listDataGraphBarBytoDay.length > 0) {
        //             $scope.createBarStackBytoDay();

        //         }
        //         if ($scope.serviceCenterList.length > 0) {
        //             $scope.createPieStackByServiceCenter();
        //             $scope.createBarStackByServiceCenter();

        //         }


        //         // console.log($scope.listDatalastCheckPiont);
        //     } else {

        //     }
        // })
    }

    $scope.onDeleteTagClick = function (item) {
        codeAppleAndDhlApiService.deleteCodeAppleAndDhl({ id: item.id }, function (result) {
            if (result.status === true) {
                $scope.reload();
            } else {
                baseService.showMessage(result.message);
            }
        });

    }

    $scope.validatecheck = function () {
        var bResult = true;
        $(".require").hide();

        if ($scope.CreateModel.reason_code == "") {
            $(".CreateModel_reason_code").show();
            bResult = false;
        }
        // if ($scope.CreateModel.code_description == "") {
        // 	$(".CreateModel_code_description").show();
        // 	bResult = false;
        // }
        // if ($scope.CreateModel.check_point == "") {
        // 	$(".CreateModel_check_point").show();
        // 	bResult = false;
        // }
        // if ($scope.CreateModel.remark == "") {
        // 	$(".CreateModel_remark").show();
        // 	bResult = false;
        // }


        return bResult;
    }

    $scope.onSaveTagClick = function () {

        var bValid = $scope.validatecheck();
        console.log($scope.CreateModel);
        if (true == bValid) {
            // console.log($scope.CreateModel);
            codeAppleAndDhlApiService.savecodeAppleAndDhl($scope.CreateModel, function (result) {
                if (result.status == true) {
                    $scope.ShowDevice();
                } else {
                    baseService.showMessage(result.message);
                }
            });

        }
    }

    $scope.createBarStackBytoDay = function () {

        // console.log($scope.listDataGraphBarBytoDay);
        var ok = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'OK'
        })
        var rt = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'RT'
        })
        var wc = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'WC'
        })
        var fd = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'FD'
        })
        var exception = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'Exception'
        })
        var other = $scope.listDataGraphBarBytoDay.filter(function (el) {
            return el.latest_checkpiont == 'Other'
        })
        // console.log(ok[0].percent,ok[0].count_total)
        $scope.DataCompletionOK = [ok[0].count_total, 0, 0, 0];
        $scope.DataCompletionRT = [rt[0].count_total, 0, 0, 0];
        $scope.DataOnprogressWC = [0, wc[0].count_total, 0, 0];
        $scope.DataOnprogressFD = [0, fd[0].count_total, 0, 0];
        $scope.DataException = [0, 0, exception[0].count_total, 0];
        $scope.DataOther = [0, 0, 0, other[0].count_total];
        $scope.DataGraphlabel = [
            "Completion",
            "On progress",
            "Exception",
            "Other",
        ];
        // console.log($scope.DataCompletionOK);
        var barChartData = {
            labels: $scope.DataGraphlabel,
            datasets: [{
                label: "OK",
                backgroundColor: "#DAF8B7",
                data: $scope.DataCompletionOK,
                stack: "Stack 0",
            },
            {
                label: "RT",
                backgroundColor: "#99f3bd",
                data: $scope.DataCompletionRT,
                stack: "Stack 1",
            },
            {
                label: "WC",
                backgroundColor: "#ffbb91",
                data: $scope.DataOnprogressWC,
                stack: "Stack 0",
            },
            {
                label: "FD",
                backgroundColor: "#fcdab7",
                data: $scope.DataOnprogressFD,
                stack: "Stack 1",
            },
            {
                label: "Exception",
                backgroundColor: "#FAB3B3",
                data: $scope.DataException,
                stack: "Stack 0",
            },
            {
                label: "Other",
                backgroundColor: "#D6D6D5",
                data: $scope.DataOther,
                stack: "Stack 0",
            },
            ],
        };
        var ctx = document.getElementById("GengraphBarBytoDay").getContext("2d");
        if (window.myBar1) window.myBar1.destroy();
        window.myBar1 = new Chart(ctx, {
            type: "bar",
            data: barChartData,
            options: {
                responsive: true,
                legend: {
                    position: "bottom",
                },
                title: {
                    display: true,
                    // text: "To Day",
                    position: "top",
                    fontSize: 15,
                    padding: 40,
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                    },],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            min: 0,
                            callback: function (value) {
                                return value
                                    .toString()
                                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                            },
                        },
                    },],
                },
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value !== 0 ?
                                value.toLocaleString( /* ... */) :
                                ''
                        },
                        anchor: 'end',
                        align: 'end',
                        color: '#888'
                    }
                }
            }
        });
    };



    // $scope.createBarStackBytoDay = function () {
    //     $scope.DatalastCheckPiont = [];
    //     $scope.DatalastCheckPiontlabel = [];
    //     // console.log($scope.listDatalastCheckPiont.length);
    //     if ($scope.listDatalastCheckPiont.length > 0) {
    //         for (var i = 0; i < $scope.listDatalastCheckPiont.length; i++) {
    //             $scope.DatalastCheckPiont.push(
    //                 parseFloat($scope.listDatalastCheckPiont[i]["count_total"])
    //             );
    //             $scope.DatalastCheckPiontlabel.push($scope.listDatalastCheckPiont[i]["latest_checkpiont"]);
    //         }
    //         var barChartData1 = {
    //             labels: $scope.DatalastCheckPiontlabel,
    //             datasets: [{
    //                 label: "label",
    //                 backgroundColor: ["#61F52A", "#15C606", "#DDF019", "#E6FC02", "#F30C0C", "#C9C8C8"],
    //                 data: $scope.DatalastCheckPiont,
    //                 borderWidth: 1,
    //                 borderColor: "#FFFFFF",
    //             },],
    //         };
    //         var ctxProduct = document
    //             .getElementById("canvas")
    //             .getContext("2d");
    //         if (window.myBar1) window.myBar1.destroy();
    //         window.myBar1 = new Chart(ctxProduct, {
    //             type: "bar",
    //             data: barChartData1,
    //             options: {
    //                 title: {
    //                     display: true,
    //                     text: "Apple reportTwo",
    //                     fontSize: 15,
    //                     padding: 20,
    //                 },
    //                 legend: {
    //                     display: true,
    //                     position: "bottom",
    //                 },
    //                 responsive: true,
    //                 scales: {
    //                     xAxes: [{

    //                         stacked: true,
    //                         maxBarThickness: 50,
    //                         ticks: {
    //                             fontSize: 10,
    //                             autoSkip: false,
    //                             maxRotation: 90,
    //                             minRotation: 90
    //                         },
    //                     },],
    //                     yAxes: [{

    //                         scaleLabel: {
    //                             display: true,
    //                             labelString: "",
    //                         },
    //                         stacked: true,
    //                         ticks: {
    //                             min: 0
    //                         },
    //                     },],
    //                 },
    //                 plugins: {
    //                     datalabels: {
    //                         formatter: (value, Tripdate) => {
    //                             const total =
    //                                 Tripdate.chart.$totalizer.totals[Tripdate.dataIndex];
    //                             return total.toLocaleString(/* ... */);
    //                         },
    //                         align: "end",
    //                         anchor: "end",
    //                         display: function (Tripdate) {
    //                             return (
    //                                 Tripdate.datasetIndex === Tripdate.chart.$totalizer.utmost
    //                             );
    //                         }
    //                     }
    //                 }
    //             },
    //         });
    //     }
    // };


    $scope.createPieStackBytoDay = function () {
        // $scope.DataGenGraphPieByToDay = $scope.listDataGraphPieBytoDay.map(status => status.count_total)
        // $scope.DataGenGraphPieByToDaylabel = $scope.listDataGraphPieBytoDay.map(status => status.latest_checkpiont)
        // console.log($scope.listDataGraphPieBytoDay);

        $scope.DataGenGraphPieByToDay = [];
        $scope.DataGenGraphPieByToDaylabel = [];
        $scope.DataGenGraphPieByToDayShowValue = [];
        var colorsPieStackBytoDays  = ["#B7F8AD", "#FBE3C4", "#FAB3B3", "#D6D6D5"];
        var colorPieStackBytoDay  = [];
        for (var i = 0; i < $scope.listDataGraphPieBytoDay.length; i++) {
            if (
                $scope.listDataGraphPieBytoDay[i]["percent"] != null &&
                $scope.listDataGraphPieBytoDay[i]["percent"] != "0.00" &&
                $scope.listDataGraphPieBytoDay[i]["percent"] != undefined
            ) {
                $scope.DataGenGraphPieByToDaylabel.push($scope.listDataGraphPieBytoDay[i]["latest_checkpiont"]);
                $scope.DataGenGraphPieByToDay.push($scope.listDataGraphPieBytoDay[i]["percent"]);
                $scope.DataGenGraphPieByToDayShowValue.push($scope.listDataGraphPieBytoDay[i]["latest_checkpiont"] + " " + $scope.listDataGraphPieBytoDay[i]["percent"] + " %");
                colorPieStackBytoDay.push(colorsPieStackBytoDays[i]);
            } else {

            }
        }
        // $scope.DataGenGraphPieByToDaylabel = ["Completion", "On Progress", "Exception", "Other"];
        // $scope.DataGenGraphPieByToDay = ["580", "7", "9", "600"];

        // console.log($scope.DataGenGraphPieByToDay);
        // $scope.listDatalastCheckPiont.map(status =>
        //     $scope.DataGenGraphPieByTotal.push(status.count_total)
        // )

        var pieChartData1 = {
            labels: $scope.DataGenGraphPieByToDaylabel,
            datasets: [{
                label: "label",
                backgroundColor: colorPieStackBytoDay,
                data: $scope.DataGenGraphPieByToDay,
                borderWidth: 1,
                borderColor: "#FFFFFF",
            },],
        };
        var ctxProduct = document
            .getElementById("GenGraphPieByToday")
            .getContext("2d");
        if (window.myPie1) window.myPie1.destroy();
        window.myPie1 = new Chart(ctxProduct, {
            type: "pie",
            data: pieChartData1,
            options: {
                title: {
                    display: true,
                    // text: "",
                    fontSize: 15,
                    padding: 20,
                },
                legend: {
                    display: true,
                    position: "right",
                    labels: {
                        // fontColor : 'reg(255, 99, 132)',
                        padding: 20

                    }
                },

                responsive: true,
                scales: {
                    xAxes: [{
                        display: false,
                        stacked: true,
                        maxBarThickness: 50,
                        ticks: {
                            fontSize: 10,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                    },],
                    yAxes: [{
                        display: false,
                        scaleLabel: {
                            display: true,
                            labelString: "",
                        },
                        stacked: true,
                        ticks: {
                            min: 0
                        },
                    },],
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 20,
                        bottom: 55,
                    },
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                    // outlabels: {
                    //     color: 'black',
                    // },
                    outlabels: {
                        display: true,
                        text: $scope.DataGenGraphPieByToDayShowValue,
                        // borderWidth: 2,
                        // borderColor: "#000000",
                        // backgroundColor: "#FFFFFF",
                        lineWidth: 2,
                        padding: 3,
                        textAlign: "center",
                        stretch: 20,
                        font: {
                            resizable: true,
                            minSize: 12,
                            maxSize: 18,
                        },
                        color: 'black',
                        // lineColor: "#000000",
                        // valuePrecision: 1,
                        // percentPrecision: 2,
                    }

                }
            },
        });
        // }
    };


    $scope.createPieStackByServiceCenter = function () {

        // $scope.serviceCenterList = [];
        // $scope.DataGenGraphPieByServiceCenter = [];
        // $scope.DataGenGraphPieByServiceCenterlabel = [];
        // $scope.DataGenGraphBarByCostcenter = ['45', '39', '50', '29', '42', '28', '30', '22', '53', '32', '27', '31', '52'];

        // for (var i = 0; i < $scope.serviceCenterList.length; i++) {
        //     // console.log($scope.serviceCenterList[i]);
        //     $scope.DataGenGraphPieByServiceCenter.push($scope.serviceCenterList[i].percent);
        //     $scope.DataGenGraphPieByServiceCenterlabel.push($scope.serviceCenterList[i].service_center);
        // }
        // console.log($scope.serviceCenterList)
        $scope.DataGenGraphPieByServiceCenter = [];
        $scope.DataGenGraphPieByServiceCenterlabel = [];
        $scope.DataGenGraphPieByServiceCenterShowValue = [];
        var colors = ["#3AFF00", "#F0FF00", "#FA5454", "#FFBB28", "#F9F382", "#21C105", "#03FF72", "#03FFCA", "#F59FFF", "#C1D7F9", "#FB9AD5", "#97F9D2", "#A0EFFC", "#D6D6D5"];
        var color = [];
        for (var i = 0; i < $scope.serviceCenterList.length; i++) {
            if (
                $scope.serviceCenterList[i]["percent"] != null &&
                $scope.serviceCenterList[i]["percent"] != "0.00" &&
                $scope.serviceCenterList[i]["percent"] != undefined
            ) {
                $scope.DataGenGraphPieByServiceCenterlabel.push($scope.serviceCenterList[i]["service_center"]);
                $scope.DataGenGraphPieByServiceCenter.push($scope.serviceCenterList[i]["percent"]);
                $scope.DataGenGraphPieByServiceCenterShowValue.push($scope.serviceCenterList[i]["service_center"] + " " + $scope.serviceCenterList[i]["percent"] + " %");
                color.push(colors[i]);
            } else {

            }
        }

        // console.log($scope.DataGenGraphPieByServiceCenterShowValue);

        // if ($scope.listDatalastCheckPiont.length > 0) {
        // 	for (var i = 0; i < $scope.listDatalastCheckPiont.length; i++) {
        // 		$scope.DatalastCheckPiont.push(
        // 			parseFloat($scope.listDatalastCheckPiont[i]["count_total"])
        // 		);
        // 		$scope.DatalastCheckPiontlabel.push($scope.listDatalastCheckPiont[i]["latest_checkpiont"]);
        // 	}
        var pieChartData2 = {
            labels: $scope.DataGenGraphPieByServiceCenterlabel,
            datasets: [{
                label: "label",
                backgroundColor: color,
                data: $scope.DataGenGraphPieByServiceCenter,
                borderWidth: 1,
                borderColor: "#FFFFFF",
            },],
            // hiddenSlices: [1, 3],

        };
        var ctxProduct = document
            .getElementById("GenGraphPieByServiceCenter")
            .getContext("2d");
        if (window.myPie2) window.myPie2.destroy();
        window.myPie2 = new Chart(ctxProduct, {
            type: "pie",
            data: pieChartData2,
            options: {
                title: {
                    display: true,
                    // text: "By Servicecenter",
                    // position: "top",
                    fontSize: 15,
                    // margin: 10,
                    padding: 10,
                },
                legend: {
                    display: true,
                    position: "right",
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        display: false,
                        stacked: true,
                        maxBarThickness: 50,
                        ticks: {
                            fontSize: 10,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                    },],
                    yAxes: [{
                        display: false,
                        scaleLabel: {
                            display: true,
                            labelString: "",
                        },
                        stacked: true,
                        ticks: {
                            min: 0
                        },
                    },],
                },
                // plugins: {
                // 	datalabels: {
                // 		formatter: (value, Tripdate) => {
                // 			const total =
                // 				Tripdate.chart.$totalizer.totals[Tripdate.dataIndex];
                // 			return total.toLocaleString(/* ... */);
                // 		},
                // 		align: "end",
                // 		anchor: "end",
                // 		display: function (Tripdate) {
                // 			return (
                // 				Tripdate.datasetIndex === Tripdate.chart.$totalizer.utmost
                // 			);
                // 		}
                // 	}
                // }
                plugins: {
                    datalabels: {
                        formatter: function (value, ctx) {
                            return value + "%";
                        },
                        // anchor: 'end',
                        // align: 'end',
                        color: '#888'
                    },
                    datalabels: {
                        display: false,
                    },
                    // outlabels: {
                    //     color: 'black',
                    //     stretch: 20,
                    // },
                    outlabels: {
                        display: true,
                        text: $scope.DataGenGraphPieByServiceCenterShowValue,
                        // borderWidth: 2,
                        // borderColor: "#000000",
                        // backgroundColor: "#FFFFFF",
                        lineWidth: 2,
                        padding: 3,
                        textAlign: "center",
                        stretch: 20,
                        font: {
                            resizable: true,
                            minSize: 12,
                            maxSize: 18,
                        },
                        color: 'black',
                        // lineColor: "#000000",
                        // valuePrecision: 1,
                        // percentPrecision: 2,
                    },
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 20,
                        bottom: 55,
                    },
                },
            },
        });

        // }
    };


    $scope.createBarStackByServiceCenter = function () {
        $scope.DataGenGraphBarByCostcenter = [];
        $scope.DataGenGraphBarByCostcenterlabel = [];
        var color = ["#3AFF00", "#F0FF00", "#FA5454", "#FFBB28", "#F9F382", "#21C105", "#03FF72", "#03FFCA", "#F59FFF", "#C1D7F9", "#FB9AD5", "#97F9D2", "#A0EFFC", "#D6D6D5"];
        // console.log($scope.listDatalastCheckPiont.length);
        var data = [];

        var barChartData2 = {
            labels: [],
            datasets: data
        };
        for (var i = 0; i < $scope.serviceCenterList.length; i++) {


            var value = [null, null, null, null, null, null, null, null, null, null, null, null, null];
            value[i] = $scope.serviceCenterList[i].count_total;
            // console.log(value);
            barChartData2.labels.push($scope.serviceCenterList[i].service_center)
            data.push({
                label: $scope.serviceCenterList[i].service_center,
                backgroundColor: color[i],
                data: value,
                borderWidth: 1,
                borderColor: "#FFFFFF",
            });
        }

        // if ($scope.listDatalastCheckPiont.length > 0) {
        // 	for (var i = 0; i < $scope.listDatalastCheckPiont.length; i++) {
        // 		$scope.DatalastCheckPiont.push(
        // 			parseFloat($scope.listDatalastCheckPiont[i]["count_total"])
        // 		);
        // 		$scope.DatalastCheckPiontlabel.push($scope.listDatalastCheckPiont[i]["latest_checkpiont"]);
        // 	}

        var ctxProduct = document
            .getElementById("GenGraphBarByServiceCenter")
            .getContext("2d");
        if (window.myBar2) window.myBar2.destroy();
        window.myBar2 = new Chart(ctxProduct, {
            type: "bar",
            data: barChartData2,
            options: {
                title: {
                    display: true,
                    // text: "By Costcenter",
                    fontSize: 15,
                    padding: 20,
                },
                legend: {
                    display: true,
                    position: "bottom",
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                        maxBarThickness: 50,
                        ticks: {
                            fontSize: 10,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                    },],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "",
                        },
                        stacked: true,
                        ticks: {
                            min: 0
                        },
                    },],
                },
                // plugins: {
                // 	datalabels: {
                // 		formatter: (value, Tripdate) => {
                // 			const total =
                // 				Tripdate.chart.$totalizer.totals[Tripdate.dataIndex];
                // 			return total.toLocaleString(/* ... */);
                // 		},
                // 		align: "end",
                // 		anchor: "end",
                // 		display: function (Tripdate) {
                // 			return (
                // 				Tripdate.datasetIndex === Tripdate.chart.$totalizer.utmost
                // 			);
                // 		}
                // 	}
                // }
            },
        });
        // }
    };

    $scope.Exportreport = function () {
        // console.log($scope.tempYearIndex);

        if($scope.tempQuaterIndex.selected != undefined){
            $scope.modelSearch.Quater = $scope.tempQuaterIndex.selected.Quater;
        }
        if($scope.tempYearIndex.selected != undefined){
            $scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
        }

        // if($scope.modelSearch.start_date != undefined){
        //     $scope.modelSearch.week = "";
        //     $scope.modelSearch.Year = "";
        // } else {
        //     if($scope.tempWeekIndex.selected != undefined){
        //     $scope.modelSearch.week = $scope.tempWeekIndex.selected.week;
        //     }
        //     if($scope.tempYearIndex.selected != undefined){
        //         $scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
        //     }
        // }

        // console.log($scope.modelSearch.start_date);
        
        // console.log($scope.modelSearch);

        reportThreeApiService.Exportreport($scope.modelSearch, function (result) {
            if (result.status === true) {
                var download_url = get_base_url('');
                // console.log(download_url);
                var file = download_url + 'upload/export_excel/' + result.message;
                window.open(file, '_blank');
            } else {
                baseService.showMessage(result.message);
            }
        })
    }


    $scope.ExportDowloadAllShipment = function (item, type) {
        // console.log(item);
        reportThreeApiService.ExportDowloadAllShipment({ data: item, type: type }, function (result) {
            if (result.status === true) {
                var download_url = get_base_url('');
                var file = download_url + 'upload/export_excel/' + result.message;
                window.open(file, '_blank');
            } else {
                baseService.showMessage(result.message);
            }
        })
    }







}]);
myApp.controller('checkpointController', ['$scope', 'Upload', '$filter', 'baseService', 'checkpointApiService',
    function ($scope, Upload, $filter, baseService, checkpointApiService) {

    $scope.modelDeviceList = [];
    $scope.CreateModel = {};
    $scope.modelSearch = {};

    
	$scope.file_Pandora_Check = {};
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    // $scope.PageIndex = 1;
    $scope.PageIndex = baseService.setPageIndex(1);
    $scope.SortColumn = baseService.setSortColumn('Id');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.TempServicecenterIndex = {};


    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide();
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();

    }

    $scope.AddNewDevice = function () {
        $scope.resetModel();
        $(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide();
        $(".addDevice").show();

    }

    $scope.onEditTagClick = function (item) {
        $scope.AddNewDevice();
        $scope.loadEditData(item);

    }

    $scope.loadEditData = function (item) {

        console.log(item);
        $scope.CreateModel = angular.copy(item);

    }

    $scope.resetModel = function () {

        $scope.CreateModel = { id: 0, reporting_code_description: "", 	reporting_code_cat_description: "", 	factor_incident: "", 	factor: ""};
        $scope.file_Pandora_Check = "";
       

    }


    $scope.resetSearch = function () {
        $scope.modelSearch = {
            // "Store": "",
            "reporting_code_description": "",
            "reporting_code_cat_description": "",
            "factor_incident": "",
            "factor": "",

        };
        $scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () {

        $scope.resetModel();
        $scope.resetSearch();

        $scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
        });

        //$scope.reload();
    };

    $scope.reload = function () {
        checkpointApiService.ListCheckpoint($scope.modelSearch, function (results) {
            var result = results.data;
            if (result.status === true) {
                $scope.totalPage = result.toTalPage;
                $scope.listPageIndex = baseService.getListPage(result.toTalPage);
                $scope.listPageIndex.forEach(function (entry, index) {
                    if ($scope.PageIndex === entry.Value)
                        $scope.TempPageIndex.selected = entry;
                });
                $scope.modelDeviceList = result.message;
                $scope.totalRecords = result.totalRecords;

            } else {

            }
        })
    }

    

		$scope.upload = function () {
            // console.log(Upload);
			// var bValid = $scope.validatecheck();
			var file_Pandora_Check = $scope.file_Pandora_Check.name;
			// var resapple = file_pnd3.substr(file_pnd3.length - 5, file_pnd3.length);
			// var fileshipment = $scope.fileshipment.name;
			// var resshipment = fileshipment.substr(fileshipment.length - 5, fileshipment.length);

				baseService.showOverlay();
				var url = get_base_url('Checkpoint/upload_file');
				// console.log(url)
				console.log($scope.file_Pandora_Check)
				Upload.upload({
					url: url,
					data: {
						file_Pandora_Check: $scope.file_Pandora_Check,

					}
				})
					.then(function (resp) {
						// console.log(resp);
						if (resp.status == 200) {
							baseService.hideOverlay();
							$scope.ShowDevice();
							$scope.file_Pandora_Check = undefined;
							baseService.showMessage("Save success");

							// if (resp.data.code == 100) {
							// 	var download_url = get_base_url('');
							// 	var file = download_url + 'upload/' + resp.data.filename;
							// 	window.open(file, '_bank');
							// }
							if (resp.data.status == true) {
								baseService.showMessage("Save success");
							} else {
								baseService.showMessage(resp.data.message);
							}
						} else {
							baseService.showMessage("Can not upload file");
						}
					});


		}

    $scope.onDeleteTagClick = function (item) {
        checkpointApiService.DeleteCheckpoint({ id: item.id }, function (result) {
            if (result.status === true) {
                $scope.reload();
            } else {
                baseService.showMessage(result.message);
            }
        });

    }

    $scope.validatecheck = function () {
        var bResult = true;
        $(".require").hide();

        // if ($scope.CreateModel.Store == "") {
        //     $(".CreateModel_Store").show();
        //     bResult = false;
        // }
        if ($scope.CreateModel.reporting_code_description == "") {
            $(".CreateModel_reporting_code_description").show();
            bResult = false;
        }

        if ($scope.CreateModel.reporting_code_cat_description == "") {
            $(".CreateModel_reporting_code_cat_description").show();
            bResult = false;
        }

        if ($scope.CreateModel.factor_incident == "") {
            $(".CreateModel_factor_incident").show();
            bResult = false;
        }

        if ($scope.CreateModel.factor == "") {
            $(".CreateModel_factor").show();
            bResult = false;
        }
        return bResult;
    }

    $scope.onSaveTagClick = function () {
        var bValid = $scope.validatecheck();

        if (true == bValid) {
            //console.log($scope.CriteriaModel);
            // $scope.CreateModel.Id = 0;
            // $scope.CreateModel.DHL_Svc = $scope.TempServicecenterIndex.selected.Svc;
            checkpointApiService.SaveCheckpoint($scope.CreateModel, function (result) {
                if (result.status == true) {
                    baseService.showMessage(result.message);
                    $scope.ShowDevice();
                } else {
                    baseService.showMessage(result.message);
                }
            });

        }
    }

    
    $scope.Exportexcel = function () {

        // if($scope.tempWeekIndex.selected != undefined){
        //     $scope.modelSearch.week = $scope.tempWeekIndex.selected.week;
        // }
        // if($scope.tempYearIndex.selected != undefined){
        //     $scope.modelSearch.Year = $scope.tempYearIndex.selected.Year;
        // }
        // console.log($scope.modelSearch);

        checkpointApiService.Exportexcel($scope.modelSearch, function (result) {
            if (result.status === true) {
                var download_url = get_base_url('');
                // console.log(download_url);
                var file = download_url + 'upload/export_excel/' + result.message;
                window.open(file, '_blank');
            } else {
                baseService.showMessage(result.message);
            }
        })
    }

}]);
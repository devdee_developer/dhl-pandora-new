myApp.controller('codeAppleAndDhlController', ['$scope', '$filter', 'baseService', 'codeAppleAndDhlApiService', function ($scope, $filter, baseService, codeAppleAndDhlApiService) {

	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};

	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('id');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system

	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$scope.reload();
	}

	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();

	}


	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {
		$scope.CreateModel = angular.copy(item);
		console.log($scope.CreateModel)
	}

	$scope.resetModel = function () {

		$scope.CreateModel = { id: 0, reason_code: "", code_description: "", check_point: "", remark: "" };
	}


	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"reason_code": "",
			"check_point": "",
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();


		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		//$scope.reload();
	}

	$scope.reload = function () {
		codeAppleAndDhlApiService.listCodeAppleAndDhl($scope.modelSearch, function (results) {
			var result = results.data;
			// console.log(result)
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});

				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
			} else {

			}
		})
	}

	$scope.onDeleteTagClick = function (item) {
		codeAppleAndDhlApiService.deleteCodeAppleAndDhl({ id: item.id }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}

	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();

		if ($scope.CreateModel.reason_code == "") {
			$(".CreateModel_reason_code").show();
			bResult = false;
		}
		if ($scope.CreateModel.check_point == "") {
			$(".CreateModel_check_point").show();
			bResult = false;
		}
		// if ($scope.CreateModel.check_point == "") {
		// 	$(".CreateModel_check_point").show();
		// 	bResult = false;
		// }
		// if ($scope.CreateModel.remark == "") {
		// 	$(".CreateModel_remark").show();
		// 	bResult = false;
		// }


		return bResult;
	}

	$scope.onSaveTagClick = function () {

		var bValid = $scope.validatecheck();
		console.log($scope.CreateModel);
		if (true == bValid) {
			// console.log($scope.CreateModel);
			codeAppleAndDhlApiService.savecodeAppleAndDhl($scope.CreateModel, function (result) {
				if (result.status == true) {
					$scope.ShowDevice();
					baseService.showMessage(result.message);
				} else {
					baseService.showMessage(result.message);
				}
			});

		}
	}


}]); 
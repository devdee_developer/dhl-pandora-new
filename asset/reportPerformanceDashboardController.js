myApp.controller("reportPerformanceDashboardController", [
	"$scope",
	"$filter",
	"baseService",
	"reportPerformanceDashboardApiService",
	function (
		$scope,
		$filter,
		baseService,
		reportPerformanceDashboardApiService
	) {
		$scope.modelDeviceList = [];
		$scope.CreateModel = {};
		$scope.modelSearch = {};

		//page system
		$scope.listPageSize = baseService.getListPageSize();
		$scope.TempPageSize = {};
		$scope.TempPageIndex = {};
		$scope.PageSize = baseService.setPageSize(20);
		$scope.totalPage = 1; //init;
		$scope.totalRecords = 0;
		$scope.PageIndex = 1;
		$scope.SortColumn = baseService.setSortColumn("id");
		$scope.SortOrder = baseService.setSortOrder("asc");

		$scope.listDatalastCheckPiont = [];

		$scope.listgraph1 = [];
		$scope.listgraph2 = [];
		$scope.listgraph3 = [];
		$scope.listgraph4 = [];
		$scope.listgraph5 = [];
		$scope.listgraph6 = [];
		$scope.listgraph7 = [];
		$scope.listgraph8 = [];
		$scope.listgraph9 = [];
		$scope.listgraph10 = [];

		$scope.tempQuaterIndex = {};
		$scope.tempYearIndex = {};

		$scope.listMonth = [];
		$scope.tempMonthIndex = {};

		$scope.listWeek = [];
		$scope.tempWeekIndex = {};

		$scope.listCountry = [];
		$scope.tempCountryIndex = {};

		$scope.listRegion = [];
		$scope.tempRegionIndex = {};

		$scope.listGateway = [];
		$scope.tempGatewayIndex = {};

		$scope.listOverSLA = [
			{ OverSLAlist: "All" },
			{ OverSLAlist: "Ontime" },
			{ OverSLAlist: "Over 1-2 days" },
			{ OverSLAlist: "Over 3-4 days" },
			{ OverSLAlist: "Over 5-6 days" },
			{ OverSLAlist: "Over > 7 days" },
		];
		$scope.tempOverSLAIndex = {};

		$scope.listCompany = [
			{ Companylist: "PANDORA SERVICES CO.,LTD." },
		];
		$scope.tempCompanyIndex = {Companylist: "PANDORA SERVICES CO.,LTD."};

		$scope.listPerformanceDashboard = [];
		$scope.tempPerformanceDashboardIndex = {};

		$scope.listQuater = [
			{ Quater: "Q1" },
			{ Quater: "Q2" },
			{ Quater: "Q3" },
			{ Quater: "Q4" },
		];

		$scope.TempSddStatusIndex = {};
		$scope.listSddStatus = [
			{ Status: "All" },
			{ Status: "Overdue & Due Today" },
			{ Status: "Future Shipments" },
		];

		$scope.DataSearchSddStatus = {};

		$scope.isView = false;

		$scope.sort = function (e) {
			baseService.sort(e);
			$scope.SortColumn = baseService.getSortColumn();
			$scope.SortOrder = baseService.getSortOrder();
			$scope.reload();
		};

		$scope.getFirstPage = function () {
			$scope.PageIndex = baseService.getFirstPage();
			$scope.reload();
		};
		$scope.getBackPage = function () {
			$scope.PageIndex = baseService.getBackPage();
			$scope.reload();
		};
		$scope.getNextPage = function () {
			$scope.PageIndex = baseService.getNextPage();
			$scope.reload();
		};
		$scope.getLastPage = function () {
			$scope.PageIndex = baseService.getLastPage();
			$scope.reload();
		};
		$scope.searchByPage = function () {
			$scope.PageIndex = baseService.setPageIndex(
				$scope.TempPageIndex.selected.PageIndex
			);
			$scope.reload();
		};
		$scope.setPageSize = function (data) {
			$scope.PageSize = baseService.setPageSize(
				$scope.TempPageSize.selected.Value
			);
		};
		$scope.loadByPageSize = function () {
			$scope.PageIndex = baseService.setPageIndex(1);
			$scope.setPageSize();
			$scope.reload();
		};
		//page system

		$scope.ShowDevice = function () {
			$(".DisplayDevice").show();
			$(".SearchDevice").hide();
			$(".addDevice").hide();
			$scope.reload();
		};

		$scope.ShowSearch = function () {
			$(".DisplayDevice").hide();
			$(".SearchDevice").show();
			$(".addDevice").hide();
		};

		$scope.LoadSearch = function () {
			$scope.ShowDevice();
		};

		$scope.AddNewDevice = function () {
			$scope.resetModel();
			$(".require").hide();
			$(".DisplayDevice").hide();
			$(".SearchDevice").hide();
			$(".addDevice").show();
		};

		$scope.onEditTagClick = function (item) {
			$scope.AddNewDevice();
			$scope.loadEditData(item);
		};

		$scope.loadEditData = function (item) {
			$scope.CreateModel = angular.copy(item);
			console.log($scope.CreateModel);
		};

		$scope.resetModel = function () {
			$scope.CreateModel = {
				id: 0,
				reason_code: "",
				code_description: "",
				check_point: "",
				remark: "",
			};
		};

		$scope.resetSearch = function () {
			$scope.modelSearch = {
				Quater: "",
				Year: "",
			};

			$scope.tempQuaterIndex.selected = $scope.listQuater[0];
			$scope.tempYearIndex.selected = $scope.listYear[0];
			// $scope.LoadSearch();
		};

		////////////////////////////////////////////////////////////////////////////////////////
		// Event
		$scope.onInit = function () {
			$(".require").hide();
			$scope.tempQuaterIndex.selected = $scope.listQuater[0];
			// console.log($scope.listQuater);
			// $scope.resetModel();
			// $scope.resetSearch();
			// $scope.getCompany();
			$scope.reload();

			// $scope.listPageSize.forEach(function (entry, index) {
			//     if (0 === index)
			//         $scope.TempPageSize.selected = entry;
			// });

			//$scope.reload();
		};

		$scope.getYear = function (item) {
			reportPerformanceDashboardApiService.getYearComboBox(
				$scope.modelSearch,
				function (result) {
					console.log(result);
					if (result.status === true) {
						$scope.listYear = result.message;
						console.log($scope.listYear);
						if ($scope.listYear.length > 0) {
							if (item == undefined) {
								$scope.tempYearIndex.selected = $scope.listYear[0];
								console.log("add");
							} else {
								$scope.listYear.forEach(function (entry, index) {
									if (entry.Year === item.Year) {
										$scope.tempYearIndex.selected = entry;
									}
								});
								console.log("edit");
							}
						} else {
							$scope.tempYearIndex.selected = undefined;
						}
					} else {
						baseService.showMessage(result.message);
					}
				}
			);
		};

		$scope.getMonth = function (item) {
			$scope.monthlist = [
				{ monthNo: "01", monthname: "Jan" },
				{ monthNo: "02", monthname: "Feb" },
				{ monthNo: "03", monthname: "Mar" },
				{ monthNo: "04", monthname: "Apr" },
				{ monthNo: "05", monthname: "May" },
				{ monthNo: "06", monthname: "Jun" },
				{ monthNo: "07", monthname: "Jul" },
				{ monthNo: "08", monthname: "Aug" },
				{ monthNo: "09", monthname: "Sep" },
				{ monthNo: "10", monthname: "Oct" },
				{ monthNo: "11", monthname: "Nov" },
				{ monthNo: "12", monthname: "Dec" },
			];

			var today = new Date();

			var month = today.getMonth();

			for (let i = month; i >= month - 2; i--) {
				$scope.listMonth.push($scope.monthlist[i]);
			}
			// $scope.tempMonthIndex.selected = $scope.listMonth[0]
		};

		$scope.getWeek = function () {
			// console.log($scope.tempMonthIndex.selected)
			$scope.listWeek = [];
			$scope.tempWeekIndex = {};

			$scope.listCountry = [];
			$scope.tempCountryIndex = {};

			$scope.listRegion = [];
			$scope.tempRegionIndex = {};

			$scope.listGateway = [];
			$scope.tempGatewayIndex = {};

			$scope.listOverSLA = [
				// { OverSLAlist: "All" },
				{ OverSLAlist: "Ontime" },
				{ OverSLAlist: "Over 1-2 days" },
				{ OverSLAlist: "Over 3-4 days" },
				{ OverSLAlist: "Over 5-6 days" },
				{ OverSLAlist: "Over > 7 days" },
			];
			$scope.tempOverSLAIndex = {};

			if ($scope.tempMonthIndex.selected.length > 0) {
				// console.log($scope.tempMonthIndex);
				$scope.txtmonth = "";
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth += $scope.tempMonthIndex.selected[i]["monthNo"];
					} else {
						$scope.txtmonth +=
							$scope.tempMonthIndex.selected[i]["monthNo"] + ",";
					}
				}
				$scope.model = { month: "(" + $scope.txtmonth + ")" };
				reportPerformanceDashboardApiService.getWeekComboBox(
					$scope.model,
					function (result) {
						// console.log(result);
						if (result.status === true) {
							result.message.forEach(function (entry, index) {
								$scope.listWeek.push(entry);
							});
						} else {
							baseService.showMessage(result.message);
						}
					}
				);
				// console.log($scope.tempMonthIndex.selected)
				// $scope.tempWeekIndex.selected = $scope.listWeek[0];
			}
		};

		$scope.getCountry = function () {
			$scope.listCountry = [];
			$scope.tempCountryIndex = {};
			$scope.listRegion = [];
			$scope.tempRegionIndex = {};
			$scope.tempGatewayIndex = {};
			$scope.listOverSLA = [
				// { OverSLAlist: "All" },
				{ OverSLAlist: "Ontime" },
				{ OverSLAlist: "Over 1-2 days" },
				{ OverSLAlist: "Over 3-4 days" },
				{ OverSLAlist: "Over 5-6 days" },
				{ OverSLAlist: "Over > 7 days" },
			];
			if (
				$scope.tempMonthIndex.selected.length > 0 &&
				$scope.tempWeekIndex.selected.length > 0
			) {
				$scope.txtmonth = "";
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthNo"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthNo"] + "',";
					}
				}
				// $scope.model = { month: "(" +$scope.txtmonth + ")" };

				$scope.txtweek = "";
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
				$scope.model = {
					week: "(" + $scope.txtweek + ")",
					month: "(" + $scope.txtmonth + ")",
				};
				// console.log($scope.model)
				reportPerformanceDashboardApiService.getCountryComboBox(
					$scope.model,
					function (result) {
						// console.log(result);
						if (result.status === true) {
							result.message.forEach(function (entry, index) {
								$scope.listCountry.push(entry);
							});
						} else {
							baseService.showMessage(result.message);
						}
					}
				);
			}
		};

		$scope.getRegion = function () {
			$scope.listRegion = [];
			$scope.tempRegionIndex = {};
			$scope.listGateway = [];
			$scope.tempGatewayIndex = {};
			$scope.listOverSLA = [
				// { OverSLAlist: "All" },
				{ OverSLAlist: "Ontime" },
				{ OverSLAlist: "Over 1-2 days" },
				{ OverSLAlist: "Over 3-4 days" },
				{ OverSLAlist: "Over 5-6 days" },
				{ OverSLAlist: "Over > 7 days" },
			];
			if (
				$scope.tempMonthIndex.selected.length > 0 &&
				$scope.tempWeekIndex.selected.length > 0 &&
				$scope.tempCountryIndex.selected.length > 0
			) {
				// }
				$scope.txtmonth = "";
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthNo"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthNo"] + "',";
					}
				}
				// $scope.model = { month: "(" +$scope.txtmonth + ")" };

				$scope.txtweek = "";
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
				// $scope.model = { week: "(" +$scope.txtweek + ")" };

				$scope.txtcountry = "";
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
				$scope.model = {
					month: "(" + $scope.txtmonth + ")",
					week: "(" + $scope.txtweek + ")",
					country: "(" + $scope.txtcountry + ")",
				};

				reportPerformanceDashboardApiService.getRegionComboBox(
					$scope.model,
					function (result) {
						// console.log(result);
						if (result.status === true) {
							result.message.forEach(function (entry, index) {
								$scope.listRegion.push(entry);
							});
						} else {
							baseService.showMessage(result.message);
						}
					}
				);
			}
		};

		$scope.getGateway = function () {
			$scope.listGateway = [];
			$scope.tempGatewayIndex = {};

			$scope.listOverSLA = [
				// { OverSLAlist: "All" },
				{ OverSLAlist: "Ontime" },
				{ OverSLAlist: "Over 1-2 days" },
				{ OverSLAlist: "Over 3-4 days" },
				{ OverSLAlist: "Over 5-6 days" },
				{ OverSLAlist: "Over > 7 days" },
			];
			$scope.tempOverSLAIndex = {};

			if (
				$scope.tempMonthIndex.selected.length > 0 &&
				$scope.tempWeekIndex.selected.length > 0 &&
				$scope.tempCountryIndex.selected.length > 0 &&
				$scope.tempRegionIndex.selected.length > 0
			) {
				$scope.txtmonth = "";
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthNo"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthNo"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };

				$scope.txtweek = "";
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
				// $scope.model = { week: "(" + $scope.txtweek + ")" };

				$scope.txtcountry = "";
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
				// $scope.model = { country: "(" + $scope.txtcountry + ")" };

				$scope.txtregion = "";
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
				$scope.model = {
					month: "(" + $scope.txtmonth + ")",
					week: "(" + $scope.txtweek + ")",
					country: "(" + $scope.txtcountry + ")",
					region: "(" + $scope.txtregion + ")",
				};

				// $scope.model = {
				// 	month: $scope.tempMonthIndex.selected.monthNo,
				// 	week: $scope.tempWeekIndex.selected.week,
				// 	country: $scope.tempCountryIndex.selected.Destination_Ctry,
				// 	region: $scope.tempRegionIndex.selected.Region,
				// };
				reportPerformanceDashboardApiService.getGatewayComboBox(
					$scope.model,
					function (result) {
						// console.log(result);
						if (result.status === true) {
							result.message.forEach(function (entry, index) {
								$scope.listGateway.push(entry);
							});
						} else {
							baseService.showMessage(result.message);
						}
					}
				);
			}
		};

		$scope.getOverSLA = function () {
			$scope.listOverSLA = [
				// { OverSLAlist: "All" },
				{ OverSLAlist: "Ontime" },
				{ OverSLAlist: "Over 1-2 days" },
				{ OverSLAlist: "Over 3-4 days" },
				{ OverSLAlist: "Over 5-6 days" },
				{ OverSLAlist: "Over > 7 days" },
			];
			$scope.tempOverSLAIndex = {};
		};

		$scope.getGarph = function () {
			// $scope.CreateGarphOverviewPerformance();
			// $scope.CreateGarphVolumebyCategory();
			// $scope.CreateGarphDHLSupport();
			$scope.CreateGarphCustomerSupport();
			// $scope.CreateGarphUncontrollable();

			// $scope.CreateGarphVolumebyProduct();
			// $scope.CreateGarphVolumebytradelane();
			// $scope.CreateGarphWeightbyproduct();
			// $scope.CreateGarphOriginCountry();
			// $scope.CreateGarphDestinationCountry();
			// $scope.CreateGarphDHLServiceableArea();
		};

		$scope.CreateGarphOverviewPerformance = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph1 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiOverviewPerformance(
				$scope.model,
				function (results) {
					$scope.listgraph1 = results.message;

					console.log(results.message);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							console.log(results);

							// $scope.data = [300, 50, 100];
							// $scope.labels = ["Red", "Blue", "Yellow"];
							$scope.data = [];
							$scope.labels = [];

							results.message.forEach(function (entry, index) {
								$scope.labels.push(entry.status);
								$scope.data.push(entry.Qty);
							});

							$scope.datasets = [
								{
									backgroundColor: [
										"rgb(91,155,213)",
										"rgb(237,125,49)",
										"rgb(165,165,165)",
									],
									borderColor: "rgb(255, 255, 255)",
									data: $scope.data,
								},
							];
							Chart.pluginService.register({
								beforeDraw: function (chart) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1);",
								},
								layout: { padding: 20 },
								responsive: true,
								legend: { display: true, position: "right" },
								title: {
									display: true,
									text: "Overview Performance",
									fontSize: 20,
								},
								tooltips: {
									enabled: false,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										display: false,
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 14 },
									},
								},
							};
							$scope.OptionGraph = {
								type: "doughnut",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartOverviewPerformance");
							if (window.myBar1) window.myBar1.destroy();
							window.myBar1 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar1) window.myBar1.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphVolumebyCategory = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph2 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiVolumebyCategory(
				$scope.model,
				function (results) {
					$scope.listgraph2 = results.message;

					console.log(results);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							// $scope.labels = ["Jan", "Feb", "Mar"];
							$scope.total = results.message
								.map((item) => parseFloat(item.Qty))
								.reduce((prev, next) => prev + next);
							console.log($scope.total);
							$scope.labels = results.message
								.map((item) => item.Month)
								.filter((value, index, self) => {
									return self.indexOf(value) === index;
								});
							$scope.custumersupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Customer Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));
							console.log($scope.custumersupport);

							$scope.DHLSupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "DHL Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							$scope.Uncontrollable = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Uncontrollable" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							// console.log($scope.labels);
							// console.log(results);

							$scope.datasets = [
								{
									label: "DHL Support",
									backgroundColor: "#5B9BD5",
									data: $scope.DHLSupport,
								},
								{
									label: "Uncontrollable",
									backgroundColor: "#ED7D31",
									data: $scope.Uncontrollable,
								},
								{
									label: "Customer Support",
									backgroundColor: "#A5A5A5",
									data: $scope.custumersupport,
								},
							];
							// Chart.pluginService.register({
							// 	beforeDraw: function (chart) {
							// 		if (
							// 			chart.config.options.chartArea &&
							// 			chart.config.options.chartArea.backgroundColor
							// 		) {
							// 			var ctx = chart.chart.ctx;
							// 			ctx.save();
							// 			ctx.fillStyle =
							// 				chart.config.options.chartArea.backgroundColor;
							// 			ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
							// 			ctx.restore();
							// 		}
							// 	},
							// });
							Chart.pluginService.register({
								beforeDraw: function (chart, easing) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										var chartArea = chart.chartArea;

										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(
											chartArea.left,
											chartArea.top,
											chartArea.right - chartArea.left,
											chartArea.bottom - chartArea.top
										);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1)",
								},
								// chartArea: {
								// 	backgroundColor: "rgb(255, 255, 255);",
								// },
								scales: { yAxes: [{ ticks: { beginAtZero: true } }] },
								responsive: true,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "Volumn by Category",
									fontSize: 20,
								},
								tooltips: {
									enabled: true,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: true,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 16 },
									},
								},
							};
							$scope.OptionGraph = {
								type: "bar",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartVolumebyCategory");
							if (window.myBar2) window.myBar2.destroy();
							window.myBar2 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar2) window.myBar2.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphDHLSupport = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph3 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiDHLSupport(
				$scope.model,
				function (results) {
					$scope.listgraph3 = results.message;

					console.log(results.message);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							console.log(results);

							// $scope.data = [300, 50, 100];
							// $scope.labels = ["Red", "Blue", "Yellow"];
							$scope.data = [];
							$scope.labels = [];

							results.message.forEach(function (entry, index) {
								$scope.labels.push(entry.Reporting_Code_Description);
								$scope.data.push(entry.Qty);
							});

							$scope.datasets = [
								{
									backgroundColor: [
										"rgb(226,170,0)",
										"rgb(226,226,135)",
										"rgb(214,215,214)",
										"rgb(147,197,196)",
										"rgb(219,132,61)",
									],
									borderColor: "rgb(255, 255, 255)",
									data: $scope.data,
								},
							];
							Chart.pluginService.register({
								beforeDraw: function (chart) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1);",
								},
								layout: { padding: 50 },
								responsive: true,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "DHL Support",
									fontSize: 20,
								},
								tooltips: {
									enabled: false,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										display: false,
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 14 },
									},
									// outlabels: {
									// 	display: false,
									// },
								},
							};
							$scope.OptionGraph = {
								type: "doughnut",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartDHLSupport");
							if (window.myBar3) window.myBar3.destroy();
							window.myBar3 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar3) window.myBar3.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphCustomerSupport = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph4 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiCustomerSupport(
				$scope.model,
				function (results) {
					$scope.listgraph4 = results.message;

					// console.log(results.message);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							console.log(results);

							// $scope.data = [300, 50, 100];
							// $scope.labels = ["Red", "Blue", "Yellow"];
							$scope.data = [];
							$scope.labels = [];

							results.message.forEach(function (entry, index) {
								$scope.labels.push(entry.Reporting_Code_Description);
								$scope.data.push(entry.Qty);
							});

							$scope.datasets = [
								{
									backgroundColor: [
										// "rgb(170,0,0)",
										"rgb(226,170,0)",
										"rgb(226,226,135)",
										"rgb(214,215,214)",
										"rgb(147,197,196)",
										"rgb(219,132,61)",
									],
									borderColor: "rgb(255, 255, 255)",
									data: $scope.data,
								},
							];
							Chart.pluginService.register({
								beforeDraw: function (chart) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1);",
								},
								layout: { padding: 20 },
								maintainAspectRatio: false,
								responsive: false,
								// cutoutPercentage: 90,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "Customer Support",
									fontSize: 20,
									padding: 15,
								},
								tooltips: {
									enabled: false,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										display: false,
										color: "#000",
										formatter: (value) => {
											return value.toLocaleString() + "%";
										},
										font: { weight: "bold", size: 14 },
									},
									outlabels: {
										// backgroundColor: "#8C1DFF", // Background color of Label
										// borderColor: "#001BFF", // Border color of Label
										// borderRadius: 17, // Border radius of Label
										// borderWidth: 10, // Thickness of border
										// color: 'white', // Font color
										display: true,
										lineWidth: 1, // Thickness of line between chart arc and Label
										padding: 1,
										stretch: 15, // The length between chart arc and Label
										text: "(%p)",
										textAlign: "center"
									}
									// outlabels: {
									// 	backgroundColor: null,
									// 	display: false,
									// },
								},
							};
							$scope.OptionGraph = {
								type: "doughnut",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartCustomerSupport");
							if (window.myBar4) window.myBar4.destroy();
							window.myBar4 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar4) window.myBar4.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphUncontrollable = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph5 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiUncontrollable(
				$scope.model,
				function (results) {
					$scope.listgraph5 = results.message;

					console.log(results.message);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							console.log(results.message);

							$scope.data = [];
							$scope.labels = [];

							results.message.forEach(function (entry, index) {
								$scope.labels.push(entry.Reporting_Code_Description);
								$scope.data.push(entry.Qty);
							});

							$scope.datasets = [
								{
									backgroundColor: [
										"rgb(226,170,0)",
										"rgb(226,226,135)",
										"rgb(214,215,214)",
										"rgb(147,197,196)",
										"rgb(219,132,61)",
									],
									borderColor: "rgb(255, 255, 255)",
									data: $scope.data,
								},
							];
							Chart.pluginService.register({
								beforeDraw: function (chart) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1);",
								},
								layout: { padding: 20 },
								responsive: true,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "Uncontrollable",
									fontSize: 20,
								},
								tooltips: {
									enabled: false,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										display: false,
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 14 },
									},
								},
							};
							$scope.OptionGraph = {
								type: "doughnut",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartUncontrollable");
							if (window.myBar6) window.myBar6.destroy();
							window.myBar6 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar6) window.myBar6.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphVolumebyProduct = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph6 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiVolumebyCategory(
				$scope.model,
				function (results) {
					$scope.listgraph6 = results.message;

					// console.log(results);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							// $scope.labels = ["Jan", "Feb", "Mar"];
							$scope.total = results.message
								.map((item) => parseFloat(item.Qty))
								.reduce((prev, next) => prev + next);
							// console.log($scope.total);
							$scope.labels = results.message
								.map((item) => item.Month)
								.filter((value, index, self) => {
									return self.indexOf(value) === index;
								});
							$scope.custumersupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Customer Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));
							console.log($scope.custumersupport);

							$scope.DHLSupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "DHL Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							$scope.Uncontrollable = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Uncontrollable" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							// console.log($scope.labels);
							// console.log(results);

							$scope.datasets = [
								{
									label: "DHL Support",
									backgroundColor: "#5B9BD5",
									data: $scope.DHLSupport,
									stack: "Stack 0",
								},
								{
									label: "Uncontrollable",
									backgroundColor: "#ED7D31",
									data: $scope.Uncontrollable,
									stack: "Stack 0",
								},
								{
									label: "Customer Support",
									backgroundColor: "#A5A5A5",
									data: $scope.custumersupport,
									stack: "Stack 0",
								},
							];
							// Chart.pluginService.register({
							// 	beforeDraw: function (chart) {
							// 		if (
							// 			chart.config.options.chartArea &&
							// 			chart.config.options.chartArea.backgroundColor
							// 		) {
							// 			var ctx = chart.chart.ctx;
							// 			ctx.save();
							// 			ctx.fillStyle =
							// 				chart.config.options.chartArea.backgroundColor;
							// 			ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
							// 			ctx.restore();
							// 		}
							// 	},
							// });
							Chart.pluginService.register({
								beforeDraw: function (chart, easing) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										var chartArea = chart.chartArea;

										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(
											chartArea.left,
											chartArea.top,
											chartArea.right - chartArea.left,
											chartArea.bottom - chartArea.top
										);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1)",
								},
								// chartArea: {
								// 	backgroundColor: "rgb(255, 255, 255);",
								// },
								responsive: true,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "Volume by Product",
									fontSize: 20,
								},
								tooltips: {
									enabled: true,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 16 },
									},
								},
							};
							$scope.OptionGraph = {
								type: "bar",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartVolumebyProduct");
							if (window.myBar6) window.myBar6.destroy();
							window.myBar6 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar6) window.myBar6.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphVolumebytradelane = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph7 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiVolumebyCategory(
				$scope.model,
				function (results) {
					$scope.listgraph7 = results.message;

					console.log(results);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							// $scope.labels = ["Jan", "Feb", "Mar"];
							$scope.total = results.message
								.map((item) => parseFloat(item.Qty))
								.reduce((prev, next) => prev + next);
							console.log($scope.total);
							$scope.labels = results.message
								.map((item) => item.Month)
								.filter((value, index, self) => {
									return self.indexOf(value) === index;
								});
							$scope.custumersupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Customer Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));
							console.log($scope.custumersupport);

							$scope.DHLSupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "DHL Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							$scope.Uncontrollable = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Uncontrollable" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							// console.log($scope.labels);
							// console.log(results);

							$scope.datasets = [
								{
									label: "DHL Support",
									backgroundColor: "#5B9BD5",
									data: $scope.DHLSupport,
								},
								{
									label: "Uncontrollable",
									backgroundColor: "#ED7D31",
									data: $scope.Uncontrollable,
								},
								{
									label: "Customer Support",
									backgroundColor: "#A5A5A5",
									data: $scope.custumersupport,
								},
							];
							// Chart.pluginService.register({
							// 	beforeDraw: function (chart) {
							// 		if (
							// 			chart.config.options.chartArea &&
							// 			chart.config.options.chartArea.backgroundColor
							// 		) {
							// 			var ctx = chart.chart.ctx;
							// 			ctx.save();
							// 			ctx.fillStyle =
							// 				chart.config.options.chartArea.backgroundColor;
							// 			ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
							// 			ctx.restore();
							// 		}
							// 	},
							// });
							Chart.pluginService.register({
								beforeDraw: function (chart, easing) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										var chartArea = chart.chartArea;

										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(
											chartArea.left,
											chartArea.top,
											chartArea.right - chartArea.left,
											chartArea.bottom - chartArea.top
										);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1)",
								},
								// chartArea: {
								// 	backgroundColor: "rgb(255, 255, 255);",
								// },
								responsive: true,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "Volume by trade lane",
									fontSize: 20,
								},
								tooltips: {
									enabled: true,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 16 },
									},
								},
							};
							$scope.OptionGraph = {
								type: "bar",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartVolumebytradelane");
							if (window.myBar7) window.myBar7.destroy();
							window.myBar7 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar7) window.myBar7.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphWeightbyproduct = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph8 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiVolumebyCategory(
				$scope.model,
				function (results) {
					$scope.listgraph8 = results.message;

					console.log(results);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							// $scope.labels = ["Jan", "Feb", "Mar"];
							$scope.total = results.message
								.map((item) => parseFloat(item.Qty))
								.reduce((prev, next) => prev + next);
							console.log($scope.total);
							$scope.labels = results.message
								.map((item) => item.Month)
								.filter((value, index, self) => {
									return self.indexOf(value) === index;
								});
							$scope.custumersupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Customer Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));
							console.log($scope.custumersupport);

							$scope.DHLSupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "DHL Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							$scope.Uncontrollable = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Uncontrollable" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							// console.log($scope.labels);
							// console.log(results);

							$scope.datasets = [
								{
									label: "DHL Support",
									backgroundColor: "#5B9BD5",
									data: $scope.DHLSupport,
								},
								{
									label: "Uncontrollable",
									backgroundColor: "#ED7D31",
									data: $scope.Uncontrollable,
								},
								{
									label: "Customer Support",
									backgroundColor: "#A5A5A5",
									data: $scope.custumersupport,
								},
							];
							// Chart.pluginService.register({
							// 	beforeDraw: function (chart) {
							// 		if (
							// 			chart.config.options.chartArea &&
							// 			chart.config.options.chartArea.backgroundColor
							// 		) {
							// 			var ctx = chart.chart.ctx;
							// 			ctx.save();
							// 			ctx.fillStyle =
							// 				chart.config.options.chartArea.backgroundColor;
							// 			ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
							// 			ctx.restore();
							// 		}
							// 	},
							// });
							Chart.pluginService.register({
								beforeDraw: function (chart, easing) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										var chartArea = chart.chartArea;

										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(
											chartArea.left,
											chartArea.top,
											chartArea.right - chartArea.left,
											chartArea.bottom - chartArea.top
										);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1)",
								},
								// chartArea: {
								// 	backgroundColor: "rgb(255, 255, 255);",
								// },
								responsive: true,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "Weight by product",
									fontSize: 20,
								},
								tooltips: {
									enabled: true,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 16 },
									},
								},
							};
							$scope.OptionGraph = {
								type: "bar",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartWeightbyproduct");
							if (window.myBar8) window.myBar8.destroy();
							window.myBar8 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar8) window.myBar8.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphOriginCountry = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph9 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiOriginCountry(
				$scope.model,
				function (results) {
					$scope.listgraph9 = results.message;

					console.log(results.message);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							console.log(results.message);

							$scope.data = [];
							$scope.labels = [];

							results.message.forEach(function (entry, index) {
								if (entry.Region != null) {
									$scope.labels.push(entry.Region);
								}
								if (entry.Qty > 0 && entry.Qty != null) {
									$scope.data.push(entry.Qty);
								}
							});

							$scope.datasets = [
								{
									backgroundColor: [
										"rgb(226,170,0)",
										"rgb(226,226,135)",
										"rgb(214,215,214)",
										"rgb(147,197,196)",
										"rgb(219,132,61)",
									],
									borderColor: "rgb(255, 255, 255)",
									data: $scope.data,
								},
							];
							Chart.pluginService.register({
								beforeDraw: function (chart) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1);",
								},
								layout: { padding: 50 },
								responsive: true,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "Origin Country",
									fontSize: 20,
								},
								tooltips: {
									enabled: false,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										display: false,
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 14 },
									},
								},
							};
							$scope.OptionGraph = {
								type: "doughnut",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartOriginCountry");
							if (window.myBar10) window.myBar10.destroy();
							window.myBar10 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar10) window.myBar10.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphDestinationCountry = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph10 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiDestinationCountry(
				$scope.model,
				function (results) {
					$scope.listgraph10 = results.message;

					// console.log(results.message);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							// console.log(results.message);

							$scope.data = [];
							$scope.labels = [];

							results.message.forEach(function (entry, index) {
								if (entry.Destination_Ctry != null) {
									$scope.labels.push(entry.Destination_Ctry);
								}
								if (entry.Qty > 0 && entry.Qty != null) {
									$scope.data.push(entry.Qty);
								}
							});

							$scope.datasets = [
								{
									backgroundColor: [
										"rgb(226,170,0)",
										"rgb(226,226,135)",
										"rgb(214,215,214)",
										"rgb(147,197,196)",
										"rgb(219,132,61)",
									],
									borderColor: "rgb(255, 255, 255)",
									data: $scope.data,
								},
							];
							Chart.pluginService.register({
								beforeDraw: function (chart) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1);",
								},
								layout: {
									padding: {
										left: 50,
									},
								},
								// cutoutPercentage: 90,
								responsive: true,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "Destination Country",
									fontSize: 20,
								},
								tooltips: {
									enabled: false,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										display: false,
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 14 },
									},
								},
							};
							$scope.OptionGraph = {
								type: "doughnut",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartDestinationCountry");
							if (window.myBar11) window.myBar11.destroy();
							window.myBar11 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar11) window.myBar11.destroy();
						}
					}
				}
			);
		};

		$scope.CreateGarphDHLServiceableArea = function () {
			$scope.model = {
				month: "",
				week: "",
				country: "",
				region: "",
				gateway: "",
				oversla: "",
			};
			$scope.listgraph11 = [];

			$scope.txtmonth = "";
			if (
				$scope.tempMonthIndex.selected != undefined &&
				$scope.tempMonthIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempMonthIndex.selected.length; i++) {
					if (i == $scope.tempMonthIndex.selected.length - 1) {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "'";
					} else {
						$scope.txtmonth +=
							"'" + $scope.tempMonthIndex.selected[i]["monthname"] + "',";
					}
				}
				// $scope.model = { month: "(" + $scope.txtmonth + ")" };
			}

			$scope.txtweek = "";
			if (
				$scope.tempWeekIndex.selected != undefined &&
				$scope.tempWeekIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempWeekIndex.selected.length; i++) {
					if (i == $scope.tempWeekIndex.selected.length - 1) {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "'";
					} else {
						$scope.txtweek +=
							"'" + $scope.tempWeekIndex.selected[i]["week"] + "',";
					}
				}
			}

			$scope.txtcountry = "";
			if (
				$scope.tempCountryIndex.selected != undefined &&
				$scope.tempCountryIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempCountryIndex.selected.length; i++) {
					if (i == $scope.tempCountryIndex.selected.length - 1) {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"'";
					} else {
						$scope.txtcountry +=
							"'" +
							$scope.tempCountryIndex.selected[i]["Destination_Ctry"] +
							"',";
					}
				}
			}

			$scope.txtregion = "";
			if (
				$scope.tempRegionIndex.selected != undefined &&
				$scope.tempRegionIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempRegionIndex.selected.length; i++) {
					if (i == $scope.tempRegionIndex.selected.length - 1) {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "'";
					} else {
						$scope.txtregion +=
							"'" + $scope.tempRegionIndex.selected[i]["Region"] + "',";
					}
				}
			}

			$scope.txtgateway = "";
			if (
				$scope.tempGatewayIndex.selected != undefined &&
				$scope.tempGatewayIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempGatewayIndex.selected.length; i++) {
					if (i == $scope.tempGatewayIndex.selected.length - 1) {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "'";
					} else {
						$scope.txtgateway +=
							"'" + $scope.tempGatewayIndex.selected[i]["Gateway"] + "',";
					}
				}
			}

			$scope.txtoversla = "";
			if (
				$scope.tempOverSLAIndex.selected != undefined &&
				$scope.tempOverSLAIndex.selected != ""
			) {
				for (let i = 0; i < $scope.tempOverSLAIndex.selected.length; i++) {
					if (i == $scope.tempOverSLAIndex.selected.length - 1) {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "'";
					} else {
						$scope.txtoversla +=
							"'" + $scope.tempOverSLAIndex.selected[i]["OverSLAlist"] + "',";
					}
				}
			}

			$scope.model = {
				month: $scope.txtmonth != "" ? "(" + $scope.txtmonth + ")" : "",
				week: $scope.txtweek != "" ? "(" + $scope.txtweek + ")" : "",
				country: $scope.txtcountry != "" ? "(" + $scope.txtcountry + ")" : "",
				region: $scope.txtregion != "" ? "(" + $scope.txtregion + ")" : "",
				gateway: $scope.txtgateway != "" ? "(" + $scope.txtgateway + ")" : "",
				oversla: $scope.txtoversla != "" ? "(" + $scope.txtoversla + ")" : "",
			};
			reportPerformanceDashboardApiService.getApiVolumebyCategory(
				$scope.model,
				function (results) {
					$scope.listgraph11 = results.message;

					console.log(results);
					if (results.message != undefined) {
						if (results.message.length > 0) {
							// $scope.labels = ["Jan", "Feb", "Mar"];
							$scope.total = results.message
								.map((item) => parseFloat(item.Qty))
								.reduce((prev, next) => prev + next);
							console.log($scope.total);
							$scope.labels = results.message
								.map((item) => item.Month)
								.filter((value, index, self) => {
									return self.indexOf(value) === index;
								});
							$scope.custumersupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Customer Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));
							console.log($scope.custumersupport);

							$scope.DHLSupport = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "DHL Support" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							$scope.Uncontrollable = results.message
								.filter((value, index, self) => {
									// console.log(value);
									if (
										value.Reporting_Code_Category == "Uncontrollable" &&
										$scope.labels.includes(value.Month)
									) {
										return true;
									}
									return false;
								})
								.map((item) => ((item.Qty / $scope.total) * 100).toFixed(0));

							// console.log($scope.labels);
							// console.log(results);

							$scope.datasets = [
								{
									label: "DHL Support",
									backgroundColor: "#5B9BD5",
									data: $scope.DHLSupport,
								},
								{
									label: "Uncontrollable",
									backgroundColor: "#ED7D31",
									data: $scope.Uncontrollable,
								},
								{
									label: "Customer Support",
									backgroundColor: "#A5A5A5",
									data: $scope.custumersupport,
								},
							];
							// Chart.pluginService.register({
							// 	beforeDraw: function (chart) {
							// 		if (
							// 			chart.config.options.chartArea &&
							// 			chart.config.options.chartArea.backgroundColor
							// 		) {
							// 			var ctx = chart.chart.ctx;
							// 			ctx.save();
							// 			ctx.fillStyle =
							// 				chart.config.options.chartArea.backgroundColor;
							// 			ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
							// 			ctx.restore();
							// 		}
							// 	},
							// });
							Chart.pluginService.register({
								beforeDraw: function (chart, easing) {
									if (
										chart.config.options.chartArea &&
										chart.config.options.chartArea.backgroundColor
									) {
										var ctx = chart.chart.ctx;
										var chartArea = chart.chartArea;

										ctx.save();
										ctx.fillStyle =
											chart.config.options.chartArea.backgroundColor;
										ctx.fillRect(
											chartArea.left,
											chartArea.top,
											chartArea.right - chartArea.left,
											chartArea.bottom - chartArea.top
										);
										ctx.restore();
									}
								},
							});
							$scope.options = {
								chartArea: {
									backgroundColor: "rgba(255, 255, 255, 1)",
								},
								// chartArea: {
								// 	backgroundColor: "rgb(255, 255, 255);",
								// },
								responsive: true,
								legend: { display: true, position: "bottom" },
								title: {
									display: true,
									text: "DHL SERVICEABLE AREA",
									fontSize: 20,
								},
								tooltips: {
									enabled: true,
									mode: "nearest",
									intersect: false,
									callbacks: {
										title: function (tooltipItem, data) {
											return data["labels"][tooltipItem[0]["index"]];
										},
										label: function (tooltipItem, data) {
											return (
												data["datasets"][0]["data"][tooltipItem["index"]] + " %"
											);
										},
									},
								},
								scales: {
									yAxes: [
										{
											position: "left",
											display: false,
											ticks: {
												min: 0,
											},
										},
									],
								},
								plugins: {
									datalabels: {
										color: "#FFFFFF",
										formatter: (value) => {
											return value.toLocaleString() + " %";
										},
										font: { weight: "bold", size: 16 },
									},
								},
							};
							$scope.OptionGraph = {
								type: "bar",
								data: {
									labels: $scope.labels,
									datasets: $scope.datasets,
								},
								options: $scope.options,
							};
							var ctx = document.getElementById("myChartDHLServiceableArea");
							if (window.myBar11) window.myBar11.destroy();
							window.myBar11 = new Chart(ctx, $scope.OptionGraph);
						} else {
							if (window.myBar11) window.myBar11.destroy();
						}
					}
				}
			);
		};

		$scope.reload = function () {
			$scope.getMonth();
		};

		$scope.Exportreport = function () {
			// $scope.listgraph1.length = []
			if (
				$scope.listgraph1.length > 0 &&
				$scope.listgraph2.length > 0 &&
				$scope.listgraph3.length > 0 &&
				$scope.listgraph4.length > 0 &&
				$scope.listgraph5.length > 0 
			) {
				var canvas1 = document.querySelector("#myChartOverviewPerformance");
				var canvas2 = document.querySelector("#myChartVolumebyCategory");
				var canvas3 = document.querySelector("#myChartDHLSupport");
				var canvas4 = document.querySelector("#myChartCustomerSupport");
				var canvas5 = document.querySelector("#myChartUncontrollable");

				var img_b64_1 = canvas1.toDataURL("image/png");
				var img_b64_2 = canvas2.toDataURL("image/png");
				var img_b64_3 = canvas3.toDataURL("image/png");
				var img_b64_4 = canvas4.toDataURL("image/png");
				var img_b64_5 = canvas5.toDataURL("image/png");

				reportPerformanceDashboardApiService.ExportGraphPdfPerformanceDashboard(
					{
						image1: img_b64_1,
						image2: img_b64_2,
						image3: img_b64_3,
						image4: img_b64_4,
						image5: img_b64_5,
					},
					function (result) {
						console.log(result);
						if (result.status == true) {
							var download_url = get_base_url("");
							var file = download_url + "upload/Graph/" + result.message;
							window.open(file);
						} else {
							baseService.showMessage(result.message);
						}
					}
				);
			} else {
				baseService.showMessage("No data");
			}
		};
	},
]);

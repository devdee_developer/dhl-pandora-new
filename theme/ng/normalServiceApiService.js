'use strict';
myApp.factory('normalServiceApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("NormalService/");
    return {
          
        //normalService 
	    listnormalService: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getNormalServiceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getnormalService: function (model, onComplete) {
            baseService.postObject(servicebase + 'getNormalServiceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savenormalService: function (model, onComplete) {
            baseService.postObject(servicebase + 'addNormalService', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletenormalService: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteNormalService', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getNormalServiceComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
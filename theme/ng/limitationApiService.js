'use strict';
myApp.factory('limitationApiService', ['$http', '$resource', 'baseService', 
function ($http, $resource, baseService) {
    var servicebase = get_base_url("Limitation/");
    return {
        listlimitation: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getlimitationModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        checklist: function (model, onComplete) {
            baseService.postObject(servicebase + 'checklist', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        downloadlimitation: function (model, onComplete) {
            baseService.postObject(servicebase + 'downloadlimitation', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);
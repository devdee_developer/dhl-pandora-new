'use strict';
myApp.factory('thaiPostApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("ThaiPost/");
    return {
          
        //thaiPost 
	    listthaiPost: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getThaiPostModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getthaiPost: function (model, onComplete) {
            baseService.postObject(servicebase + 'getThaiPostModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savethaiPost: function (model, onComplete) {
            baseService.postObject(servicebase + 'addThaiPost', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletethaiPost: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteThaiPost', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getThaiPostComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
'use strict';
myApp.factory('reportThreeApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("ReportThree/");
    return {

        //Admin 
        // listCodeAppleAndDhl: function (model, onComplete) {
        //     baseService.searchObject(servicebase + 'getCodeAppleAndDhlModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },

        // getPercentStatus: function (model, onComplete) {
        //     baseService.searchObject(servicebase + 'getPercentStatus', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },

        // Exportexcel: function (model, onComplete) { 
        //     baseService.postObject(servicebase + 'Exportexcel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },

		getOntimeChart: function (model, onComplete) {
            baseService.postObject(servicebase + 'getOntimeChart', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        Exportreport: function (model, onComplete) {
            baseService.postObject(servicebase + 'Exportreport', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        // getComboBox: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getQuaterComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        
		getYearComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getYearComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        
        // ExportDowloadAllShipment: function (model, onComplete) { 
        //     baseService.postObject(servicebase + 'ExportDowloadAllShipment', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        // getCodeAppleAndDhl: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getCodeAppleAndDhlModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        // savecodeAppleAndDhl: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'addCodeAppleAndDhl', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        // deleteCodeAppleAndDhl: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'deleteCodeAppleAndDhl', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        // getComboBox: function (model, onComplete) {
        //     baseService.searchObject(servicebase + 'getCodeAppleAndDhlComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },

    };
}]);
'use strict';
myApp.factory('customerRequestApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("CustomerRequest/");
    return {
          
        //CustomerRequest 
	    listCustomerRequest: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCustomerRequestModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getCustomerRequest: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCustomerRequestModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveCustomerRequest: function (model, onComplete) {
            baseService.postObject(servicebase + 'addCustomerRequest', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteCustomerRequest: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteCustomerRequest', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCustomerRequestComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
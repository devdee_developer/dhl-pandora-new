'use strict';
myApp.factory('nimPriceApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("nimPrice/");
    return {
          
        //nimPrice 
	    listnimPrice: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getNimPriceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getnimPrice: function (model, onComplete) {
            baseService.postObject(servicebase + 'getNimPriceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savenimPrice: function (model, onComplete) {
            baseService.postObject(servicebase + 'addNimPrice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletenimPrice: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteNimPrice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getNimPriceComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
'use strict';
myApp.factory('bankApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Bank/");
    return {
          
        //Bank 
	    listbank: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getBankModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getbank: function (model, onComplete) {
            baseService.postObject(servicebase + 'getBankModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savebank: function (model, onComplete) {
            baseService.postObject(servicebase + 'addBank', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletebank: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteBank', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getBankComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
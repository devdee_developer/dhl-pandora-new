'use strict';
myApp.factory('companyApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Company/");
    return {
          
		getcompany: function (model, onComplete) {
            baseService.postObject(servicebase + 'getcompanyModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveCompany: function (model, onComplete) {
            baseService.postObject(servicebase + 'saveCompany', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }
        
    };
}]);
'use strict';
myApp.factory('getshipmentmodelliststatusApiService', ['$http', '$resource', 'baseService', 
function ($http, $resource, baseService) {
    var servicebase = get_base_url("Appleapi/");
    return {
        getshipmentmodelliststatus: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getshipmentmodelliststatus', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        shipmentnotcheck: function (model, onComplete) {
            baseService.searchObject(servicebase + 'shipmentnotcheck', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);
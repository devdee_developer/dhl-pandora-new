'use strict';
myApp.factory('checkpointApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("checkpoint/");
    return {
          
        //agent 
	    ListCheckpoint: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCheckpointList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		// getagent: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        SaveCheckpoint: function (model, onComplete) {
            baseService.postObject(servicebase + 'SaveCheckpoint', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        DeleteCheckpoint: function (model, onComplete) {
            baseService.postObject(servicebase + 'DeleteCheckpoint', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        Exportexcel: function (model, onComplete) {
            baseService.postObject(servicebase + 'Exportexcel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
		// getComboBox: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        
    };
}]);
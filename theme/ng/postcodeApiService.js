'use strict';
myApp.factory('postcodeApiService', ['$http', '$resource', 'baseService', 
function ($http, $resource, baseService) {
    var servicebase = get_base_url("Postcode/");
    return {
        listpostcode: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getpostcodeModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        checklist: function (model, onComplete) {
            baseService.postObject(servicebase + 'checklist', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        downloadpostcode: function (model, onComplete) {
            baseService.postObject(servicebase + 'downloadpostcode', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);
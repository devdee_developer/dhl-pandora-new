'use strict';
myApp.factory('nimZoneApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("nimZone/");
    return {
          
        //nimZone 
	    listnimZone: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getNimZoneModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getnimZone: function (model, onComplete) {
            baseService.postObject(servicebase + 'getNimZoneModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savenimZone: function (model, onComplete) {
            baseService.postObject(servicebase + 'addNimZone', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletenimZone: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteNimZone', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getNimZoneComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
'use strict';
myApp.factory('pepZoneApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("PepZone/");
    return {
          
        //PepZone 
	    listpepZone: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getPepZoneModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getpepZone: function (model, onComplete) {
            baseService.postObject(servicebase + 'getPepZoneModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savepepZone: function (model, onComplete) {
            baseService.postObject(servicebase + 'addPepZone', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletepepZone: function (model, onComplete) {
            baseService.postObject(servicebase + 'deletePepZone', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getPepZoneComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
'use strict';
myApp.factory('reportApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Report/");
	var servicebase2 = get_base_url("ReportSummary/");
    return {
          
        //Report 
	    listReport: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getReportModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		listReportSummary: function (model, onComplete) {
            baseService.searchObject(servicebase2 + 'getReportModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
	    listAppleReport: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getAppleReportModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		listReportCheckPoint: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getReportCheckPoinModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		// getReport: function (model, onComplete) {
            // baseService.postObject(servicebase + 'getReportModel', model, function (result) {
                // if (undefined != onComplete) onComplete.call(this, result);
            // });
        // },
        // saveReport: function (model, onComplete) {
            // baseService.postObject(servicebase + 'addReport', model, function (result) {
                // if (undefined != onComplete) onComplete.call(this, result);
            // });
        // },
        // deleteReport: function (model, onComplete) {
            // baseService.postObject(servicebase + 'deleteReport', model, function (result) {
                // if (undefined != onComplete) onComplete.call(this, result);
            // });
        // },
		// getComboBox: function (model, onComplete) {
            // baseService.postObject(servicebase + 'getReportComboList', model, function (result) {
                // if (undefined != onComplete) onComplete.call(this, result);
            // });
        // },
        
    };
}]);
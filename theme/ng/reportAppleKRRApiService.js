'use strict';
myApp.factory('reportAppleKRRApiService', ['$http', '$resource', 'baseService', 
function ($http, $resource, baseService) {
    var servicebase = get_base_url("ReportAppleKRR/");
    return {
        downloadAppleKRR: function (model, onComplete) {
            baseService.postObject(servicebase + 'downloadAppleKRR', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        downloadAppleKRRByApi: function (model, onComplete) {
            baseService.postObject(servicebase + 'downloadAppleKRRByApi', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getPidByApi: function (model, onComplete) {
            baseService.postObject(get_base_url("ApiAppleshipment/")+ 'insertToTemp', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
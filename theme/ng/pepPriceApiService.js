'use strict';
myApp.factory('pepPriceApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("PepPrice/");
    return {
          
        //pepPrice 
	    listpepPrice: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getPepPriceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getpepPrice: function (model, onComplete) {
            baseService.postObject(servicebase + 'getPepPriceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savepepPrice: function (model, onComplete) {
            baseService.postObject(servicebase + 'addPepPrice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletepepPrice: function (model, onComplete) {
            baseService.postObject(servicebase + 'deletePepPrice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getPepPriceComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
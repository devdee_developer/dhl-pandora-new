'use strict';
myApp.factory('reportPettyCashWHTCertificateApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("ReportPettyCashWHTCertificate/");
    return {
          
        //user 
	    listCustomer: function (model, onComplete) {
            baseService.postObject(servicebase + 'getuserModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },    
    };
}]);
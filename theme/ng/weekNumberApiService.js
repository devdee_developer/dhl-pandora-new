'use strict';
myApp.factory('weekNumberApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("weekNumber/");
    return {
          
        //agent 
	    ListWeekNumber: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getWeekNumberList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		// getagent: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        SaveWeekNumber: function (model, onComplete) {
            baseService.postObject(servicebase + 'SaveWeekNumber', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        DeleteWeekNumber: function (model, onComplete) {
            baseService.postObject(servicebase + 'DeleteWeekNumber', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        Exportexcel: function (model, onComplete) {
            baseService.postObject(servicebase + 'Exportexcel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
		// getComboBox: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        
    };
}]);
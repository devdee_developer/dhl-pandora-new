'use strict';
myApp.factory('slaSvcAreaApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("slaSvcArea/");
    return {
          
        //agent 
	    ListSlaSvcArea: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getSlaSvcAreaList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		// getagent: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        SaveSlaSvcArea: function (model, onComplete) {
            baseService.postObject(servicebase + 'SaveSlaSvcArea', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        DeleteSlaSvcArea: function (model, onComplete) {
            baseService.postObject(servicebase + 'DeleteSlaSvcArea', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        Exportexcel: function (model, onComplete) {
            baseService.postObject(servicebase + 'Exportexcel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
		// getComboBox: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        
    };
}]);
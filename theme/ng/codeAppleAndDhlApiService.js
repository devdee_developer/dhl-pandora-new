'use strict';
myApp.factory('codeAppleAndDhlApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("CodeAppleAndDhl/");
    return {

        //Admin 
        listCodeAppleAndDhl: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCodeAppleAndDhlModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        // getCodeAppleAndDhl: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getCodeAppleAndDhlModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        savecodeAppleAndDhl: function (model, onComplete) {
            baseService.postObject(servicebase + 'addCodeAppleAndDhl', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteCodeAppleAndDhl: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteCodeAppleAndDhl', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCodeAppleAndDhlComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

    };
}]);
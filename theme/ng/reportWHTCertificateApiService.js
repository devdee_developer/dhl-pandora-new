'use strict';
myApp.factory('reportWHTCertificateApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("ReportWHTCertificate/");
    return {
        listCustomer: function (model, onComplete) {
            baseService.postObject(servicebase + 'getuserModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
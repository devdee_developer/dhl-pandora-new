'use strict';
myApp.factory('girthApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Girth/");
    return {
          
        //girth 
	    listgirth: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getGirthModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getgirth: function (model, onComplete) {
            baseService.postObject(servicebase + 'getGirthModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savegirth: function (model, onComplete) {
            baseService.postObject(servicebase + 'addGirth', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletegirth: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteGirth', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getGirthComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
'use strict';
myApp.factory('slaAuNzApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("slaAuNz/");
    return {
          
        //agent 
	    ListSlaAuNz: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getSlaAuNzList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		// getagent: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        SaveSlaAuNz: function (model, onComplete) {
            baseService.postObject(servicebase + 'SaveSlaAuNz', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        DeleteSlaAuNz: function (model, onComplete) {
            baseService.postObject(servicebase + 'DeleteSlaAuNz', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        Exportexcel: function (model, onComplete) {
            baseService.postObject(servicebase + 'Exportexcel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
		// getComboBox: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        
    };
}]);
'use strict';
myApp.factory('nimBoxApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("NimBox/");
    return {
          
        //nimBox 
	    listnimBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getnimBoxModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getnimBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getnimBoxModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savenimBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'addnimBox', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletenimBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'deletenimBox', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getnimBoxComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
'use strict';
myApp.factory('kerryPriceApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("KerryPrice/");
    return {
          
        //kerryPrice 
	    listkerryPrice: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getKerryPriceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getkerryPrice: function (model, onComplete) {
            baseService.postObject(servicebase + 'getKerryPriceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savekerryPrice: function (model, onComplete) {
            baseService.postObject(servicebase + 'addKerryPrice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletekerryPrice: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteKerryPrice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getKerryPriceComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
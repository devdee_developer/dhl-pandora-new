'use strict';
myApp.factory('reportPerformanceDashboardApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("ReportPerformanceDashboard/");
    return {

        //Admin 
        // listCodeAppleAndDhl: function (model, onComplete) {
        //     baseService.searchObject(servicebase + 'getCodeAppleAndDhlModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },

        // getPercentStatus: function (model, onComplete) {
        //     baseService.searchObject(servicebase + 'getPercentStatus', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },

        // Exportexcel: function (model, onComplete) { 
        //     baseService.postObject(servicebase + 'Exportexcel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },

		getProductChart: function (model, onComplete) {
            baseService.postObject(servicebase + 'getProductChart', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        Exportreport: function (model, onComplete) {
            baseService.postObject(servicebase + 'Exportreport', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        ExportGraphPdfPerformanceDashboard: function (model, onComplete) {
            baseService.postObject(servicebase + 'ExportGraphPdfPerformanceDashboard', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        // getComboBox: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getQuaterComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        
		getYearComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getYearComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getApiOverviewPerformance: function (model, onComplete) {
            baseService.postObject(servicebase + 'getOverviewPerformance', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getApiVolumebyCategory: function (model, onComplete) {
            baseService.postObject(servicebase + 'getVolumebyCategory', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getApiDHLSupport: function (model, onComplete) {
            baseService.postObject(servicebase + 'getDHLSupport', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getApiCustomerSupport: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCustomerSupport', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getApiUncontrollable: function (model, onComplete) {
            baseService.postObject(servicebase + 'getUncontrollable', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getApiWeightbyproduct: function (model, onComplete) {
            baseService.postObject(servicebase + 'getWeightbyproduct', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getApiOriginCountry: function (model, onComplete) {
            baseService.postObject(servicebase + 'getOriginCountry', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getApiDestinationCountry: function (model, onComplete) {
            baseService.postObject(servicebase + 'getDestinationCountry', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getMonthComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getMonthComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getWeekComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getWeekComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getCountryComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCountryComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getRegionComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getRegionComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getGatewayComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getGatewayComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getOverSLAComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getOverSLAComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        backup: function (model, onComplete) {
            baseService.postObject(servicebase + 'getShipmentData', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
        // ExportDowloadAllShipment: function (model, onComplete) { 
        //     baseService.postObject(servicebase + 'ExportDowloadAllShipment', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        // getCodeAppleAndDhl: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getCodeAppleAndDhlModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        // savecodeAppleAndDhl: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'addCodeAppleAndDhl', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        // deleteCodeAppleAndDhl: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'deleteCodeAppleAndDhl', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        // getComboBox: function (model, onComplete) {
        //     baseService.searchObject(servicebase + 'getCodeAppleAndDhlComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },

    };
}]);
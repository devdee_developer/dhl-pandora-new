'use strict';
myApp.factory('transectionApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("transection/");
    return {
          
        //transection 
	    listtransection: function (model, onComplete) {
            baseService.searchObject(servicebase + 'gettransectionModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		gettransection: function (model, onComplete) {
            baseService.postObject(servicebase + 'gettransectionModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savetransection: function (model, onComplete) {
            baseService.postObject(servicebase + 'addtransection', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletetransection: function (model, onComplete) {
            baseService.postObject(servicebase + 'deletetransection', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'gettransectionComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
'use strict';
myApp.factory('slaZipcodeApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("slaZipcode/");
    return {
          
        //agent 
	    ListSlaZipcode: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getSlaZipcodeList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		// getagent: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        SaveSlaZipcode: function (model, onComplete) {
            baseService.postObject(servicebase + 'SaveSlaZipcode', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        DeleteSlaZipcode: function (model, onComplete) {
            baseService.postObject(servicebase + 'DeleteSlaZipcode', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        Exportexcel: function (model, onComplete) {
            baseService.postObject(servicebase + 'Exportexcel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
		// getComboBox: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getAgentComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        
    };
}]);
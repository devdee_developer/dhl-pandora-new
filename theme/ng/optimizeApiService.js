'use strict';
myApp.factory('optimizeApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Optimize/");
    return {
          
        //optimize 
	    listoptimize: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getOptimizeModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getoptimize: function (model, onComplete) {
            baseService.postObject(servicebase + 'getOptimizeModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveoptimize: function (model, onComplete) {
            baseService.postObject(servicebase + 'addOptimize', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteoptimize: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteOptimize', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getOptimizeComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		findoptimize: function (model, onComplete) {
            baseService.postObject(servicebase + 'findOptimize', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
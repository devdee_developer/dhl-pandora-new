'use strict';
myApp.factory('emsApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Ems/");
    return {
          
        //ems 
	    listems: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getEmsModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getems: function (model, onComplete) {
            baseService.postObject(servicebase + 'getEmsModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveems: function (model, onComplete) {
            baseService.postObject(servicebase + 'addEms', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteems: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteEms', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getEmsComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
'use strict';
myApp.factory('servicecenterApiService', ['$http', '$resource', 'baseService', 
function ($http, $resource, baseService) {
    var servicebase = get_base_url("Servicecenter/");
    return {
        listservicecenter: function (model, onComplete) {
            baseService.postObject(servicebase + 'getservicecenterModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveservicecenter: function (model, onComplete) {
            baseService.postObject(servicebase + 'addservicecenter', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteservicecenter: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteservicecenter', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);
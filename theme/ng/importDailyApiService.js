'use strict';
myApp.factory('importDailyApiService', ['$http', '$resource', 'baseService', 
function ($http, $resource, baseService) {
    var servicebase = get_base_url("ImportDaily/");
    return {
        listdaily: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getdailyModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        // downloadshipment: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'downloadshipment', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // }, 
        // downloadapple: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'downloadapple', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // }, 
        deleteImport: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteImport', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }, 
        
        Exportexcel: function (model, onComplete) {
            baseService.postObject(servicebase + 'Exportexcel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

    };
}]);
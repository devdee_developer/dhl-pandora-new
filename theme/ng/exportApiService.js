'use strict';
myApp.factory('exportApiService', ['$http', '$resource', 'baseService', 
function ($http, $resource, baseService) {
    var servicebase = get_base_url("Export/");
    return {
        // listdaily: function (model, onComplete) {
        //     baseService.searchObject(servicebase + 'getdailyModelList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        // downloadshipment: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'downloadshipment', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // }, 
        // downloadapple: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'downloadapple', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // }, 
        // deleteExport: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'deleteExport', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        Exportexcel: function (model, onComplete) {
            baseService.postObject(servicebase + 'Exportexcel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        ExportexcelMonthly: function (model, onComplete) {
            baseService.postObject(servicebase + 'ExportexcelMonthly', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        ExportexcelDaily: function (model, onComplete) {
            baseService.postObject(servicebase + 'ExportexcelDaily', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        ExportexcelStatus: function (model, onComplete) {
            baseService.postObject(servicebase + 'ExportexcelStatus', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        ExportWaitCheckpoint: function (model, onComplete) {
            baseService.postObject(servicebase + 'ExportWaitCheckpoint', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getWeekComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
		getYearComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getYearComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

        getMonthComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getMonthComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

    };
}]);
'use strict';
myApp.factory('appleapiApiService', ['$http', '$resource', 'baseService', 
function ($http, $resource, baseService) {
    var servicebase = get_base_url("Appleapi/");
    return {
        searchappleapi: function (model, onComplete) {
            baseService.postObject(servicebase + 'getappleapiModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getshipmentmodelliststatus: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getshipmentmodelliststatus', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        shipmentnotcheck: function (model, onComplete) {
            baseService.searchObject(servicebase + 'shipmentnotcheck', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        reportshipmentkerry: function (model, onComplete) {
            baseService.postObject(servicebase + 'reportshipmentkerry', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        showreportkerry: function (model, onComplete) {
            baseService.searchObject(servicebase + 'showreportkerry', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        shipmenreporttKRR: function (model, onComplete) {
            baseService.searchObject(servicebase + 'showreportShipmentkrr', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);
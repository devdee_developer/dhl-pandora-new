'use strict';
myApp.factory('reportSAPWHTCertificateApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("ReportSAPWHTCertificate/");
    return {

        listCustomer3: function (model, onComplete) {
            baseService.postObject(servicebase + 'getuserModel3', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        listCustomer53: function (model, onComplete) {
            baseService.postObject(servicebase + 'getuserModel53', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        resetLineNumber: function (model, onComplete) {
            baseService.postObject(servicebase + 'resetLineNumber', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);
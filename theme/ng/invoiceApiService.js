'use strict';
myApp.factory('invoiceApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Invoice/");
    return {
          
        //Invoice 
	    listInvoice: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getInvoiceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getInvoice: function (model, onComplete) {
            baseService.postObject(servicebase + 'getInvoiceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveInvoice: function (model, onComplete) {
            baseService.postObject(servicebase + 'addInvoice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteInvoice: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteInvoice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getInvoiceComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
'use strict';
myApp.factory('custodianApiService', ['$http', '$resource', 'baseService', 
function ($http, $resource, baseService) {
    var servicebase = get_base_url("custodian/");
    return {
        listdaily: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getdailyModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        downloadshipment: function (model, onComplete) {
            baseService.postObject(servicebase + 'downloadshipment', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }, 
        downloadapple: function (model, onComplete) {
            baseService.postObject(servicebase + 'downloadapple', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }, 
    };
}]);
<?php

class MY_Controller extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      error_reporting(0);
      date_default_timezone_set('Asia/Bangkok');
      if (!$this->session->userdata('validated')) {

         //sitetest//
         // redirect('https://th-logis.com/sitetest/dhl_sso/Login');

         //host//
         // redirect('https://th-logis.com/dhl_sso/Login');

         //local//
         redirect('http://localhost/dhl_sso/Login');
      }
      $this->session->mark_as_temp('validated', 1800);
      // $this->session->mark_as_temp('validated', 20);
   }

   public function HelloWorld()
   {

      return "Hello World";
   }
}

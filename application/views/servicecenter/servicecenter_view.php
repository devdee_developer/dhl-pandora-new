<script src="<?php echo base_url('asset/servicecenterController.js');?>"></script>
<div  ng-controller="servicecenterController" ng-init="onInit()">
 
    <div class="row">
        <ul class="navigator">
            <li class="nav_active"> <?php echo $this->lang->line('Service Center');?></li>
        </ul>
    </div>

    <div class="row" >
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $this->lang->line('Service Center');?></h1>
		</div>
    </div>

    <div class="row addDevice" style="display:none;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo $this->lang->line('Service Center');?>
			</div>
			<div class="panel-body"> 
				<div class="col-lg-10 col-md-10 col-xs-10">
					<div role="form">
						<div class="form-group col-lg-12 col-md-12 col-xs-12"> 
                            <div class="col-lg-3 col-md-3 col-xs-3">
                            </div>  
							<div class="col-lg-3 col-md-3 col-xs-3">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('Service Center');?></label>
								<input class ="form-control" ng-model="CreateModel.servicecenter" >
							</div>
						</div>
                        <div class="form-group text-right">
							<br/><br/>
							<div class="col-lg-10 col-md-10 col-xs-10">
								<button class="btn btn-primary"  ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
								<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
							</div>
						</div>
					</div>  
				</div>
			</div>
		</div>
    </div>

	<div class="row DisplayDevice" >
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('Service Center');?>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-10 col-md-10 col-xs-10">
							<div role="form">
                                <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    <div class="col-lg-3 col-md-3 col-xs-3">
                                    </div>
									<div class="col-lg-1 col-md-1 col-xs-12">
										<label><span class="text-danger" ></span><?php echo $this->lang->line('Browse File');?></label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										{{file.name}}
									</div>
									<div class="col-md-1 col-sm-1 col-xs-12" ngf-select ng-model="file" name="file" ngf-pattern="'.xls,.xlsx'"
                 						ngf-accept="'.xls,.xlsx'" ngf-max-size="10MB" ngf-min-height="100" >
                 						<span class="btn btn-primary"><?php echo $this->lang->line('Browse File');?></span>
									</div>
									<div class="col-lg-1 col-md-1 col-xs-12">
										<span class="btn btn-primary" ng-click="upload()"><i class="fa fa-save"></i><?php echo $this->lang->line('Upload');?></span>
									</div>
								</div>
                                <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                <div class="col-lg-3 col-md-3 col-xs-3">
                                </div>
								<div class="col-lg-6 col-md-6 col-xs-6">
                                    <button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button>
								</div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-3">
                                </div>
								<div class="col-lg-6 col-md-6 col-xs-6">
									<div class="table-responsive"> 
										<table class="table table-striped">
											<thead>
												<tr>  
													<th><?php echo $this->lang->line('Service Center');?></th>
                                                    <th><?php echo $this->lang->line('Option');?></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="item in listservicecenter">
                                                    <td hidden ng-bind="item.id"></td> 
													<td ng-bind="item.servicecenter"></td>
                                                    <td>
												        <button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												        <button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											        </td> 
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
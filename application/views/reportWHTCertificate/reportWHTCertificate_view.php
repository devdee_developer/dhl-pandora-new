	<script src="<?php echo base_url('asset/reportWHTCertificateController.js'); ?>"></script>
	<!-- <script src="<?php echo base_url('/dist/Chart.min.js'); ?>"></script> -->
	<!-- <script src="<?php echo base_url('/utils.js'); ?>"></script> -->

<script>

</script>
<div ng-controller="reportWHTCertificateController" ng-init="onInit()">
	<div class="row">
		<ul class="navigator">
			<li class="nav_active"> Certificate Report</li>
		</ul>

	</div>

	<div class="row">
		<div class="col-lg-10">
			<h1 class="page-header">Certificate Report</h1>
			<h3 ng-show="CheckShowDevice == true">พิมพ์ตามรายชื่อ</h3>
			<h3 ng-show="CheckShowDevice == false">พิมพ์ตามรายเดือน</h3>
		</div>
		<div class="col-lg-2">
			<button class="btn btn-primary" ng-click="LoadViewByCus()" ng-show="CheckShowDevice == false">พิมพ์ตามรายชื่อ</button>
		</div>

	</div>
	
	<div class="row DisplayDevice">
		<div clas="col-12">
			<div class="col-lg-3">
				<label>เดือน</label>
				<select ng-model="start_month" class="form-control"require>
					<option value="0">มกราคม</option>
					<option value="1">กุมภาพันธ์</option>
					<option value="2">มีนาคม</option>
					<option value="3">เมษายน</option>
					<option value="4">พฤษภาคม</option>
					<option value="5">มิถุนายน</option>
					<option value="6">กรกฎาคม</option>
					<option value="7">สิงหาคม</option>
					<option value="8">กันยายน</option>
					<option value="9">ตุลาคม</option>
					<option value="10">พฤศจิกายน</option>
					<option value="11">ธันวาคม</option>
				</select>
			</div>
			<div class="col-lg-3">
				<label>ปี</label>
				<select ng-model="start_year" class="form-control"require>
					<option value="2020">2020</option>
					<option value="2019">2019</option>
					<option value="2018">2018</option>
					<option value="2017">2017</option>
					<option value="2016">2016</option>
					<option value="2015">2015</option>
					<option value="2014">2014</option>
				</select>
			</div>
			<div class="col-lg-3">
				<label>วันที่ยื่นภาษี</label>
				<input  class="form-control"ng-model="tax_date" data-date-format="dd-MM-yyyy" bs-datepicker required>
			</div>
			<div class="col-lg-3">
				<br>
				<button class="btn btn-success"ng-click="printPDF()">Print Report</button>
			</div>
		</div>
	</div>
	<div class="row DisplayByCus">
		<div class="row">
			<div class="col-lg-3">
				<label>ชื่อ-สกุล</label>
				<ui-select ng-model="TempCustomer.selected" theme="selectize">
					<ui-select-match>{{$select.selected.name}}</ui-select-match>
					<ui-select-choices repeat="List in listCustomer | filter: $select.search">
						<span ng-bind-html="List.name | highlight: $select.search"></span>
					</ui-select-choices>
				</ui-select>
			</div>
			
			<div class="col-lg-3">
				<label>วันที่เริ่มต้น</label>
				<input  class="form-control"ng-model="start_dateByC" data-date-format="dd-MM-yyyy" bs-datepicker required>
			</div>
			<div class="col-lg-3">
				<label>วันที่สิ้นสุด</label>
				<input  class="form-control"ng-model="end_dateByC" data-date-format="dd-MM-yyyy" bs-datepicker required>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3">
				<br>
				<button class="btn btn-success btn-block" ng-click="printPDFByC()">Print Report</button>
				
			</div>
			<div class="col-lg-3">
				<br>
				<button class="btn btn-danger btn-block"  ng-click="ShowDevice()">Back</button>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('asset/slaZipcodeController.js'); ?>"></script>
<div ng-controller="slaZipcodeController" ng-init="onInit()">

	<div class="row">
		<ul class="navigator">
			<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Agent');?></a></li>*/ ?>
			<!-- <li class="nav_active"> <?php echo $this->lang->line('ShopCode'); ?></li> -->
			<li class="nav_active"></li>
		</ul>
		<!-- /.col-lg-12 -->
	</div>

	<div class="row">
		<div class="col-lg-12">
			<!-- <h1 class="page-header"><?php echo $this->lang->line('ShopCode'); ?></h1> -->
			<h1 class="page-header">SLA Zipcode</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>

	<!-- /List.row types-->
	<div class="row  SearchDevice" style="display:none;">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('Search'); ?>
				</div>
				<div class="panel-body">
					<div class="form-group col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-4 col-md-4 col-xs-12">
							<label>Zipcode</label>
							<input class="form-control" ng-model="modelSearch.zipcode" maxlength="80">
							<p class="help-block"></p>
						</div>
						<div class="col-lg-4 col-md-4 col-xs-12">
							<label>Destination Country</label>
							<input class="form-control" ng-model="modelSearch.destination_country" maxlength="80">
							<p class="help-block"></p>
						</div>
						<div class="col-lg-4 col-md-4 col-xs-12">
							<label>Product Code</label>
							<input class="form-control" ng-model="modelSearch.product_code" maxlength="5">
							<p class="help-block"></p>
						</div>
						<div class="col-lg-4 col-md-4 col-xs-12">
							<label>Lane</label>
							<input class="form-control" ng-model="modelSearch.lane" maxlength="5">
							<p class="help-block"></p>
						</div>
						<div class="col-lg-4 col-md-4 col-xs-12">
							<label>Gateway</label>
							<input class="form-control" ng-model="modelSearch.gateway" maxlength="5">
							<p class="help-block"></p>
						</div>
						<!-- <div class="col-lg-3 col-md-3 col-xs-12">
							<label><?php echo $this->lang->line('Province'); ?></label>
							<input class="form-control" ng-model="modelSearch.Province" maxlength="2">
							<p class="help-block"></p>
						</div> -->
					</div>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<button type="button" class="btn btn-warning waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
						<button type="button" class="btn btn-info waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
						<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
					</div>
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /List.row types-->


	<!-- / create room types  -->
	<div class="row addDevice" style="display:none;">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('ShopCode'); ?>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
						<div role="form">
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Zipcode</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.zipcode" maxlength="180">
										<p class="CreateModel_zipcode require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Destination Country</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.destination_country" maxlength="180">
										<p class="CreateModel_destination_country require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Product Code</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.product_code" maxlength="180">
										<p class="CreateModel_product_code require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Lane</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.lane" maxlength="180">
										<p class="CreateModel_lane require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Gateway</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.gateway" maxlength="180">
										<p class="CreateModel_gateway require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>T T Sla</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.t_t_sla" maxlength="180">
										<p class="CreateModel_t_t_sla require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Mon</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.mon" maxlength="180">
										<p class="CreateModel_mon require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>
								
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Tue</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.tue" maxlength="180">
										<p class="CreateModel_tue require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Wed</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.wed" maxlength="180">
										<p class="CreateModel_wed require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Thu</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.thu" maxlength="180">
										<p class="CreateModel_thu require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Fri</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.fri" maxlength="180">
										<p class="CreateModel_fri require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Sat</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.sat" maxlength="180">
										<p class="CreateModel_sat require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>T T During Pandemic</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.t_t_during_pandemic" maxlength="180">
										<p class="CreateModel_t_t_during_pandemic require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger">*</span>Type</label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<input class="form-control" ng-model="CreateModel.type" maxlength="180">
										<p class="CreateModel_type require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group text-right">
									<br /><br />
									<div class="col-lg-12 col-md-12 col-xs-12">
										<button class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
										<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
									</div>
								</div>
							</div>
							<div class="row text-primary  " ng-show="CreateModel.id > 0" style="font-size:xx-small; margin-top:80px;">
								<div class="col-md-6 col-xs-12 timestampshow text-left">
									<?php echo $this->lang->line('Createby'); ?> {{CreateModel.create_user}} {{CreateModel.create_date}}
								</div>
								<div class="col-md-6 col-xs-12 timestampshow text-right text-left-xs">
									<?php echo $this->lang->line('Updateby'); ?> {{CreateModel.update_user}} {{CreateModel.update_date}}
								</div>
							</div>
						</div>

					</div>
					<!-- /.row (nested) -->

				</div>


				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.create room types -->


	<!-- /List.row types-->
	<div class="row DisplayDevice">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<!-- <?php echo $this->lang->line('ListOfShopCode'); ?> -->
					SLA Zipcode
				</div>
				<div class="panel-body">
					<div class="col-lg-5 col-md-5 col-xs-12">
						<!-- <button class="btn btn-success" ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button> -->
						<button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
						<button type="button" class="btn btn-warning waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
						<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="Exportexcel()"><i class="fa fa-download"></i>
							<spanclass="hidden-xs"> Download All Data </span>
						</button>
					</div>

					<div class="col-lg-7 col-md-7 col-xs-12">
						<div class="col-lg-8 col-md-8 col-xs-12">
							<input class="form-control" readonly ng-model="file_Pandora_SlaZip.name">
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12" ngf-select ng-model="file_Pandora_SlaZip" name="file_Pandora_SlaZip" ngf-pattern="'.xls,.xlsx'" ngf-accept="'.xls,.xlsx'" ngf-max-size="10MB" ngf-min-height="100">
							<span class="btn btn-primary"><?php echo $this->lang->line('Browse File'); ?></span>
						</div>

						<div class="col-lg-2 col-md-2 col-xs-12">
							<span class="btn btn-primary" ng-click="upload()"><i class="fa fa-upload"></i> <?php echo $this->lang->line('Upload'); ?> </span>
						</div>
									
					</div>

					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th ng-class='sortClass("zipcode")' ng-click='sortColumn("zipcode")' >Zipcode</th>
										<!-- <th ng-class='sortClass("")' ng-click='sortColumn("")' sort="Store">Store</th> -->
										<th ng-class='sortClass("destination_country")' ng-click='sortColumn("destination_country")' >Destination Country</th>
										<th ng-class='sortClass("product_code")' ng-click='sortColumn("product_code")' >Product_Code</th>
										<th ng-class='sortClass("lane")' ng-click='sortColumn("lane")' >Lane</th>
										<th ng-class='sortClass("gateway")' ng-click='sortColumn("gateway")' >Gateway</th>
										<th ng-class='sortClass("t_t_sla")' ng-click='sortColumn("t_t_sla")' >T T Sla</th>
										<th ng-class='sortClass("mon")' ng-click='sortColumn("mon")' >Mon</th>
										<th ng-class='sortClass("tue")' ng-click='sortColumn("tue")' >Tue</th>
										<th ng-class='sortClass("wed")' ng-click='sortColumn("wed")' >Wed</th>
										<th ng-class='sortClass("thu")' ng-click='sortColumn("thu")' >Thu</th>
										<th ng-class='sortClass("fri")' ng-click='sortColumn("fri")' >Fri</th>
										<th ng-class='sortClass("sat")' ng-click='sortColumn("sat")' >Sat</th>
										<th ng-class='sortClass("t_t_during_pandemic")' ng-click='sortColumn("t_t_during_pandemic")' >T T During Pandemic</th>
										<th ng-class='sortClass("type")' ng-click='sortColumn("type")' >Type</th>
										<!-- <th><?php echo $this->lang->line('Option'); ?></th> -->
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="item in modelDeviceList|orderBy:column:reverse">
										<td ng-bind="item.zipcode"></td>
										<!-- <td ng-bind="item.Store"></td> -->
										<td ng-bind="item.destination_country"></td>
										<td ng-bind="item.product_code"></td>
										<td ng-bind="item.lane"></td>
										<td ng-bind="item.gateway"></td>
										<td ng-bind="item.t_t_sla"></td>
										<td ng-bind="item.mon"></td>
										<td ng-bind="item.tue"></td>
										<td ng-bind="item.wed"></td>
										<td ng-bind="item.thu"></td>
										<td ng-bind="item.fri"></td>
										<td ng-bind="item.sat"></td>
										<td ng-bind="item.t_t_during_pandemic"></td>
										<td ng-bind="item.type"></td>
										<!-- <td>
											<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
											<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
										</td> -->
									</tr>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>

					<!-- ทำหน้า -->
					<div class="row tblResult small">
						<div class="col-md-7 col-sm-7 col-xs-12 ">
							<label class="col-md-4 col-sm-4 col-xs-12">
								<?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
							</label>
							<label class="col-md-4 col-sm-4 col-xs-12">
								<?php echo $this->lang->line('ResultsPerPage'); ?>
							</label>
							<div class="col-md-4 col-sm-4 col-xs-12 ">
								<ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
									<ui-select-match>{{$select.selected.Value}}</ui-select-match>
									<ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
										<span ng-bind-html="pSize.Text | highlight: $select.search"></span>
									</ui-select-choices>
								</ui-select>
							</div>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12  ">
							<label class="col-md-4 col-sm-4 col-xs-12">
								<span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
							</label>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
									<ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
									<ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
										<span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
									</ui-select-choices>
								</ui-select>
							</div>
							<label class="col-md-4 col-sm-4 col-xs-12">
								/ {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
							</label>
						</div>
					</div>
					<!-- ทำหน้า -->

				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /List.row types-->
</div>
</div>
<script src="<?php echo base_url('asset/companyController.js');?>"></script>
 <div  ng-controller="companyController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('User');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('Company');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $this->lang->line('Company');?></h1>
		</div>
		<!-- / create room types  -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php echo $this->lang->line('Company');?>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div role="form">
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label>ชื่อบริษัท</label>
											<input class="form-control" ng-model="CreateModel.name">
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12"> 
											<label>สาขา</label>
											<input class="form-control" ng-model="CreateModel.branch" maxlength="5">
										</div> 
									</div>
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label>โทรศัพท์</label>
											<input class="form-control" ng-model="CreateModel.tel">
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12"> 
											<label>E-Mail</label>
											<input class="form-control" ng-model="CreateModel.email">
										</div> 
									</div>
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label>เลขที่ผู้เสียภาษี 13 หลัก</label>
											<input class="form-control" ng-model="CreateModel.taxid_13">
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12"> 
											<label>Contact</label>
											<input class="form-control" ng-model="CreateModel.contact">
										</div>
									</div>
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default">
													<div class="panel-heading">
														ที่อยู่
													</div>
													<div class="panel-body">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-xs-12">
																<div role="form">
																	<div class="form-group col-lg-12 col-md-12 col-xs-12">
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>อาคาร</label>
																			<input class="form-control" ng-model="CreateModel.BuildingAddress">
																		</div>
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>ห้องเลขที่</label>
																			<input class="form-control" ng-model="CreateModel.Room">
																		</div>
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>ชั้นที่</label>
																			<input class="form-control" ng-model="CreateModel.Floor">
																		</div>
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>หมู่บ้าน</label>
																			<input class="form-control" ng-model="CreateModel.Village">
																		</div>
																	</div>
																	<div class="form-group col-lg-12 col-md-12 col-xs-12">
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>บ้านเลขที่</label>
																			<input class="form-control" ng-model="CreateModel.HouseNo">
																		</div>
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>หมู่ที่</label>
																			<input class="form-control" ng-model="CreateModel.VillageNo">
																		</div>
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>ตรอก/ซอย</label>
																			<input class="form-control" ng-model="CreateModel.Alley">
																		</div>
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>ถนน</label>
																			<input class="form-control" ng-model="CreateModel.Road">
																		</div>
																	</div>
																	<div class="form-group col-lg-12 col-md-12 col-xs-12">
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>ตำบล/แขวง</label>
																			<input class="form-control" ng-model="CreateModel.Subdistrict">
																		</div>
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>อำเภอ/เขต</label>
																			<input class="form-control" ng-model="CreateModel.District">
																		</div>
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>จังหวัดด</label>
																			<input class="form-control" ng-model="CreateModel.Province">
																		</div>
																		<div class="col-lg-3 col-md-3 col-xs-12"> 
																			<label>รหัสไปรษณีย์</label>
																			<input class="form-control" ng-model="CreateModel.PostalCode">
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="form-group text-right">
										<br/><br/>
										<div class="col-lg-12 col-md-12 col-xs-12">
										<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs">Update</span></button>
										</div>
									</div>
									
								</div> 
							</div>
						</div>
						<!-- /.row (nested) -->
					</div>	 
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.create room types -->
	</div>
</div>

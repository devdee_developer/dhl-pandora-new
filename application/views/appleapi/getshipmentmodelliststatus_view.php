<script src="<?php echo base_url('asset/getshipmentmodelliststatusController.js');?>"></script>
<div  ng-controller="getshipmentmodelliststatusController" ng-init="onInit()">
 
    <div class="row">
        <ul class="navigator">
            <li class="nav_active"> <?php echo $this->lang->line('getshipmentmodelliststatus');?></li>
        </ul>
    </div>

    <div class="row" >
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $this->lang->line('getshipmentmodelliststatus');?></h1>
		</div>
    </div>

	<div class="row DisplayDevice" >
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('getshipmentmodelliststatus');?>
				</div>
				<div class="panel-body">
					<div class="form-group col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-3 col-md-3 col-xs-3">
							<label><span class="text-danger" >*</span><?php echo $this->lang->line('Date');?></label>
							<input class="form-control" ng-model="modelSearch.Date" data-date-format="dd-MM-yyyy" bs-datepicker>
						</div> 
						<div class="col-lg-3 col-md-3 col-xs-3">
							<label><span class="text-danger" >*</span><?php echo $this->lang->line('token');?></label>
							<input class="form-control" ng-model="modelSearch.token">
						</div> 
						<div class="col-lg-3 col-md-3 col-xs-3">
							<label><span class="text-danger" >*</span><?php echo $this->lang->line('version');?></label>
							<input class="form-control" ng-model="modelSearch.version">
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
						<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row getshipmentmodelliststatus">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div role="form">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive"> 
										<table class="table table-striped">
											<thead>
												<tr> 
													<th><?php echo $this->lang->line('date');?></th>
													<th><?php echo $this->lang->line('waybill');?></th>
													<th><?php echo $this->lang->line('Status');?></th>
													<th><?php echo $this->lang->line('recipient_name');?></th>	
													<th><?php echo $this->lang->line('postcode');?></th>
													<th><?php echo $this->lang->line('Pick_Up_Date');?></th>
													<th><?php echo $this->lang->line('Address');?></th>												
													<th><?php echo $this->lang->line('Telephone');?></th>
													<th><?php echo $this->lang->line('Contact_Person');?></th>
													<th><?php echo $this->lang->line('PCS');?></th>
													<th><?php echo $this->lang->line('Account');?></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="item in listgetshipmentmodelliststatus">
													<td ng-bind="item.date"></td>
													<td ng-bind="item.AWB"></td> 
													<td ng-show="item.day == null">&#x2718;</td>
													<td ng-show="item.day != null">&#10003;</td>
													<td ng-bind="item.Consignee"></td> 
													<td ng-bind="item.Zip_Code"></td>
													<td ng-bind="item.Pick_Up_Date"></td>
													<td ng-bind="item.Address3"></td>
													<td ng-bind="item.Telephone"></td> 
													<td ng-bind="item.Contact_Person"></td>
													<td ng-bind="item.PCS"></td>
													<td ng-bind="item.Account"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="row tblResult small"  >
									<div class="col-md-7 col-sm-7 col-xs-12 ">
										<label class="col-md-4 col-sm-4 col-xs-12">
											<?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
										</label>
										<label class="col-md-4 col-sm-4 col-xs-12">
											<?php echo $this->lang->line('ResultsPerPage');?>
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12 ">
											<ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
												<ui-select-match>{{$select.selected.Value}}</ui-select-match>
												<ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
													<span ng-bind-html="pSize.Text | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
									</div>
									<div class="col-md-5 col-sm-5 col-xs-12  ">
										<label class="col-md-4 col-sm-4 col-xs-12">
											<span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
										</label>
										<div class="col-md-3 col-sm-3 col-xs-12">
											<ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
												<ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
												<ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
													<span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<label class="col-md-4 col-sm-4 col-xs-12">
											/ {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
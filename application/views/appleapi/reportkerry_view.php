<script src="<?php echo base_url('asset/reportAppleKRRController.js');?>"></script>
<div  ng-controller="reportAppleKRRController" ng-init="onInit()">
 
    <div class="row">
        <ul class="navigator">
            <li class="nav_active"> <?php echo $this->lang->line('reportkerry');?></li>
        </ul>
    </div>

    <div class="row" >
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $this->lang->line('reportkerry');?></h1>
		</div>
    </div>

	<div class="row DisplayDevice" >
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('reportkerry');?>
				</div>
				<div class="panel-body">
                    
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-1 col-md-1 col-xs-1">
                        <label><span class="text-danger" >*</span><?php echo $this->lang->line('date');?></label></div>
                        <div class="col-lg-4 col-md-4 col-xs-4">
							<input class="form-control" ng-model="CreateModel.date"  data-date-format="dd-MM-yyyy" bs-datepicker >
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-1"></div>
                        <div class="col-lg-4 col-md-4 col-xs-4">
                            <span class="btn btn-primary" ng-click="download(1)" ><i class="fa fa-download"></i>  <?php echo $this->lang->line('download');?></span>
                            <span class="btn btn-warning" ng-click="download(2)" ><i class="fa fa-download"></i>  New Report</span>
                        </div>
                     
                    </div>
				</div>
			</div>
		</div>
    </div>    
</div>
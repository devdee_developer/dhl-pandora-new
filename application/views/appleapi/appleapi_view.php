<script src="<?php echo base_url('asset/appleapiController.js');?>"></script>
<div  ng-controller="appleapiController" ng-init="onInit()">
 
    <div class="row">
        <ul class="navigator">
            <li class="nav_active"> <?php echo $this->lang->line('appleapi');?></li>
        </ul>
    </div>

    <div class="row" >
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $this->lang->line('appleapi');?></h1>
		</div>
    </div>

	<div class="row DisplayDevice" >
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('appleapi');?>
				</div>
				<div class="panel-body">
					<div class="form-group col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-4 col-md-4 col-xs-4">
							<label><?php echo $this->lang->line('appleapi');?></label>
							<input class="form-control" ng-model="modelSearch.waybill_no" maxlength="80">
						</div> 
						<div class="col-lg-3 col-md-3 col-xs-3">
							<label><span class="text-danger" >*</span><?php echo $this->lang->line('token');?></label>
							<input class="form-control" ng-model="modelSearch.token">
						</div> 
						<div class="col-lg-3 col-md-3 col-xs-3">
							<label><span class="text-danger" >*</span><?php echo $this->lang->line('version');?></label>
							<input class="form-control" ng-model="modelSearch.version">
						</div>
					</div> 
					<div class="col-lg-12 col-md-12 col-xs-12">
						<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
						<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row showsearch">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div role="form">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive"> 
										<table class="table table-striped">
											<thead>
												<tr>  
													<th><?php echo $this->lang->line('date');?></th>
													<th><?php echo $this->lang->line('waybill');?></th>
													<th><?php echo $this->lang->line('postcode');?></th>
													<th><?php echo $this->lang->line('SVC');?></th>
													<th><?php echo $this->lang->line('piece');?></th>
													<th><?php echo $this->lang->line('Day');?></th>
													<!-- <th><?php echo $this->lang->line('ServiceCenter');?></th> -->
													<th><?php echo $this->lang->line('Limitation');?></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="item in listappleapi">
													<td ng-bind="item.datewaybill"></td>
													<td ng-bind="item.waybill"></td> 
													<td ng-bind="item.postcode"></td>
													<td ng-bind="item.service_center"></td>
													<td ng-bind="item.unit"></td>
													<td ng-bind="item.dayD"></td>
													<!-- <td ng-bind="item.service_center1"></td>  -->
													<td ng-bind="item.limitation"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row getshipmentmodelliststatus" style="display:none;">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div role="form">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive"> 
										<table class="table table-striped">
											<thead>
												<tr>  
													<th><?php echo $this->lang->line('waybill');?></th>
													<th><?php echo $this->lang->line('postcode');?></th>
													<th><?php echo $this->lang->line('ServiceCenter');?></th>
													<th><?php echo $this->lang->line('date');?></th>
													<th><?php echo $this->lang->line('ref_no');?></th>
													<th><?php echo $this->lang->line('payerid');?></th>
													<th><?php echo $this->lang->line('recipient_name');?></th>
													<th><?php echo $this->lang->line('recipient_address1');?></th>
													<th><?php echo $this->lang->line('recipient_address2');?></th>
													<th><?php echo $this->lang->line('Apple_Pre');?></th>
													<th><?php echo $this->lang->line('recipient_contact_person');?></th>
													<th><?php echo $this->lang->line('piece');?></th>
													<th><?php echo $this->lang->line('weiht_kg');?></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="item in listappleapi">
													<td ng-bind="item.waybill"></td> 
													<td ng-bind="item.postcode"></td>
													<td ng-bind="item.service_center"></td>
													<td ng-bind="item.date"></td>
													<td ng-bind="item.ref_no"></td> 
													<td ng-bind="item.payerid"></td>
													<td ng-bind="item.recipient_name"></td>
													<td ng-bind="item.recipient_address1"></td>
													<td ng-bind="item.recipient_address2"></td> 
													<td ng-bind="item.Apple_Pre"></td>
													<td ng-bind="item.recipient_contact_person"></td>
													<td ng-bind="item.piece"></td>
													<td ng-bind="item.weiht_kg"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script src="<?php echo base_url('asset/shipmenreporttKRRController.js');?>"></script>
<div  ng-controller="shipmenreporttKRRController" ng-init="onInit()">
 
    <div class="row">
        <ul class="navigator">
            <li class="nav_active"> <?php echo $this->lang->line('Check Report Kerry');?></li>
        </ul>
    </div>

    <div class="row" >
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $this->lang->line('Check Report Kerry');?></h1>
		</div>
    </div>

	<div class="row DisplayDevice" >
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('Check Report Kerry');?>
				</div>
				<div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-xs-4">
						</div>
						<div class="col-lg-4 col-md-4 col-xs-4">
                            <label><span class="text-danger" >*</span><?php echo $this->lang->line('token');?></label>
							<input class="form-control" ng-model="CreateModel.token">
						</div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-xs-4">
						</div>
						<div class="col-lg-4 col-md-4 col-xs-4">
                            <label><span class="text-danger" >*</span><?php echo $this->lang->line('version');?></label>
							<input class="form-control" ng-model="CreateModel.version">
                        </div>
                    </div>
					
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                    <div class="col-lg-7 col-md-7 col-xs-7">
						</div>
                        <div class="col-lg-2 col-md-2 col-xs-2">
                            <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="reload()"><span class="hidden-xs"><?php echo $this->lang->line('submit');?></span></button>
                        </div>
                    </div>
				</div>
			</div>
		</div>
    </div>
    <div class="row showdetail">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div role="form">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive"> 
										<table class="table table-striped">
											<thead>
												<tr>  
													<th><?php echo $this->lang->line('AWB');?></th>
                                                    <th><?php echo $this->lang->line('PID');?></th>													
													<th><?php echo $this->lang->line('ServiceCenter');?></th>
													<th><?php echo $this->lang->line('QTY');?></th>
													<th><?php echo $this->lang->line('Status');?></th>
													<th><?php echo $this->lang->line('date');?></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="item in listreportkerry">
													<td ng-bind="item.AWB"></td>
													<td ng-bind="item.PID"></td>
													<td ng-bind="item.service_center"></td>
													<td ng-bind="item.QTY"></td>
													<td ng-bind="item.status"></td>
													<td ng-bind="item.datereport"></td>
												</tr>
											</tbody>
										</table>
									</div>
                                </div>
                                <div class="row tblResult small"  >
									<div class="col-md-7 col-sm-7 col-xs-12 ">
										<label class="col-md-4 col-sm-4 col-xs-12">
											<?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
										</label>
										<label class="col-md-4 col-sm-4 col-xs-12">
											<?php echo $this->lang->line('ResultsPerPage');?>
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12 ">
											<ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
												<ui-select-match>{{$select.selected.Value}}</ui-select-match>
												<ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
													<span ng-bind-html="pSize.Text | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
									</div>
									<div class="col-md-5 col-sm-5 col-xs-12  ">
										<label class="col-md-4 col-sm-4 col-xs-12">
											<span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
										</label>
										<div class="col-md-3 col-sm-3 col-xs-12">
											<ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
												<ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
												<ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
													<span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<label class="col-md-4 col-sm-4 col-xs-12">
											/ {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    
</div>
<script src="<?php echo base_url('asset/optimizeController.js');?>"></script>
 <div  ng-controller="optimizeController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Optimize');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('Optimize');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $this->lang->line('Optimize');?></h1>
		</div>
		<!-- /.col-lg-12 -->
   </div>
   
		<!-- / create room types  -->
		<div class="row addDevice" >
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php echo $this->lang->line('Optimize');?>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div role="form">
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><?php echo $this->lang->line('PostalCode');?></label>
											<input class="form-control" ng-model="CreateModel.postcode" maxlength="180" >
											<p class="CreateModel_postcode require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><?php echo $this->lang->line('Weight');?></label>
											<input class="form-control" ng-model="CreateModel.weight" maxlength="180" >
											<p class="CreateModel_weight require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
									</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><?php echo $this->lang->line('Height');?></label>
											<input class="form-control" ng-model="CreateModel.height" maxlength="180" >
											<p class="CreateModel_height require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><?php echo $this->lang->line('Length');?></label>
											<input class="form-control" ng-model="CreateModel.length" maxlength="180" >
											<p class="CreateModel_length require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
									</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><?php echo $this->lang->line('Width');?></label>
											<input class="form-control" ng-model="CreateModel.width" maxlength="180" >
											<p class="CreateModel_width require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><?php echo $this->lang->line('Customer');?></label>
											<input class="form-control" ng-model="CreateModel.customer" maxlength="180" >
											<p class="CreateModel_customer require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
									</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><?php echo $this->lang->line('Phone');?></label>
											<input class="form-control" ng-model="CreateModel.phone" maxlength="180" >
											<p class="CreateModel_width require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12"> 
										</div> 
									</div>
									<div class="form-group text-right">
										<br/><br/>
										<div class="col-lg-12 col-md-12 col-xs-12">
										<button class="btn btn-primary"  ng-click="onOptimizeClick()" ><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('FindOptimize');?></span></button> 
										</div>
									</div>
								</div> 
							 
							</div>
							
							<div class="col-lg-12 col-md-12 col-xs-12">
								<h3>{{OptimzeModel.suggest}}</h3>
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('ServiceName');?></th>
											<th class="text-right"><?php echo $this->lang->line('Price');?></th>
											<th class="text-right"><?php echo $this->lang->line('DeliveryTime');?></th>
										</tr>
									</thead>
									<tbody>
										 <tr> 
											<td><?php echo $this->lang->line('Kerry');?></td>
											<td class="text-right">
											<span ng-show="OptimzeModel.kerry.price > 0">{{ OptimzeModel.kerry.price | myShowCostFormat }}</span>
												<span ng-show="OptimzeModel.kerry.price == 0">{{ OptimzeModel.kerry.message }}</span>
											</td>
											<td class="text-right">{{ OptimzeModel.delivery_day.kerry }}</td>
										</tr><tr> 
											<td><?php echo $this->lang->line('PEP');?></td>
											<td class="text-right">
											<span ng-show="OptimzeModel.pep.price > 0">{{ OptimzeModel.pep.price | myShowCostFormat }}</span>
												<span ng-show="OptimzeModel.pep.price == 0">{{ OptimzeModel.pep.message }}</span>
											</td>
											<td class="text-right">{{ OptimzeModel.delivery_day.pep }}</td>
										</tr><tr> 
											<td><?php echo $this->lang->line('NIM');?></td>
											<td class="text-right">
											<span ng-show="OptimzeModel.nim.price > 0">{{ OptimzeModel.nim.price | myShowCostFormat }}</span>
												<span ng-show="OptimzeModel.nim.price == 0">{{ OptimzeModel.nim.message }}</span>
											</td>
											<td class="text-right">{{ OptimzeModel.delivery_day.nim }}</td>
										</tr><tr> 
											<td><?php echo $this->lang->line('EMS');?></td>
											<td class="text-right">
												<span ng-show="OptimzeModel.thaipost.price > 0">{{ OptimzeModel.thaipost.price | myShowCostFormat }}</span>
												<span ng-show="OptimzeModel.thaipost.price == 0">{{ OptimzeModel.thaipost.message }}</span>
											</td>
											<td class="text-right">{{ OptimzeModel.delivery_day.ems }}</td>
										</tr><tr> 
											<td><?php echo $this->lang->line('KKC');?></td>
											<td class="text-right">-&nbsp;</td>
											<td class="text-right">{{ OptimzeModel.delivery_day.kkc }}</td>
										</tr><tr> 
											<td><?php echo $this->lang->line('USM');?></td>
											<td class="text-right">-&nbsp;</td>
											<td class="text-right">{{ OptimzeModel.delivery_day.usm }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- /.row (nested) -->
					
					</div>
					
					 
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.create room types -->
			 
	</div>
</div>
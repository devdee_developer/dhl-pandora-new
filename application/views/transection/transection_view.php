<script src="<?php echo base_url('asset/transectionController.js');?>"></script>
 <div  ng-controller="transectionController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('transection');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('transection');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('transection');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body"> 
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('PID');?></label>
								<input class="form-control" ng-model="modelSearch.pid" maxlength="180" > 
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('AWB');?></label>
								<input class="form-control" ng-model="modelSearch.awb" maxlength="180" > 
							</div> 
						</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('InputDate');?></label>
								<input class="form-control"  ng-model="modelSearch.input_date"     data-date-format="yyyy-MM-dd" data-autoclose="1"  bs-datepicker>  
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('ToPerson');?></label>
								<input class="form-control" ng-model="modelSearch.to_person" maxlength="180" > 
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Transection');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('PID');?></label>
												<input class="form-control" ng-model="CreateModel.pid" maxlength="180" >
												<p class="CreateModel_pid require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('AWB');?></label>
												<input class="form-control" ng-model="CreateModel.awb" maxlength="180" >
												<p class="CreateModel_awb require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('InputDate');?></label>
												<input class="form-control"  ng-model="CreateModel.input_date" readonly   data-date-format="yyyy-MM-dd" data-autoclose="1"  bs-datepicker>
												<p class="CreateModel_input_date require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('ToPerson');?></label>
												<input class="form-control" ng-model="CreateModel.to_person" maxlength="180" >
												<p class="CreateModel_to_person require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address');?></label>
												<input class="form-control" ng-model="CreateModel.address" maxlength="180" >
												<p class="CreateModel_address require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Phone');?></label>
												<input class="form-control" ng-model="CreateModel.phone" maxlength="180" >
												<p class="CreateModel_phone require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('PostalCode');?></label>
												<input class="form-control" ng-model="CreateModel.postcode" maxlength="180" >
												<p class="CreateModel_postcode require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12"> 
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Width');?> (cm) </label>
												<input class="form-control" ng-model="CreateModel.width" maxlength="180" >
												<p class="CreateModel_width require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Length');?> (cm) </label>
												<input class="form-control" ng-model="CreateModel.length" maxlength="180" >
												<p class="CreateModel_length require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Height');?> (cm) </label>
												<input class="form-control" ng-model="CreateModel.height" maxlength="180" >
												<p class="CreateModel_height require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Weight');?> (kg) </label>
												<input class="form-control" ng-model="CreateModel.weight" maxlength="180" >
												<p class="CreateModel_weight require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Duty');?></label>
												<input class="form-control" ng-model="CreateModel.dt" maxlength="180" >
												<p class="CreateModel_dt require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('DutyCost');?> (bath) </label>
												<input class="form-control" ng-model="CreateModel.dt_cost" maxlength="180" >
												<p class="CreateModel_dt_cost require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><input   ng-model="CreateModel.dt_pay"  type="checkbox" > <?php echo $this->lang->line('DutyACC');?></label>
												<p class="CreateModel_dt_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
												<label><input   ng-model="CreateModel.dt_csh"  type="checkbox" > <?php echo $this->lang->line('DutyCSH');?></label>
												<p class="CreateModel_dt_csh require text-danger"><?php echo $this->lang->line('Require');?></p>
												<label><input   ng-model="CreateModel.dt_undel"  type="checkbox" > <?php echo $this->lang->line('DutyUndel');?></label>
												<p class="CreateModel_dt_undel require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('PayNote');?></label>
												<input class="form-control" ng-model="CreateModel.pay_note" maxlength="180" >
												<p class="CreateModel_pay_note require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('ProgramSelect');?></label>
												<input class="form-control" ng-model="CreateModel.p_select" maxlength="180" >
												<p class="CreateModel_p_select require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('ProgramPrice');?> (bath) </label>
												<input class="form-control" ng-model="CreateModel.p_price" maxlength="180" >
												<p class="CreateModel_p_price require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('CustomSelect');?></label>
												<input class="form-control" ng-model="CreateModel.c_select" maxlength="180" >
												<p class="CreateModel_c_select require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('CustomPrice');?> (bath) </label>
												<input class="form-control" ng-model="CreateModel.c_price" maxlength="180" >
												<p class="CreateModel_c_price require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('CustomNote');?></label>
												<input class="form-control" ng-model="CreateModel.c_note" maxlength="180" >
												<p class="CreateModel_c_note require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">  
											</div> 
										</div>
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>
										</div>
									</div> 
									<div class="row text-primary  " ng-show="CreateModel.id > 0" style="font-size:xx-small; margin-top:80px;" >
										<div class="col-md-6 col-xs-12 timestampshow text-left"> 
											<?php echo $this->lang->line('Createby');?> {{CreateModel.create_user}} {{CreateModel.create_date}}
										</div>
										<div class="col-md-6 col-xs-12 timestampshow text-right text-left-xs">
											<?php echo $this->lang->line('Updateby');?> {{CreateModel.update_user}} {{CreateModel.update_date}}
										</div>
									</div>
								</div>
								
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListOfTransection');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th  class="sorting" ng-click="sort($event)"  sort="pid"><?php echo $this->lang->line('InputDate');?></th>
											<th  class="sorting" ng-click="sort($event)"  sort="pid"><?php echo $this->lang->line('PID');?></th>
											<th  class="sorting" ng-click="sort($event)"  sort="awb"><?php echo $this->lang->line('AWB');?></th>
											<th  class="sorting" ng-click="sort($event)"  sort="to_person"><?php echo $this->lang->line('ToPerson');?></th>
											<th  class="sorting" ng-click="sort($event)"  sort="agent"><?php echo $this->lang->line('Agent');?></th>
											<th  class="sorting" ng-click="sort($event)"  sort="c_price"><?php echo $this->lang->line('Price');?></th>
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in modelDeviceList"> 
											<td ng-bind="item.input_date"></td>
                                            <td ng-bind="item.pid"></td>
											<td ng-bind="item.awb"></td>
											<td ng-bind="item.to_person"></td>
											<td ng-bind="item.c_select"></td>
											<td ng-bind="item.c_price"></td>
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>
	<script src="<?php echo base_url('asset/reportSAPWHTController.js'); ?>"></script>

<script>

</script>
<div ng-controller="reportSAPWHTController" ng-init="onInit()">
<div class="row">
		<ul class="navigator">
			<li class="nav_active"> PND  Report</li>
		</ul>

	</div>

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">PND  Report</h1>
		</div>

	</div>
	
	<div class="row">
		<div class="col-12">
			<div class="col-lg-3">
				<label>ภ.ด.ง.</label>
				<select ng-model="pnd_type" class="form-control"require>
					<option value="3">ภ.ด.ง.3</option>
					<option value="53">ภ.ด.ง.53</option>
				</select>
			</div>
			<div class="col-lg-9">
			
			</div>
		</div>
		<div class="col-12">
			<div class="col-lg-3">
				<label>เดือน</label>
				<select ng-model="start_month" class="form-control"require>
					<option value="1">มกราคม</option>
					<option value="2">กุมภาพันธ์</option>
					<option value="3">มีนาคม</option>
					<option value="4">เมษายน</option>
					<option value="5">พฤษภาคม</option>
					<option value="6">มิถุนายน</option>
					<option value="7">กรกฎาคม</option>
					<option value="8">สิงหาคม</option>
					<option value="9">กันยายน</option>
					<option value="10">ตุลาคม</option>
					<option value="11">พฤศจิกายน</option>
					<option value="12">ธันวาคม</option>
				</select>
			</div>
			<div class="col-lg-3">
				<label>ปี</label>
				<select ng-model="start_year" class="form-control"require>
					<option value="2020">2020</option>
					<option value="2019">2019</option>
					<option value="2018">2018</option>
					<option value="2017">2017</option>
					<option value="2016">2016</option>
					<option value="2015">2015</option>
					<option value="2014">2014</option>
				</select>
			</div>
			<div class="col-lg-3">
				<label>วันที่ยื่นภาษี</label>
				<input  class="form-control"ng-model="tax_date" data-date-format="dd-MM-yyyy" bs-datepicker required>
			</div>
			<div class="col-lg-3">
				<br>
				<button class="btn btn-success" style="width:150px;" ng-click="printPDF()">Print Report</button>
			</div>
		</div>
	</div>
</div>
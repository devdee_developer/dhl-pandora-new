<script src="<?php echo base_url('asset/reportKerryController.js');?>"></script>
 <div  ng-controller="reportKerryController" ng-init="onInit()">
  
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('ReportKerry');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('ReportKerry');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('ReportKerry');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice"  >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('StartDate');?></label>
								<input class="form-control" ng-model="modelSearch.start_date"  data-date-format="dd-MM-yyyy" bs-datepicker >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('EndDate');?></label>
								<input class="form-control" ng-model="modelSearch.end_date" data-date-format="dd-MM-yyyy" bs-datepicker >
								<p class="help-block"></p>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button> 
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="ExportExcel()"><i class="fa fa-download"></i> <span class="hidden-xs"><?php echo $this->lang->line('ExportExcel');?></span></button>  
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="ExportHandOver()"><i class="fa fa-download"></i> <span class="hidden-xs"><?php echo $this->lang->line('ExportHandOver');?></span></button>  
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ReportKerry');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('PostalCode');?></label>
												<input class="form-control" ng-model="CreateModel.post_id" maxlength="10"  >
												<p class="CreateModel_post_id require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-4 col-md-4 col-xs-4"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Province');?></label>
												<input class="form-control" ng-model="CreateModel.province"  maxlength="10"  >
												<p class="CreateModel_province require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('District');?></label>
												<input class="form-control" ng-model="CreateModel.district" maxlength="10"  >
												<p class="CreateModel_district require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-4 col-md-4 col-xs-4"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Zone');?></label>
												<input class="form-control" ng-model="CreateModel.zone"  maxlength="10"  >
												<p class="CreateModel_zone require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div> 
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>
										</div>
									</div>
								</div>  
								
								
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th>recipient_name</th> 
													<th><?php echo $this->lang->line('Province');?></th>
													<th><?php echo $this->lang->line('District');?></th> 
													<th><?php echo $this->lang->line('Zone');?></th> 
													<th><?php echo $this->lang->line('Option');?></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="item in modelDeviceList">  
													<td  ng-bind="item.post_id"></td>
													<td  ng-bind="item.province"></td>
													<td  ng-bind="item.district"></td>
													<td  ng-bind="item.zone"></td>
													<td>
														<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr> 
											</tbody>
										</table>
									</div>
									<!-- /.table-responsive -->
								</div>
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" style="display:none;" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							List of KERRY Report
						</div> 
						<div class="panel-body"> 
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('InputDate');?></th> 
											<th><?php echo $this->lang->line('PID');?></th>
											<th><?php echo $this->lang->line('AWB');?></th> 
											<th><?php echo $this->lang->line('ToPerson');?></th> 
											<th><?php echo $this->lang->line('Phone');?></th> 
											<th><?php echo $this->lang->line('Price');?></th> 
											 
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in modelDeviceList">  
											<td  ng-bind="item.input_date"></td>
											<td  ng-bind="item.pid"></td>
											<td  ng-bind="item.awb"></td>
											<td  ng-bind="item.to_person"></td>
											<td  ng-bind="item.phone"></td>
											<td  ng-bind="item.p_price | myShowCostFormat" class="text-right" ></td>
                                             
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
	<form id="TheForm" method="POST" enctype="application/x-www-form-urlencoded" action="<?php echo base_url('/Report/ExportKerryExcel');?>">
		<input type="hidden" name="searchJson" id="searchJson" />
	</form>
	<form id="TheForm2" method="POST" enctype="application/x-www-form-urlencoded" action="<?php echo base_url('/Report/ExportHandOverKerryExcel');?>">
		<input type="hidden" name="searchJson2" id="searchJson2" />
	</form>
	
</div>


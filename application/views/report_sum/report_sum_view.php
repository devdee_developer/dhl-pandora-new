<script src="<?php echo base_url('asset/reportSumController.js');?>"></script>
 <div  ng-controller="reportSumController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('ReportSum');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('ReportSum');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('ReportSum');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice"  >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('StartDate');?></label>
								<input class="form-control" ng-model="modelSearch.start_date"  data-date-format="dd-MM-yyyy" bs-datepicker >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('EndDate');?></label>
								<input class="form-control" ng-model="modelSearch.end_date" data-date-format="dd-MM-yyyy" bs-datepicker >
								<p class="help-block"></p>
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button> 
							
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="ExportExcel()"><i class="fa fa-download"></i> <span class="hidden-xs"><?php echo $this->lang->line('ExportExcel');?></span></button>  
							 
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ReportSum');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('PostalCode');?></label>
												<input class="form-control" ng-model="CreateModel.post_id" maxlength="10"  >
												<p class="CreateModel_post_id require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-4 col-md-4 col-xs-4"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Province');?></label>
												<input class="form-control" ng-model="CreateModel.province"  maxlength="10"  >
												<p class="CreateModel_province require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('District');?></label>
												<input class="form-control" ng-model="CreateModel.district" maxlength="10"  >
												<p class="CreateModel_district require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-4 col-md-4 col-xs-4"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Zone');?></label>
												<input class="form-control" ng-model="CreateModel.zone"  maxlength="10"  >
												<p class="CreateModel_zone require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div> 
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>
										</div>
									</div>
								</div>  
								
								
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th>recipient_name</th> 
													<th><?php echo $this->lang->line('Province');?></th>
													<th><?php echo $this->lang->line('District');?></th> 
													<th><?php echo $this->lang->line('Zone');?></th> 
													<th><?php echo $this->lang->line('Option');?></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="item in modelDeviceList">  
													<td  ng-bind="item.post_id"></td>
													<td  ng-bind="item.province"></td>
													<td  ng-bind="item.district"></td>
													<td  ng-bind="item.zone"></td>
													<td>
														<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr> 
											</tbody>
										</table>
									</div>
									<!-- /.table-responsive -->
								</div>
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" style="display:none;" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListOfSummary');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12"> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('AgentName');?></th> 
											<th><?php echo $this->lang->line('TotalQty');?></th>
											<th><?php echo $this->lang->line('TotalPrice');?></th>  
											 
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in modelDeviceList">  
											<td  ng-bind="item.name"></td>
											<td  ng-bind="item.count  | myShowCostFormat" class="text-right"></td>
											<td  ng-bind="item.price  | myShowCostFormat" class="text-right"></td>
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า --> 
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
	
	<form id="TheForm" method="POST" enctype="application/x-www-form-urlencoded" action="<?php echo base_url('/Report/ExportSummaryExcel');?>">
		<input type="hidden" name="searchJson" id="searchJson" />
	</form>
</div>


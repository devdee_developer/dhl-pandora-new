<script src="<?php echo base_url('asset/girthController.js');?>"></script>
 <div  ng-controller="girthController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('System');?></a></li>
		<li class="nav_active"> <?php echo $this->lang->line('Girth');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('Girth');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
        			  
			<!-- / create room types  -->
			<div class="row addDevice"  >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Girth');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('KERRY_WLH_LIMIT');?></label>
												<input class="form-control" ng-model="CreateModel.kerry" maxlength="4" type=number>
												<p class="help-block"></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12"> 
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('NIM_WLH_LIMIT');?></label>
												<input class="form-control" ng-model="CreateModel.nim" maxlength="4" type=number>
												<p class="help-block"></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12"> 
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('PEP_WLH_LIMIT');?></label>
												<input class="form-control" ng-model="CreateModel.pep" maxlength="4" type=number>
												<p class="help-block"></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12"> 
											</div> 
										</div>
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>
										</div>
									</div>
								</div>  
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types --> 
	</div>
</div>
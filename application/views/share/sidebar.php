<?php

$role_data =  $this->session->userdata('role_PANDASH');

?>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand"> Pandora-Dashboard</a>
	</div>
	<!-- /.navbar-header -->

	<ul class="nav navbar-top-links navbar-right">
		<li>
			<a href="http://localhost/dhl_sso/Mainmenu"><i class="fa fa-arrow-left"></i> Back to Mainmenu</a>

			<!-- <a href="https://th-logis.com/sitetest/dhl_sso/Mainmenu"><i class="fa fa-arrow-left"></i> Back to Mainmenu</a>   -->

			<!-- <a href="https://th-logis.com/dhl_sso/Mainmenu"><i class="fa fa-arrow-left"></i> Back to Mainmenu</a> -->
		</li>
		<!-- <li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#"><i class="fa fa-user fa-fw"></i><?php echo $this->session->userdata('user_name') ?></a>
				</li>
				<li><a href="<?php echo base_url('/Report/downloadWindowApp'); ?>"><i class="fa fa-user fa-fw"></i><?php echo  $this->lang->line('DownloadWindowApp'); ?></a>
				</li>
				<li class="divider"></li>
				<li><a href="<?php echo base_url('/Login/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> <?php echo $this->lang->line('Logout') ?></a>
				</li>
			</ul>
			/.dropdown-user
		</li> -->
	</ul>
	<!-- /.navbar-top-links -->

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<!-- <li>
					<a href="<?php echo base_url('/Dailyimport'); ?>"><i class="fa fa-upload" aria-hidden="true"></i> <?php echo $this->lang->line('Daily Import'); ?></a>
				</li>
				<li>
					<a href="<?php echo base_url('/Appleapicheck/reportkerry'); ?>"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $this->lang->line('reportkerry'); ?></a>
				</li>
				<li>
					<a href="#"><i class="fa fa-dashboard fa-fw"></i> <?php echo $this->lang->line('testapi'); ?><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/Servicecenter'); ?>"><?php echo $this->lang->line('Service Center'); ?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/Limitation'); ?>"><?php echo $this->lang->line('Limitation'); ?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/Postcode'); ?>"><?php echo $this->lang->line('postcode'); ?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/Appleapicheck/appleapi'); ?>"><?php echo $this->lang->line('appleapi'); ?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/Appleapicheck/getshipmentmodelliststatusview'); ?>"><?php echo $this->lang->line('getshipmentmodelliststatus'); ?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/Appleapicheck/shipmentnotcheck'); ?>"><?php echo $this->lang->line('shipmentnotcheck'); ?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/Appleapicheck/shipmentkerry'); ?>"><?php echo $this->lang->line('shipmentkerry'); ?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/Appleapicheck/checkreportkerry'); ?>"><?php echo $this->lang->line('Check Report Kerry'); ?></a>
						</li>
					</ul>
				</li>
				<li>
					<a href="<?php echo base_url('/upload/document/Document_AppleShipment.pdf'); ?>"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $this->lang->line('downloaddoccument'); ?></a>
				</li> -->
				<!-- <li>
					<a href="<?php echo base_url('/Cashadvance'); ?>"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $this->lang->line('cashadvance'); ?></a>
				</li> -->
				<!-- <li>
					<a href="#"><i class="fa fa-money fa-fw"></i> <?php echo $this->lang->line('cashadvance1'); ?><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/Cashadvance'); ?>"><i class="fa fa-angle-double-right fa-fw"></i> Import File</a>
						</li>
						<li>
							<a href="<?php echo base_url('ReportWHT'); ?>"><i class="fa fa-angle-double-right fa-fw"></i> PND Report</a>
						</li>
						<li>
							<a href="<?php echo base_url('ReportWHTCertificate'); ?>"><i class="fa fa-angle-double-right fa-fw"></i> Certificate Report</a>
						</li>

					</ul>
				</li> -->


				<!-- ////////////ส่วนทำ Sibar เป็น Drop Down ////////////////////////////////////////////////////////////////////////////////////// -->
				<!-- <li>
					<a href="#"><i class="fa fa-apple"></i> Pandora-Dashboard <span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('ReportPettyCashWHT'); ?>"><i class="fa fa-angle-double-right fa-fw"></i> PND Report</a>
						</li>
						<li>
							<a href="<?php echo base_url('ReportPettyCashWHTCertificate'); ?>"><i class="fa fa-angle-double-right fa-fw"></i> Certificate Report</a>
						</li>
						</li>
					</ul>
				</li> -->
				<!-- ////////////ส่วนทำ Sibar เป็น Drop Down ////////////////////////////////////////////////////////////////////////////////////// -->
				<!-- <?php if ($role_data['REP']) { ?>
					<li>
						<a href="<?php echo base_url('/ReportPerformanceDashboard'); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard </a>
					</li>
				<?php } ?> -->

				<?php if ($role_data['REP']) { ?>
					<!-- <li>
						<a href="#"><i class="fa fa-dashboard fa-fw"></i> Report<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<a href="<?php echo base_url('/ReportPerformanceDashboard'); ?>"> Performance Dashboard </a>
							</li>
							<li>
								<a href="<?php echo base_url('/ReportShipmentProfile'); ?>"> Shipment Profile </a>
							</li> -->
							<!-- <li>
								<a href="<?php echo base_url('/ReportOne'); ?>"> Report 1 </a>
							</li> -->
							<!-- <li>
								<a href="<?php echo base_url('/ReportTwo'); ?>"> Report 2 </a>
							</li> -->
							<!-- <li>
								<a href="<?php echo base_url('/ReportThree'); ?>"> Report 3 </a>
							</li> -->
							<!-- <li>
								<a href="<?php echo base_url('/ReportFour'); ?>"> Report 4 </a>
							</li> -->
						<!-- </ul> -->
					<!-- </li> -->
				<?php } ?>
				<?php if ($role_data['IMFILE']) { ?>
					<li>
						<a href="#"><i class="fa fa-dashboard fa-fw"></i> Import File<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<a href="<?php echo base_url('/ImportStatusCS'); ?>"> Import Shipment Status CS </a>
							</li>
							<li>
								<a href="<?php echo base_url('/ImportDaily'); ?>"> Import Shipment Daily </a>
							</li>
							<li>
								<a href="<?php echo base_url('/Import'); ?>"> Import Shipment Weekly</a>
							</li>
							<li>
								<a href="<?php echo base_url('/ImportWeeklyRevise'); ?>"> Updated Factor Incident</a>
							</li>
							<!-- <li>
							<a href="<?php echo base_url('/ImportEvent'); ?>"> Import Checkpoint Event </a>
						</li> -->
							<!-- <li>
								<a href="<?php echo base_url('/ImportMonthly'); ?>"> Import Shipment Monthly </a>
							</li> -->
						</ul>
					</li>
				<?php } ?>

				

				<?php if ($role_data['EFILE']) { ?>
					<li>
						<a href="<?php echo base_url('/Export'); ?>"><i class="fa fa-file-excel-o"></i> Export Shipment File </a>
					</li>
				<?php } ?>

				<?php if ($role_data['MAS']) { ?>
					<li>
						<a href="#"><i class="fa fa-dashboard fa-fw"></i> Master<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<a href="<?php echo base_url('/SlaAuNz'); ?>">SLA AU NZ </a>
							</li>
							<li>
								<a href="<?php echo base_url('/SlaSvcArea'); ?>">SLA SVC AREA </a>
							</li>
							<li>
								<a href="<?php echo base_url('/SlaZipcode'); ?>"> SLA ZIPCODE </a>
							</li>
							<li>
								<a href="<?php echo base_url('/WeekNumber'); ?>"> Week Number </a>
							</li>
							<!-- <li>
								<a href="<?php echo base_url('/Checkpoint'); ?>"> Checkpoint </a>
							</li> -->
						</ul>
					</li>
				<?php } ?>


				<!-- <li>
					<a href="#"><i class="fa fa-money fa-fw"></i> SAP <span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/Pnd3'); ?>"><i class="fa fa-angle-double-right fa-fw"></i>Import PND 3</a>
						</li>
						<li>
							<a href="<?php echo base_url('/Pnd53'); ?>"><i class="fa fa-angle-double-right fa-fw"></i>Import PND 53</a>
						</li>
						<li>
							<a href="<?php echo base_url('/Custodian'); ?>"><i class="fa fa-angle-double-right fa-fw"></i> Update Custodian</a>
						</li>
						<li>
							<a href="<?php echo base_url('ReportSAPWHT'); ?>"><i class="fa fa-angle-double-right fa-fw"></i> PND Report</a>
						</li>
						<li>
							<a href="<?php echo base_url('ReportSAPWHTCertificate'); ?>"><i class="fa fa-angle-double-right fa-fw"></i> WHT Certificate</a>
						</li>
				</li>
				<li>
					<a href="<?php echo base_url('/Company'); ?>"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <?php echo $this->lang->line('Company'); ?></a>
				</li>
			 -->
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>


<div id="page-wrapper">
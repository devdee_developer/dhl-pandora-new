<script src="<?php echo base_url('asset/exportController.js'); ?>"></script>
<div ng-controller="exportController" ng-init="onInit()">
	<div class="row">
		<ul class="navigator">
			<li class="nav_active">Export File</li>
		</ul>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Pandora Export File</h1>
		</div>
	</div>



	<div class="row exportdaily">
		<div class="col-lg-12">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#week">Weekly report</a></li>
				<!-- <li><a data-toggle="tab" href="#monthly">Monthly</a></li> -->
				<li><a data-toggle="tab" href="#period">Pandora Dashboard</a></li>
				<li><a data-toggle="tab" href="#csshipmentstatus ">CS Shipment Status </a></li>
			</ul>

			<div class="tab-content">
				<div class="panel panel-default tab-pane fade in active" id="week">
					<div class="panel-heading">
						<!-- <?php echo $this->lang->line('Browse File'); ?> -->
						Export Shipment File By Weekly report
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div role="form">
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger"></span>Year :</label>
											<ui-select ng-model="tempYearIndex.selected" theme="selectize">
												<ui-select-match>{{$select.selected.Year}}</ui-select-match>
												<ui-select-choices repeat="pYear in listYear | filter: $select.search">
													<span ng-bind-html="pYear.Year  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger"></span>Week No. :</label>
											<ui-select ng-model="tempWeekIndex.selected" theme="selectize">
												<ui-select-match>{{$select.selected.week_number}}</ui-select-match>
												<ui-select-choices repeat="pWeek in listWeek | filter: $select.search">
													<span ng-bind-html="pWeek.week_number  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-warning  waves-effect waves-light btn-sm m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
											<button class="btn btn-info waves-effect waves-light btn-sm m-b-5" ng-click="Exportexcel()"><i class="fa fa-download"></i>
												<spanclass="hidden-xs"> Export Shipment </span>
											</button>
											<!-- <button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="ExportWaitCheckpoint()"><i class="fa fa-download"></i>
											<spanclass="hidden-xs"> download shipment wait for checkpoint event </span>
										</button> -->
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default tab-pane fade" id="monthly">
					<div class="panel-heading">
						<!-- <?php echo $this->lang->line('Browse File'); ?> -->
						Export Shipment File By Month
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div role="form">
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger"></span>Year :</label>
											<ui-select ng-model="tempYearIndex.selected" theme="selectize">
												<ui-select-match>{{$select.selected.Year}}</ui-select-match>
												<ui-select-choices repeat="pYear in listYear | filter: $select.search">
													<span ng-bind-html="pYear.Year  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger"></span>Month :</label>
											<ui-select ng-model="tempMonthIndex.selected" theme="selectize">
												<ui-select-match>{{$select.selected.shipmonth}}</ui-select-match>
												<ui-select-choices repeat="pMonth in listMonth | filter: $select.search">
													<span ng-bind-html="pMonth.shipmonth  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-warning  waves-effect waves-light btn-sm m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
											<button class="btn btn-info waves-effect waves-light btn-sm m-b-5" ng-click="ExportexcelMonthly()"><i class="fa fa-download"></i>
												<spanclass="hidden-xs"> Export Shipment </span>
											</button>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default tab-pane fade" id="period">
					<div class="panel-heading">
						<!-- <?php echo $this->lang->line('Browse File'); ?> -->
						Export Shipment File By Pandora Dashboard
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div role="form">
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger"></span>Year :</label>
											<ui-select ng-model="tempYearPandoraDashboardIndex.selected" theme="selectize" ng-click="getYearPandoraDashboard()">
												<ui-select-match>{{$select.selected.Yearlist}}</ui-select-match>
												<ui-select-choices repeat="pYearPandoraDashboard in listYearPandoraDashboard | filter: $select.search">
													<span ng-bind-html="pYearPandoraDashboard.Yearlist  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger"></span>Quarter :</label>
											<ui-select ng-model="tempQuarterIndex.selected" theme="selectize" ng-click="getQuarter(item)">
												<ui-select-match>{{$select.selected.Quarterlist}}</ui-select-match>
												<ui-select-choices repeat="pQuarter in listQuarter | filter: $select.search">
													<span ng-bind-html="pQuarter.Quarterlist  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>

										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><?php echo $this->lang->line('StartDate'); ?> : </label>
											<input class="form-control" data-min-date={{minDate}} data-max-date="{{maxDate}}" ng-model="modelSearch.start_date" ng-disabled="tempQuarterIndex.selected == undefined" data-date-format="dd-MM-yyyy" bs-datepicker>
											<p class="CreateModel_start_date require text-danger"><?php echo $this->lang->line('Require'); ?></p>
											<p class="help-block"></p>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><?php echo $this->lang->line('EndDate'); ?> : </label>
											<input class="form-control" data-min-date={{minDate}} data-max-date="{{maxDate}}" ng-model="modelSearch.end_date" ng-disabled="tempQuarterIndex.selected == undefined" data-date-format="dd-MM-yyyy" bs-datepicker>
											<p class="CreateModel_end_date require text-danger"><?php echo $this->lang->line('Require'); ?></p>
											<p class="help-block"></p>
										</div>

										<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-warning  waves-effect waves-light btn-sm m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
											<button class="btn btn-info waves-effect waves-light btn-sm m-b-5" ng-click="ExportexcelDaily()"><i class="fa fa-download"></i>
												<spanclass="hidden-xs"> Export Shipment </span>
											</button>
											<!-- <button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="ExportWaitCheckpoint()"><i class="fa fa-download"></i>
											<spanclass="hidden-xs"> download shipment wait for checkpoint event </span>
										</button> -->
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default tab-pane fade" id="csshipmentstatus">
					<div class="panel-heading">
						<!-- <?php echo $this->lang->line('Browse File'); ?> -->
						Export Shipment File By CS Shipment Status
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div role="form">
									<div class="form-group col-lg-12 col-md-12 col-xs-12">

										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger"></span>Check Shipment Status :</label>
											<ui-select ng-model="tempCheckStatusIndex.selected" theme="selectize">
												<ui-select-match>{{$select.selected.CheckStatuslist}}</ui-select-match>
												<ui-select-choices repeat="pCheckStatus in listCheckStatus | filter: $select.search">
													<span ng-bind-html="pCheckStatus.CheckStatuslist  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-warning  waves-effect waves-light btn-sm m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
											<button class="btn btn-info waves-effect waves-light btn-sm m-b-5" ng-click="ExportexcelStatus()"><i class="fa fa-download"></i>
												<spanclass="hidden-xs"> Export Shipment </span>
											</button>
											<!-- <button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="ExportWaitCheckpoint()"><i class="fa fa-download"></i>
											<spanclass="hidden-xs"> download shipment wait for checkpoint event </span>
										</button> -->
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- 
	<div class="col-lg-12 col-md-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>
							<?php echo $this->lang->line('filename'); ?></th>

						<th><?php echo $this->lang->line('Date'); ?></th>
						<th><?php echo $this->lang->line('Option'); ?></th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="item in modelDeviceList">
						<td>{{item.file_name}}</td>
						<td ng-bind="item.upload_date"></td>
						<td>
							<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="onDownload(item)"><i class="fa fa-download"></i>
								<spanclass="hidden-xs"> Download </span>
							</button>
							<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div> -->
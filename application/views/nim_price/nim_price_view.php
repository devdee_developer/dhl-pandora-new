<script src="<?php echo base_url('asset/nimPriceController.js');?>"></script>
 <div  ng-controller="nimPriceController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('NIM_price');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('NIM_price');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('NIM_price');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('Weight_Max');?></label>
								<input class="form-control" ng-model="modelSearch.weight_max" maxlength="20"  >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12"> 
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('NIM_price');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Weight_Min');?></label>
												<input class="form-control" ng-model="CreateModel.weight_min" maxlength="10"  >
												<p class="CreateModel_weight_min require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-4 col-md-4 col-xs-4"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Weight_Max');?></label>
												<input class="form-control" ng-model="CreateModel.weight_max"  maxlength="10"  >
												<p class="CreateModel_weight_max require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Bkk');?></label>
												<input class="form-control" ng-model="CreateModel.bkk" maxlength="10"  >
												<p class="CreateModel_bkk require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-4 col-md-4 col-xs-4"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Mid');?></label>
												<input class="form-control" ng-model="CreateModel.mid"  maxlength="10"  >
												<p class="CreateModel_mid require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div><div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('East');?></label>
												<input class="form-control" ng-model="CreateModel.east" maxlength="10"  >
												<p class="CreateModel_east require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div>  
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('NorthTop');?></label>
												<input class="form-control" ng-model="CreateModel.north_top" maxlength="10"  >
												<p class="CreateModel_north_top require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-4 col-md-4 col-xs-4"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('NorthBottom');?></label>
												<input class="form-control" ng-model="CreateModel.north_bottom"  maxlength="10"  >
												<p class="CreateModel_north_bottom require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div>
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('NorthEastTop');?></label>
												<input class="form-control" ng-model="CreateModel.north_east_top" maxlength="5" >
												<p class="CreateModel_north_east_top require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('NorthEastBottom');?></label>
												<input class="form-control" ng-model="CreateModel.north_east_bottom" maxlength="10"  >
												<p class="CreateModel_north_east_bottom require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-4 col-md-4 col-xs-4"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('SouthTop');?></label>
												<input class="form-control" ng-model="CreateModel.south_top"  maxlength="10"  >
												<p class="CreateModel_south_top require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div>
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('SouthMid');?></label>
												<input class="form-control" ng-model="CreateModel.south_mid" maxlength="5" >
												<p class="CreateModel_south_mid require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-4 col-md-4 col-xs-4">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('SouthBottom');?></label>
												<input class="form-control" ng-model="CreateModel.south_bottom" maxlength="10"  >
												<p class="CreateModel_south_bottom require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-4 col-md-4 col-xs-4"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('SouthRed');?></label>
												<input class="form-control" ng-model="CreateModel.south_red"  maxlength="10"  >
												<p class="CreateModel_south_red require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>
										</div>
									</div>
								</div>  
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListOfNimPrice');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('Weight_Min');?></th> 
											<th><?php echo $this->lang->line('Weight_Max');?></th>
											<th><?php echo $this->lang->line('Bkk');?></th> 
											<th><?php echo $this->lang->line('Mid');?></th>
											<th><?php echo $this->lang->line('East');?></th> 
											<th><?php echo $this->lang->line('NorthTop');?></th>
											<th><?php echo $this->lang->line('NorthBottom');?></th>
											<th><?php echo $this->lang->line('NorthEastTop');?></th>
											<th><?php echo $this->lang->line('NorthEastBottom');?></th>
											<th><?php echo $this->lang->line('SouthTop');?></th> 
											<th><?php echo $this->lang->line('SouthMid');?></th>
											<th><?php echo $this->lang->line('SouthBottom');?></th>
											<th><?php echo $this->lang->line('SouthRed');?></th>
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in modelDeviceList">
											<td class="text-right" ng-bind="addCommas(item.weight_min)"></td> 
											<td class="text-right" ng-bind="addCommas(item.weight_max)"></td> 
											<td class="text-right" ng-bind="addCommas(item.bkk)"></td>
											<td class="text-right" ng-bind="addCommas(item.mid)"></td> 
											<td class="text-right" ng-bind="addCommas(item.east)"></td>
											<td class="text-right" ng-bind="addCommas(item.north_top)"></td> 
											<td class="text-right" ng-bind="addCommas(item.north_bottom)"></td>
											<td class="text-right" ng-bind="addCommas(item.north_east_top)"></td>
											<td class="text-right" ng-bind="addCommas(item.north_east_bottom)"></td> 
											<td class="text-right" ng-bind="addCommas(item.south_top)"></td>
											<td class="text-right" ng-bind="addCommas(item.south_mid)"></td> 
											<td class="text-right" ng-bind="addCommas(item.south_bottom)"></td> 
											<td class="text-right" ng-bind="addCommas(item.south_red)"></td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>
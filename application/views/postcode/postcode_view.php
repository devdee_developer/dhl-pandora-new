<script src="<?php echo base_url('asset/postcodeController.js');?>"></script>
<div  ng-controller="postcodeController" ng-init="onInit()">
 
    <div class="row">
        <ul class="navigator">
            <li class="nav_active"> <?php echo $this->lang->line('postcode');?></li>
        </ul>
    </div>

    <div class="row" >
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $this->lang->line('postcode');?></h1>
		</div>
    </div>

    <div class="row addDevice" style="display:none;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo $this->lang->line('postcode');?>
			</div>
			<div class="panel-body"> 
				<div class="col-lg-10 col-md-10 col-xs-10">
					<div role="form">
						<div class="form-group col-lg-12 col-md-12 col-xs-12"> 
                            <div class="col-lg-3 col-md-3 col-xs-3">
                            </div>  
							<div class="col-lg-3 col-md-3 col-xs-3">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('postcode');?></label>
								<input class ="form-control" ng-model="CreateModel.postcode" >
							</div>
						</div>
                        <div class="form-group text-right">
							<br/><br/>
							<div class="col-lg-10 col-md-10 col-xs-10">
								<button class="btn btn-primary"  ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
								<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
							</div>
						</div>
					</div>  
				</div>
			</div>
		</div>
    </div>

	<div class="row DisplayDevice" >
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('postcode');?>
				</div>
				<div class="panel-body">
					<div class="row">
                    <div class="col-lg-1 col-md-1 col-xs-12">
                    </div>
						<div class="col-lg-10 col-md-10 col-xs-10">
							<div role="form">
                                <div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-1 col-md-1 col-xs-12">
										<label><span class="text-danger" ></span><?php echo $this->lang->line('Browse File');?></label>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-12">
										{{file.name}}
									</div>
									<div class="col-md-1 col-sm-1 col-xs-12" ngf-select ng-model="file" name="file" ngf-pattern="'.xls,.xlsx'"
                 						ngf-accept="'.xls,.xlsx'" ngf-max-size="10MB" ngf-min-height="100" >
                 						<span class="btn btn-primary"><?php echo $this->lang->line('Browse File');?></span>
									</div>
									<div class="col-lg-1 col-md-1 col-xs-12">
										<span class="btn btn-primary" ng-click="upload()" ><i class="fa fa-upload"></i><?php echo $this->lang->line('Upload');?></span>
									</div>
									<div class="col-lg-1 col-md-1 col-xs-12">
										<span class="btn btn-primary" ng-click="download()" ><i class="fa fa-download"></i><?php echo $this->lang->line('download');?></span>
									</div>
								</div>
                                
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive"> 
										<table class="table table-striped">
											<thead>
												<tr>  
                                                    <th><?php echo $this->lang->line('day');?></th>
													<th><?php echo $this->lang->line('postcode');?></th>
                                                    <th><?php echo $this->lang->line('first_priority');?></th>
                                                    <th><?php echo $this->lang->line('second_priority');?></th>
													<th><?php echo $this->lang->line('upload_date');?></th>
                                                    
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="item in listpostcode">
                                                    <td hidden ng-bind="item.id"></td> 
                                                    <td ng-bind="item.day"></td>
                                                    <td ng-bind="item.postcode"></td>
                                                    <td ng-bind="item.first_priority"></td>
                                                    <td ng-bind="item.second_priority"></td>
                                                    <td ng-bind="item.date"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
                                <div class="row tblResult small"  >
									<div class="col-md-7 col-sm-7 col-xs-12 ">
										<label class="col-md-4 col-sm-4 col-xs-12">
											<?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
										</label>
										<label class="col-md-4 col-sm-4 col-xs-12">
											<?php echo $this->lang->line('ResultsPerPage');?>
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12 ">
											<ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
												<ui-select-match>{{$select.selected.Value}}</ui-select-match>
												<ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
													<span ng-bind-html="pSize.Text | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
									</div>
									<div class="col-md-5 col-sm-5 col-xs-12  ">
										<label class="col-md-4 col-sm-4 col-xs-12">
											<span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
										</label>
										<div class="col-md-3 col-sm-3 col-xs-12">
											<ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
												<ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
												<ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
													<span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<label class="col-md-4 col-sm-4 col-xs-12">
											/ {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
										</label>
									</div>
								</div>  
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script src="<?php echo base_url('asset/reportOneController.js'); ?>"></script>
<!-- <link href="<?php echo base_url('theme/bootstrap/css/newstyle.css'); ?>" rel="stylesheet"> -->
<div ng-controller="reportOneController" ng-init="onInit()">

	<!-- <div class="row">
		<ul class="navigator">
			<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Vendor');?></a></li>*/ ?>
			<li class="nav_active"> Mapping code Apple & DHL </li>
		</ul>
	</div> -->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"> Shipment profile by product & Trade lane volume </h1>
		</div>
	</div>

	<div class="panel-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12">
				<div role="form">
					<div class="form-group col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<label><span class="text-danger"></span>Year :</label>
							<ui-select ng-model="tempYearIndex.selected"   theme="selectize"  >
									<ui-select-match>{{$select.selected.Year}}</ui-select-match>
									<ui-select-choices repeat="pYear in listYear | filter: $select.search">
										<span ng-bind-html="pYear.Year  | highlight: $select.search"></span>
									</ui-select-choices>
							</ui-select>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12">
							<label><span class="text-danger"></span>Quater :</label>
							<ui-select ng-model="tempQuaterIndex.selected"   theme="selectize"  >
									<ui-select-match>{{$select.selected.Quater}}</ui-select-match>
									<ui-select-choices repeat="pQuater in listQuater | filter: $select.search">
										<span ng-bind-html="pQuater.Quater  | highlight: $select.search"></span>
									</ui-select-choices>
							</ui-select>
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button class="btn btn-warning  waves-effect waves-light btn-sm m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="getChart()"><i></i>
								<spanclass="hidden-xs"> Run Chart </span>
							</button>
							<button class="btn btn-info waves-effect waves-light btn-sm m-b-5" ng-click="Exportreport()"><i class="fa fa-download"></i>
								<spanclass="hidden-xs"> Export </span>
							</button>
						</div>
					</div>

				</div>
			</div>


			<div class="container">
                <div class="panel panel-default">
                    <div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<canvas id="productChart" width="400" height="400"></canvas>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12">
								<canvas id="laneChart" width="400" height="400"></canvas>
							</div>
						</div>

						<div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
							<div class="col-lg-6 col-md-6 col-xs-12" >
								<table class="table table-striped text-center table-bordered table-hover" style="width: 98%;">
									<tbody>
										<tr>
											<td width="9%"><b>Month</b></td>
											<td>{{modelDeviceList.month.month1}}</td>
											<td>{{modelDeviceList.month.month2}}</td>
											<td>{{modelDeviceList.month.month3}}</td>
										</tr>
										<tr>
											<td style="background-color:rgb(75,192,192);"><b>DOX</b></td>
											<td>{{modelDeviceList.dox.mon3}}</td>
											<td>{{modelDeviceList.dox.mon2}}</td>
											<td>{{modelDeviceList.dox.mon1}}</td>
										</tr>
										<tr>
											<td style="background-color:rgb(255,205,86);"><b>WPX</b></td>
											<td>{{modelDeviceList.wpx.mon3}}</td>
											<td>{{modelDeviceList.wpx.mon2}}</td>
											<td>{{modelDeviceList.wpx.mon1}}</td>
										</tr>
										<tr>
											<td  width="9%" style="background-color:rgb(255,159,64);"><b>BBX</b></td>
											<td>{{modelDeviceList.bbx.mon3}}</td>
											<td>{{modelDeviceList.bbx.mon2}}</td>
											<td>{{modelDeviceList.bbx.mon1}}</td>
										</tr>
										<tr>
											<td style="background-color:rgb(255,99,132);"><b>Total</b></td>
											<td>{{modelDeviceList.total.mon3}}</td>
											<td>{{modelDeviceList.total.mon2}}</td>
											<td>{{modelDeviceList.total.mon1}}</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12">
								<table class="table table-striped text-center table-bordered table-hover" style="width: 98%;">
									<tbody>	
										<tr>
											<td width="9%"><b>Month</b></td>
											<td>{{modelDeviceList.month.month1}}</td>
											<td>{{modelDeviceList.month.month2}}</td>
											<td>{{modelDeviceList.month.month3}}</td>
										</tr>
										<tr>
											<td style="background-color:rgb(75,192,192);" width="9%"><b>ROA</b></td>
											<td>{{modelDeviceList.roa.mon3}}</td>
											<td>{{modelDeviceList.roa.mon2}}</td>
											<td>{{modelDeviceList.roa.mon1}}</td>
										</tr>
										<tr>
											<td style="background-color:rgb(255,205,86);"><b>PJC</b></td>
											<td>{{modelDeviceList.pjc.mon3}}</td>
											<td>{{modelDeviceList.pjc.mon2}}</td>
											<td>{{modelDeviceList.pjc.mon1}}</td>
										</tr>
										<tr>
											<td style="background-color:rgb(255,159,64);"><b>PAC</b></td>
											<td>{{modelDeviceList.pac.mon3}}</td>
											<td>{{modelDeviceList.pac.mon2}}</td>
											<td>{{modelDeviceList.pac.mon1}}</td>
										</tr>
										<!-- <tr>
											<td style="background-color:rgb(6,57,81);"><b>Total</b></td>
											<td>{{modelDeviceList.roa.mon1+modelDeviceList.pjx.mon1+modelDeviceList.pac.mon1}}</td>
											<td>{{modelDeviceList.roa.mon2+modelDeviceList.pjx.mon2+modelDeviceList.pac.mon2}}</td>
											<td>{{modelDeviceList.roa.mon3+modelDeviceList.pjx.mon3+modelDeviceList.pac.mon3}}</td>
										</tr> -->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
	
</div>

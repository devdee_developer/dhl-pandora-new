<script src="<?php echo base_url('asset/reportTwoController.js'); ?>"></script>
<!-- <link href="<?php echo base_url('theme/bootstrap/css/newstyle.css'); ?>" rel="stylesheet"> -->
<div ng-controller="reportTwoController" ng-init="onInit()">

    <!-- <div class="row">
		<ul class="navigator">
			<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Vendor');?></a></li>*/ ?>
			<li class="nav_active"> Mapping code Apple & DHL </li>
		</ul>
	</div> -->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> Transit time performance </h1>
        </div>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div role="form">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger"></span>Year :</label>
                            <ui-select ng-model="tempYearIndex.selected" theme="selectize">
                                <ui-select-match>{{$select.selected.Year}}</ui-select-match>
                                <ui-select-choices repeat="pYear in listYear | filter: $select.search">
                                    <span ng-bind-html="pYear.Year  | highlight: $select.search"></span>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger"></span>Quater :</label>
                            <ui-select ng-model="tempQuaterIndex.selected" theme="selectize">
                                <ui-select-match>{{$select.selected.Quater}}</ui-select-match>
                                <ui-select-choices repeat="pQuater in listQuater | filter: $select.search">
                                    <span ng-bind-html="pQuater.Quater  | highlight: $select.search"></span>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <button class="btn btn-warning  waves-effect waves-light btn-sm m-b-5"
                                ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span
                                    class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
                            <button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"
                                ng-click="getChart()"><i></i>
                                <spanclass="hidden-xs"> Run Chart </span>
                            </button>
                            <button class="btn btn-info waves-effect waves-light btn-sm m-b-5"
                                ng-click="Exportreport()"><i class="fa fa-download"></i>
                                <spanclass="hidden-xs"> Export </span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <canvas id="" width="400" height="400"></canvas>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12">
                                <canvas id="onTimePerformance" width="400" height="400"></canvas>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <canvas id="onTimePerformanceQuarterly" width="400" height="400"></canvas>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12" align="right">
                            AVG {{modelDeviceList.avg}}%
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <table class="table table-striped text-center table-bordered table-hover"
                                    style="width: 98%;">
                                    <tbody>
                                        <tr>
                                            <td width="9%"><b>Month</b></td>
                                            <td>{{modelDeviceList.month.month1}}</td>
                                            <td>{{modelDeviceList.month.month2}}</td>
                                            <td>{{modelDeviceList.month.month3}}</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:rgb(75,192,192);"><b>Actual On time (%)</b></td>
                                            <td>{{modelDeviceList.ontime.mon3}}%</td>
                                            <td>{{modelDeviceList.ontime.mon2}}%</td>
                                            <td>{{modelDeviceList.ontime.mon1}}%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
                            <div class="col-lg-5 col-md-5 col-xs-12">
                                <canvas id="dhlSupport" width="400" height="400"></canvas>
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-12">
                                <canvas id="" width="400" height="400"></canvas>
                            </div>
                            <div class="col-lg-5 col-md-5 col-xs-12">
                                <canvas id="uncontrollable" width="400" height="400"></canvas>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
                            <div class="col-lg-6 col-md-6 col-xs-12">
                                <table class="table table-striped text-center table-bordered table-hover"
                                    style="width: 98%;">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td ng-repeat="item in dataDHLsupport" ng-bind="item.factor_incident">
                                                <b>Factor
                                                    Incident</b></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:rgb(120, 185, 255);"><b>%</b></td>
                                            <td ng-repeat="item in percentdhl track by $index">{{item}}%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12">
                                <table class="table table-striped text-center table-bordered table-hover"
                                    style="width: 98%;">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td ng-repeat="item in dataUncontrollable" ng-bind="item.factor_incident">
                                                <b>Factor
                                                    Incident</b></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:rgb(255,99,132);"><b>%</b></td>
                                            <td ng-repeat="item in percentuncontrollable track by $index">{{item}}%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:50px">
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <canvas id="" width="400" height="400"></canvas>
                            </div>
                            <div class="col-lg-5 col-md-5 col-xs-12">
                                <canvas id="customerSupport" width="400" height="400"></canvas>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
                            <div class="col-lg-2 col-md-2 col-xs-12">

                            </div>
                            <div class="col-lg-8 col-md-8 col-xs-12">
                                <table class="table table-striped text-center table-bordered table-hover"
                                    style="width: 98%;">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td ng-repeat="item in dataCustomersupport" ng-bind="item.factor_incident">
                                                <b>Factor
                                                    Incident</b></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:rgb(75,192,192);"><b>%</b></td>
                                            <td ng-repeat="item in percentcustomer track by $index">{{item}}%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

</div>
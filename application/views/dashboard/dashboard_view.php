<script src="<?php echo base_url('asset/dashboardController.js'); ?>"></script>
<!-- <link href="<?php echo base_url('theme/bootstrap/css/newstyle.css'); ?>" rel="stylesheet"> -->
<div ng-controller="dashboardController" ng-init="onInit()">

	<!-- <div class="row">
		<ul class="navigator">
			<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Vendor');?></a></li>*/ ?>
			<li class="nav_active"> Mapping code Apple & DHL </li>
		</ul>
	</div> -->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"> Pandora-Dashboard </h1>
		</div>

	</div>
	<div class="panel panel-primary">
		<!-- <div class="panel panel-default" style="background:LightSkyBlue;margin:1.5%"> -->
		<div class="panel-heading" style="background:LightSkyBlue;height:50px">
			<div class="col-lg-6">
				<label style="font-size:20px">SDD : {{SddView | date:"y-MM-dd"}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Last Update : {{last_update | date:"dd-MM-y" }}</label>
			</div>
			<div class="col-lg-3 text-right">
			<label style="font-size:20px">SDD Status : </label>
			</div>
			<div class="col-lg-3" style="margin-top: -3px;">
					<ui-select ng-model="TempSddStatusIndex.selected" theme="selectize" ng-change="reload();">
						<ui-select-match placeholder="Please Select">
							{{$select.selected.Status}}
						</ui-select-match>
						<ui-select-choices repeat="pIndex in listSddStatus | filter: $select.search">
							<span ng-bind-html="pIndex.Status"></span>
						</ui-select-choices>
					</ui-select>
			</div>
		</div>


		<div class="panel-body">


			<div class="form-group col-lg-12 col-md-12 col-xs-12 text-right">

				<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="Exportexcel(item)"><i class="fa fa-download"></i> <span class="hidden-xs"> Download today SDD </span></button>
				<!-- <button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="ExportDowloadAllShipment()"><i class="fa fa-download"></i> <span class="hidden-xs">Download all shipment </span></button> -->

			</div>



			<div class="form-group col-lg-12 col-md-12 col-xs-12 " style="border-style:solid; border-color:#FB4D3B;padding:0">
				<div class="form-group col-lg-12 col-md-12 col-xs-12">
					<div class="col-lg-6 col-md-6 col-xs-12">
						<div class="chart-container text-center">
							<h4>
								<p style="color:#6F6D6D;padding-top:20px"><b> Percent By Status </b></p>
							</h4>
							<canvas id="GenGraphPieByToday" style="height:200px"></canvas>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-xs-12 text-center">
						<div class="chart-container">
							<h4>
								<p style="color:#6F6D6D;padding-top:20px"><b> Total By Status </b></p>
							</h4>
							<canvas id="GengraphBarBytoDay" style="height:200px"></canvas>
						</div>
					</div>
				</div>

				<div class="form-group col-lg-12 col-md-12 col-xs-12">
				</div>

				<div class="form-group col-lg-12 col-md-12 col-xs-12">
				</div>
				<div class="form-group col-lg-12 col-md-12 col-xs-12">
				</div>





				<style>
					table,
					th,
					td {
						border: 1px solid black;
					}
				</style>

				<style>
					th,
					td {
						text-align: center;
					}
				</style>

				<div class="col-lg-12 col-md-12 col-xs-12">
					<div class="table-responsive text-center">
						<table class="table table-striped text-center">

							<thead>
								<tr>
									<th rowspan="2" style="background-color:#FAF9BD;">Total</th>
									<th colspan="4" style="background-color:#B7F8AD;"><a style="cursor: pointer;" ng-click="Exportexcel('Completion','group')">Completion</a></th>
									<th colspan="4" style="background-color:#FBE3C4;"><a style="cursor: pointer;" ng-click="Exportexcel('On progress','group')">On progress</a></th>
									<th colspan="2" style="background-color:#FAB3B3;"><a style="cursor: pointer;" ng-click="Exportexcel('Exception','group')">Exception</a></th>
									<!-- <p style="color:#ffff">Exception</p> -->
									<th colspan="2" style="background-color:#D6D6D5;"><a style="cursor: pointer;" ng-click="Exportexcel('Other','group')">Other</a></th>
								</tr>
								<tr>
									<th style="background-color:#B7F8AD;"><a style="cursor: pointer;" ng-click="Exportexcel('OK','status')">OK</a></th>
									<th style="background-color:#B7F8AD;">%</th>
									<th style="background-color:#B7F8AD;"><a style="cursor: pointer;" ng-click="Exportexcel('RT','status')">RT</a></th>
									<th style="background-color:#B7F8AD;">%</th>
									<th style="background-color:#FBE3C4;"><a style="cursor: pointer;" ng-click="Exportexcel('WC','status')">WC</a></th>
									<th style="background-color:#FBE3C4;">%</th>
									<th style="background-color:#FBE3C4;"><a style="cursor: pointer;" ng-click="Exportexcel('FD','status')">FD</a></th>
									<th style="background-color:#FBE3C4;">%</th>
									<th style="background-color:#FAB3B3;"><a style="cursor: pointer;" ng-click="Exportexcel('BA,NH,AD,RD','status')">BA,NH,AD,RD</a></th>
									<th style="background-color:#FAB3B3;">%</th>
									<th style="background-color:#D6D6D5;"><a style="cursor: pointer;" ng-click="Exportexcel('Other','group')">Other</th>
									<th style="background-color:#D6D6D5;">%</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in modelDeviceList">
									<td ng-bind="item.total"></td>

									<td style="background-color: #B7F8AD;"><b ng-bind="item.num_of_ok | number"></b></td>
									<td ng-bind="item.percent_of_ok | number:2"></td>

									<td style="background-color: #B7F8AD;"><b ng-bind="item.num_of_rt | number"></b></td>
									<td ng-bind="item.percent_of_rt | number:2"></td>

									<td style="background-color: #FBE3C4;"><b ng-bind="item.num_of_wc | number"></b></td>
									<td ng-bind="item.percent_of_wc | number:2"></td>

									<td style="background-color: #FBE3C4;"><b ng-bind="item.num_of_fd | number"></b></td>
									<td ng-bind="item.percent_of_fd | number:2"></td>

									<td style="background-color: #FAB3B3;"><b ng-bind="item.num_of_exception | number"></b></td>
									<td ng-bind="item.percent_of_exception | number:2"></td>

									<td style="background-color: #D6D6D5;"><b ng-bind="item.num_of_other | number"></b></td>
									<td ng-bind="item.percent_of_other | number:2"></td>
									<!-- <td>
											<button ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
											<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
										</td> -->
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
				</div>
			</div>
			<div class="form-group col-lg-12 col-md-12 col-xs-12">
			</div>

			<div class="form-group col-lg-12 col-md-12 col-xs-12">
			</div>



			<div class="form-group col-lg-12 col-md-12 col-xs-12 " style="border-style:solid; border-color:#FB4D3B;padding:25px 0px 25px">
				<div class="form-group col-lg-12 col-md-12 col-xs-12">
					<div class="col-lg-8 col-md-8 col-xs-12">
						<div class="chart-container text-center">
							<h4>
								<p style="color:#6F6D6D"><b> Percent By Servicecenter </b></p>
							</h4>
							<canvas id="GenGraphPieByServiceCenter" style="height:200px"></canvas>
						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-xs-12">
						<div class="table-responsive text-center">
							<table class="table table-striped">
								<thead>
									<tr>
										<th width="30%" style="background-color:#fcdab7;"> On progress by SVC </th>
										<th width="30%" style="background-color:#fcdab7;"> No.of WB </th>
										<th width="30%" style="background-color:#fcdab7;"> % </th>

									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="item in serviceCenterList">
										<td><a style="cursor: pointer;" ng-click="Exportexcel(item.service_center,'servicecenter')">{{item.service_center}}</a></td>
										<td ng-bind="item.count_total | number"></td>
										<td ng-bind="item.percent | number:2"></td>

										<!-- <td>
											<button ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
											<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
										</td> -->
									</tr>
									<tr>
										<td> Total</td>
										<td>{{TotalOnprogress | number}}</td>
										<td> 100.00 </td>

										<!-- <td>
											<button ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
											<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
										</td> -->
									</tr>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>

				</div>


				<div class="form-group col-lg-12 col-md-12 col-xs-12">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="chart-container text-center">
							<h4>
								<p style="color:#6F6D6D"><b> Total By Servicecenter </b></p>
							</h4>
							<canvas id="GenGraphBarByServiceCenter" style="height:200px"></canvas>
						</div>
					</div>
				</div>


			</div>



			<!-- <div class="form-group col-lg-12 col-md-12 col-xs-12">
			</div> -->




			<!-- <div class="form-group col-lg-12 col-md-12 col-xs-12">
				<div class="form-group col-lg-12 col-md-12 col-xs-12">
					<div class="col-lg-2 col-md-2 col-xs-12" style="width:20%">
						<div class="market-updates">
							<div class="col-md-20  market-update-gd">
								<div class="market-update-block clr-block-21">
									<div class="col-md-20 market-update-left" style="height:200px">
										<h4><label style="color:gray"> Total </label></h4> <br>

										<h4><label style="color:gray"> Total : </label> <label ng-bind="modelDeviceList.total | number" style="color:gray"></label></h4>
										
									</div>
								
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-xs-12" style="width:20%">
						<div class="market-updates">
							<div class="col-md-20  market-update-gd">
								<div class="market-update-block clr-block-23">
									<div class="col-md-20 market-update-left" style="height:200px">
										<h4><label style="color:gray"> Completion </label></h4> <br>

										<h4><label style="color:gray"> OK : </label> <label style="color:gray"> {{modelDeviceList.num_of_ok | number}}</label></h4>
										<h4><label style="color:gray"> Percent : </label> <label style="color:gray"> {{modelDeviceList.percent_of_ok | number:2}} %</label></h4><br>

										<h4><label style="color:gray"> RT : </label> <label style="color:gray"> {{modelDeviceList.num_of_rt | number}}</label></h4>
										<h4><label style="color:gray">Percent : </label> <label style="color:gray"> {{modelDeviceList.percent_of_rt | number:2}} %</label></h4>

										
									</div>
									
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-xs-12" style="width:20%">
						<div class="market-updates">
							<div class="col-md-20  market-update-gd">
								<div class="market-update-block clr-block-24">
									<div class="col-md-20 market-update-left" style="height:200px">
										<h4><label style="color:gray"> On progress </label></h4> <br>

										<h4><label style="color:gray">WC :</label> <label style="color:gray"> {{modelDeviceList.num_of_wc | number}}</label></h4>

										<h4><label style="color:gray">Percent : </label> <label style="color:gray"> {{modelDeviceList.percent_of_wc | number:2}} %</label></h5> <br>

											<h4><label style="color:gray"> FD :</label> <label style="color:gray"> {{modelDeviceList.num_of_fd | number}}</label></h4>

											<h4><label style="color:gray">Percent :</label> <label style="color:gray"> {{modelDeviceList.percent_of_fd | number:2}} %</label></h4>
											เว้นวรรค &nbsp 

									</div>
								
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-xs-12" style="width:20%">
						<div class="market-updates">
							<div class="col-md-20  market-update-gd">
								<div class="market-update-block clr-block-25">
									<div class="col-md-20 market-update-left" style="height:200px">
										<h4><label style="color:white"> Exception </label></h4> <br>

										<h4><label style="color:white"> BA,NH,AD,RD : </label> <label style="color:white"> {{modelDeviceList.num_of_exception | number}}</label></h4> <br>

										<h4><label style="color:white"> Percent : </label> <label style="color:white"> {{modelDeviceList.percent_of_exception | number:2}} %</label></h4>

										
									</div>
									
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-xs-12" style="width:20%">
						<div class="market-updates">
							<div class="col-md-20  market-update-gd">
								<div class="market-update-block clr-block-26">
									<div class="col-md-20 market-update-left" style="height:200px">
										<h4><label style="color:white"> Other </label></h4> <br>

										<h4><label style="color:white"> Other : </label> <label style="color:white"> {{modelDeviceList.num_of_other | number}}</label></h4> <br>

										<h4><label style="color:white"> Percent : </label> <label style="color:white">{{modelDeviceList.percent_of_other | number:2}} %</label></h4>

									</div>

									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>

			</div> -->
		</div>
		<!-- </div> -->
		<!-- end disposal -->


	</div>
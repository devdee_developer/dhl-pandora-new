<script src="<?php echo base_url('asset/reportFourController.js'); ?>"></script>
<!-- <link href="<?php echo base_url('theme/bootstrap/css/newstyle.css'); ?>" rel="stylesheet"> -->
<div ng-controller="reportFourController" ng-init="onInit()">

    <!-- <div class="row">
		<ul class="navigator">
			<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Vendor');?></a></li>*/ ?>
			<li class="nav_active"> Mapping code Apple & DHL </li>
		</ul>
	</div> -->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> Guideline ship day AU/NZ </h1>
        </div>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div role="form">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger"></span>Year :</label>
                            <ui-select ng-model="tempYearIndex.selected" theme="selectize">
                                <ui-select-match>{{$select.selected.Year}}</ui-select-match>
                                <ui-select-choices repeat="pYear in listYear | filter: $select.search">
                                    <span ng-bind-html="pYear.Year  | highlight: $select.search"></span>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger"></span>Quater :</label>
                            <ui-select ng-model="tempQuaterIndex.selected" theme="selectize">
                                <ui-select-match>{{$select.selected.Quater}}</ui-select-match>
                                <ui-select-choices repeat="pQuater in listQuater | filter: $select.search">
                                    <span ng-bind-html="pQuater.Quater  | highlight: $select.search"></span>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <button class="btn btn-warning  waves-effect waves-light btn-sm m-b-5"
                                ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span
                                    class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
                            <button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"
                                ng-click="getChart()"><i></i>
                                <spanclass="hidden-xs"> Run Chart </span>
                            </button>
                            <button class="btn btn-info waves-effect waves-light btn-sm m-b-5"
                                ng-click="Exportreport()"><i class="fa fa-download"></i>
                                <spanclass="hidden-xs"> Export </span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <canvas id="" width="400" height="0"></canvas>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <canvas id="" width="400" height="0"></canvas>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <canvas id="" width="400" height="0"></canvas>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12" >
                                    <table class="table table-striped text-center table-bordered table-hover"
                                        style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <b>Day</b>
                                                </td>
                                                <td>
                                                    <b>Count of Ontime</b>
                                                </td>
                                                <td>
                                                    <b>No# Delay flight connection</b>
                                                </td>
                                                <td>
                                                    <b>% Delay flight connection</b>
                                                </td>
                                                <td>
                                                    <b>Total shpt PU on each day</b>
                                                </td>
                                            <tr>
                                            <tr>
                                                <td colspan="5" style="background-color:rgb(255,230,153);">
                                                    <b>AKL</b>
                                                </td>
                                            <tr>
                                                <td ><b>MON</b></td>
                                                <td ng-repeat="item in aklmon track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>TUE</b></td>
                                                <td ng-repeat="item in akltue track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>WED</b></td>
                                                <td ng-repeat="item in aklwed track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>THU</b></td>
                                                <td ng-repeat="item in aklthu track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>FRI</b></td>
                                                <td ng-repeat="item in aklfri track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>SAT</b></td>
                                                <td ng-repeat="item in aklsat track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" style="background-color:rgb(255,230,153);">
                                                    <b>BNE</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td ><b>MON</b></td>
                                                <td ng-repeat="item in bnemon track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>TUE</b></td>
                                                <td ng-repeat="item in bnetue track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>WED</b></td>
                                                <td ng-repeat="item in bnewed track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>THU</b></td>
                                                <td ng-repeat="item in bnethu track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>FRI</b></td>
                                                <td ng-repeat="item in bnefri track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>SAT</b></td>
                                                <td ng-repeat="item in bnesat track by $index">{{item}}</td>
                                            </tr>
                                                <td colspan="5" style="background-color:rgb(255,230,153);">
                                                    <b>MEL</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td ><b>MON</b></td>
                                                <td ng-repeat="item in melmon track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>TUE</b></td>
                                                <td ng-repeat="item in meltue track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>WED</b></td>
                                                <td ng-repeat="item in melwed track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>THU</b></td>
                                                <td ng-repeat="item in melthu track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>FRI</b></td>
                                                <td ng-repeat="item in melfri track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>SAT</b></td>
                                                <td ng-repeat="item in melsat track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" style="background-color:rgb(255,230,153);">
                                                    <b>PER</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td ><b>MON</b></td>
                                                <td ng-repeat="item in permon track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>TUE</b></td>
                                                <td ng-repeat="item in pertue track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>WED</b></td>
                                                <td ng-repeat="item in perwed track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>THU</b></td>
                                                <td ng-repeat="item in perthu track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>FRI</b></td>
                                                <td ng-repeat="item in perfri track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>SAT</b></td>
                                                <td ng-repeat="item in persat track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" style="background-color:rgb(255,230,153);">
                                                    <b>SYD</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td ><b>MON</b></td>
                                                <td ng-repeat="item in sydmon track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>TUE</b></td>
                                                <td ng-repeat="item in sydtue track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>WED</b></td>
                                                <td ng-repeat="item in sydwed track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>THU</b></td>
                                                <td ng-repeat="item in sydthu track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>FRI</b></td>
                                                <td ng-repeat="item in sydfri track by $index">{{item}}</td>
                                            </tr>
                                            <tr>
                                                <td ><b>SAT</b></td>
                                                <td ng-repeat="item in sydsat track by $index">{{item}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

</div>
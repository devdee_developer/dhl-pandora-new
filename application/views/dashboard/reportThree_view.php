<script src="<?php echo base_url('asset/reportThreeController.js'); ?>"></script>
<!-- <link href="<?php echo base_url('theme/bootstrap/css/newstyle.css'); ?>" rel="stylesheet"> -->
<div ng-controller="reportThreeController" ng-init="onInit()">

    <!-- <div class="row">
		<ul class="navigator">
			<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Vendor');?></a></li>*/ ?>
			<li class="nav_active"> Mapping code Apple & DHL </li>
		</ul>
	</div> -->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> AU NZ PERFORMANCE </h1>
        </div>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div role="form">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger"></span>Year :</label>
                            <ui-select ng-model="tempYearIndex.selected" theme="selectize">
                                <ui-select-match>{{$select.selected.Year}}</ui-select-match>
                                <ui-select-choices repeat="pYear in listYear | filter: $select.search">
                                    <span ng-bind-html="pYear.Year  | highlight: $select.search"></span>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger"></span>Quater :</label>
                            <ui-select ng-model="tempQuaterIndex.selected" theme="selectize">
                                <ui-select-match>{{$select.selected.Quater}}</ui-select-match>
                                <ui-select-choices repeat="pQuater in listQuater | filter: $select.search">
                                    <span ng-bind-html="pQuater.Quater  | highlight: $select.search"></span>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <button class="btn btn-warning  waves-effect waves-light btn-sm m-b-5"
                                ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span
                                    class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
                            <button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"
                                ng-click="getChart()"><i></i>
                                <spanclass="hidden-xs"> Run Chart </span>
                            </button>
                            <button class="btn btn-info waves-effect waves-light btn-sm m-b-5"
                                ng-click="Exportreport()"><i class="fa fa-download"></i>
                                <spanclass="hidden-xs"> Export </span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <canvas id="" width="400" height="400"></canvas>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12">
                                <canvas id="onTimePerformance" width="400" height="400"></canvas>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <canvas id="onTimePerformanceQuarterly" width="400" height="400"></canvas>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <table class="table table-striped text-center table-bordered table-hover"
                                    style="width: 98%;">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td ng-repeat="item in dataorder" ng-bind="item.Gateway">
                                                <b>Factor Incident</b></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:rgb(75,192,192);"><b>Uncontrollable</b></td>
                                            <td ng-repeat="item in percentuncontrollable track by $index">{{item}}%</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:rgb(255,230,153);"><b>DHL Support</b></td>
                                            <td ng-repeat="item in percentdhl track by $index">{{item}}%</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:rgb(255,159,64);"><b>Customer Support</b></td>
                                            <td ng-repeat="item in percentcustomer track by $index">{{item}}%</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:rgb(255,99,132);"><b>Ontime</b></td>
                                            <td ng-repeat="item in percentontime track by $index">{{item}}%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:40px">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <canvas id="" width="400" height="0"></canvas>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <canvas id="" width="400" height="0"></canvas>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-12">
                                    <canvas id="" width="400" height="0"></canvas>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12" >
                                    <table class="table table-striped text-center table-bordered table-hover"
                                        style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <b>Over 1-2 days</b>
                                                </td>
                                                <td><b>AKL</b></td>
                                                <td><b>BNE</b></td>
                                                <td><b>MEL</b></td>
                                                <td><b>PER</b></td>
                                                <td><b>SYD</b></td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Late flight connection</b></td>
                                                <td ng-repeat="item in lateflight track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Force majeure</b></td>
                                                <td ng-repeat="item in forcemajeure track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Airline technicle</b></td>
                                                <td ng-repeat="item in airlinetech track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Agreed delivery by consignee</b></td>
                                                <td ng-repeat="item in agreeddelivery track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>On forwarding</b></td>
                                                <td ng-repeat="item in onforward track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Other</b></td>
                                                <td ng-repeat="item in other track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Over 3-4 days</b>
                                                </td>
                                                <td><b>AKL</b></td>
                                                <td><b>BNE</b></td>
                                                <td><b>MEL</b></td>
                                                <td><b>PER</b></td>
                                                <td><b>SYD</b></td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Late flight connection</b></td>
                                                <td ng-repeat="item in lateflight2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Force majeure</b></td>
                                                <td ng-repeat="item in forcemajeure2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Airline technicle</b></td>
                                                <td ng-repeat="item in airlinetech2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Agreed delivery by consignee</b></td>
                                                <td ng-repeat="item in agreeddelivery2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>On forwarding</b></td>
                                                <td ng-repeat="item in onforward2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Other</b></td>
                                                <td ng-repeat="item in other2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Over 5-6 days</b>
                                                </td>
                                                <td><b>AKL</b></td>
                                                <td><b>BNE</b></td>
                                                <td><b>MEL</b></td>
                                                <td><b>PER</b></td>
                                                <td><b>SYD</b></td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Late flight connection</b></td>
                                                <td ng-repeat="item in lateflight3 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Force majeure</b></td>
                                                <td ng-repeat="item in forcemajeure3 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr >
                                                <td style="background-color:rgb(255,230,153);"><b>Agreed delivery by consignee</b></td>
                                                <td ng-repeat="item in agreeddelivery3 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>On forwarding</b></td>
                                                <td ng-repeat="item in onforward3 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Other</b></td>
                                                <td ng-repeat="item in other3 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Over 3-4 days</b>
                                                </td>
                                                <td><b>AKL</b></td>
                                                <td><b>BNE</b></td>
                                                <td><b>MEL</b></td>
                                                <td><b>PER</b></td>
                                                <td><b>SYD</b></td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Late flight connection</b></td>
                                                <td ng-repeat="item in lateflight2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Force majeure</b></td>
                                                <td ng-repeat="item in forcemajeure2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Airline technicle</b></td>
                                                <td ng-repeat="item in airlinetech2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Agreed delivery by consignee</b></td>
                                                <td ng-repeat="item in agreeddelivery2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>On forwarding</b></td>
                                                <td ng-repeat="item in onforward2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Other</b></td>
                                                <td ng-repeat="item in other2 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Over 7 days</b>
                                                </td>
                                                <td><b>AKL</b></td>
                                                <td><b>BNE</b></td>
                                                <td><b>MEL</b></td>
                                                <td><b>PER</b></td>
                                                <td><b>SYD</b></td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Agreed delivery by consignee</b></td>
                                                <td ng-repeat="item in agreeddelivery4 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td style="background-color:rgb(255,230,153);"><b>Other</b></td>
                                                <td ng-repeat="item in other4 track by $index">{{item}}%</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Over 7 days</b>
                                                </td>
                                                <td ng-repeat="item in total track by $index">{{item}}%</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

</div>
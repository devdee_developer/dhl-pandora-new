<script src="<?php echo base_url('asset/reportPerformanceDashboardController.js'); ?>"></script>
<!-- <link href="<?php echo base_url('theme/bootstrap/css/newstyle.css'); ?>" rel="stylesheet"> -->
<div ng-controller="reportPerformanceDashboardController" ng-init="onInit()">

	<!-- <div class="row">
		<ul class="navigator">
			<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Vendor');?></a></li>*/ ?>
			<li class="nav_active"> Mapping code Apple & DHL </li>
		</ul>
	</div> -->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"> Dashboard </h1>
		</div>
	</div>

	<div class="panel-body">
		<div class="row">
			<!-- Selection Criteria -->
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Selection Criteria </div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div role="form">
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-4 col-md-4 col-xs-12">
											<label><span class="text-danger"></span>Shipper Company :</label>
											<div class="col">
												<select (change)="onSelected()" style="border-radius: 4px;height: 32px;width: 400px;border-color: #cdcdcd;" theme="bootstrap">
													<option default>PANDORA SERVICES CO.,LTD.</option>
												</select>
											</div>
										</div>
									</div>

									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-4 col-md-4 col-xs-12">
											<label><span class="text-danger"></span>Month :</label>
											<ui-select multiple ng-model="tempMonthIndex.selected" ng-change="getWeek()" theme="bootstrap">
												<ui-select-match>{{$item.monthname}}</ui-select-match>
												<ui-select-choices repeat="pMonth in listMonth | filter: $select.search">
													<span ng-bind-html="pMonth.monthname  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<div class="col-lg-4 col-md-4 col-xs-12">
											<label><span class="text-danger"></span>Week :</label>
											<ui-select multiple ng-model="tempWeekIndex.selected" ng-change="getCountry()" ng-disabled="tempMonthIndex.selected == undefined || tempMonthIndex.selected.length == 0" theme="bootstrap">
												<ui-select-match>{{$item.week}}</ui-select-match>
												<ui-select-choices repeat="pWeek in listWeek | filter: $select.search">
													<span ng-bind-html="pWeek.week  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>

										<div class="col-lg-4 col-md-4 col-xs-12">
											<label><span class="text-danger"></span>Country :</label>
											<ui-select multiple ng-model="tempCountryIndex.selected" ng-change="getRegion()" ng-disabled="tempWeekIndex.selected == undefined || tempWeekIndex.selected.length == 0" theme="bootstrap">
												<ui-select-match>{{$item.Destination_Ctry}}</ui-select-match>
												<ui-select-choices repeat="pCountry in listCountry | filter: $select.search">
													<span ng-bind-html="pCountry.Destination_Ctry  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
									</div>

									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-4 col-md-4 col-xs-12">
											<label><span class="text-danger"></span>Region :</label>
											<ui-select multiple ng-model="tempRegionIndex.selected" ng-change="getGateway()" ng-disabled="tempCountryIndex.selected == undefined || tempCountryIndex.selected.length == 0" theme="bootstrap">
												<ui-select-match>{{$item.Region}}</ui-select-match>
												<ui-select-choices repeat="pRegion in listRegion | filter: $select.search">
													<span ng-bind-html="pRegion.Region  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
										<div class="col-lg-4 col-md-4 col-xs-12">
											<label><span class="text-danger"></span>Gateway :</label>
											<ui-select multiple ng-model="tempGatewayIndex.selected" ng-change="getOverSLA()" ng-disabled="tempRegionIndex.selected == undefined || tempRegionIndex.selected.length == 0" theme="bootstrap">
												<ui-select-match>{{$item.Gateway}}</ui-select-match>
												<ui-select-choices repeat="pGateway in listGateway | filter: $select.search">
													<span ng-bind-html="pGateway.Gateway  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>

										<div class="col-lg-4 col-md-4 col-xs-12">
											<label><span class="text-danger"></span>Over SLA (day) :</label>
											<ui-select multiple ng-model="tempOverSLAIndex.selected" ng-disabled="tempGatewayIndex.selected == undefined || tempGatewayIndex.selected.length == 0" theme="bootstrap">
												<ui-select-match>{{$item.OverSLAlist}}</ui-select-match>
												<ui-select-choices repeat="pOverSLA in listOverSLA | filter: $select.search">
													<span ng-bind-html="pOverSLA.OverSLAlist  | highlight: $select.search"></span>
												</ui-select-choices>
											</ui-select>
										</div>
									</div>
								</div>

							</div>

							<div class="col-lg-12 col-md-12 col-xs-12">
								<button class="btn btn-warning  waves-effect waves-light btn-sm m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
								<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="getGarph()"><i></i>
									<spanclass="hidden-xs"> Run Chart </span>
								</button>
								<button class="invalid-feedback btn btn-info waves-effect waves-light btn-sm m-b-5" ng-click="Exportreport()"><i class="fa fa-download"></i>
									<spanclass="hidden-xs"> Export PDF </span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- End Selection Criteria -->


			<!-- Chart -->
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Performance Dashboard </div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div role="form">
									<div class="form-group col-lg-12 col-md-12 col-xs-12" style="border-style: solid;border-top-color: white;border-left-color: white;border-right-color: white;border-bottom-color: #d6d7d6;">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<canvas id="myChartOverviewPerformance" width="400" height="400"></canvas>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12">
											<canvas id="myChartVolumebyCategory" width="400" height="400"></canvas>
										</div>
									</div>
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-12 col-md-12 col-xs-12">
										</div>
									</div>
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-4 col-md-4 col-xs-12">
											<canvas id="myChartDHLSupport" width="400" height="400"></canvas>
										</div>
										<div class="col-lg-4 col-md-4 col-xs-12" style="display: block;border-style: solid;border-top-color: white;border-bottom-color: white;border-right-color: #d6d7d6;border-left-color: #d6d7d6;">
											<canvas id="myChartCustomerSupport" width="400" height="400"></canvas>
										</div>
										<div class="col-lg-4 col-md-4 col-xs-12">
											<canvas id="myChartUncontrollable" width="400" height="400"></canvas>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- End Chart -->

			<!-- Chart -->
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Shipment Profile </div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div role="form">
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-4 col-md-4 col-xs-12">
											<canvas id="myChartVolumebyProduct" width="400" height="400"></canvas>
										</div>
										<div class="col-lg-4 col-md-4 col-xs-12">
											<canvas id="myChartVolumebytradelane" width="400" height="400"></canvas>
										</div>
										<div class="col-lg-4 col-md-4 col-xs-12">
											<canvas id="myChartWeightbyproduct" width="400" height="400"></canvas>
										</div>
									</div>
									<hr>
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-6">
											<canvas id="myChartOriginCountry" width="50" height="50"></canvas>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-6">
											<canvas id="myChartDestinationCountry" width="50" height="50"></canvas>
										</div>
									</div>
									<div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-12 col-md-12 col-xs-12">
											<canvas id="myChartDHLServiceableArea" width="50" height="50"></canvas>
										</div>
									</div>
									<!-- <div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-3 col-md-3 col-xs-3">
										</div>
										<div class="col-lg-6 col-md-6 col-xs-6">
											<canvas id="myChartDestinationCountry" width="50" height="50"></canvas>
										</div>
										<div class="col-lg-3 col-md-3 col-xs-3">
										</div>
									</div> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- End Chart -->
		</div> <!-- End row -->
	</div>
</div>
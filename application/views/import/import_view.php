<script src="<?php echo base_url('asset/importController.js'); ?>"></script>
<div ng-controller="importController" ng-init="onInit()">

	<div class="row">
		<ul class="navigator">
			<li class="nav_active">File Upload </li>
		</ul>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Import Shipment Weekly</h1>
		</div>
	</div>




	<div class="row importdaily">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('Browse File'); ?>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div role="form">
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-12 col-md-12 col-xs-12">
										<span class="btn btn-primary" ng-click="download()"><i class="fa fa-download"></i> Download Template </span>
										<!-- <span class="btn btn-warning" ng-click="backup()"><i class="fa fa-download"></i> Run Report API </span> -->
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-2 col-md-2 col-xs-12">
										<label><span class="text-danger"></span>File : Excel Pandora </label>
									</div>
									<div class="col-lg-8 col-md-8 col-xs-12">
										<input class="form-control" readonly ng-model="file_Pandora_Dashboard.name">
									</div>
									<div class="col-md-1 col-sm-1 col-xs-12" ngf-select ng-model="file_Pandora_Dashboard" name="file_Pandora_Dashboard" ngf-pattern="'.xls,.xlsx'" ngf-accept="'.xls,.xlsx'" ngf-max-size="10MB" ngf-min-height="100">
										<span class="btn btn-primary"><?php echo $this->lang->line('Browse File'); ?></span>
									</div>
									<div class="col-lg-1 col-md-1 col-xs-12">
										<span class="btn btn-primary" ng-click="upload()"><i class="fa fa-upload"></i> <?php echo $this->lang->line('Upload'); ?> </span>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								<?php echo $this->lang->line('filename'); ?></th>

							<th><?php echo $this->lang->line('Date'); ?></th>
							<th><?php echo $this->lang->line('Option'); ?></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="item in modelDeviceList | orderBy:'-upload_date'">
							<td>{{item.file_name}}</td>
							<td ng-bind="item.upload_date"></td>
							<td>
								<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="onDownload(item)"><i class="fa fa-download"></i>
									<spanclass="hidden-xs"> Download </span>
								</button>

								<!-- <button class="btn btn-danger waves-effect waves-light btn-sm m-b-5" ng-click="onDeleteTagClick(item)"><i class="fa fa-trash"></i>
								<spanclass="hidden-xs"> Delete </span>
							</button> -->
								<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- /.table-responsive -->

	<!-- ทำหน้า -->

	<div class="row   small">
		<div class="col-md-7 col-sm-7 col-xs-12 ">
			<label class="col-md-4 col-sm-4 col-xs-12">
				<?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
			</label>
			<label class="col-md-4 col-sm-4 col-xs-12">
				<?php echo $this->lang->line('ResultsPerPage'); ?>
			</label>
			<div class="col-md-4 col-sm-4 col-xs-12 ">
				<ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
					<ui-select-match>{{$select.selected.Value}}</ui-select-match>
					<ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
						<span ng-bind-html="pSize.Text | highlight: $select.search"></span>
					</ui-select-choices>
				</ui-select>
			</div>
		</div>
		<div class="col-md-5 col-sm-5 col-xs-12  ">
			<label class="col-md-4 col-sm-4 col-xs-12">
				<span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
			</label>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
					<ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
					<ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
						<span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
					</ui-select-choices>
				</ui-select>
			</div>
			<label class="col-md-4 col-sm-4 col-xs-12">
				/ {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
			</label>
		</div>
	</div>
	<!-- ทำหน้า -->
</div>
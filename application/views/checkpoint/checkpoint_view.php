<script src="<?php echo base_url('asset/checkpointController.js'); ?>"></script>
<div ng-controller="checkpointController" ng-init="onInit()">

	<div class="row">
		<ul class="navigator">
			<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Agent');?></a></li>*/ ?>
			<!-- <li class="nav_active"> <?php echo $this->lang->line('ShopCode'); ?></li> -->
			<li class="nav_active"></li>
		</ul>
		<!-- /.col-lg-12 -->
	</div>

	<div class="row">
		<div class="col-lg-12">
			<!-- <h1 class="page-header"><?php echo $this->lang->line('ShopCode'); ?></h1> -->
			<h1 class="page-header"> Checkpoint</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>

	<!-- /List.row types-->
	<div class="row  SearchDevice" style="display:none;">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('Search'); ?>
				</div>
				<div class="panel-body">
					<div class="form-group col-lg-12 col-md-12 col-xs-12">

						<div class="col-lg-4 col-md-4 col-xs-12">
							<label> Reporting code description</label>
							<input class="form-control" ng-model="modelSearch.reporting_code_description" maxlength="80">
							<p class="help-block"></p>
						</div>

						<div class="col-lg-4 col-md-4 col-xs-12">
							<label> Reporting code cat description</label>
							<input class="form-control" ng-model="modelSearch.reporting_code_cat_description" maxlength="80">
							<p class="help-block"></p>
						</div>

						
						<div class="col-lg-4 col-md-4 col-xs-12">
							<label> Factor incident</label>
							<input class="form-control" ng-model="modelSearch.factor_incident" maxlength="80">
							<p class="help-block"></p>
						</div>

						
						<div class="col-lg-4 col-md-4 col-xs-12">
							<label> Factor</label>
							<input class="form-control" ng-model="modelSearch.factor" maxlength="80">
							<p class="help-block"></p>
						</div>


					</div>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<button type="button" class="btn btn-warning waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
						<button type="button" class="btn btn-info waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
						<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
					</div>
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /List.row types-->


	<!-- / create room types  -->
	<div class="row addDevice" style="display:none;">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<!-- <?php echo $this->lang->line('ShopCode'); ?> -->
						Checkpoint
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div role="form">
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger">*</span> Reporting code description </label>
									</div>
									<div class="col-lg-6 col-md-6 col-xs-12">
										<input class="form-control" ng-model="CreateModel.reporting_code_description" maxlength="180">
										<p class="CreateModel_reporting_code_description require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger">*</span> Reporting code cat description </label>
									</div>
									<div class="col-lg-6 col-md-6 col-xs-12">
										<input class="form-control" ng-model="CreateModel.reporting_code_cat_description" maxlength="180">
										<p class="CreateModel_reporting_code_cat_description require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger">*</span> Factor incident </label>
									</div>
									<div class="col-lg-6 col-md-6 col-xs-12">
										<input class="form-control" ng-model="CreateModel.factor_incident" maxlength="180">
										<p class="CreateModel_factor_incident require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>

								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger">*</span> Factor</label>
									</div>
									<div class="col-lg-6 col-md-6 col-xs-12">
										<input class="form-control" ng-model="CreateModel.factor" maxlength="180">
										<p class="CreateModel_factor require text-danger"><?php echo $this->lang->line('Require'); ?></p>
									</div>
								</div>


								<div class="form-group text-right">
									<br /><br />
									<div class="col-lg-12 col-md-12 col-xs-12">
										<button class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
										<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
									</div>
								</div>
							</div>
							<div class="row text-primary  " ng-show="CreateModel.id > 0" style="font-size:xx-small; margin-top:80px;">
								<div class="col-md-6 col-xs-12 timestampshow text-left">
									<?php echo $this->lang->line('Createby'); ?> {{CreateModel.create_user}} {{CreateModel.create_date}}
								</div>
								<div class="col-md-6 col-xs-12 timestampshow text-right text-left-xs">
									<?php echo $this->lang->line('Updateby'); ?> {{CreateModel.update_user}} {{CreateModel.update_date}}
								</div>
							</div>
						</div>

					</div>
					<!-- /.row (nested) -->

				</div>


				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.create room types -->


	<!-- /List.row types-->
	<div class="row DisplayDevice">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<!-- <?php echo $this->lang->line('ListOfShopCode'); ?> -->
					Checkpoint
				</div>
				<div class="panel-body">
					<div class="col-lg-5 col-md-5 col-xs-12">
						<button class="btn btn-success" ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button>
						<button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
						<button type="button" class="btn btn-warning waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
						<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ng-click="Exportexcel()"><i class="fa fa-download"></i>
							<spanclass="hidden-xs"> Download </span>
						</button>
					</div>

					<div class="col-lg-7 col-md-7 col-xs-12">
						<div class="col-lg-8 col-md-8 col-xs-12">
							<input class="form-control" readonly ng-model="file_Pandora_Check.name">
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12" ngf-select ng-model="file_Pandora_Check" name="file_Pandora_Check" ngf-pattern="'.xls,.xlsx'" ngf-accept="'.xls,.xlsx'" ngf-max-size="10MB" ngf-min-height="100">
							<span class="btn btn-primary"><?php echo $this->lang->line('Browse File'); ?></span>
						</div>

						<div class="col-lg-2 col-md-2 col-xs-12">
							<span class="btn btn-primary" ng-click="upload()"><i class="fa fa-upload"></i> <?php echo $this->lang->line('Upload'); ?> </span>
						</div>
									
					</div>


					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th class="sorting" ng-click="sort($event)" sort="reporting_code_description">Reporting code description</th>
										<th class="sorting" ng-click="sort($event)" sort="reporting_code_cat_description">Reporting code cat description</th>
										<th class="sorting" ng-click="sort($event)" sort="factor_incident">Factor incident</th>
										<th class="sorting" ng-click="sort($event)" sort="factor">Factor</th>
										<th><?php echo $this->lang->line('Option'); ?></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="item in modelDeviceList">
										<td ng-bind="item.reporting_code_description"></td>
										<td ng-bind="item.reporting_code_cat_description"></td>
										<td ng-bind="item.factor_incident"></td>
										<td ng-bind="item.factor"></td>
										<td>
											<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
											<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>

					<!-- ทำหน้า -->
					<div class="row tblResult small">
						<div class="col-md-7 col-sm-7 col-xs-12 ">
							<label class="col-md-4 col-sm-4 col-xs-12">
								<?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
							</label>
							<label class="col-md-4 col-sm-4 col-xs-12">
								<?php echo $this->lang->line('ResultsPerPage'); ?>
							</label>
							<div class="col-md-4 col-sm-4 col-xs-12 ">
								<ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
									<ui-select-match>{{$select.selected.Value}}</ui-select-match>
									<ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
										<span ng-bind-html="pSize.Text | highlight: $select.search"></span>
									</ui-select-choices>
								</ui-select>
							</div>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12  ">
							<label class="col-md-4 col-sm-4 col-xs-12">
								<span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
							</label>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
									<ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
									<ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
										<span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
									</ui-select-choices>
								</ui-select>
							</div>
							<label class="col-md-4 col-sm-4 col-xs-12">
								/ {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
							</label>
						</div>
					</div>
					<!-- ทำหน้า -->

				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /List.row types-->
</div>
</div>
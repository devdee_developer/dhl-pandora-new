<?php	
$lang['welcome_message'] = 'Type message in German';
$lang['Dashboard'] = 'Dashboard';
$lang['Dormitory'] = 'Dormitory';
$lang['Rooms'] = 'Rooms';
$lang['Customer'] = 'Customer';
$lang['Invoice'] = 'Invoice';
$lang['MaCost'] = 'MA Cost';
$lang['Report'] = 'Report';
$lang['DormitoryName'] = 'Dormitory Name';
$lang['Branch'] = 'Branch';
$lang['Address'] = 'Address';
$lang['Save'] = 'Save';
$lang['Cancel'] = 'Cancel';
$lang['Edit'] = 'Edit';
$lang['Delete'] = 'Delete';
$lang['Contact'] = 'Contact';
$lang['Email'] = 'Email';
$lang['Tel'] = 'Tel';
$lang['Fax'] = 'Fax';
$lang['DormitoryList'] = 'Dormitory List';
$lang['Option'] = 'Option';
$lang['RoomType'] = 'Room Type';
$lang['RoomItem'] = 'Room Item';
$lang['RoomInDorm'] = 'Room In Dorm';
$lang['ViewDatail'] = 'View Details';
$lang['RoomTypeCode'] = 'Type Code';
$lang['RoomTypeName'] = 'Type Name';
$lang['RoomPrice'] = 'Prices';
$lang['Note'] = 'Note'; 
$lang['RoomTypeList'] = 'Room Type List';
$lang['RoomItemCode'] = 'Item Code';
$lang['RoomItemName'] = 'Item Name';
$lang['ItemPrice'] = 'Prices';
$lang['RoomItemList'] = 'Room Item List';
$lang['PleaseSelectDorm'] = 'Please select dormitory';
$lang['Next'] = 'Next';
$lang['RoomNumber'] = 'Room Number';
$lang['RoomFloor'] = 'Floor';
$lang['RoomInDormList'] = 'Room in Dormitory List';
$lang['CustomerName'] = 'Name';
$lang['CustomerSurName'] = 'SurName';
$lang['CustomerIDCard'] = 'ID Card';
$lang['CustomerAddress1'] = 'Address 1';
$lang['CustomerAddress2'] = 'Address 2';
$lang['Picture'] = 'Picture';
$lang['Deposit'] = 'Deposit';
$lang['NameSurName'] = 'Name & Surname';
$lang['Day'] = 'Day';
$lang['Week'] = 'Week';
$lang['Month'] = 'Month';
$lang['Year'] = 'Year';
$lang['Booking'] = 'Booking';
$lang['CheckInDate'] = 'Check In';
$lang['CheckOutDate'] = 'Check Out';
$lang['CustomerType'] = 'Customer Type'; 
$lang['DepositMonth'] = 'DepositMonth';
$lang['OldCustomer'] = 'Old customer'; 
$lang['NewCustomer'] = 'New customer';
$lang['FileInput'] = 'File input';
$lang['PictureIDCard'] = 'PictureIDCard';
$lang['Price'] = 'Price';
$lang['ContactDate'] = 'Contact Date';
$lang['ContactPerson'] = 'Contact Person';
$lang['Relatetion'] = 'Relatetion';
$lang['QtyPerson'] = 'Qty Person';
$lang['OptionNote'] = 'Option Note';
$lang['DisCountNote'] = 'DisCount Note';
$lang['Insurrance'] = 'Insurrance';
$lang['RoomStatus'] = 'Status';
$lang['RoomManage'] = 'Manage';
$lang['RoomAll'] = 'All';
$lang['RoomFull'] = 'Room Full';
$lang['RoomEmpty'] = 'Room Empty';
$lang['Vat'] = 'Vat';
$lang['Quatation'] = 'Quatation';
$lang['Invoice'] = 'Invoice';
$lang['System'] = 'System';
$lang['Company'] = 'Company';
$lang['CompanyAddress'] = 'Company Address';
$lang['CompanyID'] = 'Company ID (13 digit)';
$lang['CompanyID2'] = 'Company ID';
$lang['CusCompany'] = 'Customer Company';
$lang['CusCompanyAddress'] = 'Customer Company Address';
$lang['CusCompanyID'] = 'Customer Company ID (13 digit)';
$lang['CusCompanyID2'] = 'Customer Company ID';
$lang['VatBook'] = 'Vat Book';
$lang['BookNo'] =  'Book No.';
$lang['NumNo'] =  'Num No.';
$lang['CompanyNameEx'] =  'ผู้มีหน้าที่หักภาษีณ ที่จ่าย';
$lang['CompanyNameEx2'] =  'ผู้ถูกหักภาษีณ ที่จ่าย';
$lang['withHoldingTax'] =  'ภาษีที่หักและนำส่งไว้';
$lang['PayPrices'] =  'จำนวนเงินที่จ่าย';
$lang['PayDate'] =  'วันเดือนหรือปีภาษีที่จ่าย';
$lang['PayDate'] =  'วันเดือนหรือปีภาษีที่จ่าย';
$lang['ListOfWHT'] =  'รายการภาษีหัก ณ ที่จ่าย';
$lang['Date'] =  'Date';
$lang['DueDate'] =  'Due Date';
$lang['CustomerID'] =  'Customer ID';
$lang['InvoiceRef'] =  'Invoice Ref';
$lang['DateRef'] =  'Date Ref';
$lang['Company'] =  'Company';
$lang['CompanyDetail'] =  'Company Detail';
$lang['Contact'] =  'Contact';
$lang['Address1'] =  'Address1';
$lang['Address2'] =  'Address2';
$lang['Address3'] =  'Address3';
$lang['taxid'] =  'taxid';
$lang['website'] =  'website';
$lang['savesuccess'] = 'save success';
$lang['error'] = 'error';
$lang['Project'] = 'Project';
$lang['CompanyCode'] = 'Company Code';
$lang['ListOfCustomer'] = 'List Of Customer';
$lang['DoYouWantToDelete'] = 'Do you want to delete ?';
$lang['NoCancel'] = 'No';
$lang['Yes'] = 'Yes';
$lang['Close'] = 'Close';
$lang['Confirmation'] = 'Confirmation';
$lang['Information'] = 'Information';
$lang['Search'] = 'Search';
$lang['ResetSearch'] = 'Reset Search';
$lang['Search'] = 'Search';
$lang['Add'] = 'Add';
$lang['Total'] = 'Total';
$lang['Records'] = 'Records';
$lang['ResultsPerPage'] = 'ResultsPerPage';
$lang['Previous'] = 'Previous';
$lang['Next'] = 'Next';
$lang['ProjectName'] = 'Project Name';
$lang['budget'] = 'Budget';
$lang['note'] = 'Note';
$lang['projectyear'] = 'Project Year';
$lang['ListOfProject'] = 'List of Project';
$lang['Require'] = 'Require';
$lang['Unit'] = 'Unit';
$lang['DESCRIPTION'] = 'DESCRIPTION';
$lang['Quantity'] = 'Qty';
$lang['Price'] = 'Price';
$lang['Amount'] = 'Amount';
$lang['ITEM'] = 'ITEM';
$lang['SubTotal'] = 'Sub Total';
$lang['VAT'] = 'VAT 7%';
$lang['Total'] = 'Total';
$lang['sub_alphabet'] = 'รวมเงินภาษีที่หักนำส่ง (ตัวอักษร) ';
$lang['SignName'] = 'ลงชื่อผู้จ่ายเงิน';
$lang['SignDate'] = 'วัน เดือน ปี ที่ออกหนังสือรับรอง';
$lang['ListOfQuatation'] = 'List of quatation';
$lang['Print'] = 'Print';
$lang['Payment'] = 'Perment term and condition';
$lang['SocialFund'] = 'Social Fund';
$lang['Fund'] = 'Fund';
$lang['Income'] = 'Income';
$lang['revenue'] = 'revenue';
$lang['expenditure'] = 'expenditure';
$lang['Item'] = 'รายละเอียด';
$lang['RequireMoreZero'] = 'ต้องมีค่ามากกว่า 0';
$lang['ListOfIncome'] = 'รายการรายรับ รายจ่าย';
$lang['Status'] = 'Status';
$lang['StatusWorking'] = 'กำลังดำเนินการ';
$lang['StatusPending'] = 'ยกเลิก';
$lang['StatusSuccess'] = 'อนุมัติ';
$lang['PleaseSelectStatus'] = 'กรุณาเลือกสถานะ';
$lang['ReportTotal']= 'รายงานแบบรวมทุก Project';
$lang['ReportSeperate']= 'รายงานแบบแยก Project';
$lang['ServiceAllAgent']= 'Service All Agent';
$lang['ServiceAsNormal']= 'Service As Normal';
$lang['Bank']= 'Bank';
$lang['Agent']= 'Agent';
$lang['CostOptimize']= 'Cost Optimize';
$lang['BankName']= 'Name';
$lang['PostalCode']= 'Postal Code';
$lang['ProvinceName']= 'Province Name';
$lang['Kerry']= 'Kerry';
$lang['PEP']= 'PEP';
$lang['NIM']= 'NIM';
$lang['EMS']= 'EMS';
$lang['KKC']= 'KKC';
$lang['USM']= 'USM'; 
$lang['AgentService'] = 'Agent Service';
$lang['ListOfagentService'] = 'List of Agent Service';
$lang['Phone'] = 'Phone';
$lang['ServiceCenter'] = 'Service Center';
$lang['NormalService'] = 'Normal Service';
$lang['ListOfNormalService'] = 'List of Normal Service'; 
$lang['ListOfEMS'] = 'List of EMS';
$lang['Weight'] = 'Weight (g)';
$lang['EMS_price'] = 'EMS Price';
$lang['MaxWeight'] = 'Max Weight (g)';
$lang['CalculateData'] = 'Calculate Data';
$lang['PEP_price'] = 'PEP Price';
$lang['NIM_price'] = 'NIM Price';
$lang['Kerry_price'] = 'Kerry Price';
$lang['PEP_Zone'] = 'PEP Zone';
$lang['NIM_Zone'] = 'NIM Zone';
$lang['Kerry_Zone'] = 'Kerry Zone';
$lang['Weight_Min'] = 'Weight Min (g)';
$lang['Weight_Max'] = 'Weight Max (g)';
$lang['Whl_length'] = '(W + L + H) (cm)';// 'Dimensional Weight Guideline (W + L + H)';
$lang['Whl_multiple'] =  '(W x L x H) (cm^3)';//'Dimensional Weight Guideline (W x L x H)';
$lang['Bkk'] = 'Bkk';
$lang['Upc1'] = 'Upc1';
$lang['Upc2'] = 'Upc2';
$lang['ListOfPepPrice'] = 'List of Pep Price';
$lang['ListOfPepZone'] = 'List of Pep Zone';
$lang['Zone'] = 'Zone';
$lang['Province'] = 'Province';
$lang['District'] = 'District';
$lang['ListOfKerryPrice'] = 'List of Kerry Price';
$lang['Delivery'] = 'Delivery';
$lang['PickUp'] = 'PickUp';
$lang['Mid'] = 'Mid';
$lang['East'] = 'E';
$lang['NorthTop'] = 'NT';
$lang['NorthBottom'] = 'NB';
$lang['NorthEastTop'] = 'NET';
$lang['NorthEastBottom'] = 'NEB';
$lang['SouthTop'] = 'ST';
$lang['SouthMid'] = 'SM';
$lang['SouthBottom'] = 'SB';
$lang['SouthRed'] = 'SR';
$lang['ListOfNimPrice'] = 'List of Nim Price';
$lang['ListOfNimZone'] = 'List of Nim Zone';
$lang['ListOfBank'] = 'List of Bank';
$lang['ThaiLandPostCode'] = 'All Thai post code';
$lang['Config'] = 'Config';
$lang['ThaiPost'] = 'ThaiPost';
$lang['LocationCode'] = 'Location Code';
$lang['ProvinceTH'] = 'Province TH';
$lang['ProvinceEN'] = 'Province EN';
$lang['Distrcit'] = 'Distrcit';
$lang['Locality'] = 'Locality';
$lang['ListOfThaiPost'] = 'List Of ThaiPost';
$lang['LimitOfGirth'] = 'Limit Of Girth';
$lang['PEP_WLH_LIMIT'] = 'PEP W+L+H (cm)';
$lang['NIM_WLH_LIMIT'] = 'NIM W+L+H (cm)';
$lang['KERRY_WLH_LIMIT'] = 'KERRY W+L+H (cm)';
$lang['Girth'] = 'Girth';
$lang['Agent'] = 'Agent';
$lang['ListOfAgent'] = 'List Of Agent';
$lang['AgentName'] = 'Agent Name';
$lang['Createby'] = 'Create by';
$lang['Updateby'] = 'Update by';
$lang['HOG'] = 'HOG';
$lang['Logout'] = 'Logout';
$lang['ApiTest'] = 'Api Test';
$lang['OptimizeCost'] = 'Optimize Cost';
$lang['Optimize'] = 'Optimize Cost';
$lang['Weight'] = 'Weight';
$lang['Height'] = 'Height';
$lang['Length'] = 'Length';
$lang['Width'] = 'Width';
$lang['Customer'] = 'Customer';
$lang['Tel'] = 'Tel';
$lang['OverWLH_Length'] = 'Over limit W+L+H ';
$lang['OverWLH_Multiple'] = 'Over limit WxLxH ';
$lang['OverWeight'] = 'Over limit weight ';
$lang['NoServicePostId'] = 'No service in this post code ';
$lang['NoFound'] = 'No found';
$lang['Success'] = 'Success';
$lang['ReportKerry'] = 'Kerry report';
$lang['ReportEMS'] = 'EMS report';
$lang['ReportPEP'] = 'PEP report';
$lang['ReportNIM'] = 'NIM report';
$lang['ReportApple'] = 'Apple report';
$lang['StartDate'] = 'Start Date';
$lang['EndDate'] = 'End Date';
$lang['FindOptimize'] = 'Find Optimize';
$lang['ServiceName'] = 'Service Name';
$lang['DeliveryTime'] = 'Delivery Time';
$lang['CustomerRequest'] = 'Customer Request';
$lang['ListOfCustomerRequest'] = 'List Of Customer Request';
$lang['InputDate'] = 'Input Date'; 
$lang['CustomerRequestName'] = "Customer Name";
$lang['TransectionName'] = "Transection Name";
$lang['Transection'] = "Transection";
$lang['ListOfTransection'] = "List Of Transection";
$lang['PID'] = "PID";
$lang['AWB'] = "AWB";
$lang['ToPerson'] = "To Person"; 
$lang['Duty'] = "Duty";
$lang['DutyCost'] = "Duty Cost";
$lang['DutyPay'] = "Duty Pay";
$lang['DutyCSH'] = "CSH";
$lang['DutyUndel'] = "UNDEL";
$lang['PayNote'] = "Pay Note";
$lang['ProgramSelect'] = "Program Select";
$lang['ProgramPrice'] = "Program Price";
$lang['CustomSelect'] = "Custom Select";
$lang['CustomPrice'] = "Custom Price";
$lang['CustomNote'] = "Custom Note";
$lang['Code'] = "Code";
$lang['ACC'] = "ACC";
$lang['ListOfPep'] = "List of PEP Report";
$lang['ExportExcel'] = "Export to Excel";
$lang['ReportSum'] = "Report Summary";
$lang['TotalPrice'] = "Total Price";
$lang['TotalQty'] = "Total Qty";
$lang['PEPReport1'] = "PEP Report 1";
$lang['PEPReport2'] = "PEP Report 2";
$lang['ReportDaily'] = "Report Daily";
$lang['ListOfSummary'] = "List of summary";
$lang['Usermanage'] = "Manage User";
$lang['User'] = "User";
$lang['ListOfUser'] = "List Of User";
$lang['UserName'] = "User Name";
$lang['Password'] = "Password";
$lang['DownloadWindowApp'] = "Download Window App";
$lang['DutyACC'] = "ACC";
$lang['ListOfDaily'] = "List of Daily";
$lang['NimBox'] = "Nim Box";
$lang['ListOfNimBox'] = "List of Nim Box";
$lang['BoxCode'] = "Box Code";
$lang['BoxSize'] = "ขนาด";
$lang['WeightPerUnit'] = "น้ำหนัก (kg)";
$lang['Divider'] = "ตัวหารปริมาตร";
$lang['VolumeWeight'] = "น้ำหนักตามปริมาตร";
$lang['Volume'] = "ปริมาตร";
$lang['cm'] = "(cm)";
$lang['kg'] = "(kg)";
$lang['VolumeCm'] = "(ลบ. ซม.)";
$lang['Check'] = "Check";
$lang['ShipmentNumberCost'] = "Shipment number and cost";
$lang['ShipmentAcc'] = "Shipment accumulate number per day";
$lang['ExportHandOver'] = "Export Hand Over";
$lang['ReportCheckPoint'] = "Report Check Point";
$lang['ListOfCheckPoint'] = "List of Check Point";
$lang['Status'] = 'Status';
$lang['CheckPoint'] = 'CheckPoint';
$lang['Apple Shipment'] = 'Apple Shipment';
$lang['Daily Import'] = 'Daily Import';
$lang['Service Center'] = 'Service Center';
$lang['Limitation'] = 'Limitation';
$lang['postcode'] = 'Postcode';
$lang['Browse File'] = 'Browse File';
$lang['Upload'] = 'Upload';
$lang['Booking_no'] = 'Booking_no';
$lang['consignment_no'] = 'consignment_no';
$lang['ref_no'] = 'ref_no';
$lang['payerid'] = 'payerid';
$lang['recipient_name'] = 'recipient_name';
$lang['recipient_address1'] = 'recipient_address1';
$lang['recipient_address2'] = 'recipient_address2';
$lang['recipient_zipcode'] = 'recipient_zipcode';
$lang['Apple_Pre'] = 'Apple_Pre';
$lang['recipient_contact_person'] = 'recipient_contact_person';
$lang['piece'] = 'piece';
$lang['weiht_kg'] = 'weiht_kg';
$lang['order'] = 'Order';
$lang['filename'] = 'File Name';
$lang['upload_date'] = 'Upload Date';
$lang['detail'] = 'Detail';
$lang['download'] = 'Download';
$lang['limitation'] = 'Limitaton';
$lang['day'] = 'Day';
$lang['svc'] = 'SVC';
$lang['DoYouWantToUpload'] = 'The original data will be deleted.Do you want to delete ?';
$lang['first_priority'] = 'First Priority';
$lang['second_priority'] = 'Second Priority';
$lang['appleapi'] = 'Appleapi';
$lang['waybill'] = 'Way Bill';
$lang['date'] = 'Date';
$lang['ref_no'] = 'Ref No';
$lang['payerid'] = 'Payerid';
$lang['recipient_name'] = 'Name';
$lang['recipient_address1'] = 'Address1';
$lang['recipient_address2'] = 'Address2';
$lang['Apple_Pre'] = 'Apple Pre';
$lang['recipient_contact_person'] = 'Recipient Contact Person';
$lang['piece'] = 'piece';
$lang['weiht_kg'] = 'Weiht kg';
$lang['getshipmentmodelliststatus'] = 'Getshipment Status';
$lang['shipmentnotcheck'] = 'Shipment Not Check';
$lang['token'] = 'Token';
$lang['version'] = 'Version';
$lang['File Apple'] = 'File : Apple';
$lang['File TH DHL Shipment'] = 'File : TH DHL Shipment';
$lang['filenameapple'] = 'File Name Apple';
$lang['filenameshipment'] = 'File Name Shipment';
$lang['SVC'] = 'SVC';
$lang['Pick_Up_Date'] = 'Pick Up Date';
$lang['Telephone'] = 'Telephone';
$lang['Contact_Person'] = 'Contact Person';
$lang['PCS'] = 'PCS';
$lang['Account'] = 'Account';
$lang['reportkerry'] = 'Report Kerry';
$lang['PID'] = 'PID';
$lang['submit'] = 'Submit';
$lang['shipmentkerry'] = 'Shipment Kerry';
$lang['QTY'] = 'QTY.';
$lang['Check Report Kerry'] = 'Check Report Kerry';
$lang['testapi'] = 'Test Api';
$lang['downloaddoccument'] = 'Download Doccument';
$lang['cashadvance'] = 'Import Cash Advance';
$lang['cashadvance1'] = 'Cash Advance';
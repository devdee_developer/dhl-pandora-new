<?php
// require "DevDeeModel.php";
class CheckpointModel extends CI_Model {
	
    private $tbl_name = 't_checkpoint';
	private $Id = 'Id';
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Bangkok");
    }

	public function getCheckpointModel($dataPost)
    {
        try {
            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 10;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'asc';
            $DataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->SQL_getCheckpointComboList($DataModel, $PageSize, $offset, $direction, $SortOrder);

            $result['totalRecords'] = $this->SQL_getCheckpointTotalList($DataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getCheckpointComboList($DataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
    {
        // print_r("test");
        $sql = 'SELECT * From '.$this->tbl_name.' Where delete_flag = 0';

        $sql =  $this->GetSearchQuery($sql, $DataModel);	

        // if ($Order != '') {
        //     $sql .= ' ORDER BY '.$Order.' '.$direction;
        // }
        $sql .= " LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_getCheckpointTotalList($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where delete_flag = 0';

        $sql =  $this->GetSearchQuery($sql, $DataModel);

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

	public function SaveCheckpoint($dataPost)
    {
        try {
			// print_r($dataPost);die();
            $DataModel['id'] = isset($dataPost['id']) ? $dataPost['id'] : '';
            $DataModel['reporting_code_description'] = isset($dataPost['reporting_code_description']) ? $dataPost['reporting_code_description'] : '';
            $DataModel['reporting_code_cat_description'] = isset($dataPost['reporting_code_cat_description']) ? $dataPost['reporting_code_cat_description'] : '';
            $DataModel['factor_incident'] = isset($dataPost['factor_incident']) ? $dataPost['factor_incident'] : '';
            $DataModel['factor'] = isset($dataPost['factor']) ? $dataPost['factor'] : '';


			
            if ($dataPost['id'] == 0) {
                $DataModel['create_date'] = date('Y-m-d H:i:s');
                $DataModel['create_user'] = $this->session->userdata('user_name');
                $nResult = $this->SQL_insertCheckpoint($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                    $result['message'] = 'Save Success';
                } else {
                    $result['status'] = false;
                    $result['message'] = 'Save Fail';
                }
            } else {
                $DataModel['update_date'] = date('Y-m-d H:i:s');
                $DataModel['update_user'] = $this->session->userdata('user_name');
                $uResult = $this->SQL_updateCheckpoint($DataModel);
                if ($uResult) {
                    $result['status'] = true;
                    $result['message'] = 'Update Success';
                } else {
                    $result['status'] = false;
                    $result['message'] = 'Update Fail';
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

	public function SQL_insertCheckpoint($DataModel)
    {
		// print_r($DataModel);die();
        $this->db->insert($this->tbl_name, $DataModel);

        return $this->db->insert_id();
    }

	public function SQL_updateCheckpoint($DataModel)
    {
        $this->db->where('id', $DataModel['id']);

        return $this->db->update($this->tbl_name, $DataModel);
    }


    public function GetSearchQuery($sql, $dataModel)
    {
        
        if (isset($dataModel['reporting_code_description']) && $dataModel['reporting_code_description'] != "") {
            $sql .= " and reporting_code_description like '%" . $dataModel['reporting_code_description'] . "%' ";
        }

        if (isset($dataModel['reporting_code_cat_description']) && $dataModel['reporting_code_cat_description'] != "") {
            $sql .= " and reporting_code_cat_description like '%" . $dataModel['reporting_code_cat_description'] . "%' ";
        }
        if (isset($dataModel['factor_incident']) && $dataModel['factor_incident'] != "") {
            $sql .= " and factor_incident like '%" . $dataModel['factor_incident'] . "%' ";
        }
        if (isset($dataModel['factor']) && $dataModel['factor'] != "") {
            $sql .= " and factor like '%" . $dataModel['factor'] . "%' ";
        }
        
        return $sql;
    }

    public function DeleteCheckpoint($dataPost)
    {
        try {
            $DataModel['id'] = isset($dataPost['id']) ? $dataPost['id'] : 0;
            $nResult = $this->SQL_DeleteCheckpoint($DataModel);
            if ($nResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line('DELETESUCCESS');
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line('DELETEFAIL');
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }


    public function SQL_DeleteCheckpoint($DataModel)
    {
        $this->db->where('id', $DataModel['id']);
        $DataModel = [
            'delete_flag' => 1,
        ];

        return $this->db->update($this->tbl_name, $DataModel);
    }



    public function delete_check()
    {

        $sql = 'DELETE FROM '. $this->tbl_name.' WHERE new_flag = 0';
		$query = $this->db->query($sql);

        // return $query->result_array();
    }
    
    public function delete_newdata()
    {

        $sql = 'DELETE FROM '. $this->tbl_name.' WHERE new_flag = 1';
		$query = $this->db->query($sql);

        // return $query->result_array();
    }

    public function update_check($DataModel)
    {
        // print_r($DataModel);
        $sql = 'UPDATE `t_checkpoint` SET new_flag = 0 , update_date = "' .date('Y-m-d H:i:s'). '", update_user = "'.$this->session->userdata('user_name').'"';
        $query = $this->db->query($sql);

        // return $query->result_array();
    }

    public function ExportExcel($dataModel)
	{
        // print_r($dataModel);

		// $sql = "SELECT * FROM v_sla_grouping WHERE week_number = '".$dataModel['week']."' AND Year = ".$dataModel['Year'];
        $sql = 'SELECT * From '.$this->tbl_name.' Where new_flag = 0 And delete_flag = 0';
		// $sql = "SELECT * FROM '.$this->tbl_name.'  WHERE new_flag = 0'";

        $sql =  $this->GetSearchQuery($sql, $dataModel);	

		$query = $this->db->query($sql);
		// print_r($sql);
		return $query->result_array();
	}

	public function insert_to_check($modelData)
	{
		// print_r($modelData);die();
		foreach ($modelData as $k => $v) {
			if (strstr($k," ")){
			   $this->db->set('`'.addslashes($k).'`','"'.$v.'"', false);
			   unset($modelData[$k]);
			}
		}
		$returnDB = $this->db->insert($this->tbl_name, $modelData);


		// $this->db->insert('t_temp_shipment', $modelData);
		return $this->db->insert_id();
	}

    public function getCheckModel(){

        $sql = 'SELECT COUNT(`id`),`reporting_code_description`,`reporting_code_cat_description` ,`factor_incident`,`factor` FROM `t_checkpoint` WHERE `new_flag` = 1 GROUP BY `reporting_code_description`,`reporting_code_cat_description` ,`factor_incident`,`factor` HAVING COUNT(`id`) > 1';
        $query = $this->db->query($sql);
        return $query->result_array();
	}


    // public function getStoreByShopCode($datapost)
    // {
    //     // print_r($datapost);die;

    //     $sql = "SELECT * FROM t_mapping_shop_code WHERE DeleteFlag = 0  and DHL_Shop_Code = '" . $datapost['DHL_Shop_Code'] . "'";
    //     // echo ($sql);
    //     // die();
    //     // print_r($sql);die;
    //     $query = $this->db->query($sql);
    //     // print_r($query);die;

    //     return  $query->result_array();
    // }

    // public function getStoreByShopCode2($datapost)
    // {

    //     $sql = "SELECT * FROM t_mapping_shop_code WHERE DeleteFlag = 0  and DHL_Shop_Code = '" . $datapost['shopCode'] . "'";
    //     // echo ($sql);
    //     // die();
    //     // print_r($sql);die;
    //     $query = $this->db->query($sql);
    //     // print_r($query);die;

    //     return  $query->result_array();
    // }

	// public function getAgentById($id){
	// 	$this->db->where($this->id, $id);
	// 	return $this->db->get($this->tbl_name);
	// }
	
	// public function insert($modelData){
		 
	//  	$this->db->insert($this->tbl_name, $modelData); 
	// 	return $this->db->insert_id(); 
    // }
     
    // public function update($id, $modelData){
    //     $this->db->where($this->id, $id);
    //     return $this->db->update($this->tbl_name, $modelData);
    // }
	
	// public function getAgentAllList(){
    //     //return $this->db->count_all($this->tbl_name);
        
    //     $this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
	// 	//$this->db->where('Agent_delete_flag', 0);
    //     $query =  $this->db->get($this->tbl_name);
		
	// 	return $query->result_array();
    // }
	
	// public function getAgentModel(){
    //     //return $this->db->count_all($this->tbl_name);
        
    //     //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
	// 	//$this->db->where('Agent_delete_flag', 0);
    //     $query =  $this->db->get($this->tbl_name);
		
	// 	return $query->result_array();
    // }
	
	// // public function getSearchQuery($sql, $dataModel){
		
	// // 	//print_r($dataModel);
		
	// // 	if(isset($dataModel['name']) && $dataModel['name'] != ""){
	// // 	 	$sql .= " and  name like '%".$this->db->escape_str( $dataModel['name'])."%' ";
	// // 	}
		
	// // 	if(isset($dataModel['code']) && $dataModel['code'] != ""){
	// // 	 	$sql .= " and  code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
	// // 	}
		  
	// // 	return $sql;
	// // }
	
	// public function getTotal($dataModel ){
		
	// 	$sql = "SELECT count(*) as qty FROM ". $this->tbl_name  ."   WHERE  delete_flag = 0  ";
				
	// 	$sql =  $this->getSearchQuery($sql, $dataModel);
		
	// 	$query = $this->db->query($sql);	
	    
	// 	$data =  $query->result_array() ;
		 
	// 	return  $data[0]['qty'];
	// }
	
	// public function getShopCodeModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
	// 	$sql = "SELECT * FROM ". $this->tbl_name . " WHERE delete_flag = 0 ";
		
	// 	$sql =  $this->getSearchQuery($sql, $dataModel);		
		
	// 	if($order != ""){
	// 		$sql .= " ORDER BY ".$order." ".$direction;
	// 	}else{
	// 		$sql .= " ORDER BY ".$this->id." ".$direction;
	// 	}
		
	// 	$sql .= " LIMIT ".$offset.", ".$limit;
		
	// 	$query = $this->db->query($sql);
	// 	//$query = $this->db->query($sql, array( "%".$dataModel['Agent_name']."%"));// $dataModel);
		
	// 	return  $query->result_array();
	// }		
	
	// public function deleteAgent($id){
	// 	$result = false;
	// 	try{
	// 		$query = $this->getAgentById($id);
	// 		$modelData;			
	// 		foreach ($query->result() as $row)
	// 		{
			   		
	// 			$modelData = array( 
	// 				'update_date' => date("Y-m-d H:i:s"),
	// 				'update_user' => $this->session->userdata('user_name'),
	// 				'delete_flag' => 1 //$row->Site_delete_flag 
	// 			); 
	// 		}
			
	// 		$this->db->where($this->id, $id);
    //     	return $this->db->update($this->tbl_name, $modelData);
			
	// 	}catch(Exception $ex){
	// 		return $result;
	// 	}
    // }
	
	// public function getAgentComboList(){
		
	// 	$sql = "SELECT id, 	name FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
	// 	$query = $this->db->query($sql);
	// 	return  $query->result_array();
	// }
	
	
	// public function dropAgent(){
		
	// 	$sql = "DELETE  FROM ". $this->tbl_name  ;
	// 	$query = $this->db->query($sql);		 
		
	// 	//return  $query->num_rows() ;
	// }
}

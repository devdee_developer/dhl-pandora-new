<?php
// require "DevDeeModel.php";
class SlaSvcAreaModel extends CI_Model {
	
    private $tbl_name = 'sla_svc_area';
	// private $Id = 'Id';
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Bangkok");
    }

	public function getSlaSvcAreaModel($dataPost)
    {
        try {
            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 10;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'asc';
            $DataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->SQL_getSlaSvcAreaComboList($DataModel, $PageSize, $offset, $direction, $SortOrder);

            $result['totalRecords'] = $this->SQL_getSlaSvcAreaTotalList($DataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getSlaSvcAreaComboList($DataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
    {
        $sql = 'SELECT * From '.$this->tbl_name;

        $sql =  $this->GetSearchQuery($sql, $DataModel);	

        // if ($Order != '') {
        //     $sql .= ' ORDER BY '.$Order.' '.$direction;
        // }
        $sql .= " LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_getSlaSvcAreaTotalList($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name;

        $sql =  $this->GetSearchQuery($sql, $DataModel);

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

	public function SaveSlaSvcArea($dataPost)
    {
        try {
			// print_r($dataPost);die();
            $DataModel['id'] = isset($dataPost['id']) ? $dataPost['id'] : 0;
            // $DataModel['Store'] = isset($dataPost['Store']) ? $dataPost['Store'] : '';
            $DataModel['destination_svc_area_code'] = isset($dataPost['destination_svc_area_code']) ? $dataPost['destination_svc_area_code'] : '';
            $DataModel['destination_country'] = isset($dataPost['destination_country']) ? $dataPost['destination_country'] : '';
            $DataModel['product_code'] = isset($dataPost['product_code']) ? $dataPost['product_code'] : '';
            $DataModel['lane'] = isset($dataPost['lane']) ? $dataPost['lane'] : '';
			$DataModel['gateway'] = isset($dataPost['gateway']) ? $dataPost['gateway'] : '';
			$DataModel['t_t_sla'] = isset($dataPost['t_t_sla']) ? $dataPost['t_t_sla'] : '';
            $DataModel['1_mon'] = isset($dataPost['mon']) ? $dataPost['mon'] : '';
            $DataModel['2_tue'] = isset($dataPost['tue']) ? $dataPost['tue'] : '';
            $DataModel['3_wed'] = isset($dataPost['wed']) ? $dataPost['wed'] : '';
            $DataModel['4_thu'] = isset($dataPost['thu']) ? $dataPost['thu'] : '';
            $DataModel['5_fri'] = isset($dataPost['fri']) ? $dataPost['fri'] : '';
            $DataModel['6_sat'] = isset($dataPost['sat']) ? $dataPost['sat'] : '';
            $DataModel['t_t_during_pandemic'] = isset($dataPost['t_t_during_pandemic']) ? $dataPost['t_t_during_pandemic'] : '';
            $DataModel['type'] = isset($dataPost['type']) ? $dataPost['type'] : '';
			
            if ($DataModel['id'] == 0) {
                $DataModel['create_date'] = date('Y-m-d H:i:s');
                $DataModel['create_user'] = $this->session->userdata('user_name');
                $nResult = $this->SQL_insertSlaSvcArea($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                    $result['message'] = 'Save Success';
                } else {
                    $result['status'] = false;
                    $result['message'] = 'Save Fail';
                }
            } else {
                $DataModel['update_date'] = date('Y-m-d H:i:s');
                $DataModel['update_user'] = $this->session->userdata('user_name');
                $uResult = $this->SQL_updateSlaSvcArea($DataModel);
                if ($uResult) {
                    $result['status'] = true;
                    $result['message'] = 'Update Success';
                } else {
                    $result['status'] = false;
                    $result['message'] = 'Update Fail';
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

	public function SQL_insertSlaSvcArea($DataModel)
    {
		// print_r($DataModel);die();
        $this->db->insert($this->tbl_name, $DataModel);

        return $this->db->insert_id();
    }

	public function SQL_updateSlaSvcArea($DataModel)
    {
        $this->db->where('id', $DataModel['id']);

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function GetSearchQuery($sql, $dataModel)
    {
        // print_r($dataModel);die();

        // if (isset($dataModel['Store']) && $dataModel['Store'] != "") {
        //     $sql .= " and Store ='" . $dataModel['Store'] . "'";
        // }
        $sql .= " WHERE destination_svc_area_code like '%" . $dataModel['destination_svc_area_code'] . "%' ";

        // if (isset($dataModel['destination_svc_area_code']) && $dataModel['destination_svc_area_code'] != "") {
        //     $sql .= " and destination_svc_area_code like '%" . $dataModel['destination_svc_area_code'] . "%' ";
        // }

        if (isset($dataModel['destination_country']) && $dataModel['destination_country'] != "") {
            $sql .= " and destination_country like '%" . $dataModel['destination_country'] . "%' ";
        }

        if (isset($dataModel['product_code']) && $dataModel['product_code'] != "") {
            $sql .= " and product_code like '%" . $dataModel['product_code'] . "%' ";
        }
        
        if (isset($dataModel['lane']) && $dataModel['lane'] != "") {
            $sql .= " and lane like '%" . $dataModel['lane'] . "%' ";
        }

        if (isset($dataModel['gateway']) && $dataModel['gateway'] != "") {
            $sql .= " and gateway like '%" . $dataModel['gateway'] . "%' ";
        }
        
        return $sql;
    }

    public function DeleteSlaSvcArea($dataPost)
    {
        try {
            $DataModel['id'] = isset($dataPost['id']) ? $dataPost['id'] : 0;
            $nResult = $this->SQL_DeleteSlaSvcArea($DataModel);
            if ($nResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line('DELETESUCCESS');
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line('DELETEFAIL');
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }


    public function SQL_DeleteSlaSvcArea($DataModel)
    {
        $this->db->where('id', $DataModel['id']);
        $DataModel = [
            'delete_flag' => 1,
        ];

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function delete_slaSvcArea()
    {

        $sql = 'DELETE FROM '. $this->tbl_name;
		$query = $this->db->query($sql);

        // return $query->result_array();
    }
    
    public function update_slaSvcArea($DataModel)
    {
        // print_r($DataModel);
        $sql = 'UPDATE `sla_svc_area` SET new_flag = 0 , update_date = "' .date('Y-m-d H:i:s'). '", update_user = "'.$this->session->userdata('user_name').'"';
        $query = $this->db->query($sql);

        // return $query->result_array();
    }

    public function ExportExcel($dataModel)
	{
        // print_r($dataModel);

		// $sql = "SELECT * FROM v_sla_grouping WHERE week_number = '".$dataModel['week']."' AND Year = ".$dataModel['Year'];
        $sql = 'SELECT * From '.$this->tbl_name;
		// $sql = "SELECT * FROM '.$this->tbl_name.'  WHERE new_flag = 0'";

        // $sql =  $this->GetSearchQuery($sql, $dataModel);

		$query = $this->db->query($sql);

		// print_r($sql);
		return $query->result_array();
	}

    public function insert_to_slaSvcArea($modelData)
	{
		// print_r($modelData);die();
		foreach ($modelData as $k => $v) {
			if (strstr($k," ")){
			   $this->db->set('`'.addslashes($k).'`','"'.$v.'"', false);
			   unset($modelData[$k]);
			}
		}
		$returnDB = $this->db->insert($this->tbl_name, $modelData);


		// $this->db->insert('t_temp_shipment', $modelData);
		return $this->db->insert_id();
	}

    // public function getStoreByShopCode($datapost)
    // {
    //     // print_r($datapost);die;

    //     $sql = "SELECT * FROM t_mapping_shop_code WHERE DeleteFlag = 0  and DHL_Shop_Code = '" . $datapost['DHL_Shop_Code'] . "'";
    //     // echo ($sql);
    //     // die();
    //     // print_r($sql);die;
    //     $query = $this->db->query($sql);
    //     // print_r($query);die;

    //     return  $query->result_array();
    // }

    // public function getStoreByShopCode2($datapost)
    // {

    //     $sql = "SELECT * FROM t_mapping_shop_code WHERE DeleteFlag = 0  and DHL_Shop_Code = '" . $datapost['shopCode'] . "'";
    //     // echo ($sql);
    //     // die();
    //     // print_r($sql);die;
    //     $query = $this->db->query($sql);
    //     // print_r($query);die;

    //     return  $query->result_array();
    // }

	// public function getAgentById($id){
	// 	$this->db->where($this->id, $id);
	// 	return $this->db->get($this->tbl_name);
	// }
	
	// public function insert($modelData){
		 
	//  	$this->db->insert($this->tbl_name, $modelData); 
	// 	return $this->db->insert_id(); 
    // }
     
    // public function update($id, $modelData){
    //     $this->db->where($this->id, $id);
    //     return $this->db->update($this->tbl_name, $modelData);
    // }
	
	// public function getAgentAllList(){
    //     //return $this->db->count_all($this->tbl_name);
        
    //     $this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
	// 	//$this->db->where('Agent_delete_flag', 0);
    //     $query =  $this->db->get($this->tbl_name);
		
	// 	return $query->result_array();
    // }
	
	// public function getAgentModel(){
    //     //return $this->db->count_all($this->tbl_name);
        
    //     //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
	// 	//$this->db->where('Agent_delete_flag', 0);
    //     $query =  $this->db->get($this->tbl_name);
		
	// 	return $query->result_array();
    // }
	
	// // public function getSearchQuery($sql, $dataModel){
		
	// // 	//print_r($dataModel);
		
	// // 	if(isset($dataModel['name']) && $dataModel['name'] != ""){
	// // 	 	$sql .= " and  name like '%".$this->db->escape_str( $dataModel['name'])."%' ";
	// // 	}
		
	// // 	if(isset($dataModel['code']) && $dataModel['code'] != ""){
	// // 	 	$sql .= " and  code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
	// // 	}
		  
	// // 	return $sql;
	// // }
	
	// public function getTotal($dataModel ){
		
	// 	$sql = "SELECT count(*) as qty FROM ". $this->tbl_name  ."   WHERE  delete_flag = 0  ";
				
	// 	$sql =  $this->getSearchQuery($sql, $dataModel);
		
	// 	$query = $this->db->query($sql);	
	    
	// 	$data =  $query->result_array() ;
		 
	// 	return  $data[0]['qty'];
	// }
	
	// public function getShopCodeModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
	// 	$sql = "SELECT * FROM ". $this->tbl_name . " WHERE delete_flag = 0 ";
		
	// 	$sql =  $this->getSearchQuery($sql, $dataModel);		
		
	// 	if($order != ""){
	// 		$sql .= " ORDER BY ".$order." ".$direction;
	// 	}else{
	// 		$sql .= " ORDER BY ".$this->id." ".$direction;
	// 	}
		
	// 	$sql .= " LIMIT ".$offset.", ".$limit;
		
	// 	$query = $this->db->query($sql);
	// 	//$query = $this->db->query($sql, array( "%".$dataModel['Agent_name']."%"));// $dataModel);
		
	// 	return  $query->result_array();
	// }		
	
	// public function deleteAgent($id){
	// 	$result = false;
	// 	try{
	// 		$query = $this->getAgentById($id);
	// 		$modelData;			
	// 		foreach ($query->result() as $row)
	// 		{
			   		
	// 			$modelData = array( 
	// 				'update_date' => date("Y-m-d H:i:s"),
	// 				'update_user' => $this->session->userdata('user_name'),
	// 				'delete_flag' => 1 //$row->Site_delete_flag 
	// 			); 
	// 		}
			
	// 		$this->db->where($this->id, $id);
    //     	return $this->db->update($this->tbl_name, $modelData);
			
	// 	}catch(Exception $ex){
	// 		return $result;
	// 	}
    // }
	
	// public function getAgentComboList(){
		
	// 	$sql = "SELECT id, 	name FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
	// 	$query = $this->db->query($sql);
	// 	return  $query->result_array();
	// }
	
	
	// public function dropAgent(){
		
	// 	$sql = "DELETE  FROM ". $this->tbl_name  ;
	// 	$query = $this->db->query($sql);		 
		
	// 	//return  $query->num_rows() ;
	// }
}

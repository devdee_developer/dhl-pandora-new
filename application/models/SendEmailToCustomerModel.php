<?php

class SendEmailToCustomerModel extends CI_Model
{

	private $tbl_nameapple = '';
	private $tbl_nameshipment = '';
	public function __construct()
	{
		parent::__construct();
	}

	public function ExportExcelDailyToEmail()
	{
		$month = date("m");

		switch ($month) {
			case "01":
				$last_month = "12";
				break;
			case "02":
				$last_month = "01";
				break;
			case "03":
				$last_month = "02";
				break;
			case "04":
				$last_month = "03";
				break;
			case "05":
				$last_month = "04";
				break;
			case "06":
				$last_month = "05";
				break;
			case "07":
				$last_month = "06";
				break;
			case "08":
				$last_month = "07";
				break;
			case "09":
				$last_month = "08";
				break;
			case "10":
				$last_month = "09";
				break;
			case "11":
				$last_month = "10";
				break;
			case "12":
				$last_month = "11";
				break;
			default:
				echo "";
		}

		if ($month == "01") {
			$StartDate = (date("Y") - 1) . "-" . "12";
		} else {
			$StartDate = (date("Y")) . "-" . $last_month;
		}




		$EndtDate = date("Y-m-d");

		$sql = "SELECT * FROM `v_shipment_daily` 
				WHERE `Pickup_Date` >= date('$StartDate-01') AND `Pickup_Date` <= date('$EndtDate')";

		// print_r($sql);
		// die();

		$query = $this->db->query($sql);
		return $query->result_array();
	}
}

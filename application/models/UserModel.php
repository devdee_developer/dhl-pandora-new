<?php
  
class UserModel extends CI_Model {
	
    private $tbl_name = 'user';
	private $id = 'id';
 
   public function __construct() {
        parent::__construct(); 
		// if(! $this->session->userdata('validated')){
            // redirect('login');
        // }
		
		if(! $this->session->userdata('user_name')){
            $data = array("user_name" => "test");
			$this->session->set_userdata($data);
		}
		
		$this->load->library('MySSO');
		$this->load->library('MyEnDecode');
    }
	
	public function getUserNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	 
    public function validate($username, $password){
        // grab user input
       // $username = $this->security->xss_clean($this->input->post('uname'));
        //$password = $this->security->xss_clean($this->input->post('pswsss'));
        
        // Prep the query
        $this->db->where('user', $username);
        $this->db->where('password', md5($password));
        
        // Run the query
        $query = $this->db->get( $this->tbl_name );
        // Let's check if there are any results
        if($query->num_rows() == 1)
        {
            // If there is a user, then create session data
            $row = $query->row();
			//print_r($row);
            $data = array(
                    'id' => $row->id,
                    'user' => $row->user, 
					'user_name' =>  $row->user, 
                    'validated' => true
                    );
            $this->session->set_userdata($data);
            return true;
        }else{
			//echo "count ". $query->num_rows(). "no found ".  md5($password). $username;
		}
        // If the previous process did not validate
        // then return false.
        return false;
    }
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getUserNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('User_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getUserModel(){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('User_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['user']) && $dataModel['user'] != ""){
		 	$sql .= " and user like '%".$this->db->escape_str( $dataModel['user'])."%' ";
		}
		
		// if(isset($dataModel['num_no']) && $dataModel['num_no'] != ""){
		 	// $sql .= " and num_no like '%".$this->db->escape_str( $dataModel['num_no'])."%' ";
		// }
		
		// if(isset($dataModel['com_name']) && $dataModel['com_name'] != ""){
		 	// $sql .= " and com_name like '%".$this->db->escape_str( $dataModel['com_name'])."%' ";
		// }
		
		return $sql;
	}
	
	 
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT count(*) as qty FROM ". $this->tbl_name  ."   WHERE  delete_flag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);	
	    
		$data =  $query->result_array() ;
		 
		return  $data[0]['qty'];
	}
	
	public function getUserNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT id, user, create_date, create_user, update_date, update_user FROM ". $this->tbl_name . " WHERE  delete_flag = 0 ";
		
		$sql =  $this->getSearchQuery($sql, $dataModel);
		   
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}		
	
	public function deleteUsername($id){
		$result = false;
		try{
			$query = $this->getUserNameById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array(  
					'delete_flag' => 1 //$row->Project_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getUserComboList(){
		
		$sql = "SELECT id, 	name FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function validate_with_sso($username, $password){
		 
		$result = $this->mysso->ssoLogin($username, $password);
		  
			
		if(count($result) > 0){
		
			$clear_pass = $this->myendecode->aes_decrypt($result['password'],  'DevdeeThailand');
			 
			if($clear_pass == $password){ 
				$roleData = $this->mysso->getRoleData($result['role_id']);
				
				//ดึงแค่ของ WLHOG 
				$result['role_data'] = $roleData['APPDASH'];
				$result['validated'] =  true;				 
				$this->session->set_userdata($result);
				
				return true;
			}
			
		}
		
		return false;
	}
	
}
?>
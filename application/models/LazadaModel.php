<?php

class LazadaModel extends CI_Model
{

	private $tbl_nameapple = 't_headapple';
	private $tbl_nameshipment = 't_headshipment';
	public function __construct()
	{
		parent::__construct();
	}
	public function getdailyModelList($dataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
	{
		$sql = "SELECT * FROM t_log_import where type = 'lazada' ORDER BY id DESC";

		$sql .= " LIMIT $offset, $limit";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getTotal($dataModel)
	{
		$sql = "SELECT *
						FROM t_headapple
						LEFT JOIN t_headshipment
						ON t_headapple.id = t_headshipment.id";
		$query = $this->db->query($sql);
		return  $query->num_rows();
	}

	public function downloadapple($dataModel)
	{
		$sql = "SELECT filenameapple FROM " . $this->tbl_nameapple . " WHERE id = '" . $dataModel . "'";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function downloadshipment($dataModel)
	{
		$sql = "SELECT filenameshipment FROM " . $this->tbl_nameshipment . " WHERE id = '" . $dataModel . "'";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function insert($modelData)
	{
		// print_r($modelData);die();

		$this->db->insert('t_temp_lazada', $modelData);
		return $this->db->insert_id();
	}

	public function insert_log_lazada($modelData)
	{
		$this->db->insert('t_log_import', $modelData);
		return $this->db->insert_id();
	}
	public function delete_temp_lazada()
	{
		$sql = "Delete FROM t_temp_lazada";
		$query = $this->db->query($sql);
	}
	public function check_duplicate()
	{
		$sql = "SELECT t1.order_item_id
		
		from t_temp_lazada as t1 
		inner join t_lazada as t2
		
		on  t1.order_item_id = t2.order_item_id";


		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function insertto_t_lazada()
	{
		$sql = "INSERT INTO t_lazada SELECT * FROM t_temp_lazada";
		$query = $this->db->query($sql);
	}

	public function deleteImport($dataModel)
	{
		$sql = "Delete from t_lazada where file_name = '" . $dataModel . "'";
		$query = $this->db->query($sql);
		$sql = "Delete from t_customer where file_name = '" . $dataModel . "'";
		$query = $this->db->query($sql);
		$sql1 = "Delete from t_log_import where file_name = '" . $dataModel . "'";
		return  $query = $this->db->query($sql1);
	}

	public function insertto_customer($file_name, $import_date)
	{
		$sql = "SET @row_number = (SELECT IF(RIGHT(MAX(customer_no),6) is null, 0, RIGHT(MAX(customer_no),6)) FROM t_customer)";
		$query = $this->db->query($sql);
		$sql = "
		INSERT INTO t_customer
		SELECT 0 id,CONCAT('L',LPAD((@row_number:=@row_number + 1),6,0)) AS customer_no, t1.*,'" . $import_date . "' import_date,'" . $file_name . "' file_name, 0 delete_flag FROM(
		SELECT DISTINCT t1.shipping_name customer_name,LEFT(t1.shipping_address,50) address1,SUBSTRING(t1.shipping_address,51,50) address2,
		SUBSTRING(t1.shipping_address,101,50) address3, SUBSTRING(t1.shipping_address,151,50) address4 , t1.shipping_address4 district, t2.prov_state province, 
		t1.shipping_address5 postal_code, '134' bang, '9EC' type, 'LOCAL' end_user_type, 'Sale-Lazada' invoice_category, '1' corporate_credit, 'D000000' corporate_customer, t1.tax_code 
		FROM t_temp_lazada t1
		LEFT JOIN t_prov_cus t2 on t2.description 
		LIKE concat ('%',t1.shipping_address3,'%')
		
		ORDER BY t1.id
		) t1";

		return $query = $this->db->query($sql);
	}

	public function CreateNumberSystem($dataModel)
	{
		$dataModel['order_number'] = sprintf('%f', $dataModel['order_number']);
		$dataModel['order_number'] = number_format($dataModel['order_number'], 0);
		$dataModel['order_number'] = str_replace(",", "", $dataModel['order_number']);
		$year = substr($dataModel['updated_at'], 0, 4);
		$Fullyear = $year + 543;
		$y = substr($Fullyear, 2, 2);
		// echo $y ;die();
		$sql = "SELECT * FROM t_temp_lazada";

		$query = $this->db->query($sql);
		$tempdata = $query->result_array();
		if ($tempdata) {
			$sql = "SELECT * FROM t_temp_lazada where order_number='" . $dataModel['order_number'] . "'";
			// echo $sql;die();
			$query = $this->db->query($sql);
			$tempdatainorder = $query->result_array();
			if ($tempdatainorder) {
				// echo  $tempdatainorder[0]['order_number_system'],':',1; die();
				$ordernumbersystem = $tempdatainorder[0]['order_number_system'];
			} else {

				$sql = "SELECT CONCAT('L" . $y . "',LPAD(max(RIGHT(order_number_system,7))+1,7,0)) ordernumbersystem FROM t_temp_lazada WHERE delete_flag = 0";
				$query = $this->db->query($sql);
				$ordernumbersystem = $query->result_array()[0]['ordernumbersystem'];
				// echo $ordernumbersystem,':',2; die();
			}
		} else {
			$sql = "SELECT * FROM t_lazada where order_number='" . $dataModel['order_number'] . "'";
			$query = $this->db->query($sql);
			$datainorder = $query->result_array();
			if ($datainorder) {
				// echo  $dataModel['order_number'],':',3; die();
				$ordernumbersystem = $datainorder[0]['order_number_system'];
			} else {

				$sql = "SELECT CONCAT('L" . $y . "',LPAD(IF(max(RIGHT(order_number_system,7)) is null,0,max(RIGHT(order_number_system,7)))+1,7,0)) ordernumbersystem  FROM t_lazada WHERE delete_flag = 0";
				$query = $this->db->query($sql);
				$ordernumbersystem = $query->result_array()[0]['ordernumbersystem'];
				// echo  $ordernumbersystem,':',4; die();
			}
		}

		return $ordernumbersystem;
	}

	public function GetcustomerExport($dataModel)
	{
		$sql = "SELECT * FROM t_customer WHERE delete_flag = 0";
		if (isset($dataModel['file_name']) && $dataModel['file_name'] != "") {
			$sql .= " and file_name = '" . $dataModel['file_name'] . "'";
		}
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getDataOderHeader($dataModel)
	{
		$sql = "SELECT  t1.order_item_id, t1.order_number_system order_number, t2.customer_no,DATE(t1.updated_at) order_date,'BDC' warehouse 
		FROM t_lazada t1 
		INNER JOIN t_customer t2 on t1.shipping_name = t2.customer_name 
		WHERE t1.delete_flag = 0 and t1.file_name='" . $dataModel['file_name'] . "' and t2.file_name='" . $dataModel['file_name'] . "'";

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getDataOderDetail($dataModel)
	{
		$sql = "SET @row_number := 0";
		$query = $this->db->query($sql);

		$sql = "SET @order_no := '' COLLATE utf8_unicode_ci";
		$query = $this->db->query($sql);

		$sql = "SELECT
		t1.order_item_id,
	t1.order_number_system,
	@row_number :=
CASE
WHEN @order_no = t1.order_number THEN
	@row_number + 1 ELSE 1 
	END AS line,
	@order_no := t1.order_number order_number,
	seller_sku item,
	COUNT( t1.seller_sku ) qty,
	CAST( t1.paid_price - ( t1.paid_price * 7 / 107 ) AS DECIMAL ( 20, 2 ) ) price,
	t1.shipping_name customer,
	t1.paid_price - ( t1.paid_price * 7 / 107 ) paid_inc,
	t1.paid_price 
FROM
	t_lazada t1 
	WHERE
	t1.delete_flag = 0 and t1.file_name='" . $dataModel['file_name'] . "' 
	GROUP BY
		t1.order_number_system,
		t1.seller_sku
	ORDER BY id";

		// echo $sql;die();

		$query = $this->db->query($sql);
		return  $query->result_array();
	}
}

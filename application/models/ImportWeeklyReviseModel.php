<?php

class ImportWeeklyReviseModel extends CI_Model
{

	private $tbl_nameapple = 't_headapple';
	private $tbl_nameshipment = 't_headshipment';
	public function __construct()
	{
		parent::__construct();
	}
	public function getdailyModelList($dataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
	{
		$sql = "SELECT * FROM t_log_shipment_weekly_revise where type = 'Excel Shipment Weekly Revise Upload' and 	delete_flag = 0 ORDER BY file_name DESC";

		$sql .= " LIMIT $offset, $limit";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getDataCarrierTrackingNo($lastdate)
	{
		// print_r($lastdate);
		$sql = "SELECT DISTINCT carrier_tracking_no FROM t_shipment_weekly_revise WHERE delete_flag = 0 AND latest_checkpiont not in('OK','RT') AND  ssd_status in('Due Today','Overdue','Future Shipments') 
		AND date(create_date) ='" . $lastdate . "'";

		// echo $sql;
		// die();

		// $sql = "SELECT DISTINCT carrier_tracking_no FROM t_shipment_weekly_revise WHERE carrier_tracking_no = '6347384956'";  
		// $sql = "SELECT DISTINCT carrier_tracking_no FROM t_shipment_weekly_revise WHERE carrier_tracking_no = '5719649806'";  

		// $sql .= " LIMIT $offset, $limit";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getLastDate()
	{
		$sql = "SELECT max(date(create_date)) last_date FROM t_shipment_weekly_revise WHERE delete_flag = 0 ";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function getDatalastCheckPointStatus_OH()
	{
		$sql = "SELECT DISTINCT carrier_tracking_no FROM t_shipment_weekly_revise WHERE delete_flag = 0 AND latest_checkpiont ='OH' AND  ssd_status in('Due Today','Overdue','Future Shipments')";

		// $sql = "SELECT DISTINCT carrier_tracking_no FROM t_shipment_weekly_revise WHERE carrier_tracking_no = '6347384956'";  
		// $sql = "SELECT DISTINCT carrier_tracking_no FROM t_shipment_weekly_revise WHERE carrier_tracking_no = '5719649806'";  

		// $sql .= " LIMIT $offset, $limit";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getTotal($dataModel)
	{
		$sql = 'SELECT count(*) as qty FROM t_log_shipment_weekly_revise  WHERE  delete_flag = 0  ';

		// $sql = $this->getSearchQuery($sql, $dataModel);

		$query = $this->db->query($sql);

		$data = $query->result_array();

		return  $data[0]['qty'];
	}

	public function downloadapple($dataModel)
	{
		$sql = "SELECT filenameapple FROM " . $this->tbl_nameapple . " WHERE id = '" . $dataModel . "'";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function downloadshipment($dataModel)
	{
		$sql = "SELECT filenameshipment FROM " . $this->tbl_nameshipment . " WHERE id = '" . $dataModel . "'";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function insert_to_temp_weekly_revise($modelData)
	{
		// print_r($modelData);die();
		foreach ($modelData as $k => $v) {
			if (strstr($k, " ")) {
				$this->db->set('`' . addslashes($k) . '`', '"' . $v . '"', false);
				unset($modelData[$k]);
			}
		}
		$returnDB = $this->db->insert('t_temp_shipment_weekly_revise', $modelData);


		// $this->db->insert('t_temp_shipment_weekly_revise', $modelData);
		return $this->db->insert_id();
	}

	public function insert_log_shipment($modelData)
	{
		// print_r($modelData);die();
		$this->db->insert('t_log_shipment_weekly_revise', $modelData);
		return $this->db->insert_id();
	}
	public function insert_t_shipment_weekly_revise($modelData)
	{
		$this->db->insert('t_shipment_weekly_revise', $modelData);
		return $this->db->insert_id();
	}
	public function delete_temp_weekly_revise()
	{
		$sql = "Delete FROM t_temp_shipment_weekly_revise";
		$query = $this->db->query($sql);

		// echo $sql;
		// die();
	}
	public function check_duplicate($Waybill)
	{
		$sql = "SELECT awb_no FROM t_shipment_weekly_revise WHERE awb_no = '" . $Waybill . "'";

		$query = $this->db->query($sql);
		// print_r($query->num_rows());die();
		// print_r($sql);die();
		return  $query->num_rows();
	}

	public function check_nulldata()
	{
		$sql = "SELECT Factor_Incident,Factor FROM t_temp_shipment_weekly_revise WHERE Factor_Incident = '' OR Factor = '';";

		$query = $this->db->query($sql);
		// print_r($query->num_rows());die();
		// print_r($sql);die();
		return  $query->num_rows();
	}
	// public function check_status_date()
	// {
	// 	$sql = "SELECT * FROM `t_temp_shipment_weekly_revise` WHERE Status_Date = '1970-01-01' ";

	// 	$query = $this->db->query($sql);
	// 	// print_r($query->num_rows());die();
	// 	// print_r($sql);die();
	// 	// return  $query->num_rows();
	// 	return $query->result_array();
	// }

	public function update_t_shipment_weekly_revise()
	{
		$sql = "UPDATE t_shipment_weekly_revise t1
		INNER JOIN t_temp_shipment_weekly_revise AS t2 
		ON t1.awb_no = t2.awb_no

		SET  t1.awb_no = t2.awb_no
			,t1.`Group_Diff_SLA_(Include_weekend)` = t2.`Group_Diff_SLA_(Include_weekend)`
			,t1.factor = t2.factor
			,t1.factor_incident = t2.factor_incident
			,t1.import_date = t2.import_date
			
			,t1.file_name = t2.file_name
			,t1.create_user = t2.create_user
			,t1.create_date = t2.create_date
			,t1.update_user = t2.update_user
			,t1.update_date = NOW()
		
		
		";

		// print_r($sql);die();

		$query = $this->db->query($sql);
		// return  $query->num_rows();
	}

	public function update_t_shipment_weekly_revise_to_cs()
	{
		$sql = "UPDATE t_shipment_weekly_revise t1
		INNER JOIN t_shipment_status_cs AS t2 
		ON t1.awb_no = t2.Waybill
		SET 	t2.Factor_Incident = t1.factor_incident
				,t2.Factor = t1.factor
		WHERE t2.Factor_Incident <> t1.factor_incident 
		   OR t2.Factor <> t1.factor
		";

		// print_r($sql);die();

		$query = $this->db->query($sql);
		// return  $query->num_rows();
	}

	public function checkpoint_shipment_weekly_revise()
	{
		$sql = "UPDATE t_shipment_weekly_revise as t1
				INNER JOIN t_shipment_daily as t2 on t1.Waybill = t2.HAWB_No
				LEFT JOIN v_sla_grouping as t3 on t1.Waybill = `t3`.`Waybill Number`
				SET t1.Factor = `t3`.`On time status` ,
				t1.Factor_Incident = t3.factor_incident
				WHERE  t2.SLA_Remark <> 'On time' and (t1.Factor_Incident = 'Ontime' or t1.Factor_Incident ='')
		 		AND	t3.`Group Diff SLA ( Include weekend )` is not null and t3.`Group Diff SLA ( Include weekend )` <> 'On time' 
				AND t3.factor_incident <> 'Wait for check checkpoint event'";

		return  $this->db->query($sql);
		// return  $query->result_array();
	}

	public function updatename_shipment_status_cs()
	{
		$sql = "UPDATE t_shipment_weekly_revise as t1
		INNER join t_shipment_daily as t2 on t1.Waybill = t2.HAWB_No
		SET t2.POD = t1.Receiver_name , t2.Delivery = 'Yes' , t2.Status_Date = t1.Delivery_date
		WHERE t2.POD = ''
		and t1.Receiver_name <> ''
		and t1.Delivery_date <> ''";

		return  $this->db->query($sql);
		// return  $query->result_array();
	}

	public function DeleteTableTemp()
	{
		$sql = "TRUNCATE t_temp_shipment_weekly_revise";


		return  $this->db->query($sql);
		// return  $query->result_array();
	}

	public function delete_t_shipment_weekly_revise()
	{
		// print_r('test');
		$sql = "TRUNCATE t_shipment_weekly_revise";


		return  $this->db->query($sql);
		// return  $query->result_array();
	}

	public function delete_t_temp_shipment_weekly_revise()
	{
		// print_r('test');
		$sql = "TRUNCATE t_temp_shipment_weekly_revise";


		return  $this->db->query($sql);
		// return  $query->result_array();
	}

	public function insertto_t_shipment_weekly_revise()
	{
		$sql = "INSERT INTO t_shipment_weekly_revise SELECT * FROM t_temp_shipment_weekly_revise ";
		// ของเดิม มี Where เพิ่ม ssd_status != 'Future Shipments' and   Comment วันที่ 2021-09-01
		$query = $this->db->query($sql);
	}

	public function deleteImport($dataModel)
	{
		// echo $dataModel;
		$sql = "Delete from t_shipment_weekly_revise where file_name = '" . $dataModel . "'";
		$query = $this->db->query($sql);
		$sql1 = "Delete from t_temp_shipment_weekly_revise where file_name = '" . $dataModel . "'";
		$query = $this->db->query($sql1);
		$sql2 = "Delete from t_log_shipment_weekly_revise where file_name = '" . $dataModel . "'";
		return  $query = $this->db->query($sql2);
	}

	// public function insertto_customer($file_name, $import_date)
	// {
	// 	$sql = "SET @row_number = (SELECT IF(RIGHT(MAX(customer_no),6) is null, 0, RIGHT(MAX(customer_no),6)) FROM t_customer)";
	// 	$query = $this->db->query($sql);
	// 	$sql = "
	// 	INSERT INTO t_customer
	// 	SELECT 0 id,CONCAT('L',LPAD((@row_number:=@row_number + 1),6,0)) AS customer_no, t1.*,'" . $import_date . "' import_date,'" . $file_name . "' file_name, 0 delete_flag FROM(
	// 	SELECT DISTINCT t1.shipping_name customer_name,LEFT(t1.shipping_address,50) address1,SUBSTRING(t1.shipping_address,51,50) address2,
	// 	SUBSTRING(t1.shipping_address,101,50) address3, SUBSTRING(t1.shipping_address,151,50) address4 , t1.shipping_address4 district, t2.prov_state province, 
	// 	t1.shipping_address5 postal_code, '134' bang, '9EC' type, 'LOCAL' end_user_type, 'Sale-Lazada' invoice_category, '1' corporate_credit, 'D000000' corporate_customer, t1.tax_code 
	// 	FROM t_temp_lazada t1
	// 	LEFT JOIN t_prov_cus t2 on t2.description 
	// 	LIKE concat ('%',t1.shipping_address3,'%')

	// 	ORDER BY t1.id
	// 	) t1";

	// 	return $query = $this->db->query($sql);
	// }

	public function CreateNumberSystem($dataModel)
	{
		$dataModel['order_number'] = sprintf('%f', $dataModel['order_number']);
		$dataModel['order_number'] = number_format($dataModel['order_number'], 0);
		$dataModel['order_number'] = str_replace(",", "", $dataModel['order_number']);
		$year = substr($dataModel['updated_at'], 0, 4);
		$Fullyear = $year + 543;
		$y = substr($Fullyear, 2, 2);
		// echo $y ;die();
		$sql = "SELECT * FROM t_temp_lazada";

		$query = $this->db->query($sql);
		$tempdata = $query->result_array();
		if ($tempdata) {
			$sql = "SELECT * FROM t_temp_lazada where order_number='" . $dataModel['order_number'] . "'";
			// echo $sql;die();
			$query = $this->db->query($sql);
			$tempdatainorder = $query->result_array();
			if ($tempdatainorder) {
				// echo  $tempdatainorder[0]['order_number_system'],':',1; die();
				$ordernumbersystem = $tempdatainorder[0]['order_number_system'];
			} else {

				$sql = "SELECT CONCAT('L" . $y . "',LPAD(max(RIGHT(order_number_system,7))+1,7,0)) ordernumbersystem FROM t_temp_lazada WHERE delete_flag = 0";
				$query = $this->db->query($sql);
				$ordernumbersystem = $query->result_array()[0]['ordernumbersystem'];
				// echo $ordernumbersystem,':',2; die();
			}
		} else {
			$sql = "SELECT * FROM t_lazada where order_number='" . $dataModel['order_number'] . "'";
			$query = $this->db->query($sql);
			$datainorder = $query->result_array();
			if ($datainorder) {
				// echo  $dataModel['order_number'],':',3; die();
				$ordernumbersystem = $datainorder[0]['order_number_system'];
			} else {

				$sql = "SELECT CONCAT('L" . $y . "',LPAD(IF(max(RIGHT(order_number_system,7)) is null,0,max(RIGHT(order_number_system,7)))+1,7,0)) ordernumbersystem  FROM t_lazada WHERE delete_flag = 0";
				$query = $this->db->query($sql);
				$ordernumbersystem = $query->result_array()[0]['ordernumbersystem'];
				// echo  $ordernumbersystem,':',4; die();
			}
		}

		return $ordernumbersystem;
	}

	public function GetcustomerExport($dataModel)
	{
		$sql = "SELECT * FROM t_customer WHERE delete_flag = 0";
		if (isset($dataModel['file_name']) && $dataModel['file_name'] != "") {
			$sql .= " and file_name = '" . $dataModel['file_name'] . "'";
		}
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getDataOderHeader($dataModel)
	{
		$sql = "SELECT  t1.order_item_id, t1.order_number_system order_number, t2.customer_no,DATE(t1.updated_at) order_date,'BDC' warehouse 
		FROM t_lazada t1 
		INNER JOIN t_customer t2 on t1.shipping_name = t2.customer_name 
		WHERE t1.delete_flag = 0 and t1.file_name='" . $dataModel['file_name'] . "' and t2.file_name='" . $dataModel['file_name'] . "'";

		$query = $this->db->query($sql);
		return  $query->result_array();
	}


	public function insert_t_status_OH($modelData)
	{
		// print_r($modelData);die();

		$this->db->insert('t_status_oh', $modelData);
		return $this->db->insert_id();
	}


	public function update_carrier_tracking_no($data_carrier_tracking_no, $modelData)
	{
		$this->db->where('carrier_tracking_no', $data_carrier_tracking_no);
		return $this->db->update('t_shipment_weekly_revise', $modelData);
	}

	public function update_service_center($data_service_center, $modelData)
	{
		$this->db->where('service_center', $data_service_center);
		return $this->db->update('t_shipment_weekly_revise', $modelData);
	}

	public function getDataOderDetail($dataModel)
	{
		$sql = "SET @row_number := 0";
		$query = $this->db->query($sql);

		$sql = "SET @order_no := '' COLLATE utf8_unicode_ci";
		$query = $this->db->query($sql);

		$sql = "SELECT
		t1.order_item_id,
	t1.order_number_system,
	@row_number :=
CASE
WHEN @order_no = t1.order_number THEN
	@row_number + 1 ELSE 1 
	END AS line,
	@order_no := t1.order_number order_number,
	seller_sku item,
	COUNT( t1.seller_sku ) qty,
	CAST( t1.paid_price - ( t1.paid_price * 7 / 107 ) AS DECIMAL ( 20, 2 ) ) price,
	t1.shipping_name customer,
	t1.paid_price - ( t1.paid_price * 7 / 107 ) paid_inc,
	t1.paid_price 
FROM
	t_lazada t1 
	WHERE
	t1.delete_flag = 0 and t1.file_name='" . $dataModel['file_name'] . "' 
	GROUP BY
		t1.order_number_system,
		t1.seller_sku
	ORDER BY id";

		// echo $sql;die();

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function UpdateOhToNewStatus()
	{
		$sql = "UPDATE t_shipment_weekly_revise t3 
		INNER JOIN 
		
			(SELECT t1.* FROM t_status_oh as t1
			INNER JOIN
			(SELECT MAX(id) as last_id,carrier_tracking_no FROM `t_status_oh` WHERE latest_checkpiont !='OH' GROUP BY carrier_tracking_no) t2 

		on t2.last_id = t1.id) as V1 ON V1.carrier_tracking_no = t3.carrier_tracking_no 
		SET t3.latest_checkpiont = V1.latest_checkpiont, t3.service_center = V1.service_center, t3.event_location_state_code = V1.event_location_state_code";

		$query = $this->db->query($sql);


		$sql = "TRUNCATE t_status_oh";
		$query = $this->db->query($sql);

		return  $query;
	}
}

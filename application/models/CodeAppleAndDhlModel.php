<?php

class CodeAppleAndDhlModel extends CI_Model
{

	private $tbl_name = 't_code_apple_and_dhl';
	private $id = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	public function getCodeAppleAndDhlNameById($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}

	public function insert($modelData)
	{

		$this->db->insert($this->tbl_name, $modelData);
		return $this->db->insert_id();
	}

	public function update($id, $modelData)
	{
		$this->db->where($this->id, $id);
		return $this->db->update($this->tbl_name, $modelData);
	}

	public function getcodeappleanddhlNameAllList()
	{
		//return $this->db->count_all($this->tbl_name);

		$this->db->select('id', 'name', 'contact', 'address1', 'address2', 'address3', 'tel', 'email', 'taxid', 'website');
		//$this->db->where('Admin_delete_flag', 0);
		$query =  $this->db->get($this->tbl_name);

		return $query->result_array();
	}

	public function getcodeappleanddhlModel($id)
	{
		//return $this->db->count_all($this->tbl_name);

		//$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('delete_flag', 1);
		$this->db->where($this->id, $id);
		$query =  $this->db->get($this->tbl_name);

		return $query->result_array();
	}

	public function getSearchQuery($sql, $dataModel)
	{

		//print_r($dataModel);

		if (isset($dataModel['reason_code']) && $dataModel['reason_code'] != "") {
			$sql .= " and reason_code like '%" . $this->db->escape_str($dataModel['reason_code']) . "%' ";
		}

		if (isset($dataModel['check_point']) && $dataModel['check_point'] != "") {
			$sql .= " and check_point like '%" . $this->db->escape_str($dataModel['reason_code']) . "%' ";
		}


		return $sql;
	}

	public function getTotal($dataModel)
	{

		$sql = "SELECT * FROM " . $this->tbl_name  . " WHERE delete_flag = 0 ";

		$sql =  $this->getSearchQuery($sql, $dataModel);

		$query = $this->db->query($sql);

		return  $query->num_rows();
	}

	public function getCodeAppleAndDhlNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc')
	{

		$sql = "SELECT * FROM " . $this->tbl_name . " WHERE delete_flag = 0 ";

		$sql =  $this->getSearchQuery($sql, $dataModel);


		// if($order != ""){
		// $this->db->order_by($order, $direction);
		// }else{
		// $this->db->order_by($this->id ,$direction); 
		// }

		if ($order != "") {
			$sql .= " ORDER BY " . $order . " " . $direction;
		} else {
			$sql .= " ORDER BY " . $this->id . " " . $direction;
		}

		$sql .= " LIMIT $offset, $limit";

		//print($sql );

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function deleteCodeAppleAndDhlBy($id)
	{
		$result = false;
		try {
			$query = $this->getCodeAppleAndDhlNameById($id);
			$modelData;
			foreach ($query->result() as $row) {

				$modelData = array(
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'delete_flag' => 1 //$row->Admin_delete_flag 
				);
			}

			$this->db->where($this->id, $id);
			return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);

		} catch (Exception $ex) {
			return $result;
		}
	}

	public function getCodeAppleAndDhlComboList()
	{

		$sql = "SELECT *  FROM " . $this->tbl_name . "delete_flag = 0";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}


	public function check_duplicate($dataCheckPoint)
	{
		if($dataCheckPoint['id'] == '0'){
			$sql = "SELECT * FROM " . $this->tbl_name . " WHERE delete_flag = 0 and check_point = '" . $dataCheckPoint['check_point'] ."'";
		} else {
			$sql = "SELECT * FROM " . $this->tbl_name . " WHERE id != '" . $dataCheckPoint['id'] ."' and delete_flag = 0 and check_point = '" . $dataCheckPoint['check_point'] ."'";
		}
		  

		$query = $this->db->query($sql);

		return  $query->result_array();
	}
}

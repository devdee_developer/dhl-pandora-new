<?php

use LDAP\Result;
use setasign\Fpdi\Fpdi;

class ReportPerformanceDashboardModel extends CI_Model
{

	private $tbl_name = 't_code_apple_and_dhl';
	private $tbl_t_shipment_daily = 't_shipment_daily';
	private $tbl_wk = 'wk';
	private $v_shipment_daily = 'v_shipment_daily';
	private $id = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	public function getCodeAppleAndDhlNameById($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}

	// public function getQuaterModelList($dataModel, $limit = 100, $offset = 0, $Order = '', $direction = 'desc')
	// {
	// 	$sql = "SELECT * FROM `v_sla_grouping` ORDER BY `Quater` DESC";

	// 	$sql .= " LIMIT $offset, $limit";
	// 	$query = $this->db->query($sql);
	// 	return  $query->result_array();
	// }



	public function getYearModelList($dataModel, $limit = 100, $offset = 0, $Order = '', $direction = 'desc')
	{
		$sql = "SELECT DISTINCT `Year` FROM `v_shipment_daily` ORDER BY `Year` DESC";

		// print_r($sql);
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function ExportGraphPdfPerformanceDashboard($dataPost)
	{
		// print_r($dataPost);
		$image1 = isset($dataPost['image1']) ? $dataPost['image1'] : '';
		$image2 = isset($dataPost['image2']) ? $dataPost['image2'] : '';
		$image3 = isset($dataPost['image3']) ? $dataPost['image3'] : '';
		$image4 = isset($dataPost['image4']) ? $dataPost['image4'] : '';
		$image5 = isset($dataPost['image5']) ? $dataPost['image5'] : '';
		//ต้องตัดคำว่า นี้ data:image/png;base64, ออกไปก่อน
		$b64_1 = substr($image1, 22);
		$b64_2 = substr($image2, 22);
		$b64_3 = substr($image3, 22);
		$b64_4 = substr($image4, 22);
		$b64_5 = substr($image5, 22);
		// Obtain the original content (usually binary data)
		$bin1 = base64_decode($b64_1);
		$bin2 = base64_decode($b64_2);
		$bin3 = base64_decode($b64_3);
		$bin4 = base64_decode($b64_4);
		$bin5 = base64_decode($b64_5);
		// Load GD resource from binary data
		$im1 = imagecreatefromstring($bin1);
		$im2 = imagecreatefromstring($bin2);
		$im3 = imagecreatefromstring($bin3);
		$im4 = imagecreatefromstring($bin4);
		$im5 = imagecreatefromstring($bin5);

		// Make sure that the GD library was able to load the image
		// This is important, because you should not miss corrupted or unsupported images
		if (!$im1) {
			print_r($image1);
			die('Base64 value is not a valid image');
		}
		if (!$im2) {
			print_r($image2);
			die('Base64 value is not a valid image');
		}
		if (!$im3) {
			print_r($image3);
			die('Base64 value is not a valid image');
		}
		if (!$im4) {
			print_r($image4);
			die('Base64 value is not a valid image');
		}
		if (!$im5) {
			print_r($image5);
			die('Base64 value is not a valid image');
		}

		$img_name1 = 'OverviewPerformance' . '_' . date('Y-m-d-H-i-s') . '.png';
		$filelocation_img1 = 'pandora' . '/' . 'img/';

		$img_name2 = 'VolumebyCategory' . '_' . date('Y-m-d-H-i-s') . '.png';
		$filelocation_img2 = 'pandora' . '/' . 'img/';

		$img_name3 = 'DHLSupport' . '_' . date('Y-m-d-H-i-s') . '.png';
		$filelocation_img3 = 'pandora' . '/' . 'img/';

		$img_name4 = 'CustomerSupport' . '_' . date('Y-m-d-H-i-s') . '.png';
		$filelocation_img4 = 'pandora' . '/' . 'img/';

		$img_name5 = 'Uncontrollable' . '_' . date('Y-m-d-H-i-s') . '.png';
		$filelocation_img5 = 'pandora' . '/' . 'img/';

		$file_name = 'pandora' . '_' . date('Y-m-d-H-i-s') . '.pdf';
		$filelocation_pdf = 'pandora' . '/' . 'pdf/';
		// Specify the location where you want to save the image
		$img_file1 = FCPATH . '/upload/Graph/' . $filelocation_img1 . $img_name1;
		$img_url1 = './upload/Graph/' . $filelocation_img1 . $img_name1;

		$img_file2 = FCPATH . '/upload/Graph/' . $filelocation_img2 . $img_name2;
		$img_url2 = './upload/Graph/' . $filelocation_img2 . $img_name2;

		$img_file3 = FCPATH . '/upload/Graph/' . $filelocation_img3 . $img_name3;
		$img_url3 = './upload/Graph/' . $filelocation_img3 . $img_name3;

		$img_file4 = FCPATH . '/upload/Graph/' . $filelocation_img4 . $img_name4;
		$img_url4 = './upload/Graph/' . $filelocation_img4 . $img_name4;

		$img_file5 = FCPATH . '/upload/Graph/' . $filelocation_img5 . $img_name5;
		$img_url5 = './upload/Graph/' . $filelocation_img5 . $img_name5;
		$pdf_file = FCPATH . '/upload/Graph/' . $filelocation_pdf . $file_name;

		// check path
		$file_pathimgexist1 = FCPATH . '/upload/Graph/' . $filelocation_img1;
		$file_pathimgexist2 = FCPATH . '/upload/Graph/' . $filelocation_img2;
		$file_pathimgexist3 = FCPATH . '/upload/Graph/' . $filelocation_img3;
		$file_pathimgexist4 = FCPATH . '/upload/Graph/' . $filelocation_img4;
		$file_pathimgexist5 = FCPATH . '/upload/Graph/' . $filelocation_img5;

		$file_pathpdfexist = FCPATH . '/upload/Graph/' . $filelocation_pdf;
		if (!file_exists($file_pathimgexist1)) {
			mkdir($file_pathimgexist1, 0777, true);
		}
		if (!file_exists($file_pathimgexist2)) {
			mkdir($file_pathimgexist2, 0777, true);
		}
		if (!file_exists($file_pathimgexist3)) {
			mkdir($file_pathimgexist3, 0777, true);
		}
		if (!file_exists($file_pathimgexist4)) {
			mkdir($file_pathimgexist4, 0777, true);
		}
		if (!file_exists($file_pathimgexist5)) {
			mkdir($file_pathimgexist5, 0777, true);
		}
		if (!file_exists($file_pathpdfexist)) {

			mkdir($file_pathpdfexist, 0777, true);
		}
		// Save the GD resource as PNG in the best possible quality (no compression)
		// This will strip any metadata or invalid contents (including, the PHP backdoor)
		// To block any possible exploits, consider increasing the compression level
		imagepng($im1, $img_file1, 0);
		imagepng($im2, $img_file2, 0);
		imagepng($im3, $img_file3, 0);
		imagepng($im4, $img_file4, 0);
		imagepng($im5, $img_file5, 0);

		global $pdf;
		define('FPDF_FONTPATH', APPPATH . 'fpdf/font/');
		require APPPATH . 'fpdf/fpdf.php';
		require_once APPPATH . 'fpdi/autoload.php';
		$pdf = new FPDI('L', 'mm', 'A4');
		$filename = APPPATH . '/../application/form/pandora.pdf';
		$pdf->AddPage();
		$pdf->setSourceFile($filename);
		$tplIdx = $pdf->importPage(1);
		$pdf->useTemplate($tplIdx, 0, 0, 297);
		$pdf->Image($img_url1, 45, 45, 90);
		$pdf->Image($img_url2, 175, 45, 90);
		$pdf->Image($img_url3, 55, 137, 45);
		$pdf->Image($img_url4, 125, 137, 45);
		$pdf->Image($img_url5, 195, 137, 45);

		$pdf->Output($pdf_file, 'F');
		// $result['status'] = true;
		// $result['message'] = $filelocation_pdf . $file_name;

		return $filelocation_pdf . $file_name;
		// print_r($sql);
		// $query = $this->db->query($sql);
		// return  $query->result_array();
	}

	public function getOverviewPerformance($dataPost)
	{
		// print_r($dataPost);

		$sql = 'SELECT status,COUNT(status) AS Qty FROM ' . $this->v_shipment_daily . ' AS t1';

		if ($dataPost['month'] != '(undefined)' and $dataPost['month'] != '') {
			$sql .= ' WHERE t1.Month in ' . $dataPost['month'];
		} else {
			$sql .= " WHERE Month in (LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 2 MONTH,'%M'),3),LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 1 MONTH,'%M'),3), LEFT(DATE_FORMAT(CURRENT_DATE,'%M'),3))";
		}
		if ($dataPost['week'] != '(undefined)' and $dataPost['week'] != '') {
			$sql .= " AND t1.week in " . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != '(undefined)' and $dataPost['country'] != '') {
			$sql .= " AND t1.Destination_Ctry in " . $dataPost['country'];
		} else {
		}
		if ($dataPost['region'] != '(undefined)' and $dataPost['region'] != '') {
			$sql .= " AND t1.Region in " . $dataPost['region'];
		} else {
		}
		if ($dataPost['gateway'] != '(undefined)' and $dataPost['gateway'] != '') {
			$sql .= " AND t1.Gateway in " . $dataPost['gateway'];
		} else {
		}
		if ($dataPost['oversla'] != '(undefined)' and $dataPost['oversla'] != '') {
			$sql .= " AND t1.Group_Diff_SLA in " . $dataPost['oversla'];
		} else {
		}
		$sql .= " GROUP BY status";
		// print_r($sql);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getVolumebyCategory($dataPost)
	{
		// print_r($dataPost);

		$sql = "SELECT Reporting_Code_Category,COUNT(Reporting_Code_Category) AS Qty,Month FROM ". $this->v_shipment_daily ." AS t1
				WHERE Reporting_Code_Category IS NOT null AND Reporting_Code_Category <> ''";

		if ($dataPost['month'] != '(undefined)' and $dataPost['month'] != '') {
			$sql .= ' AND t1.Month in ' . $dataPost['month'];
		} else {
			$sql .= " AND Month in (LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 2 MONTH,'%M'),3),LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 1 MONTH,'%M'),3), LEFT(DATE_FORMAT(CURRENT_DATE,'%M'),3))";
		}
		if ($dataPost['week'] != '(undefined)' and $dataPost['week'] != '') {
			$sql .= " AND t1.week in " . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != '(undefined)' and $dataPost['country'] != '') {
			$sql .= " AND t1.Destination_Ctry in " . $dataPost['country'];
		} else {
		}
		if ($dataPost['region'] != '(undefined)' and $dataPost['region'] != '') {
			$sql .= " AND t1.Region in " . $dataPost['region'];
		} else {
		}
		if ($dataPost['gateway'] != '(undefined)' and $dataPost['gateway'] != '') {
			$sql .= " AND t1.Gateway in " . $dataPost['gateway'];
		} else {
		}
		if ($dataPost['oversla'] != '(undefined)' and $dataPost['oversla'] != '') {
			$sql .= " AND t1.Group_Diff_SLA in " . $dataPost['oversla'];
		} else {
		}
		$sql .= " GROUP BY Reporting_Code_Category,Month ORDER BY Month";
		// print_r($sql);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getDHLSupport($dataPost)
	{
		// print_r($dataPost);

		$sql = 'SELECT Reporting_Code_Description,COUNT(Reporting_Code_Description) AS Qty FROM ' . $this->v_shipment_daily . ' AS t1 WHERE Reporting_Code_Category = "DHL Support"'  ;

		if ($dataPost['month'] != '(undefined)' and $dataPost['month'] != '') {
			$sql .= ' AND t1.Month in ' . $dataPost['month'];
		} else {
			$sql .= " AND Month in (LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 2 MONTH,'%M'),3),LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 1 MONTH,'%M'),3), LEFT(DATE_FORMAT(CURRENT_DATE,'%M'),3))";
		}
		if ($dataPost['week'] != '(undefined)' and $dataPost['week'] != '') {
			$sql .= " AND t1.week in " . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != '(undefined)' and $dataPost['country'] != '') {
			$sql .= " AND t1.Destination_Ctry in " . $dataPost['country'];
		} else {
		}
		if ($dataPost['region'] != '(undefined)' and $dataPost['region'] != '') {
			$sql .= " AND t1.Region in " . $dataPost['region'];
		} else {
		}
		if ($dataPost['gateway'] != '(undefined)' and $dataPost['gateway'] != '') {
			$sql .= " AND t1.Gateway in " . $dataPost['gateway'];
		} else {
		}
		if ($dataPost['oversla'] != '(undefined)' and $dataPost['oversla'] != '') {
			$sql .= " AND t1.Group_Diff_SLA in " . $dataPost['oversla'];
		} else {
		}
		$sql .= " GROUP BY Reporting_Code_Description";
		// print_r($sql);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getCustomerSupport($dataPost)
	{
		// print_r($dataPost);

		$sql = 'SELECT Reporting_Code_Description,COUNT(Reporting_Code_Description) AS Qty FROM ' . $this->v_shipment_daily . ' AS t1 WHERE Reporting_Code_Category = "Customer Support"'  ;

		if ($dataPost['month'] != '(undefined)' and $dataPost['month'] != '') {
			$sql .= ' AND t1.Month in ' . $dataPost['month'];
		} else {
			$sql .= " AND Month in (LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 2 MONTH,'%M'),3),LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 1 MONTH,'%M'),3), LEFT(DATE_FORMAT(CURRENT_DATE,'%M'),3))";
		}
		if ($dataPost['week'] != '(undefined)' and $dataPost['week'] != '') {
			$sql .= " AND t1.week in " . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != '(undefined)' and $dataPost['country'] != '') {
			$sql .= " AND t1.Destination_Ctry in " . $dataPost['country'];
		} else {
		}
		if ($dataPost['region'] != '(undefined)' and $dataPost['region'] != '') {
			$sql .= " AND t1.Region in " . $dataPost['region'];
		} else {
		}
		if ($dataPost['gateway'] != '(undefined)' and $dataPost['gateway'] != '') {
			$sql .= " AND t1.Gateway in " . $dataPost['gateway'];
		} else {
		}
		if ($dataPost['oversla'] != '(undefined)' and $dataPost['oversla'] != '') {
			$sql .= " AND t1.Group_Diff_SLA in " . $dataPost['oversla'];
		} else {
		}
		$sql .= " GROUP BY Reporting_Code_Description";
		// print_r($sql);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getUncontrollable($dataPost)
	{
		// print_r($dataPost);

		$sql = 'SELECT Reporting_Code_Description,COUNT(Reporting_Code_Description) AS Qty FROM ' . $this->v_shipment_daily . ' AS t1 WHERE Reporting_Code_Category = "Uncontrollable"'  ;

		if ($dataPost['month'] != '(undefined)' and $dataPost['month'] != '') {
			$sql .= ' AND t1.Month in ' . $dataPost['month'];
		} else {
			$sql .= " AND Month in (LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 2 MONTH,'%M'),3),LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 1 MONTH,'%M'),3), LEFT(DATE_FORMAT(CURRENT_DATE,'%M'),3))";
		}
		if ($dataPost['week'] != '(undefined)' and $dataPost['week'] != '') {
			$sql .= " AND t1.week in " . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != '(undefined)' and $dataPost['country'] != '') {
			$sql .= " AND t1.Destination_Ctry in " . $dataPost['country'];
		} else {
		}
		if ($dataPost['region'] != '(undefined)' and $dataPost['region'] != '') {
			$sql .= " AND t1.Region in " . $dataPost['region'];
		} else {
		}
		if ($dataPost['gateway'] != '(undefined)' and $dataPost['gateway'] != '') {
			$sql .= " AND t1.Gateway in " . $dataPost['gateway'];
		} else {
		}
		if ($dataPost['oversla'] != '(undefined)' and $dataPost['oversla'] != '') {
			$sql .= " AND t1.Group_Diff_SLA in " . $dataPost['oversla'];
		} else {
		}
		$sql .= " GROUP BY Reporting_Code_Description";
		// print_r($sql);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getWeightbyproduct($dataPost)
	{
		// print_r($dataPost);

		$sql = "SELECT A_Weight,SUM(A_Weight) AS Qty,Month FROM ". $this->v_shipment_daily ." AS t1
				WHERE A_Weight IS NOT null AND A_Weight <> ''";

		if ($dataPost['month'] != '(undefined)' and $dataPost['month'] != '') {
			$sql .= ' AND t1.Month in ' . $dataPost['month'];
		} else {
			$sql .= " AND Month in (LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 2 MONTH,'%M'),3),LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 1 MONTH,'%M'),3), LEFT(DATE_FORMAT(CURRENT_DATE,'%M'),3))";
		}
		if ($dataPost['week'] != '(undefined)' and $dataPost['week'] != '') {
			$sql .= " AND t1.week in " . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != '(undefined)' and $dataPost['country'] != '') {
			$sql .= " AND t1.Destination_Ctry in " . $dataPost['country'];
		} else {
		}
		if ($dataPost['region'] != '(undefined)' and $dataPost['region'] != '') {
			$sql .= " AND t1.Region in " . $dataPost['region'];
		} else {
		}
		if ($dataPost['gateway'] != '(undefined)' and $dataPost['gateway'] != '') {
			$sql .= " AND t1.Gateway in " . $dataPost['gateway'];
		} else {
		}
		if ($dataPost['oversla'] != '(undefined)' and $dataPost['oversla'] != '') {
			$sql .= " AND t1.Group_Diff_SLA in " . $dataPost['oversla'];
		} else {
		}
		$sql .= " GROUP BY A_Weight,Month ORDER BY Month";
		print_r($sql);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getOriginCountry($dataPost)
	{
		// print_r($dataPost);

		$sql = 'SELECT Region,COUNT(Region) AS Qty FROM ' . $this->v_shipment_daily . ' AS t1'  ;

		if ($dataPost['month'] != '(undefined)' and $dataPost['month'] != '') {
			$sql .= ' WHERE t1.Month in ' . $dataPost['month'];
		} else {
			$sql .= " WHERE Month in (LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 2 MONTH,'%M'),3),LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 1 MONTH,'%M'),3), LEFT(DATE_FORMAT(CURRENT_DATE,'%M'),3))";
		}
		if ($dataPost['week'] != '(undefined)' and $dataPost['week'] != '') {
			$sql .= " AND t1.week in " . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != '(undefined)' and $dataPost['country'] != '') {
			$sql .= " AND t1.Destination_Ctry in " . $dataPost['country'];
		} else {
		}
		if ($dataPost['region'] != '(undefined)' and $dataPost['region'] != '') {
			$sql .= " AND t1.Region in " . $dataPost['region'];
		} else {
		}
		if ($dataPost['gateway'] != '(undefined)' and $dataPost['gateway'] != '') {
			$sql .= " AND t1.Gateway in " . $dataPost['gateway'];
		} else {
		}
		if ($dataPost['oversla'] != '(undefined)' and $dataPost['oversla'] != '') {
			$sql .= " AND t1.Group_Diff_SLA in " . $dataPost['oversla'];
		} else {
		}
		$sql .= " GROUP BY Region";
		// print_r($sql);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getDestinationCountry($dataPost)
	{
		// print_r($dataPost);

		$sql = 'SELECT Destination_Ctry,COUNT(Destination_Ctry) AS Qty FROM ' . $this->v_shipment_daily . ' AS t1'  ;

		if ($dataPost['month'] != '(undefined)' and $dataPost['month'] != '') {
			$sql .= ' WHERE t1.Month in ' . $dataPost['month'];
		} else {
			$sql .= " WHERE Month in (LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 2 MONTH,'%M'),3),LEFT(DATE_FORMAT(CURRENT_DATE - INTERVAL 1 MONTH,'%M'),3), LEFT(DATE_FORMAT(CURRENT_DATE,'%M'),3))";
		}
		if ($dataPost['week'] != '(undefined)' and $dataPost['week'] != '') {
			$sql .= " AND t1.week in " . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != '(undefined)' and $dataPost['country'] != '') {
			$sql .= " AND t1.Destination_Ctry in " . $dataPost['country'];
		} else {
		}
		if ($dataPost['region'] != '(undefined)' and $dataPost['region'] != '') {
			$sql .= " AND t1.Region in " . $dataPost['region'];
		} else {
		}
		if ($dataPost['gateway'] != '(undefined)' and $dataPost['gateway'] != '') {
			$sql .= " AND t1.Gateway in " . $dataPost['gateway'];
		} else {
		}
		if ($dataPost['oversla'] != '(undefined)' and $dataPost['oversla'] != '') {
			$sql .= " AND t1.Group_Diff_SLA in " . $dataPost['oversla'];
		} else {
		}
		$sql .= " GROUP BY Destination_Ctry";
		// print_r($sql);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getMonthModelList()
	{
		$sql = "SELECT DISTINCT `Month` FROM `v_shipment_daily` ORDER BY `Month` DESC";

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getWeekModelList($dataPost)
	{
		$sql = 'SELECT DISTINCT t2.week FROM ' . $this->tbl_t_shipment_daily . ' AS t1 left outer join ' . $this->tbl_wk . ' as t2 on t1.Pickup_Date 
				BETWEEN t2.start and t2.end';

		if ($dataPost['month'] != '00') {
			$sql .= ' WHERE DATE_FORMAT(t1.Pickup_Date,"%m") in ' . $dataPost['month'];
		} else {
			$sql .= ' WHERE MONTH(t1.Pickup_Date) BETWEEN MONTH(CURRENT_DATE - INTERVAL 2 MONTH) AND MONTH(CURRENT_DATE)';
		}

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getCountryModelList($dataPost)
	{
		$sql = "SELECT DISTINCT t1.Destination_Ctry FROM " . $this->tbl_t_shipment_daily . " AS t1 left outer join " . $this->tbl_wk . " as t2 on t1.Pickup_Date 
		BETWEEN t2.start and t2.end";

		if ($dataPost['month'] != '00') {
			$sql .= ' WHERE DATE_FORMAT(t1.Pickup_Date,"%m") in ' . $dataPost['month'];
		} else {
			$sql .= ' WHERE MONTH(t1.Pickup_Date) BETWEEN MONTH(CURRENT_DATE - INTERVAL 2 MONTH) AND MONTH(CURRENT_DATE)';
		}
		if ($dataPost['week'] != 'All') {
			$sql .= " AND t2.week in " . $dataPost['week'];
		} else {
		}

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getRegionModelList($dataPost)
	{

		$sql = "SELECT
						DISTINCT case when t1.`Destination_Ctry` in ('AU', 'NZ') THEN t3.lane when t5.lane IS NOT NULL
						and t1.`Destination_Ctry` not in ('AU', 'NZ') THEN t5.lane when t4.lane IS NOT NULL
						and t1.`Destination_Ctry` not in ('AU', 'NZ') THEN t4.lane else 'Other Region' end as Region
						FROM
						t_shipment_daily AS t1
						left outer join wk as t2 on t1.Pickup_Date BETWEEN t2.start
						and t2.end
						LEFT JOIN sla_au_nz as t3 on t1.Destination_City = t3.destination_city
						and t1.`Postal_Code` = t3.destination_postcode
						and t1.`Destination` = t3.destination_svc_area_code
						and t1.`Destination_Ctry` = t3.destination_country_code
						and t1.`Product` = t3.product
						and t1.`Destination_Ctry` in ('AU', 'NZ')
						LEFT JOIN sla_svc_area as t4 on t1.`Destination` = t4.destination_svc_area_code
						and t1.`Destination_Ctry` = t4.destination_country
						and t1.`Destination_Ctry` <> 'AU'
						and t1.`Destination_Ctry` <> 'NZ'
						and t1.`Product` = t4.product_code
						LEFT JOIN sla_zipcode as t5 on t1.`Postal_Code` = t5.zipcode
						and t1.`Destination` = t5.destination_country
						and t1.`Product` = t5.product_code
						and t1.`Destination_Ctry` <> 'AU'
						and t1.`Destination_Ctry` <> 'NZ'";
		if ($dataPost['month'] != '00') {
			$sql .= ' WHERE DATE_FORMAT(t1.Pickup_Date,"%m") in ' . $dataPost['month'];
		} else {
			$sql .= ' WHERE MONTH(t1.Pickup_Date) BETWEEN MONTH(CURRENT_DATE - INTERVAL 2 MONTH) AND MONTH(CURRENT_DATE)';
		}
		if ($dataPost['week'] != 'All') {
			$sql .= " AND t2.week in" . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != 'All') {
			$sql .= " AND t1.Destination_Ctry in " . $dataPost['country'];
		} else {
		}

		// print_r($dataPost);
		// die();
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getGatewayModelList($dataPost)
	{
		$sql = "SELECT
						DISTINCT case when t1.`Destination_Ctry` in ('AU', 'NZ') THEN t3.gateway when t5.gateway IS NOT NULL
						and t1.`Destination_Ctry` not in ('AU', 'NZ') THEN t5.gateway when t4.gateway IS NOT NULL
						and t1.`Destination_Ctry` not in ('AU', 'NZ') THEN t4.gateway else 'Other Gateway' end as Gateway
					FROM
						t_shipment_daily AS t1
						left outer join wk as t2 on t1.Pickup_Date BETWEEN t2.start
						and t2.end
						LEFT JOIN sla_au_nz as t3 on t1.Destination_City = t3.destination_city
						and t1.`Postal_Code` = t3.destination_postcode
						and t1.`Destination` = t3.destination_svc_area_code
						and t1.`Destination_Ctry` = t3.destination_country_code
						and t1.`Product` = t3.product
						and t1.`Destination_Ctry` in ('AU', 'NZ')
						LEFT JOIN sla_svc_area as t4 on t1.`Destination` = t4.destination_svc_area_code
						and t1.`Destination_Ctry` = t4.destination_country
						and t1.`Destination_Ctry` <> 'AU'
						and t1.`Destination_Ctry` <> 'NZ'
						and t1.`Product` = t4.product_code
						LEFT JOIN sla_zipcode as t5 on t1.`Postal_Code` = t5.zipcode
						and t1.`Destination` = t5.destination_country
						and t1.`Product` = t5.product_code
						and t1.`Destination_Ctry` <> 'AU'
						and t1.`Destination_Ctry` <> 'NZ'";
		if ($dataPost['month'] != '00') {
			$sql .= ' WHERE DATE_FORMAT(t1.Pickup_Date,"%m") in ' . $dataPost['month'];
		} else {
			$sql .= ' WHERE MONTH(t1.Pickup_Date) BETWEEN MONTH(CURRENT_DATE - INTERVAL 2 MONTH) AND MONTH(CURRENT_DATE)';
		}
		if ($dataPost['week'] != 'All') {
			$sql .= " AND t2.week in " . $dataPost['week'];
		} else {
		}
		if ($dataPost['country'] != 'All') {
			$sql .= " AND t1.Destination_Ctry in" . $dataPost['country'];
		} else {
		}
		if ($dataPost['region'] != 'All') {
			$sql .= " AND (case when t1.`Destination_Ctry` in ('AU', 'NZ') THEN t3.lane when t5.lane IS NOT NULL
			and t1.`Destination_Ctry` not in ('AU', 'NZ') THEN t5.lane when t4.lane IS NOT NULL
			and t1.`Destination_Ctry` not in ('AU', 'NZ') THEN t4.lane else 'Other Region' end) in" . $dataPost['region'];
		} else {
		}
		// $sql .= " LIMIT $offset, $limit";
		// print_r($sql);
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	// public function getOverSLAModelList()
	// {
	// 	$sql = "SELECT DISTINCT `Group_Diff_SLA` FROM `v_shipment_daily` ORDER BY `Group_Diff_SLA` DESC";

	// 	// $sql .= " LIMIT $offset, $limit";
	// 	// print_r($sql);
	// 	$query = $this->db->query($sql);
	// 	return  $query->result_array();
	// }

	// public function getProductChart($dataPost)
	// {
	// 	// print_r($dataPost);
	// 	// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
	// 	$sql = "SELECT `Product Name`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' GROUP BY `Product Name`, `shipmonth`,`Year`,`Quater`";

	// 	// print_r($sql);
	// 	$query = $this->db->query($sql);
	// 	// print_r($query->result_array());

	// 	return  $query->result_array();
	// }

	public function getProductChart($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		$sql = "SELECT Group_Diff_SLA FROM `v_shipment_daily`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getLaneChart($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		$sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}


	public function getProductDataChart()
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		$sql = "SELECT `Product Name`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `v_sla_grouping` 
		GROUP BY `Product Name`, `shipmonth`,`Year`,`Quater`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getShipmentExcel()
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		$sql = "SELECT * FROM `v_sla_grouping`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getLaneDataChart()
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		$sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `v_sla_grouping` 
		WHERE `lane` != '' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getOntimeData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		$sql = "SELECT COUNT(`On time status`) AS `OnTimeStatus`, `shipmonth`, `Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Exclude weekend )` = '" . $dataPost['ontimestatus'] . "' GROUP BY `shipmonth`, `Quater`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getPreviousQuarterData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		$sql = "SELECT COUNT(`On time status`) AS `OnTimeStatus`, `Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '" . $dataPost['YearPrev'] . "' AND `Quater` = '" . $dataPost['QuaterPrev'] . "' AND `Group Over SLA ( Exclude weekend )` = '" . $dataPost['ontimestatus'] . "' GROUP BY `Quater`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getOrderData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`On time status`) AS `OnTimeStatus`, `shipmonth` FROM `tr_1_shipment_excel` WHERE `factor_incident` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' GROUP BY `shipmonth`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getPreviousOrderData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`On time status`) AS `OnTimeStatus` FROM `tr_1_shipment_excel` WHERE `factor_incident` != '' AND `Year` = '" . $dataPost['YearPrev'] . "' AND `Quater` = '" . $dataPost['QuaterPrev'] . "'";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getDHLsupportData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `factor_incident` FROM `tr_1_shipment_excel` WHERE `On time status` = '" . $dataPost['dhlsupport'] . "' AND `factor_incident` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' GROUP BY `factor_incident`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getCustomersupportData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `factor_incident` FROM `tr_1_shipment_excel` WHERE `On time status` = '" . $dataPost['customersupport'] . "' AND `factor_incident` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' GROUP BY `factor_incident`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getUncontrollableData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `factor_incident` FROM `tr_1_shipment_excel` WHERE `On time status` = '" . $dataPost['uncontrollable'] . "' AND `factor_incident` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' GROUP BY `factor_incident`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getFactorData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `On time status` FROM `tr_1_shipment_excel` WHERE `factor_incident` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' GROUP BY `On time status`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzPerformanceData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `Gateway`, `On time status` FROM `tr_1_shipment_excel` WHERE `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' GROUP BY `Gateway`, `On time status`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzCustomerData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `Gateway`, `On time status` FROM `tr_1_shipment_excel` WHERE `On time status` = '" . $dataPost['customer'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `On time status` = '" . $dataPost['customer'] . "' GROUP BY `Gateway`, `On time status`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzDhlData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `Gateway`, `On time status` FROM `tr_1_shipment_excel` WHERE `On time status` = '" . $dataPost['dhl'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `On time status` = '" . $dataPost['dhl'] . "' GROUP BY `Gateway`, `On time status`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}



	public function getAuNzUncontrollableData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `Gateway`, `On time status` FROM `tr_1_shipment_excel` WHERE `On time status` = '" . $dataPost['uncontroll'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `On time status` = '" . $dataPost['uncontroll'] . "' GROUP BY `Gateway`, `On time status`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzOntimeData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `Gateway`, `Quater` FROM `tr_1_shipment_excel` WHERE `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' GROUP BY `Gateway`, `Quater` ";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzPrevOntimeData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `Gateway`, `Quater` FROM `tr_1_shipment_excel` WHERE `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['YearPrev'] . "' AND `Quater` = '" . $dataPost['QuaterPrev'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Group Over SLA ( Exclude weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['YearPrev'] . "' AND `Quater` = '" . $dataPost['QuaterPrev'] . "' GROUP BY `Gateway`, `Quater` ";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzOrderData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `Gateway` FROM `tr_1_shipment_excel` WHERE `Gateway` != '' AND `factor_incident` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater` = '" . $dataPost['Quater'] . "' GROUP BY `Gateway` ";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzPrevOrderData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `CountWaybillNumber`, `Gateway` FROM `tr_1_shipment_excel` WHERE `Gateway` != '' AND `factor_incident` != '' AND `Year` = '" . $dataPost['YearPrev'] . "' AND `Quater` = '" . $dataPost['QuaterPrev'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `Gateway` != '' AND `factor_incident` != '' AND `Year` = '" . $dataPost['YearPrev'] . "' AND `Quater` = '" . $dataPost['QuaterPrev'] . "' GROUP BY `Gateway` ";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getOverSlaOnetoTwoData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT factor_incident, sum( if( gateway = 'AKL', 1, 0 ) ) AS AKL ,sum( if( gateway = 'BNE', 1, 0 ) ) AS BNE,sum( if( gateway = 'MEL', 1, 0 ) ) AS MEL,sum( if( gateway = 'PER', 1, 0 ) ) AS PER,sum( if( gateway = 'SYD', 1, 0 ) ) AS SYD FROM `tr_1_shipment_excel` WHERE `factor_incident` != '' AND `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Group Over SLA ( Include weekend )` = '" . $dataPost['overonetotwoday'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' OR `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Group Over SLA ( Include weekend )` = '" . $dataPost['overonetotwoday'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `factor_incident` != '' GROUP BY factor_incident";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getOverSlaThreetoFourData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT factor_incident, sum( if( gateway = 'AKL', 1, 0 ) ) AS AKL ,sum( if( gateway = 'BNE', 1, 0 ) ) AS BNE,sum( if( gateway = 'MEL', 1, 0 ) ) AS MEL,sum( if( gateway = 'PER', 1, 0 ) ) AS PER,sum( if( gateway = 'SYD', 1, 0 ) ) AS SYD FROM `tr_1_shipment_excel` WHERE `factor_incident` != '' AND `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Group Over SLA ( Include weekend )` = '" . $dataPost['overthreetofourday'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' OR `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Group Over SLA ( Include weekend )` = '" . $dataPost['overthreetofourday'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `factor_incident` != '' GROUP BY factor_incident";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getOverSlaFivetoSixData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT factor_incident, sum( if( gateway = 'AKL', 1, 0 ) ) AS AKL ,sum( if( gateway = 'BNE', 1, 0 ) ) AS BNE,sum( if( gateway = 'MEL', 1, 0 ) ) AS MEL,sum( if( gateway = 'PER', 1, 0 ) ) AS PER,sum( if( gateway = 'SYD', 1, 0 ) ) AS SYD FROM `tr_1_shipment_excel` WHERE `factor_incident` != '' AND `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Group Over SLA ( Include weekend )` = '" . $dataPost['overfivetosixday'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' OR `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Group Over SLA ( Include weekend )` = '" . $dataPost['overfivetosixday'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `factor_incident` != '' GROUP BY factor_incident";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getOverSlaSevenData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT factor_incident, sum( if( gateway = 'AKL', 1, 0 ) ) AS AKL ,sum( if( gateway = 'BNE', 1, 0 ) ) AS BNE,sum( if( gateway = 'MEL', 1, 0 ) ) AS MEL,sum( if( gateway = 'PER', 1, 0 ) ) AS PER,sum( if( gateway = 'SYD', 1, 0 ) ) AS SYD FROM `tr_1_shipment_excel` WHERE `factor_incident` != '' AND `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Group Over SLA ( Include weekend )` = '" . $dataPost['oversevenday'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' OR `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Group Over SLA ( Include weekend )` = '" . $dataPost['oversevenday'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `factor_incident` != '' GROUP BY factor_incident";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getOverSlaTotalData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `Countwaybill`, `gateway` FROM `tr_1_shipment_excel` WHERE `factor_incident` != '' AND `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Group Over SLA ( Include weekend )` != '" . $dataPost['ontimestatus'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `factor_incident` != '' GROUP BY `gateway`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAllFactorData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		$sql = "SELECT COUNT(`Waybill Number`) AS `Countwaybill`, `gateway` FROM `tr_1_shipment_excel` WHERE `factor_incident` != '' AND `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' OR `gateway` != '' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `factor_incident` != '' GROUP BY `gateway`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzDayData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		// $sql = "SELECT COUNT(`Waybill Number`) AS `Countwaybill`, `gateway`, `Startclock Day of Week` FROM `tr_1_shipment_excel` WHERE `Group Over SLA ( Include weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '".$dataPost['Year']."' AND `Quater`= '".$dataPost['Quater']."' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `gateway` != '' OR `Group Over SLA ( Include weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '".$dataPost['Year']."' AND `Quater`= '".$dataPost['Quater']."' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `gateway` != '' GROUP BY `gateway`, `Startclock Day of Week`";
		$sql = "SELECT `Pickup day`, sum( if( gateway = 'AKL', 1, 0 ) ) AS AKL ,sum( if( gateway = 'BNE', 1, 0 ) ) AS BNE,sum( if( gateway = 'MEL', 1, 0 ) ) AS MEL,sum( if( gateway = 'PER', 1, 0 ) ) AS PER,sum( if( gateway = 'SYD', 1, 0 ) ) AS SYD FROM `tr_1_shipment_excel` WHERE `Group Over SLA ( Include weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `gateway` != '' OR `Group Over SLA ( Include weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `gateway` != '' GROUP BY `Pickup day`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzDayFlightDelayData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		// $sql = "SELECT COUNT(`Waybill Number`) AS `Countwaybill`, `gateway`, `Startclock Day of Week` FROM `tr_1_shipment_excel` WHERE `factor_incident` = '" . $dataPost['flightdelay'] . "' AND `Year` = '".$dataPost['Year']."' AND `Quater`= '".$dataPost['Quater']."' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `gateway` != '' OR `factor_incident` = '" . $dataPost['flightdelay'] . "' AND `Year` = '".$dataPost['Year']."' AND `Quater`= '".$dataPost['Quater']."' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `gateway` != '' GROUP BY `gateway`, `Startclock Day of Week`";
		$sql = "SELECT `Pickup day`, sum( if( gateway = 'AKL', 1, 0 ) ) AS AKL ,sum( if( gateway = 'BNE', 1, 0 ) ) AS BNE,sum( if( gateway = 'MEL', 1, 0 ) ) AS MEL,sum( if( gateway = 'PER', 1, 0 ) ) AS PER,sum( if( gateway = 'SYD', 1, 0 ) ) AS SYD FROM `tr_1_shipment_excel` WHERE `factor_incident` = '" . $dataPost['flightdelay'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `gateway` != '' OR `factor_incident` = '" . $dataPost['flightdelay'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `gateway` != '' GROUP BY `Pickup day`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function getAuNzDayTotalData($dataPost)
	{
		// print_r($dataPost);
		// $sql = "SELECT COUNT(`Waybill Number`) AS CountWaybill,`Product Name`,COUNT(`Product Code`) AS CountProduct FROM `t_shipment` GROUP BY `Product Name`";
		// $sql = "SELECT `lane`, COUNT(`Waybill Number`) AS CountWaybill, `shipmonth`,`Year`,`Quater` FROM `tr_1_shipment_excel` WHERE `Year` = '".$dataPost['Year']."' AND `Quater` = '".$dataPost['Quater']."' AND `lane` != '0' GROUP BY `lane`, `shipmonth`,`Year`,`Quater`";
		// $sql = "SELECT COUNT(`Waybill Number`) AS `Countwaybill`, `gateway`, `Startclock Day of Week` FROM `tr_1_shipment_excel` WHERE `factor_incident` = '" . $dataPost['flightdelay'] . "' AND `Year` = '".$dataPost['Year']."' AND `Quater`= '".$dataPost['Quater']."' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `gateway` != '' OR `factor_incident` = '" . $dataPost['flightdelay'] . "' AND `Year` = '".$dataPost['Year']."' AND `Quater`= '".$dataPost['Quater']."' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `gateway` != '' GROUP BY `gateway`, `Startclock Day of Week`";
		$sql = "SELECT `Pickup day`, sum( if( gateway = 'AKL', 1, 0 ) ) AS AKL ,sum( if( gateway = 'BNE', 1, 0 ) ) AS BNE,sum( if( gateway = 'MEL', 1, 0 ) ) AS MEL,sum( if( gateway = 'PER', 1, 0 ) ) AS PER,sum( if( gateway = 'SYD', 1, 0 ) ) AS SYD FROM `tr_1_shipment_excel` WHERE `factor_incident` = '" . $dataPost['flightdelay'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `gateway` != '' OR `factor_incident` = '" . $dataPost['flightdelay'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `gateway` != '' OR `Group Over SLA ( Include weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['au'] . "' AND `gateway` != '' OR `Group Over SLA ( Include weekend )` = '" . $dataPost['ontimestatus'] . "' AND `Year` = '" . $dataPost['Year'] . "' AND `Quater`= '" . $dataPost['Quater'] . "' AND `Destination Country/Territory Area Code` = '" . $dataPost['nz'] . "' AND `gateway` != '' GROUP BY `Pickup day`";

		// print_r($sql);
		$query = $this->db->query($sql);
		// print_r($query->result_array());

		return  $query->result_array();
	}

	public function Exportreport($dataModel)
	{
		// print_r($dataModel);

		$sql = "SELECT * FROM `tr_1_shipment_excel` WHERE `Year` = '" . $dataModel['Year'] . "' AND `Quater` = '" . $dataModel['Quater'] . "'";
		// $sql = "SELECT * FROM v_sla_grouping WHERE week_number = '".$dataModel['week'];

		// $sql =  $this->GetSearchQuery($sql, $dataModel);	
		// print_r($sql);


		$query = $this->db->query($sql);
		// print_r($sql);
		return $query->result_array();
	}

	public function delete_tr_1_1_shipement_by_prouduct()
	{
		$sql = "Delete FROM tr_1_1_shipement_by_prouduct";
		$query = $this->db->query($sql);
	}

	public function delete_tr_1_shipment_excel()
	{
		$sql = "Delete FROM tr_1_shipment_excel";
		$query = $this->db->query($sql);
	}

	public function delete_tr_1_2_shipement_by_lane()
	{
		$sql = "Delete FROM tr_1_2_shipement_by_lane";
		$query = $this->db->query($sql);
	}

	public function insert($modelData)
	{
		$this->db->insert($this->tbl_name, $modelData);
		return $this->db->insert_id();
	}

	public function insertProductChart($modelData)
	{

		$this->db->insert('tr_1_1_shipement_by_prouduct', $modelData);
		return $this->db->insert_id();
	}

	public function insertShipmentExcel($modelData)
	{
		foreach ($modelData as $k => $v) {
			if (strstr($k, " ")) {
				$this->db->set('`' . addslashes($k) . '`', '"' . $v . '"', false);
				unset($modelData[$k]);
			}
		}

		$returnDB = $this->db->insert('tr_1_shipment_excel', $modelData);
		return $this->db->insert_id();
	}

	public function insertLaneChart($modelData)
	{

		$this->db->insert('tr_1_2_shipement_by_lane', $modelData);
		return $this->db->insert_id();
	}

	public function update($id, $modelData)
	{
		$this->db->where($this->id, $id);
		return $this->db->update($this->tbl_name, $modelData);
	}

	public function getcodeappleanddhlNameAllList()
	{
		//return $this->db->count_all($this->tbl_name);

		$this->db->select('id', 'name', 'contact', 'address1', 'address2', 'address3', 'tel', 'email', 'taxid', 'website');
		//$this->db->where('Admin_delete_flag', 0);
		$query =  $this->db->get($this->tbl_name);

		return $query->result_array();
	}

	public function getcodeappleanddhlModel($id)
	{
		//return $this->db->count_all($this->tbl_name);

		//$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('delete_flag', 1);
		$this->db->where($this->id, $id);
		$query =  $this->db->get($this->tbl_name);

		return $query->result_array();
	}
	// public function getLastDate()
	// {
	// 	$sql = "SELECT max(date(create_date)) last_date FROM t_open_pod WHERE delete_flag = 0 ";

	// 	$query = $this->db->query($sql);

	// 	return $query->result_array();
	// }

	public function getSearchQuery($sql, $dataModel)
	{

		//print_r($dataModel);

		if (isset($dataModel['Quater']) && $dataModel['Quater'] != "") {
			$sql .= " WHERE `Quater` like '%" . $dataModel['Quater'] . "%' ";
		}

		if (isset($dataModel['Year']) && $dataModel['Year'] != "") {
			$sql .= " AND `Year` like '%" . $dataModel['Year'] . "%' ";
		}


		return $sql;
	}

	public function getTotal($dataModel)
	{

		$sql = "SELECT * FROM " . $this->tbl_name  . " WHERE delete_flag = 0 ";

		$sql =  $this->getSearchQuery($sql, $dataModel);

		$query = $this->db->query($sql);

		return  $query->num_rows();
	}

	public function getCodeAppleAndDhlNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc')
	{

		$sql = "SELECT * FROM " . $this->tbl_name . " WHERE delete_flag = 0 ";

		$sql =  $this->getSearchQuery($sql, $dataModel);


		// if($order != ""){
		// $this->db->order_by($order, $direction);
		// }else{
		// $this->db->order_by($this->id ,$direction); 
		// }

		if ($order != "") {
			$sql .= " ORDER BY " . $order . " " . $direction;
		} else {
			$sql .= " ORDER BY " . $this->id . " " . $direction;
		}

		$sql .= " LIMIT $offset, $limit";

		//print($sql );

		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	// public function deleteCodeAppleAndDhlBy($id)
	// {
	// 	$result = false;
	// 	try {
	// 		$query = $this->getCodeAppleAndDhlNameById($id);
	// 		$modelData;
	// 		foreach ($query->result() as $row) {

	// 			$modelData = array(
	// 				//'update_date' => date("Y-m-d H:i:s"),
	// 				//'update_user' => $this->session->userdata('user_name'),
	// 				'delete_flag' => 1 //$row->Admin_delete_flag 
	// 			);
	// 		}

	// 		$this->db->where($this->id, $id);
	// 		return $this->db->update($this->tbl_name, $modelData);
	// 		//return $this->update($id, $modelData);
	// 		//$sql = "Delete FROM ". $this->tbl_name; 
	// 		//return  $this->db->query($sql);

	// 	} catch (Exception $ex) {
	// 		return $result;
	// 	}
	// }




	public function ExportExcelToSddBytoDay($data, $date, $SddStatus, $flagallshipment = false)
	{
		// print_r($SddStatus);die();t1.ssd_status = '".$SddStatus."'
		$sql = "SELECT * ,DATE_FORMAT(event_date, '%Y%m%d') as new_event_date,
		DATE_FORMAT(em_scheduled_delivery_date, '%d/%m/%Y') as new_em_scheduled_delivery_date,
		DATE_FORMAT(em_current_event_datetime, '%m/%d/%Y %r') as new_em_current_event_datetime,
		DATE_FORMAT(em_ship_confirmation_last_date, '%m/%d/%Y') as new_em_ship_confirmation_last_date,
		DATE_FORMAT(em_ship_confirmation_last_time, '%r') as new_em_ship_confirmation_last_time,
		DATE_FORMAT(em_shipment_picked_up_last_datetime, '%m/%d/%Y %r') as new_em_shipment_picked_up_last_datetime,
		DATE_FORMAT(em_uplift_last_datetime, '%m/%d/%Y %r') as new_em_uplift_last_datetime,
		DATE_FORMAT(em_first_attempt_date, '%d/%m/%Y') as new_em_first_attempt_date,
		DATE_FORMAT(em_delivery_not_completed_last_date, '%m/%d/%Y') as new_em_delivery_not_completed_last_date,
		DATE_FORMAT(em_shipment_delay_last_date, '%m/%d/%Y') as new_em_shipment_delay_last_date 
		FROM t_open_pod as t1 WHERE t1.ssd_status in($SddStatus)";
		if ($flagallshipment == true) {
			$sql .= " and date(create_date) > DATE_SUB('" . $date . "', INTERVAL 7 DAY) and date(create_date) <= '" . $date . "'";
		} else {
			$sql .= " and date(create_date) = '" . $date . "'";
		}
		if ($data['type'] == 'group') {
			if ($data['data'] == 'Completion') {
				$sql .= " and t1.latest_checkpiont in('OK','RT')   and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'";
			}

			if ($data['data'] == 'On progress') {
				$sql .= " and t1.latest_checkpiont in('WC','FD')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'";
			}

			if ($data['data'] == 'Exception') {
				$sql .= " and t1.latest_checkpiont in('BA','NH','AD','RD') and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'";
			}


			if ($data['data'] == 'Other') {
				$sql .= " and t1.latest_checkpiont not in('OK','RT','WC','FD','BA','NH','AD','RD')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'";
			}
		}
		if ($data['type'] == 'status') {

			$sql .= " and t1.latest_checkpiont in('" . str_replace(",", "','", $data['data']) . "')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'";
		}

		if ($data['type'] == 'servicecenter') {
			if ($data['data'] == 'Other') {
				$sql .= " and t1.service_center not in('APD','GDR','RMT','TZB','ZVB','CXM','NBK','NKR', 'PCB', 'PGT', 'EGW', 'LZB', 'UPC')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')";
			} else {
				$sql .= " and t1.service_center in('" . str_replace(",", "','", $data['data']) . "')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')";
			}
		}
		// $sql .= " Limit 1420";
		// print_r($sql);die();
		$query = $this->db->query($sql);
		// print_r($query->result_array());die();
		return  $query->result_array();
	}

	public function GetlastUpdateTableLogUpdate($system)
	{

		$sql = "SELECT MAX(time_stamp) last_update FROM t_log_update  WHERE customer = '" . $system . "'";

		$query = $this->db->query($sql);
		return  $query->result_array();
	}


	public function UpdateDataMappingToDB()
	{
		$sql = "UPDATE t_open_pod
		INNER JOIN t_code_apple_and_dhl ON t_open_pod.latest_checkpiont = t_code_apple_and_dhl.check_point AND t_code_apple_and_dhl.delete_flag = 0
		SET t_open_pod.comments = t_code_apple_and_dhl.reason_code,
		t_open_pod.remark = t_code_apple_and_dhl.remark";

		return  $this->db->query($sql);
	}

	public function InsertTimeStamp()
	{
		$sql = "INSERT INTO t_log_update(customer) VALUES ('Apple')";

		return $this->db->query($sql);
	}

	// public function ExportExcelToSddBytoDay()
	// {
	// 	$sql = "SELECT * FROM " . $this->tbl_name . " WHERE date(create_date) = '" . $date . "'";

	// 	$query = $this->db->query($sql);
	// 	return  $query->result_array();
	// }


	public function GetDataGraphPieBytoDay($date, $SddStatus)
	{
		$sql = "SELECT v1.*,ROUND(v1.count_total*100/v2.count_total,2) percent from (
			SELECT  'Completion' latest_checkpiont,count(t1.id) AS count_total
			FROM t_open_pod t1
			where t1.latest_checkpiont in('OK','RT')   and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'
	
			UNION ALL
			SELECT  'On progress' latest_checkpiont,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.latest_checkpiont in('WC','FD')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'
			
			UNION ALL
			SELECT 'Exception' latest_checkpiont,count(t1.id) AS count_total
			FROM t_open_pod t1
			where t1.latest_checkpiont in('BA','NH','AD','RD') and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'
			
			UNION ALL
			SELECT 'Other' latest_checkpiont,count(t1.id) AS count_total
			FROM t_open_pod t1
			where t1.latest_checkpiont not in('OK','RT','WC','FD','BA','NH','AD','RD')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'
			) v1
			join 	(SELECT count(id) count_total from t_open_pod WHERE ssd_status in($SddStatus)  and date(create_date) = '" . $date . "') v2
	";

		// $query = $this->db->query($sql);
		// return  $query->result_array();
		// $sql = "SELECT 'OK'latest_checkpiont,950 AS count_total,'63' percent


		// UNION ALL
		// SELECT'RT'latest_checkpiont, 10 AS count_total,'1' percent


		// UNION ALL
		// SELECT 'WC'latest_checkpiont,170 AS count_total,'11' percent


		// UNION ALL
		// SELECT  'FD'latest_checkpiont,310 AS count_total,'21' percent


		// UNION ALL
		// SELECT 'Exception' latest_checkpiont,30 AS count_total,'2' percent


		// UNION ALL
		// SELECT 'OTHER' latest_checkpiont,30 AS count_total,'2' percent
		// ";

		$query = $this->db->query($sql);
		return  $query->result_array();
	}


	public function GetDataGraphBarBytoDay($date, $SddStatus)
	{
		$sql = "SELECT v1.*,v1.count_total*100/v2.count_total percent from (
			SELECT 'Completion' checkpoint_type,'OK' latest_checkpiont ,count(t1.id) AS count_total
			FROM t_open_pod t1
			where t1.latest_checkpiont in('OK')   and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'

			UNION ALL
			SELECT  'Completion' checkpoint_type,'RT' latest_checkpiont ,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.latest_checkpiont in('RT')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'

			UNION ALL
			SELECT  'On progress'checkpoint_type,'WC' latest_checkpiont ,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.latest_checkpiont in('WC')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'
	
			UNION ALL
			SELECT  'On progress'checkpoint_type,'FD' latest_checkpiont ,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.latest_checkpiont in('FD')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'
			
			UNION ALL
			SELECT 'Exception'checkpoint_type,'Exception' latest_checkpiont ,count(t1.id) AS count_total
			FROM t_open_pod t1
			where t1.latest_checkpiont in('BA','NH','AD','RD') and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'
			
			UNION ALL
			SELECT 'Other' checkpoint_type,'Other' latest_checkpiont,count(t1.id) AS count_total
			FROM t_open_pod t1
			where t1.latest_checkpiont not in('OK','RT','WC','FD','BA','NH','AD','RD')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "'
			) v1
			join 	(SELECT count(id) count_total from t_open_pod WHERE ssd_status in($SddStatus)  and date(create_date) = '" . $date . "') v2
	";

		// echo $sql;

		// $query = $this->db->query($sql);
		// return  $query->result_array();
		// $sql = "SELECT 'OK'latest_checkpiont,950 AS count_total,'63' percent


		// UNION ALL
		// SELECT'RT'latest_checkpiont, 10 AS count_total,'1' percent


		// UNION ALL
		// SELECT 'WC'latest_checkpiont,170 AS count_total,'11' percent


		// UNION ALL
		// SELECT  'FD'latest_checkpiont,310 AS count_total,'21' percent


		// UNION ALL
		// SELECT 'Exception' latest_checkpiont,30 AS count_total,'2' percent


		// UNION ALL
		// SELECT 'OTHER' latest_checkpiont,30 AS count_total,'2' percent
		// ";

		$query = $this->db->query($sql);
		return  $query->result_array();
	}


	public function GetDataGraphPieByServiceCenter($date, $SddStatus)
	{
		$sql = "SELECT v1.*,ROUND(v1.count_total*100/v2.count_total,2) percent from (
			SELECT  'APD' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where t1.service_center in('APD')   and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
	
			UNION ALL
			SELECT  'GDR' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('GDR')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')

			UNION ALL
			SELECT  'RMT' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('RMT')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
			
			UNION ALL
			SELECT  'TZB' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('TZB')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
			
			UNION ALL
			SELECT  'ZVB' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('ZVB')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
			
			UNION ALL
			SELECT  'CXM' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('CXM')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
			
			UNION ALL
			SELECT  'NBK' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('NBK')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
			
			UNION ALL
			SELECT  'NKR' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('NKR')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
			
			UNION ALL
			SELECT  'PCB' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('PCB')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
			
			UNION ALL
			SELECT  'PGT' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('PGT')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')

			UNION ALL
			SELECT  'EGW' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('EGW')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')

			UNION ALL
			SELECT  'LZB' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('LZB')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')

			UNION ALL
			SELECT  'UPC' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where  t1.service_center in('UPC')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
			
			UNION ALL
			SELECT 'Other' service_center,count(t1.id) AS count_total
			FROM t_open_pod t1
			where t1.service_center not in('APD','GDR','RMT','TZB','ZVB','CXM','NBK','NKR', 'PCB', 'PGT', 'EGW', 'LZB', 'UPC')  and t1.ssd_status in($SddStatus) and date(create_date) = '" . $date . "' and t1.latest_checkpiont in('WC','FD')
			) v1
			join (SELECT count(id) count_total from t_open_pod WHERE ssd_status in($SddStatus)  and date(create_date) = '" . $date . "' and latest_checkpiont in('WC','FD')) v2
	";
		// echo $sql;die();
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getPercentStatus($date, $SddStatus)
	{
		$sql = "SELECT '" . $date . "' data_date,
			count(t1.id) total ,t2.num_of_ok,t2.num_of_ok*100/count(t1.id) percent_of_ok ,t3.num_of_rt,t3.num_of_rt*100/count(t1.id) percent_of_rt ,
			t4.num_of_wc,t4.num_of_wc*100/count(t1.id) percent_of_wc ,t5.num_of_fd,t5.num_of_fd*100/count(t1.id) percent_of_fd ,t6.num_of_exception,t6.num_of_exception*100/count(t1.id) percent_of_exception ,
			t7.num_of_other,t7.num_of_other*100/count(t1.id) percent_of_other
		FROM
			t_open_pod t1
			CROSS JOIN (SELECT count(id) AS num_of_ok FROM t_open_pod where latest_checkpiont='OK' and date(create_date ) = '" . $date . "' and ssd_status in($SddStatus)) t2
			CROSS JOIN (SELECT count(id) AS num_of_rt FROM t_open_pod where latest_checkpiont='RT' and date(create_date ) = '" . $date . "' and ssd_status in($SddStatus)) t3
			CROSS JOIN (SELECT count(id) AS num_of_wc FROM t_open_pod where latest_checkpiont='WC' and date(create_date ) = '" . $date . "' and ssd_status in($SddStatus)) t4
			CROSS JOIN (SELECT count(id) AS num_of_fd FROM t_open_pod where latest_checkpiont='FD' and date(create_date ) = '" . $date . "' and ssd_status in($SddStatus)) t5
			CROSS JOIN (SELECT count(id) AS num_of_exception FROM t_open_pod where latest_checkpiont IN('BA','NH','AD','RD') and date(create_date ) = '" . $date . "' and ssd_status in($SddStatus)) t6
			CROSS JOIN (SELECT count(id) AS num_of_other FROM t_open_pod where latest_checkpiont NOT IN('OK','RT','WC','FD','BA','NH','AD','RD') and date(create_date ) = '" . $date . "' and ssd_status in($SddStatus)) t7
			WHERE t1.ssd_status in($SddStatus)  and date(t1.create_date) = '" . $date . "'";

		// echo $sql;die();
		// $sql = "SELECT '" . $date . "' data_date,
		// 	1500 total ,950 num_of_ok,950*100/1500 percent_of_ok ,10 num_of_rt,10*100/1500  percent_of_rt ,
		// 	170 num_of_wc,170*100/1500  percent_of_wc , 310 num_of_fd,310*100/1500  percent_of_fd ,30 num_of_exception,30*100/1500  percent_of_exception ,
		// 	30 num_of_other,30*100/1500 percent_of_other
		// ";
		// echo $sql;die();
		// $sql =  $this->GetSearchQueryPercentStatus($sql, $dataModel);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');
ini_set('max_execution_time', '-1');
class ApiAppleshipment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
    }

    public function index()
    {
        // $this->load->view('share/head');
        // $this->load->view('share/sidebar');
        // $this->load->view('ApiAppleshipment/ApiAppleshipment_view');
        // $this->load->view('share/footer');
    }
    public function test()
    {
        $data = array(
            '4277262113'

            // 'JJD014600005944290335',
            // 'JJD014600005944290334',
            // 'JJD014600005944290333',
            // 'JJD014600005944290332'
        );
        $result = $this->getDataFromAWBApi($data);
        // print_r($result);die();
        // print_r($result['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem']);
        foreach ($result as $AWBInfoItem) {
            if ($AWBInfoItem['ShipmentInfo']['Pieces'] == 1) {
                $ArrayOfPieceInfoItem = $AWBInfoItem['Pieces']['PieceInfo'];
            } else {
                $ArrayOfPieceInfoItem = $AWBInfoItem['Pieces']['PieceInfo']['ArrayOfPieceInfoItem'];
            }

            // echo count($ArrayOfPieceInfoItem);
            // print_r($ArrayOfPieceInfoItem);

            foreach ($ArrayOfPieceInfoItem as $PieceInfoItem) {
                $item['id'] = 0;
                $item['AWB'] = $PieceInfoItem['PieceDetails']['AWBNumber'];
                $item['PID'] = $PieceInfoItem['PieceDetails']['LicensePlate'];
                $item['weight'] = isset($PieceInfoItem['PieceDetails']['Weight']) ? $PieceInfoItem['PieceDetails']['Weight'] : null;
                $item['actual_weight'] = isset($PieceInfoItem['PieceDetails']['ActualWeight']) ? $PieceInfoItem['PieceDetails']['ActualWeight'] : null;

                $item['QTY'] = $AWBInfoItem['ShipmentInfo']['Pieces'];
                print_r($PieceInfoItem);
                // print_r($result['list_insert']);
            }
            //    print_r($ArrayOfAWBInfoItem['AWBNumber']);
        }
    }

    public function getDataByApiSpeedD()
    {

        $this->load->model('ImportModel', '', true);

        $this->load->model('DashboardModel', '', true);

        try {

        $dataPost = json_decode($this->input->raw_input_stream, true);
        $lastdate =  $this->ImportModel->getLastDate()[0]['last_date'];
        $listAllAwbNotInPid = $this->ImportModel->getDataCarrierTrackingNo($lastdate);
        $date = date('Y-m-d-H-i-s');
        // print_r($listAllAwbNotInPid);
        // die();
        $list_insert = [];

        if (count($listAllAwbNotInPid) > 0) {
            foreach ($listAllAwbNotInPid as $Awb) {
                // print_r($Awb['carrier_tracking_no']);
                // die();
                $ch = curl_init();
                $username = "dhlTH";
                $password = "T^9uJ@4aX#2q";
                curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
                curl_setopt($ch, CURLOPT_URL, "https://express.api.dhl.com/mydhlapi/tracking?shipmentTrackingNumber=" . $Awb['carrier_tracking_no'] . "&trackingView=last-checkpoint&levelOfDetail=all");
                // Data ที่จะส่ง 
                // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($destination, JSON_UNESCAPED_UNICODE));
                // curl_setopt($ch, CURLOPT_POST, true);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $content = curl_exec($ch);
                $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                $error = curl_error($ch);

                $resTime = date('Y-m-d H:i:s');

                curl_close($ch);
                $content = json_decode($content, true);
                // print_r($content);
                // die();
                // print_r($content['shipments'][0]['status']);
                // die();
                if ($content['shipments'][0]['status'] == "Success") {

                    $last_check_point = $content['shipments'][0]['events'][0]['typeCode'];
                    $date_stamp =  $content['shipments'][0]['events'][0]['date'];
                    $time_stamp = $content['shipments'][0]['events'][0]['time'];
                    // print_r($date_stamp);
                    // die();
                    
                    $item['carrier_tracking_no'] = $content['shipments'][0]['shipmentTrackingNumber'];
                    $item['latest_checkpiont'] = $last_check_point;
                    $item['service_center'] = $content['shipments'][0]['receiverDetails']['serviceArea'][0]['facilityCode'];
                    $item['latest_checkpiont_update'] = $date;
                    $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;

                    // print_r($item);die();
                    if ($item['latest_checkpiont'] == 'FD') {
                        $item['service_center'] = 'UPC';
                    }

                    $nResult = $this->ImportModel->update_carrier_tracking_no($item['carrier_tracking_no'], $item);
                    array_push($list_insert, $item);

                } else {

                }

            }
        }

        //     foreach ($data as $rowdata) {
        //         // print_r($rowdata);die();

        //         $data['awb'] = $addInlist['awb'];
        //         // print_r($data['awb']);die;
        //         $ch = curl_init();

        //         $username = "dhlTH";
        //         $password = "T^9uJ@4aX#2q";
        //         curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        //         curl_setopt($ch, CURLOPT_URL, "https://express.api.dhl.com/mydhlapi/tracking?shipmentTrackingNumber=" . $data['awb'] . "&trackingView=last-checkpoint&levelOfDetail=all");
        //         // Data ที่จะส่ง 
        //         // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($destination, JSON_UNESCAPED_UNICODE));
        //         // curl_setopt($ch, CURLOPT_POST, true);

        //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //         $content = curl_exec($ch);
        //         $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //         $error = curl_error($ch);

        //         $resTime = date('Y-m-d H:i:s');

        //         // $modelData = [
        //         //     'request_url' => "https://demo.nimexpress.com/nss/AddBillDHLEX.htm", 'request_time' => $reqTime, 'response_time' => $resTime, 'status_code' => $statusCode, 'error_message' => $error, 'response_message' => $content, 'post_data' => json_encode($destination, JSON_UNESCAPED_UNICODE)
        //         // ];
        //         // $this->db->insert('t_request_log', $modelData);

        //         curl_close($ch);


        //         $content = json_decode($content, true);
        //         $dataRow = $content['data'];
        //         // $dataRow = $test[0]['data'];
        //         // print_r();die;
        //         // print_r($dataRow);die;

        //         // $dataBooking = $content;
        //         $addtracking['t_tracking'] = $dataRow['bookingNo'];
        //         $addtracking['consignment'] =  $dataRow['orders'][0]['consignment'];
        //         $addtracking['pid'] =  $dataRow['orders'][0]['references']['code'][0];
        //         // print_r($statusCode);die();
        //         // print_r($addtracking);die;
        //         $error = $content['message'][0];
        //         // print_r($error);die;
        //         $pidError = $rowdata['orders'][0]['references']['code'][0];
        //         $textError['message'] = $error;
        //         $textError['pid'] = $pidError;
        //         if ($statusCode == "200") {

        //             $result['status_code'] = "200";
        //             $this->TransectionModel->updateTracking($addtracking);
        //         } else {
        //             // print_r("is_error");die();
        //             $result['status_code'] = "200";
        //             $result['is_error'][] = $textError;
        //             // print_r($error);die();
        //             // $this->TransectionModel->updateTracking($addtracking);
        //         }
        //     }
        //     // $result['message'] = "";
        $DataNewStatusOh = $this->ImportModel->UpdateOhToNewStatus();
        $DataMapping = $this->DashboardModel->UpdateDataMappingToDB();
        $DataTimstamp = $this->DashboardModel->InsertTimeStamp();
        $result['status'] = true;
        $result['message'] = $this->lang->line('savesuccess');
        $result['list_insert'] = $list_insert;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        // echo json_encode($content, JSON_UNESCAPED_UNICODE);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }


    public function getDataFromAWBApi($AWB = null, $CheckPointType = 'LAST_CHECKPOINT_ONLY')
    {
        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 0);

        try {
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $dawaybill = isset($dataPost['waybill']) ? $dataPost['waybill'] : 0;
            $wsse_header = new WsseAuthHeader('dhlTH', 'T^9uJ@4aX#2q'); //, "http://www.dhl.com" );
            //"https://wsbexpress.dhl.com/gbl/glDHLExpressTrack?WSDL"
            //"https://wsbexpress.dhl.com/sndpt/glDHLExpressTrack?WSDL"
            $client2 = new SoapClient('https://wsbexpress.dhl.com/gbl/glDHLExpressTrack?WSDL', array('trace' => 1, 'exception' => 1, 'dhl' => 'http://www.dhl.com'));
            $header = array($wsse_header);
            $client2->__setSoapHeaders($header);
            $ServiceHeader = array(
                'MessageTime' => '2014-07-01T13:39:57.938Z',
                'MessageReference' => '123456789123456789123456789123',
                'WebstorePlatform' => '',
                'WebstorePlatformVersion' => '',
                'ShippingSystemPlatform' => '',
                'ShippingSystemPlatformVersion' => '',
                'PlugIn' => '',
                'PlugInVersion' => '',
            );
            // $AWBvalue = $dawaybill;
            if ($AWB == null) {
                $AWBvalue = $dawaybill;
            } else {
                $AWBvalue = $AWB;
            }
            // "2147483647";//intval ("9951579795");//"4246866551"; settype($AWBvalue, "float");// intval ("4246866551");//9951579795;//  intval ("9951579795");
            // $szData = gettype($AWBvalue); echo "Type " . $szData;
            // echo "<p><h3>send</h3>" .$AWBvalue ."</p>";

            $AWBNumber = array(
                'ArrayOfAWBNumberItem' => $AWBvalue,
            ); //เพิ่มจำนวนตัวที่ตรงนี้

            $ClientDetail = array(
                'sso' => '?',
                'plant' => '?',
            );

            $params = array(
                // 'ClientDetail' => $ClientDetail,
                'Request' => array('ServiceHeader' => $ServiceHeader),
                //'LanguageCode' => "?",
                //'LanguageScriptCode' => "?",
                //'LanguageCountryCode' => "?",
                'AWBNumber' => $AWBNumber,
                //'LPNumber' => array('ArrayOfTrackingPieceIDItem' => ""),
                //'ReferenceQuery' => array(
                //'ShipperAccountNumber' => "",
                //'ShipmentReferences' =>  array (
                //		'ShipmentReference' => "",
                //		'ShipmentReferenceType' => ""),
                //'ShipmentDateRange' =>  array (
                //		'From' => "",
                //		'To' => ""),
                //),
                // 'LevelOfDetails' => "ALL_CHECK_POINTS",
                'LevelOfDetails' => $CheckPointType,
                'PiecesEnabled' => 'B',
                // 'EstimatedDeliveryDateEnabled' => '',
            );
            $trackingRequest = array(
                'trackShipmentRequest' => array(
                    'trackingRequest' => array('TrackingRequest' => $params),
                ),
            );

            $params2 = array(
                'strName' => 'Weerachai Nukitram',
                'strEmail' => 'is_php@hotmail.com',
            );

            // echo "Try to access soap ... <br/>";
            // echo "function ::: ";
            // var_dump($client2->__getFunctions());
            // echo "<hr>";

            $xml = '<trackShipmentRequest>  
						<trackingRequest>   
							<TrackingRequest>
								  <Request>
									  <ServiceHeader>
										 <MessageTime>2014-07-01T13:39:57.938Z</MessageTime>
										 <MessageReference>123456789123456789123456789123</MessageReference>
									  </ServiceHeader>
								   </Request>
								   <AWBNumber>
									<ArrayOfAWBNumberItem>9951579794</ArrayOfAWBNumberItem> 
									</AWBNumber>
								   <LevelOfDetails>LAST_CHECKPOINT_ONLY</LevelOfDetails>
								   <PiecesEnabled>B</PiecesEnabled>
								</TrackingRequest>
							</trackingRequest>
						</trackShipmentRequest>';
            $query = new SoapVar($xml, XSD_ANYXML);
            // echo "call function ... <br/>";

            // $data = $client2->__soapCall("trackShipmentRequest",  array($query));// $trackingRequest );

            $data = $client2->__soapCall('trackShipmentRequest', $trackingRequest);



            $result['status'] = true;
            // echo "return ... <br/>";
            $result['message'] = json_decode(json_encode($data), true);
            // print_r($result);die();
            // print_r($result);
        } catch (SoapFault  $e) {
            $result['status'] = false;
            $result['message'] = $e;
            // echo "<h3>Error</h3>";

            // echo "REQUEST HEADERS:\n" . $client2->__getLastRequestHeaders() . "\n<br/>";
            // //print_r($client2);
            // echo "<br/>";
            // echo "REQUEST  :\n" . $client2->__getLastRequest() . "\n<br/>";

            // echo "Response HEADERS:\n" . $client2->__getLastResponseHeaders() . "\n";
            // echo "<br/>";
            // echo "Response:\n" . $client2->__getLastResponse() . "\n";

            // echo "<h3>Error from e</h3>";
            // print_r($e);
        }
        if ($AWB == null) {
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            return  $result;
        }
    }

    public function ClearTable()
    {
        $this->load->model('ImportModel', '', true);
        $lastdate =  $this->ImportModel->getLastDate()[0]['last_date'];
        $sql = "DELETE FROM t_open_pod WHERE date(create_date) <> '" . $lastdate . "'";
        $query = $this->db->query($sql);

        $this->ImportModel->delete_temp_apple_dashboard();

        echo 'Run Success';
    }


    public function GetStatusPandoraDashboardByApi()
    {
        $this->load->model('ImportModel', '', true);

        $this->load->model('DashboardModel', '', true);

        try {

            $lastdate =  $this->ImportModel->getLastDate()[0]['last_date'];
            // print_r($lastdate);die();
            $listAllAwbNotInPid = $this->ImportModel->getDataCarrierTrackingNo($lastdate);

            // print_r($listAllAwbNotInPid);die();

            $list_insert = [];
            if (count($listAllAwbNotInPid) > 0) {


                $z = ceil(count($listAllAwbNotInPid) / 100);

                $start = date('Y-m-d H:i:s');
                // echo $z;die();
                // for ($i = 0; $i <= $z; ++$i) {
                $AWBvalue = [];
                //     $listAwbNotInPid = $this->PidModel->getListAwbNotInPid(100);
                $count = 0;
                foreach ($listAllAwbNotInPid as $Awb) {
                    array_push($AWBvalue, trim($Awb['carrier_tracking_no']));

                    $count++;
                    if ($count == 100) {
                        sleep(1);
                        $data = $this->getDataFromAWBApi($AWBvalue);
                        $date = date('Y-m-d-H-i-s');


                        $count = 0;
                        $AWBvalue = [];

                        if ($data['status'] == true) {
                            if (count($AWBvalue) == 1) {
                                $ArrayOfAWBInfoItem = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo'];
                            } else {
                                $ArrayOfAWBInfoItem = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem'];
                            }
                            // print_r($ArrayOfAWBInfoItem[1]);die();
                            //  print_r($ArrayOfAWBInfoItem[1]['ShipmentInfo']['DestinationServiceArea']['FacilityCode']);die();

                            foreach ($ArrayOfAWBInfoItem as $AWBInfoItem) {
                                // print_r($AWBInfoItem);die();
                                // echo count($ArrayOfPieceInfoItem);
                                // print_r($ArrayOfPieceInfoItem);

                                // foreach ($ArrayOfPieceInfoItem as $PieceInfoItem) {
                                if ($AWBInfoItem['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['ServiceEvent']['EventCode']) {
                                    $last_check_point = $AWBInfoItem['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['ServiceEvent']['EventCode'];
                                    $date_stamp = $AWBInfoItem['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['Date'];
                                    $time_stamp = $AWBInfoItem['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['Time'];
                                } else {
                                    $last_check_point = $AWBInfoItem['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['ServiceEvent']['EventCode'];
                                    $date_stamp =  $AWBInfoItem['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['Date'];
                                    $time_stamp =  $AWBInfoItem['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['Time'];
                                }


                                $item['carrier_tracking_no'] = $AWBInfoItem['AWBNumber'];
                                $item['latest_checkpiont'] = $last_check_point;
                                $item['service_center'] = $AWBInfoItem['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];
                                $item['latest_checkpiont_update'] = $date;
                                $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;

                                if ($item['latest_checkpiont'] == 'FD') {
                                    $item['service_center'] = 'UPC';
                                }
                                // $item['AWB'] = $PieceInfoItem['PieceDetails']['AWBNumber'];
                                // $item['PID'] = $PieceInfoItem['PieceDetails']['LicensePlate'];
                                // $item['weight'] = isset($PieceInfoItem['PieceDetails']['Weight']) ? $PieceInfoItem['PieceDetails']['Weight'] : null;
                                // $item['actual_weight'] = isset($PieceInfoItem['PieceDetails']['ActualWeight']) ? $PieceInfoItem['PieceDetails']['ActualWeight'] : null;

                                // if ($item['latest_checkpiont'] != 'OH') {
                                $nResult = $this->ImportModel->update_carrier_tracking_no($item['carrier_tracking_no'], $item);
                                array_push($list_insert, $item);
                                // }

                                // $nResult = $this->ImportModel->update_service_center($item['service_center'], $item);


                                // print_r($result['list_insert']);
                                // }
                                //    print_r($ArrayOfAWBInfoItem['AWBNumber']);
                            }
                        } else {
                            // array_push($list_insert, "fail");
                            // print_r($data);die();
                        }
                    }
                }

                if ($AWBvalue > 0) {
                    $data = $this->getDataFromAWBApi($AWBvalue);


                    if ($data['status'] == true) {
                        if (count($AWBvalue) == 1) {
                            $ArrayOfAWBInfoItem = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo'];
                        } else {
                            $ArrayOfAWBInfoItem = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem'];
                        }
                        // print_r($ArrayOfAWBInfoItem[1]);die();
                        //  print_r($ArrayOfAWBInfoItem[1]['ShipmentInfo']['DestinationServiceArea']['FacilityCode']);die();

                        foreach ($ArrayOfAWBInfoItem as $AWBInfoItem) {
                            // if( $AWBInfoItem['AWBNumber'] = '4809259372'){
                            //     echo $AWBInfoItem['AWBNumber'];
                            // }
                            // print_r($AWBInfoItem);
                            // die();


                            // print_r($ArrayOfPieceInfoItem);

                            // foreach ($ArrayOfPieceInfoItem as $PieceInfoItem) {
                            if ($AWBInfoItem['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['ServiceEvent']['EventCode']) {
                                $last_check_point = $AWBInfoItem['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['ServiceEvent']['EventCode'];
                                $date_stamp = $AWBInfoItem['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['Date'];
                                $time_stamp = $AWBInfoItem['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['Time'];
                            } else {
                                $last_check_point = $AWBInfoItem['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['ServiceEvent']['EventCode'];
                                $date_stamp =  $AWBInfoItem['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['Date'];
                                $time_stamp =  $AWBInfoItem['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['Time'];
                            }



                            $item['carrier_tracking_no'] = $AWBInfoItem['AWBNumber'];
                            $item['latest_checkpiont'] =  $last_check_point;
                            $item['service_center'] = $AWBInfoItem['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];
                            $item['latest_checkpiont_update'] = $date;
                            $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;
                            // 
                            if ($item['latest_checkpiont'] == 'FD') {
                                $item['service_center'] = 'UPC';
                            }

                            // if ($item['latest_checkpiont'] != 'OH') {
                            $nResult = $this->ImportModel->update_carrier_tracking_no($item['carrier_tracking_no'], $item);
                            array_push($list_insert, $item);
                            // }
                            // print_r($item['latest_checkpiont']);
                            // die();

                            // $item['AWB'] = $PieceInfoItem['PieceDetails']['AWBNumber'];
                            // $item['PID'] = $PieceInfoItem['PieceDetails']['LicensePlate'];
                            // $item['weight'] = isset($PieceInfoItem['PieceDetails']['Weight']) ? $PieceInfoItem['PieceDetails']['Weight'] : null;
                            // $item['actual_weight'] = isset($PieceInfoItem['PieceDetails']['ActualWeight']) ? $PieceInfoItem['PieceDetails']['ActualWeight'] : null;




                            // $nResult = $this->ImportModel->update_service_center($item['service_center'], $item);


                            // print_r($result['list_insert']);
                            // }
                            //    print_r($ArrayOfAWBInfoItem['AWBNumber']);
                        }
                    } else {
                        // array_push($list_insert, "fail");
                        // print_r($data);die();
                    }
                }

                // print_r($AWBvalue);die();
                // $AWBvalue = $listAwbNotInPid[$i]['AWB'];
                // $AWBvalue = [8996720832];

                // print_r($data);
                // die();

                // if ($i == $z) {
                //     $listAllAwbNotInPid = $this->PidModel->getListAwbNotInPid();
                //     if (count($listAllAwbNotInPid) > 0) {
                //         $z = ceil(count($listAllAwbNotInPid) / 100);
                //         $i = 0;
                //     }
                // }
                // }
            }

            $lastCheckpiont_OH = $this->ImportModel->getDatalastCheckPointStatus_OH();

            if (count($lastCheckpiont_OH) > 0) {

                $z = ceil(count($lastCheckpiont_OH) / 100);

                $start = date('Y-m-d H:i:s');
                // echo $z;die();
                // for ($i = 0; $i <= $z; ++$i) {
                $Status_OH = [];
                //     $listAwbNotInPid = $this->PidModel->getListAwbNotInPid(100);
                $count = 0;
                foreach ($lastCheckpiont_OH as $OH) {
                    array_push($Status_OH, trim($OH['carrier_tracking_no']));

                    $count++;
                    if ($count == 100) {
                        sleep(1);
                        $data = $this->getDataFromAWBApi($Status_OH, 'ALL_CHECKPOINTS');
                        $date = date('Y-m-d-H-i-s');


                        $count = 0;
                        $Status_OH = [];

                        if ($data['status'] == true) {
                            if (count($Status_OH) == 1) {
                                $ShipmentEvent = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem']['ShipmentInfo']['ShipmentEvent'];
                                $CarrierTrackingNo = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem']['AWBNumber'];
                                $ServiceCenter = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem']['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];

                                foreach ($ShipmentEvent as $ShipmentEventItemStatus_OH) {

                                    if (count($Status_OH) > 1) {
                                        $ShipmentEventItemStatus_OH = $ShipmentEventItemStatus_OH['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem'];
                                    }

                                    foreach ($ShipmentEventItemStatus_OH as $ItemStatus_OH) {

                                        // print_r($ItemStatus_OH);
                                        $Status_Oh = $ItemStatus_OH['ServiceEvent']['EventCode'];
                                        $date_stamp  = $ItemStatus_OH['Date'];
                                        $time_stamp  = $ItemStatus_OH['Time'];
                                        // foreach ($ArrayOfPieceInfoItem as $PieceInfoItem) {
                                        // if ($ItemStatus_OH['ServiceEvent']['EventCode']) {
                                        //     $last_check_point = $ItemStatus_OH['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['ServiceEvent']['EventCode'];
                                        //     $date_stamp = $ItemStatus_OH['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['Date'];
                                        //     $time_stamp = $ItemStatus_OH['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['Time'];
                                        // } else {
                                        //     $last_check_point = $ItemStatus_OH['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['ServiceEvent']['EventCode'];
                                        //     $date_stamp =  $ItemStatus_OH['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['Date'];
                                        //     $time_stamp =  $ItemStatus_OH['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['Time'];
                                        // }

                                        $item['carrier_tracking_no'] = $CarrierTrackingNo;
                                        $item['latest_checkpiont'] = $Status_Oh;
                                        $item['service_center'] = $ServiceCenter;
                                        $item['latest_checkpiont_update'] = $date;
                                        $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;

                                        // $item['carrier_tracking_no'] = $AWBInfoItem['AWBNumber'];
                                        // $item['latest_checkpiont'] = $last_check_point;
                                        // $item['service_center'] = $AWBInfoItem['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];
                                        // $item['latest_checkpiont_update'] = $date;
                                        // $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;
                                        // print_r($item['date']);
                                        // die();
                                        // $item['latest_checkpiont'] =  $last_check_point;
                                        // $item['service_center'] = $ItemStatus_OH['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];
                                        // $item['latest_checkpiont_update'] = $date;
                                        // $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;
                                        // 
                                        if ($item['latest_checkpiont'] == 'FD') {
                                            $item['service_center'] = 'UPC';
                                        }
                                        $nResult = $this->ImportModel->insert_t_status_OH($item);
                                        array_push($list_insert, $item);
                                    }
                                }
                            } else {

                                $ShipmentEvent = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem'];

                                foreach ($ShipmentEvent as $NewShipmentEvent) {
                                    // print_r();die();
                                    $ServiceCenter = $NewShipmentEvent['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];
                                    $AWBNumber = $NewShipmentEvent['AWBNumber'];
                                    $ArrayOfShipmentEventItem = $NewShipmentEvent['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem'];
                                    // $ServiceCenter = NewShipmentEvent['']

                                    foreach ($ArrayOfShipmentEventItem as $ServiceAndEventCode) {

                                        // print_r($ServiceAndEventCode);
                                        // die();
                                        $date_stamp  = $ServiceAndEventCode['Date'];
                                        $time_stamp = $ServiceAndEventCode['Time'];
                                        $Status_Oh = $ServiceAndEventCode['ServiceEvent']['EventCode'];


                                        $item['carrier_tracking_no'] = $AWBNumber;
                                        $item['latest_checkpiont'] = $Status_Oh;
                                        $item['service_center'] = $ServiceCenter;
                                        $item['latest_checkpiont_update'] = $date;
                                        $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;

                                        if ($item['latest_checkpiont'] == 'FD') {
                                            $item['service_center'] = 'UPC';
                                        }
                                        $nResult = $this->ImportModel->insert_t_status_OH($item);
                                        array_push($list_insert, $item);
                                    }
                                    // print_r($NewShipmentEvent['แกะต่อให้ถึง['ArrayOfAWBInfoItem'] ฟิวนี้ แล้วสร้างตัวแปลมารับเพื่อเอาไปวนลูปอีกทีหนึ่ง แล้วแกต่อให้ถึง ['Eventcode']]);die();  
                                }
                            }
                        } else {
                        }
                    }
                }

                if ($Status_OH > 0) {
                    $data = $this->getDataFromAWBApi($Status_OH, 'ALL_CHECKPOINTS');
                    $date = date('Y-m-d-H-i-s');
                    // print_r($Status_OH );die();

                    if ($data['status'] == true) {
                        if (count($Status_OH) == 1) {
                            $ShipmentEvent = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem']['ShipmentInfo']['ShipmentEvent'];
                            $CarrierTrackingNo = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem']['AWBNumber'];
                            $ServiceCenter = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem']['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];

                            foreach ($ShipmentEvent as $ShipmentEventItemStatus_OH) {

                                if (count($Status_OH) > 1) {
                                    $ShipmentEventItemStatus_OH = $ShipmentEventItemStatus_OH['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem'];
                                }

                                foreach ($ShipmentEventItemStatus_OH as $ItemStatus_OH) {

                                    // print_r($ItemStatus_OH);
                                    $Status_Oh = $ItemStatus_OH['ServiceEvent']['EventCode'];
                                    $date_stamp  = $ItemStatus_OH['Date'];
                                    $time_stamp  = $ItemStatus_OH['Time'];
                                    // foreach ($ArrayOfPieceInfoItem as $PieceInfoItem) {
                                    // if ($ItemStatus_OH['ServiceEvent']['EventCode']) {
                                    //     $last_check_point = $ItemStatus_OH['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['ServiceEvent']['EventCode'];
                                    //     $date_stamp = $ItemStatus_OH['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['Date'];
                                    //     $time_stamp = $ItemStatus_OH['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem']['Time'];
                                    // } else {
                                    //     $last_check_point = $ItemStatus_OH['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['ServiceEvent']['EventCode'];
                                    //     $date_stamp =  $ItemStatus_OH['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['Date'];
                                    //     $time_stamp =  $ItemStatus_OH['Pieces']['PieceInfo']['ArrayOfPieceInfoItem']['PieceEvent']['ArrayOfPieceEventItem']['Time'];
                                    // }

                                    $item['carrier_tracking_no'] = $CarrierTrackingNo;
                                    $item['latest_checkpiont'] = $Status_Oh;
                                    $item['service_center'] = $ServiceCenter;
                                    $item['latest_checkpiont_update'] = $date;
                                    $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;

                                    // $item['carrier_tracking_no'] = $AWBInfoItem['AWBNumber'];
                                    // $item['latest_checkpiont'] = $last_check_point;
                                    // $item['service_center'] = $AWBInfoItem['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];
                                    // $item['latest_checkpiont_update'] = $date;
                                    // $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;
                                    // print_r($item['date']);
                                    // die();
                                    // $item['latest_checkpiont'] =  $last_check_point;
                                    // $item['service_center'] = $ItemStatus_OH['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];
                                    // $item['latest_checkpiont_update'] = $date;
                                    // $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;
                                    // 
                                    if ($item['latest_checkpiont'] == 'FD') {
                                        $item['service_center'] = 'UPC';
                                    }
                                    $nResult = $this->ImportModel->insert_t_status_OH($item);
                                    array_push($list_insert, $item);
                                }
                            }
                        } else {

                            $ShipmentEvent = $data['message']['trackingResponse']['TrackingResponse']['AWBInfo']['ArrayOfAWBInfoItem'];
                            // print_r(count($ShipmentEvent));die();
                            foreach ($ShipmentEvent as $NewShipmentEvent) {
                                // print_r();die();
                                $ServiceCenter = $NewShipmentEvent['ShipmentInfo']['DestinationServiceArea']['FacilityCode'];
                                $AWBNumber = $NewShipmentEvent['AWBNumber'];
                                $ArrayOfShipmentEventItem = $NewShipmentEvent['ShipmentInfo']['ShipmentEvent']['ArrayOfShipmentEventItem'];
                                // $ServiceCenter = NewShipmentEvent['']

                                foreach ($ArrayOfShipmentEventItem as $ServiceAndEventCode) {

                                    // print_r($ServiceAndEventCode);
                                    // die();
                                    $date_stamp  = $ServiceAndEventCode['Date'];
                                    $time_stamp = $ServiceAndEventCode['Time'];
                                    $Status_Oh = $ServiceAndEventCode['ServiceEvent']['EventCode'];


                                    $item['carrier_tracking_no'] = $AWBNumber;
                                    $item['latest_checkpiont'] = $Status_Oh;
                                    $item['service_center'] = $ServiceCenter;
                                    $item['latest_checkpiont_update'] = $date;
                                    $item['event_location_state_code'] = $date_stamp . ' ' . $time_stamp;

                                    if ($item['latest_checkpiont'] == 'FD') {
                                        $item['service_center'] = 'UPC';
                                    }
                                    $nResult = $this->ImportModel->insert_t_status_OH($item);
                                    array_push($list_insert, $item);
                                }
                                // print_r($NewShipmentEvent['แกะต่อให้ถึง['ArrayOfAWBInfoItem'] ฟิวนี้ แล้วสร้างตัวแปลมารับเพื่อเอาไปวนลูปอีกทีหนึ่ง แล้วแกต่อให้ถึง ['Eventcode']]);die();  
                            }
                        }
                    } else {
                    }
                }
            }
            $DataNewStatusOh = $this->ImportModel->UpdateOhToNewStatus();
            $DataMapping = $this->DashboardModel->UpdateDataMappingToDB();
            $DataTimstamp = $this->DashboardModel->InsertTimeStamp();
            $result['status'] = true;
            $result['message'] = $this->lang->line('savesuccess');
            $result['list_insert'] = $list_insert;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function alert_line_notify($start, $record)
    {
        $Token = 'zyNjOIvKdeeLfaroiO5Wiw0TkM3ulvTgn3MuOD9QplF';
        $from = 'DHL';

        // call line api
        // $message = "บันทึกสำเร็จ จำนวน ".$record. " record " .date("Y-m-d H:i:s");
        $message = 'อัพเดทสำเร็จ เริ่ม' . $start . ' สิ้นสุด ' . date('Y-m-d H:i:s');
        // echo $message;
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = array('Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . '');
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
            // echo 'error:' . curl_error($chOne);
        } else {
            $result_ = json_decode($result, true);
            // echo "status : ".$result_['status']; echo "message : ". $result_['message'];
            // redirect('/menu_list/ordered','refresh');
        }
        curl_close($chOne);
    }
}
class WsseAuthHeader extends SoapHeader
{
    private $wss_ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    public function __construct($user, $pass, $ns = null)
    {
        if ($ns) {
            $this->wss_ns = $ns;
        }

        $auth = new stdClass();
        $auth->Username = new SoapVar($user, XSD_STRING, null, $this->wss_ns, null, $this->wss_ns);
        $auth->Password = new SoapVar($pass, XSD_STRING, null, $this->wss_ns, null, $this->wss_ns);

        $username_token = new stdClass();
        $username_token->UsernameToken = new SoapVar($auth, SOAP_ENC_OBJECT, null, $this->wss_ns, 'UsernameToken', $this->wss_ns);

        $security_sv = new SoapVar(
            new SoapVar($username_token, SOAP_ENC_OBJECT, null, $this->wss_ns, 'UsernameToken', $this->wss_ns),
            SOAP_ENC_OBJECT,
            null,
            $this->wss_ns,
            'Security',
            $this->wss_ns
        );
        parent::__construct($this->wss_ns, 'Security', $security_sv, true);
    }
}

<?php

use setasign\Fpdi\PdfParser\Filter\Flate;

defined('BASEPATH') or exit('No direct script access allowed');

class ReportTwo extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        ini_set("memory_limit", "512M");
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_PANDASH');
        if (!$role_data['REP']) {
            redirect('Login');
        }
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        // $this->load->view('dashboard/dashboard_view');
        $this->load->view('dashboard/reportTwo_view');
        $this->load->view('share/footer');
    }

    public function callView()
    {
        $this->load->view('test');
    }


    public function addCodeAppleAndDhl()
    {
        // $this->output->set_content_type('application/json');
        $nResult = 0;

        try {

            $this->load->model('CodeAppleAndDhlModel', '', TRUE);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            /*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/

            //$dateRecord = date("Y-m-d H:i:s"); 
            $data['id'] =  isset($dataPost['id']) ? $dataPost['id'] : 0;
            $data['reason_code'] =  isset($dataPost['reason_code']) ? $dataPost['reason_code'] : "";
            $data['code_description'] =  isset($dataPost['code_description']) ? $dataPost['code_description'] : "";
            $data['check_point'] =  isset($dataPost['check_point']) ? $dataPost['check_point'] : "";
            $data['remark'] =  isset($dataPost['remark']) ? $dataPost['remark'] : "";
            //$data['IsActive'] = isset($dataPost['IsActive'])?$dataPost['IsActive']: "1";
            //$data['website'] =  isset($dataPost['website'])?$dataPost['website']: "";

            //print_r($data);

            //$data['update_date'] = $dateRecord;
            //$data['update_user'] = $this->session->userdata('user_name'); 
            // load model 
            if ($data['id'] == 0) {
                // $data['IsActive'] = 1;
                //$data['create_date'] = $dateRecord;
                //$data['create_user'] =  $this->session->userdata('user_name'); 
                $nResult = $this->CodeAppleAndDhlModel->insert($data);
            } else {
                $nResult = $this->CodeAppleAndDhlModel->update($data['id'], $data);
            }

            if ($nResult > 0) {
                $result['status'] = true;
                $result['message'] = $this->lang->line("savesuccess");
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line("error");
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

	public function getProductChart()
    {
        // print_r("php");
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        try {
            $this->load->model('ReportOneModel', '', TRUE);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result['status'] = true;
            $result['message']['product'] = $this->ReportOneModel->getProductChart($dataPost);
            $result['message']['lane'] = $this->ReportOneModel->getLaneChart($dataPost);
            
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        // print_r($result);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getOntimeChart()
    {
        // print_r("php");
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        try {
            $this->load->model('ReportOneModel', '', TRUE);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result['status'] = true;
            $result['message']['ontime'] = $this->ReportOneModel->getOntimeData($dataPost);
            $result['message']['order'] = $this->ReportOneModel->getOrderData($dataPost);
            $result['message']['dhlsupport'] = $this->ReportOneModel->getDHLsupportData($dataPost);
            $result['message']['customersupport'] = $this->ReportOneModel->getCustomersupportData($dataPost);
            $result['message']['uncontrollable'] = $this->ReportOneModel->getUncontrollableData($dataPost);
            $result['message']['factordata'] = $this->ReportOneModel->getFactorData($dataPost);
            $result['message']['previousquarter'] = $this->ReportOneModel->getPreviousQuarterData($dataPost);
            $result['message']['previousorder'] = $this->ReportOneModel->getPreviousOrderData($dataPost); 
            
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        // print_r($result);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getDataApi()
    {
        // print_r("php");
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        try {
            $this->load->model('ReportOneModel', '', TRUE);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result['status'] = true;

            $this->ReportOneModel->delete_tr_1_1_shipement_by_prouduct();
            $this->ReportOneModel->delete_tr_1_2_shipement_by_lane();

            $product = $this->ReportOneModel->getProductDataChart();
            $lane = $this->ReportOneModel->getLaneDataChart();
		    // print_r($product);

            foreach($product as $row){
            $dataProduct['id'] =  isset($row['id']) ? $row['id'] : 0;
            $dataProduct['ProductName'] =  isset($row['Product Name']) ? $row['Product Name'] : "";
            $dataProduct['CountWaybill'] =  isset($row['CountWaybill']) ? $row['CountWaybill'] : 0;
            $dataProduct['shipmonth'] =  isset($row['shipmonth']) ? $row['shipmonth'] : "";
            $dataProduct['Year'] =  isset($row['Year']) ? $row['Year'] : 0;
            $dataProduct['Quater'] =  isset($row['Quater']) ? $row['Quater'] : "";
            $result['message']['product'] = $this->ReportOneModel->insertProductChart($dataProduct);

            }
         
            foreach($lane as $row){
            $dataLane['id'] =  isset($row['id']) ? $row['id'] : 0;
            $dataLane['lane'] =  isset($row['lane']) ? $row['lane'] : "";
            $dataLane['CountWaybill'] =  isset($row['CountWaybill']) ? $row['CountWaybill'] : 0;
            $dataLane['shipmonth'] =  isset($row['shipmonth']) ? $row['shipmonth'] : "";
            $dataLane['Year'] =  isset($row['Year']) ? $row['Year'] : 0;
            $dataLane['Quater'] =  isset($row['Quater']) ? $row['Quater'] : "";
            $result['message']['lane'] = $this->ReportOneModel->insertLaneChart($dataLane);

            }

            // echo "Product ->".$result['message']['product'];
            // echo "Lane ->".$result['message']['lane'];
            
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        // print_r($result);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getShipmentData()
    {
        // print_r("php");
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        try {
            $this->load->model('ReportOneModel', '', TRUE);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result['status'] = true;

            $this->ReportOneModel->delete_tr_1_shipment_excel();

            $shipment = $this->ReportOneModel->getShipmentExcel();
		    // print_r($shipm ent);

            foreach($shipment as $row){
            $dataShipment['id'] =  isset($row['id']) ? $row['id'] : 0;
            $dataShipment['Group Over SLA ( Exclude weekend )'] =  isset($row['Group Over SLA ( Exclude weekend )']) ? $row['Group Over SLA ( Exclude weekend )'] : "";
            $dataShipment['Group Over SLA ( Include weekend )'] =  isset($row['Group Over SLA ( Include weekend )']) ? $row['Group Over SLA ( Include weekend )'] : 0;
            $dataShipment['factor_incident'] =  isset($row['factor_incident']) ? $row['factor_incident'] : "";
            $dataShipment['On time status'] =  isset($row['On time status']) ? $row['On time status'] : 0;
            $dataShipment['Group T/T Include weekend range'] =  isset($row['Group T/T Include weekend range']) ? $row['Group T/T Include weekend range'] : "";
            $dataShipment['factor_incident_cal'] =  isset($row['factor_incident_cal']) ? $row['factor_incident_cal'] : 0;
            $dataShipment['factor'] =  isset($row['factor']) ? $row['factor'] : "";
            $dataShipment['lane'] =  isset($row['lane']) ? $row['lane'] : 0;
            $dataShipment['Diff SLA ( Include weekend )'] =  isset($row['Diff SLA ( Include weekend )']) ? $row['Diff SLA ( Include weekend )'] : "";
            $dataShipment['Diff SLA ( Exclude weekend )'] =  isset($row['Diff SLA ( Exclude weekend )']) ? $row['Diff SLA ( Exclude weekend )'] : 0;
            $dataShipment['T/T included weekend by_pickup_day'] =  isset($row['T/T included weekend by_pickup_day']) ? $row['T/T included weekend by_pickup_day'] : "";
            $dataShipment['T/T excluded weekend'] =  isset($row['T/T excluded weekend']) ? $row['T/T excluded weekend'] : 0;
            $dataShipment['T/T included weekend'] =  isset($row['T/T included weekend']) ? $row['T/T included weekend'] : "";
            $dataShipment['delivery_provider'] =  isset($row['delivery_provider']) ? $row['delivery_provider'] : 0;
            $dataShipment['gateway'] =  isset($row['gateway']) ? $row['gateway'] : "";
            $dataShipment['shipmonth'] =  isset($row['shipmonth']) ? $row['shipmonth'] : 0;
            $dataShipment['Quater'] =  isset($row['Quater']) ? $row['Quater'] : "";
            $dataShipment['Year'] =  isset($row['Year']) ? $row['Year'] : 0;
            $dataShipment['week_number'] =  isset($row['week_number']) ? $row['week_number'] : "";
            $dataShipment['Waybill Number'] =  isset($row['Waybill Number']) ? $row['Waybill Number'] : 0;
            $dataShipment['Product Code'] =  isset($row['Product Code']) ? $row['Product Code'] : "";
            $dataShipment['Product Name'] =  isset($row['Product Name']) ? $row['Product Name'] : 0;
            $dataShipment['Shipper Reference'] =  isset($row['Shipper Reference']) ? $row['Shipper Reference'] : "";
            $dataShipment['Shipper Company Name'] =  isset($row['Shipper Company Name']) ? $row['Shipper Company Name'] : 0;
            $dataShipment['Destination Country/Territory Name'] =  isset($row['Destination Country/Territory Name']) ? $row['Destination Country/Territory Name'] : "";
            $dataShipment['Destination Country/Territory Area Code'] =  isset($row['Destination Country/Territory Area Code']) ? $row['Destination Country/Territory Area Code'] : 0;
            $dataShipment['Destination Service Area Code'] =  isset($row['Destination Service Area Code']) ? $row['Destination Service Area Code'] : "";
            $dataShipment['Number of Days in Customs'] =  isset($row['Number of Days in Customs']) ? $row['Number of Days in Customs'] : 0;
            $dataShipment['Startclock Day of Week'] =  isset($row['Startclock Day of Week']) ? $row['Startclock Day of Week'] : "";
            $dataShipment['Startclock Date'] =  isset($row['Startclock Date']) ? $row['Startclock Date'] : 0;
            $dataShipment['Stopclock Date'] =  isset($row['Stopclock Date']) ? $row['Stopclock Date'] : "";
            $dataShipment['Stopclock Day of Week'] =  isset($row['Stopclock Day of Week']) ? $row['Stopclock Day of Week'] : 0;
            $dataShipment['Signatory'] =  isset($row['Signatory']) ? $row['Signatory'] : "";
            $dataShipment['Elapsed Actual Transit Days'] =  isset($row['Elapsed Actual Transit Days']) ? $row['Elapsed Actual Transit Days'] : 0;
            $dataShipment['Reporting Code Description'] =  isset($row['Reporting Code Description']) ? $row['Reporting Code Description'] : "";
            $dataShipment['Reporting Code Category Description'] =  isset($row['Reporting Code Category Description']) ? $row['Reporting Code Category Description'] : 0;
            $dataShipment['Consignee Company Name'] =  isset($row['Consignee Company Name']) ? $row['Consignee Company Name'] : "";
            $dataShipment['Consignee Address'] =  isset($row['Consignee Address']) ? $row['Consignee Address'] : 0;
            $dataShipment['Consignee Zip'] =  isset($row['Consignee Zip']) ? $row['Consignee Zip'] : "";
            $dataShipment['Consignee City'] =  isset($row['Consignee City']) ? $row['Consignee City'] : 0;
            $dataShipment['Billing Account Number'] =  isset($row['Billing Account Number']) ? $row['Billing Account Number'] : "";
            $dataShipment['Manifested Number of Pieces'] =  isset($row['Manifested Number of Pieces']) ? $row['Manifested Number of Pieces'] : 0;
            $dataShipment['Billed Weight'] =  isset($row['Billed Weight']) ? $row['Billed Weight'] : "";
            $dataShipment['Customs Declared Value'] =  isset($row['Customs Declared Value']) ? $row['Customs Declared Value'] : 0;
            $dataShipment['Currency Code of Declared Value'] =  isset($row['Currency Code of Declared Value']) ? $row['Currency Code of Declared Value'] : "";
            $dataShipment['file_name'] =  isset($row['file_name']) ? $row['file_name'] : 0;
            $dataShipment['create_user'] =  isset($row['create_user']) ? $row['create_user'] : "";
            $dataShipment['create_date'] =  isset($row['create_date']) ? $row['create_date'] : 0;
            $dataShipment['update_user'] =  isset($row['update_user']) ? $row['update_user'] : "";
            $dataShipment['update_date'] =  isset($row['update_date']) ? $row['update_date'] : 0;
            $dataShipment['Unknown POD Status'] =  isset($row['Unknown POD Status']) ? $row['Unknown POD Status'] : "";
            // print_r($dataShipment);
            // die;
            $result['message'] = $this->ReportOneModel->insertShipmentExcel($dataShipment);

            }
            // echo "Product ->".$result['message']['product'];
            // echo "Lane ->".$result['message']['lane'];
            
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        // print_r($result);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getPercentStatus()
    {
        // $this->output->set_content_type('application/json');
        $nResult = 0;

        try {

            $this->load->model('ReportOneModel', '', TRUE);
            ini_set('max_execution_time', 300);
            set_time_limit(300);
            $date =  $this->ReportOneModel->getLastDate()[0]['last_date'];
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $system = $dataPost['mSearch']['customer'];
            $SddStatus = $dataPost['mSearch']['SddStatus'];
            // print_r($SddStatus );die();
            // print_r($dataPost );die();
            $result['status'] = true;
            $result['sddview'] = $date;
            $result['last_update'] = $this->ReportOneModel->GetlastUpdateTableLogUpdate($system)[0]['last_update'];
            $result['message'] = $this->ReportOneModel->getPercentStatus($date,$SddStatus);
            // print_r($result['message']);die();
            $result['listDataGraphPieBytoDay'] = $this->ReportOneModel->GetDataGraphPieBytoDay($date,$SddStatus);
            $result['listDataGraphBarBytoDay'] = $this->ReportOneModel->GetDataGraphBarBytoDay($date,$SddStatus);
            $result['listDataGraphPieByServiceCenter'] = $this->ReportOneModel->GetDataGraphPieByServiceCenter($date,$SddStatus);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }



    public function deleteCodeAppleAndDhl()
    {
        try {
            $this->load->model('CodeAppleAndDhlModel', '', TRUE);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $id =  isset($dataPost['id']) ? $dataPost['id'] : 0; // $this->input->post('ap_id');

            $bResult = $this->CodeAppleAndDhlModel->deleteCodeAppleAndDhlBy($id);

            if ($bResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line("savesuccess");
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line("error_faliure");
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    // public function getQuaterComboList()
    // {
    //     // print_r("test");
    //     try {
    //         $this->load->model('DashboardModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);

    //         $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
    //         $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
    //         $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
    //         $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
    //         $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

    //         $offset = ($PageIndex - 1) * $PageSize;

    //         $dataModel['test'] = "test";
    //         $result['status'] = true;
    //         $result['message'] = $this->DashboardModel->getQuaterModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
    //         // $result['totalRecords'] = $this->CashadvanceModel->getTotal($dataModel);
    //         // $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }

    
    public function getYearComboList()
    {
        try {
            $this->load->model('ReportOneModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ReportOneModel->getYearModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            // $result['totalRecords'] = $this->CashadvanceModel->getTotal($dataModel);
            // $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    // public function getcodeappleanddhlModelList()
    // {

    //     try {
    //         $this->load->model('CodeAppleAndDhlModel', '', TRUE);

    //         /*$data['m_id'] =  $this->input->post('mab_m_id');

    // 		$limit =  $this->input->post('limit');
    // 		$offset =  $this->input->post('offset');
    // 		$order = $this->input->post('order');
    // 		$direction = $this->input->post('direction');

    // 		$result = $this->AdminModel->getAdminNameAllList($data, $limit , $offset, $order, $direction); */

    //         $result['status'] = true;
    //         $result['message'] = $this->lang->line("savesuccess");
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = "exception: " . $ex;
    //     }

    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }


    public function getCodeAppleAndDhlModel()
    {

        try {
            $this->load->model('CodeAppleAndDhlModel', '', TRUE);
            $dataPost = json_decode($this->input->raw_input_stream, true);

            //print_r($_POST);
            //print_r($this->input->post()); 
            //echo $this->input->raw_input_stream;  
            //print_r($dataPost);die();
            //$dateRecord = date("Y-m-d H:i:s"); 
            $PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->CodeAppleAndDhlModel->getCodeAppleAndDhlNameList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            $result['totalRecords'] = $this->CodeAppleAndDhlModel->getTotal($dataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

            //$result['message'] = $this->AdminModel->getAdminModel(); 

        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getCodeAppleAndDhlComboList()
    {

        try {
            $this->load->model('CodeAppleAndDhlModel', '', TRUE);
            $result['status'] = true;
            $result['message'] = $this->CodeAppleAndDhlModel->getCodeAppleAndDhlComboList();
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }



    public function Exportexcel()
    {
        // log_message('error', 'Exportexcel');
        ini_set("memory_limit", "10000M");
        ini_set('MAX_EXECUTION_TIME', 0);
        try {
            $this->load->model('ReportOneModel', '', TRUE);

            $this->load->library('MyExcel');
            
            $dataPost = json_decode($this->input->raw_input_stream, true);

            // print_r($dataPost);
            // die();

            $dataModel['file_name'] = isset($dataPost['file_name']) ? $dataPost['file_name'] : "";
            $dataModel['data'] = isset($dataPost['data']) ? $dataPost['data'] : "";
            $dataModel['type'] = isset($dataPost['type']) ? $dataPost['type'] : "";

            $SddStatus = isset($dataPost['SddStatus']) ? $dataPost['SddStatus'] : "";

            // print_r($dataPost);
            
            // print_r($SddStatus);
            // die();

            $date =  $this->ReportOneModel->getLastDate()[0]['last_date'];
            // print_r($date);
            // die();

            // $date = '2021-07-28';

            // log_message('error', 'Can get date');
            // $data = [];
            $data = $this->ReportOneModel->ExportExcelToSddBytoDay($dataModel, $date, $SddStatus);
            // log_message('error', 'Can get data');
            // print_r($data);
            // die();

            //2021-07-29//
            // ini_set('max_execution_time', 300); //300 seconds = 5 minutes
            // set_time_limit(300);
            //2021-07-29//

            // print_r($data[0]['file_name']);die();
            $objPHPExcel = PHPExcel_IOFactory::load('upload/import_excel/' . $data[0]['file_name']);
            log_message('error', 'No.1');

            $objPHPExcel->removeSheetByIndex(0);
            $objPHPExcel->createSheet(0);


            // $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("OpenPOD");
            log_message('error', 'No.2');

            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                    )
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FFFFFF')
                ),
                'font' => [
                    'size' => 9,
                    'bold'  => true,
                    'name'  => 'Arial',
                ]

            );


            $headtableColumRtoV = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                    )
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '5D64C6')
                ),
                'font' => [
                    'size' => 9,
                    'bold'  => true,
                    'name'  => 'Arial',
                    'color'  => array('rgb' => 'FFFFFF'),
                ]

            );


            $headtableColumWtoZ = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                    )
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C36FEE')
                ),
                'font' => [
                    'size' => 9,
                    'bold'  => true,
                    'name'  => 'Arial',
                    'color'  => array('rgb' => '000000'),
                ]

            );


            $titleExcel = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    // 'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'font' => [
                    'size' => 12,
                    'bold'  => true,
                    'name'  => 'Arial',
                    'color'  => array('rgb' => '000000'),
                ]

            );
            log_message('error', 'No.3');


            $objPHPExcel->getActiveSheet()->mergeCells('B2:E2')->getStyle("B2:E2")->applyFromArray($titleExcel);

            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setAutoSize(TRUE);


            // if (isset($dataModel['end_date']) && $dataModel['end_date'] != "") {
            //     $EndDateTS = strtotime($dataModel['end_date']);
            //     if ($EndDateTS !== false) {
            //         date('Y-m-d', $EndDateTS);
            //     }
            // }
            // if (isset($dataModel['date']) && $dataModel['date'] != "") {
            //     $StarDateTS = strtotime($dataModel['date']);
            //     if ($StarDateTS !== false) {
            //         date('Y-m-d', $StarDateTS);
            //     }
            // }

            log_message('error', 'No.5');
            $objPHPExcel->setActiveSheetIndex(0)

                // ->setCellValue('A1', 'Customer')
                ->setCellValue('B2', $data[0]['title_excel'])
                ->setCellValue('B4', 'Carrier Tracking No.')
                ->setCellValue('C4', 'Previous Carrier Tracking No.')
                ->setCellValue('D4', 'Shipment ID (AUSID)')
                ->setCellValue('E4', 'SCAC')
                ->setCellValue('F4', 'Event Code(As per the Spec)')
                ->setCellValue('G4', 'Reason Code(As per the spec)')
                ->setCellValue('H4', 'Event Date ( YYYYMMDD)')
                ->setCellValue('I4', 'Event Time(HH:MM:SS)')
                ->setCellValue('J4', 'Event Time zone(UTC+/-HH:MM)')
                ->setCellValue('K4', 'Event Location-City')
                ->setCellValue('L4', 'Event Location-State code')
                ->setCellValue('M4', 'Event Location-Country code')
                ->setCellValue('N4', 'Comments')
                ->setCellValue('O4', 'Latest Checkpoint')
                ->setCellValue('P4', 'Service Center')
                ->setCellValue('Q4', 'Remark')
                ->setCellValue('R4', 'EM Scheduled Delivery Date')
                ->setCellValue('S4', 'SDD')
                ->setCellValue('T4', 'Aging from SDD')
                ->setCellValue('U4', 'SDD Status')
                ->setCellValue('V4', 'Delivery Performance (OTD)')
                ->setCellValue('W4', 'EDD Status')
                ->setCellValue('X4', 'EDD Delivery Performance (OTD)')
                ->setCellValue('Y4', 'EDD')
                ->setCellValue('Z4', 'Controllable/Uncontrollable')
                ->setCellValue('AA4', 'EM Sales District Code')
                ->setCellValue('AB4', 'EM Web Order ID')
                ->setCellValue('AC4', 'EM Tracking Number (RollUp)')
                ->setCellValue('AD4', '')
                ->setCellValue('AE4', 'EM Sales Order ID')
                ->setCellValue('AF4', 'EM Origin(UDM)')
                ->setCellValue('AG4', 'EM Current Event Name')
                ->setCellValue('AH4', 'EM Current Event DateTime')
                ->setCellValue('AI4', 'EM Current Event Reason Code')
                ->setCellValue('AJ4', 'EM Current Event Location Description')
                ->setCellValue('AK4', 'DQ (Postship) Carrier')
                ->setCellValue('AL4', 'Ops Channel Level 2 Code')
                ->setCellValue('AM4', 'EM Ship Condition Code')
                ->setCellValue('AN4', 'EM Ship Point Type(UDM)')
                ->setCellValue('AO4', 'EM Ship Point Code')
                ->setCellValue('AP4', 'EM City Name')
                ->setCellValue('AQ4', 'EM Postal Code')
                ->setCellValue('AR4', 'EM Ship Confirmation Last Date')
                ->setCellValue('AS4', 'EM Ship Confirmation Last Time')
                ->setCellValue('AT4', 'EM Shipment Picked up Last DateTime')
                ->setCellValue('AU4', 'EM Uplift Last DateTime')
                ->setCellValue('AV4', 'EM First Attempt Date')
                ->setCellValue('AW4', 'EM First Attempt Reason Code')
                ->setCellValue('AX4', 'EM First Attempt Reason Code Description')
                ->setCellValue('AY4', 'EM Delivery Not Completed Last Date')
                ->setCellValue('AZ4', 'EM Delivery Not Completed Last Reason Code Description')
                ->setCellValue('BA4', 'EM Shipment Delay Last Date')
                ->setCellValue('BB4', 'EM Shipment Delay Last Reason Code')
                ->setCellValue('BC4', 'EM Shipment Delay Last Reason Code Description')
                ->setCellValue('BD4', 'EM Count of Delivery Attempts');

            log_message('error', 'No.6');

            $objPHPExcel->getActiveSheet()->getStyle('B4:BC4')
                ->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($titleExcel);
            $objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('E4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('F4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('G4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('H4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('I4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('J4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('K4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('L4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('M4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('N4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('O4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('P4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Q4')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('R4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('S4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('T4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('U4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('V4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('W4')->applyFromArray($headtableColumWtoZ);
            $objPHPExcel->getActiveSheet()->getStyle('X4')->applyFromArray($headtableColumWtoZ);
            $objPHPExcel->getActiveSheet()->getStyle('Y4')->applyFromArray($headtableColumWtoZ);
            $objPHPExcel->getActiveSheet()->getStyle('Z4')->applyFromArray($headtableColumWtoZ);
            $objPHPExcel->getActiveSheet()->getStyle('AA4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AB4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AC4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AD4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AE4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AF4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AG4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AH4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AI4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AJ4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AK4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AL4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AM4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AN4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AO4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AP4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AQ4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AR4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AS4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AT4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AU4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AV4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AW4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AX4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AY4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('AZ4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('BA4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('BB4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('BC4')->applyFromArray($headtableColumRtoV);
            $objPHPExcel->getActiveSheet()->getStyle('BD4')->applyFromArray($headtableColumRtoV);

            $start_row = 5;
            // log_message('error', 'startloop');
            // log_message('error', count($data));
            foreach ($data as $row) {

                // if ($start_row == 23 || $start_row == 24) {
                //     $keys = array_keys($row);
                //     log_message('error', 'header' . implode(",", $keys));
                //     log_message('error', 'data row 24' . implode(",", $row));
                // }

                $objPHPExcel->setActiveSheetIndex(0)

                    // ->setCellValue('A' . $start_row, date('d/m/Y', strtotime($row['update_date'])))
                    // ->setCellValue('A' . $start_row, $row['customer_no'])

                    ->setCellValue('B' . $start_row, $row['carrier_tracking_no'])
                    ->setCellValue('C' . $start_row, $row['previous_carrier_tracking_no'])
                    ->setCellValue('D' . $start_row, $row['shipment_id'])
                    ->setCellValue('E' . $start_row, $row['scac'])
                    ->setCellValue('F' . $start_row, $row['event_code'])
                    ->setCellValue('G' . $start_row, $row['reason_code'])
                    ->setCellValue('H' . $start_row, $row['new_event_date'])
                    ->setCellValue('I' . $start_row, $row['event_time'])
                    ->setCellValue('J' . $start_row, $row['event_time_zone'])
                    ->setCellValue('K' . $start_row, $row['event_location_city'])
                    ->setCellValue('L' . $start_row, $row['event_location_state_code'])
                    ->setCellValue('M' . $start_row, $row['event_location_country_code'])
                    ->setCellValue('N' . $start_row, $row['comments'])
                    ->setCellValue('O' . $start_row, $row['latest_checkpiont'])
                    ->setCellValue('P' . $start_row, $row['service_center'])
                    ->setCellValue('Q' . $start_row, $row['remark'])
                    ->setCellValue('R' . $start_row, $row['new_em_scheduled_delivery_date'])
                    ->setCellValue('S' . $start_row, $row['sdd'])
                    ->setCellValue('T' . $start_row, $row['aging_from_sdd'])
                    ->setCellValue('U' . $start_row, $row['ssd_status'])
                    ->setCellValue('V' . $start_row, $row['delivery_performance'])
                    ->setCellValue('W' . $start_row, $row['edd_status'])
                    ->setCellValue('X' . $start_row, $row['edd_delivery_performance'])
                    ->setCellValue('Y' . $start_row, $row['edd'])
                    ->setCellValue('Z' . $start_row, $row['controllable_uncontrollable'])
                    ->setCellValue('AA' . $start_row, $row['em_sales_district_code'])
                    ->setCellValue('AB' . $start_row, $row['em_web_order_id'])
                    ->setCellValue('AC' . $start_row, $row['em_tracking_number'])
                    ->setCellValue('AD' . $start_row, $row['column_ad'])
                    ->setCellValue('AE' . $start_row, $row['em_sales_order_id'])
                    ->setCellValue('AF' . $start_row, $row['em_origin'])
                    ->setCellValue('AG' . $start_row, $row['em_current_event_name']) //2021-07-29//
                    ->setCellValue('AH' . $start_row, $row['new_em_current_event_datetime'])    //2021-07-29//
                    ->setCellValue('AI' . $start_row, $row['em_current_event_reason_code'])
                    ->setCellValue('AJ' . $start_row, $row['em_current_event_location_description'])
                    ->setCellValue('AK' . $start_row, $row['dq_postship_carrier'])
                    ->setCellValue('AL' . $start_row, $row['ops_channel_level_2_code'])
                    ->setCellValue('AM' . $start_row, $row['em_ship_condition_code'])
                    ->setCellValue('AN' . $start_row, $row['em_ship_point_type']) //2021-07-29//
                    ->setCellValue('AO' . $start_row, $row['em_ship_point_code'])
                    ->setCellValue('AP' . $start_row, $row['em_city_name'])
                    ->setCellValue('AQ' . $start_row, $row['em_postal_code'])
                    ->setCellValue('AR' . $start_row, $row['new_em_ship_confirmation_last_date'])
                    ->setCellValue('AS' . $start_row, $row['new_em_ship_confirmation_last_time'])
                    ->setCellValue('AT' . $start_row, $row['new_em_shipment_picked_up_last_datetime'])
                    ->setCellValue('AU' . $start_row, $row['new_em_uplift_last_datetime'])
                    // isset($row['em_uplift_last_datetime']) && $row['em_uplift_last_datetime'] != '0000-00-00 00:00:00' ? null:$row['em_uplift_last_datetime'])
                    ->setCellValue('AV' . $start_row, $row['new_em_first_attempt_date'])
                    ->setCellValue('AW' . $start_row, $row['em_first_attempt_reason_code']) //2021-07-29//
                    ->setCellValue('AX' . $start_row, $row['em_first_attempt_reason_code_description']) //2021-07-29//
                    ->setCellValue('AY' . $start_row, $row['new_em_delivery_not_completed_last_date'])
                    ->setCellValue('AZ' . $start_row, $row['em_delivery_not_completed_last_reason_code_description'])
                    ->setCellValue('BA' . $start_row, $row['new_em_shipment_delay_last_date'])
                    ->setCellValue('BB' . $start_row, $row['em_shipment_delay_last_reason_code'])
                    ->setCellValue('BC' . $start_row, $row['em_shipment_delay_last_reason_code_description'])
                    ->setCellValue('BD' . $start_row, $row['em_count_of_delivery_attempts']);
                // ->setCellValueExplicit('P' . $start_row, $row['tax_code'], PHPExcel_Cell_DataType::TYPE_STRING);

                // log_message('error', 'row no.' . $start_row);
                $start_row++;
            }
            log_message('error', 'End loop');

            $EndStartRow = $start_row - 1;
            $objPHPExcel->getActiveSheet()->getStyle('B5:BD' . $EndStartRow)->applyFromArray($styleArray);
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

            // $filename = $start.' to '.$end . '.xlsx';

            $filename = 'Apple_Dashboard_Export-' . date("Y-m-d-H-i-s") . '.xlsx';
            // log_message('error', 'Can get filename' . $filename);

            // echo FCPATH; die();
            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);
            $result['message'] = $filename;
            $result['status'] = true;
        } catch (Exception $ex) {
            log_message('error', $ex->getMessage());
            $result['status'] = false;
            $result['message'] = "exception: " . $ex->getMessage();
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function Exportreport()
    {
        // log_message('error', 'Exportexcel');
        ini_set("memory_limit", "10000M");
        ini_set('MAX_EXECUTION_TIME', 0);
        
        try {
            $this->load->model('ReportOneModel', '', true);

            $this->load->library('MyExcel');
            
            $dataPost = json_decode($this->input->raw_input_stream, true);

            // $data = $this->DashboardModel->Exportreport($dataPost);


            // $objPHPExcel = new PHPExcel();

            // $objPHPExcel->setActiveSheetIndex(0);
            // $objPHPExcel->getActiveSheet()->setTitle("Shipment");



            $objPHPExcel = PHPExcel_IOFactory::load('upload/Report_template.xlsx');
            log_message('error', 'No.1');

            $objPHPExcel->setActiveSheetIndexByName('Shipment');
            // $sheetIndex = $objPHPExcel->getActiveSheetIndex();
            // $objPHPExcel->removeSheetByIndex($sheetIndex);

            // $objPHPExcel->removeSheetByIndex(0);
            // $objPHPExcel->createSheet(0);


            // $objPHPExcel->setActiveSheetIndex(0);
            // $objPHPExcel->getActiveSheet()->setTitle("Shipment");
            // log_message('error', 'No.2');


            // die();

            //กำหนด style ส่วนหัว//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_NONE
            //         )
            //     ),
            //     // 'alignment' => array(
            //     //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            //     // )
            // );

            // $headtable = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_NONE
            //         )
            //     ),
            //     'font' => [
            //         // 'size' => 9,
            //         'bold'  => true,
            //         // 'name'  => 'Arial',
            //     ]

            //     // 'alignment' => array(
            //     //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

            //     // ),
            //     // 'fill' => array(
            //     //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
            //     //     'color' => array('rgb' => '00ffff')
            //     // )
            // );


            // $headbody = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_NONE
            //         )
            //     ),
            //     // 'alignment' => array(
            //     //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

            //     // ),
            //     // 'fill' => array(
            //     //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
            //     //     'color' => array('rgb' => 'ffff00')
            //     // )
            // );
            // // //กำหนด style ส่วน body//
            // // $styleArray = array(
            // //     'borders' => array(
            // //         'allborders' => array(
            // //             'style' => PHPExcel_Style_Border::BORDER_THIN
            // //         )
            // //     )
            // // );
            // $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);

            // $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(TRUE);

            // // if (isset($dataModel['end_date']) && $dataModel['end_date'] != "") {
            // //     $EndDateTS = strtotime($dataModel['end_date']);
            // //     if ($EndDateTS !== false) {
            // //         date('Y-m-d', $EndDateTS);
            // //     }
            // // }
            // // if (isset($dataModel['date']) && $dataModel['date'] != "") {
            // //     $StarDateTS = strtotime($dataModel['date']);
            // //     if ($StarDateTS !== false) {
            // //         date('Y-m-d', $StarDateTS);
            // //     }
            // // }
            // $objPHPExcel->setActiveSheetIndex(0)

            //     ->setCellValue('A1', 'Group Over SLA ( Exclude weekend )')
            //     ->setCellValue('B1', 'Group Over SLA ( Include weekend )')
            //     ->setCellValue('C1', 'Reporting Code Description')
            //     ->setCellValue('D1', 'factor_incident')
            //     ->setCellValue('E1', 'On time status')
            //     ->setCellValue('F1', 'Group T/T Include weekend range')
            //     ->setCellValue('G1', 'factor_incident_cal')
            //     ->setCellValue('H1', 'factor')
            //     ->setCellValue('I1', 'lane')
            //     ->setCellValue('J1', 'Diff SLA ( Include weekend )')
            //     ->setCellValue('K1', 'Diff SLA ( Exclude weekend )')
            //     ->setCellValue('L1', 'T/T included weekend by_pickup_day')
            //     ->setCellValue('M1', 'T/T excluded weekend')
            //     ->setCellValue('N1', 'T/T included weekend')
            //     ->setCellValue('O1', 'delivery_provider ')
            //     ->setCellValue('P1', 'gateway')
            //     ->setCellValue('Q1', 'shipmonth')
            //     ->setCellValue('R1', 'Quater')
            //     ->setCellValue('S1', 'Year')
            //     ->setCellValue('T1', 'week_number')
            //     ->setCellValue('U1', 'Waybill Number')
            //     ->setCellValue('V1', 'Product Code')
            //     ->setCellValue('W1', 'Product Name')
            //     ->setCellValue('X1', 'Shipper Reference')
            //     ->setCellValue('Y1', 'Shipper Company Name')
            //     ->setCellValue('Z1', 'Destination Country/Territory Name')

            //     ->setCellValue('AA1', 'Destination Country/Territory Area Code')
            //     ->setCellValue('AB1', 'Destination Service Area Code')
            //     ->setCellValue('AC1', 'Number of Days in Customs')
            //     ->setCellValue('AD1', 'Startclock Day of Week')
            //     ->setCellValue('AE1', 'Startclock Date')
            //     ->setCellValue('AF1', 'Stopclock Date')
            //     ->setCellValue('AG1', 'Stopclock Day of Week')
            //     ->setCellValue('AH1', 'Signatory')
            //     ->setCellValue('AI1', 'Elapsed Actual Transit Days')
            //     ->setCellValue('AJ1', 'Reporting Code Description')
            //     ->setCellValue('AK1', 'Reporting Code Category Description')
            //     ->setCellValue('AL1', 'Consignee Company Name')
            //     ->setCellValue('AM1', 'Consignee Address')
            //     ->setCellValue('AN1', 'Consignee Zip')
            //     ->setCellValue('AO1', 'Consignee City ')
            //     ->setCellValue('AP1', 'Billing Account Number')
            //     ->setCellValue('AQ1', 'Manifested Number of Pieces')
            //     ->setCellValue('AR1', 'Billed Weight')
            //     ->setCellValue('AS1', 'Customs Declared Value')
            //     ->setCellValue('AT1', 'Currency Code of Declared Value')
            //     ->setCellValue('AU1', 'Unknown POD Status');

            // // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headbody);
            // // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headbody);
            // // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headbody);
            // // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headbody);
            // // $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($headbody);
            // // $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($headbody);
            // // $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($headbody);
            // // $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($headbody);

            // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($headtable);

            // $objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AN1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AO1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AP1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AQ1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AR1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AS1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AT1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AU1')->applyFromArray($headtable);

            // $start_row = 2;
            // foreach ($data as $row) {

            //     $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('Q' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('R' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('S' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('T' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('U' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('V' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('W' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('X' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('Y' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('Z' . $start_row)->applyFromArray($styleArray);

            //     $objPHPExcel->getActiveSheet()->getStyle('AA' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AB' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AC' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AD' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AE' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AF' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AG' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AH' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AI' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AJ' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AK' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AL' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AM' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AN' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AO' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AP' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AQ' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AR' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AS' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AT' . $start_row)->applyFromArray($styleArray);
            //     $objPHPExcel->getActiveSheet()->getStyle('AU' . $start_row)->applyFromArray($styleArray);

            //     $objPHPExcel->setActiveSheetIndex(0)
            //         // ->setCellValue('A' . $start_row, $row['Group Over SLA ( Exclude weekend )'])
            //         ->setCellValue('A' . $start_row, "Test");
            //         // ->setCellValue('B' . $start_row, $row['Group Over SLA ( Include weekend )'])
            //         // ->setCellValue('C' . $start_row, $row['Reporting Code Description'])
            //         // ->setCellValue('D' . $start_row, $row['factor_incident'])
            //         // ->setCellValue('E' . $start_row, $row['On time status'])
            //         // ->setCellValueExplicit("F". $start_row, $row['Group T/T Include weekend range'], PHPExcel_Cell_DataType::TYPE_STRING)
            //         // ->setCellValue('G' . $start_row, $row['factor_incident_cal'])
            //         // ->setCellValue('H' . $start_row, $row['factor'])
            //         // ->setCellValue('I' . $start_row, $row['lane'])
            //         // ->setCellValue('J' . $start_row, $row['Diff SLA ( Include weekend )'])
            //         // ->setCellValue('K' . $start_row, $row['Diff SLA ( Exclude weekend )'])
            //         // ->setCellValue('L' . $start_row, $row['T/T included weekend by_pickup_day'])
            //         // ->setCellValue('M' . $start_row, $row['T/T excluded weekend'])
            //         // ->setCellValue('N' . $start_row, $row['T/T included weekend'])
            //         // ->setCellValue('O' . $start_row, $row['delivery_provider'])
            //         // ->setCellValue('P' . $start_row, $row['gateway'])
            //         // ->setCellValue('Q' . $start_row, $row['shipmonth'])
            //         // ->setCellValue('R' . $start_row, $row['Quater'])
            //         // ->setCellValue('S' . $start_row, $row['Year'])
            //         // ->setCellValue('T' . $start_row, $row['week_number'])
            //         // ->setCellValue('U' . $start_row, $row['Waybill Number'])
            //         // ->setCellValue('V' . $start_row, $row['Product Code'])
            //         // ->setCellValue('W' . $start_row, $row['Product Name'])
            //         // ->setCellValue('X' . $start_row, $row['Shipper Reference'])
            //         // ->setCellValue('Y' . $start_row, $row['Shipper Company Name'])
            //         // ->setCellValue('Z' . $start_row, $row['Destination Country/Territory Name'])

            //         // ->setCellValue('AA' . $start_row, $row['Destination Country/Territory Area Code'])
            //         // ->setCellValue('AB' . $start_row, $row['Destination Service Area Code'])
            //         // ->setCellValue('AC' . $start_row, $row['Number of Days in Customs'])
            //         // ->setCellValue('AD' . $start_row, $row['Startclock Day of Week'])
            //         // ->setCellValue('AE' . $start_row, $row['Startclock Date'])
            //         // ->setCellValue('AF' . $start_row, $row['Stopclock Date'])
            //         // ->setCellValue('AG' . $start_row, $row['Stopclock Day of Week'])
            //         // ->setCellValue('AH' . $start_row, $row['Signatory'])
            //         // ->setCellValue('AI' . $start_row, $row['Elapsed Actual Transit Days'])
            //         // ->setCellValue('AJ' . $start_row, $row['Reporting Code Description'])
            //         // ->setCellValue('AK' . $start_row, $row['Reporting Code Category Description'])
            //         // ->setCellValue('AL' . $start_row, $row['Consignee Company Name'])
            //         // ->setCellValue('AM' . $start_row, $row['Consignee Address'])
            //         // ->setCellValue('AN' . $start_row, $row['Consignee Zip'])
            //         // ->setCellValue('AO' . $start_row, $row['Consignee City'])
            //         // ->setCellValue('AP' . $start_row, $row['Billing Account Number'])
            //         // ->setCellValue('AQ' . $start_row, $row['Manifested Number of Pieces'])
            //         // ->setCellValue('AR' . $start_row, $row['Billed Weight'])
            //         // ->setCellValue('AS' . $start_row, $row['Customs Declared Value'])
            //         // ->setCellValue('AT' . $start_row, $row['Currency Code of Declared Value'])
            //         // ->setCellValue('AU' . $start_row, $row['Unknown POD Status']);




            //     $start_row++;
            // }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $format = "Excel2007";
            // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $format);
            // $filename = $start.' to '.$end . '.xlsx';

            // $filename = "Data_".$dataPost['Year']."_".$dataPost['week']. date("Y-m-d-H-i-s") . '.xlsx';
            // echo FCPATH; die();
            $filename = 'Report_template.xlsx';

            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);
            //  $objWriter->save('upload/Report_template.xlsx');
            // $objWriter->save($filename); // I guess something with this line here

            $result['message'] = $filename;
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }



    public function Exporttestreport()
    {
        // log_message('error', 'Exportexcel');
        ini_set("memory_limit", "10000M");
        ini_set('MAX_EXECUTION_TIME', 0);
        
        try {
            $this->load->model('ReportOneModel', '', true);

            $this->load->library('MyExcel');
            
            $dataPost = json_decode($this->input->raw_input_stream, true);

            // $data = $this->DashboardModel->Exportreport($dataPost);


            // $objPHPExcel = new PHPExcel();

            // $objPHPExcel->setActiveSheetIndex(0);
            // $objPHPExcel->getActiveSheet()->setTitle("Shipment");



            $objPHPExcel = PHPExcel_IOFactory::load('upload/Report_template.xlsx');
            // $objPHPExcel = PHPExcel_IOFactory::load("./forms/english/cash.xlsx");

            
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', "No")
            ->setCellValue('B2', "Name")
            ->setCellValue('C2', "Email")
            ->setCellValue('D2', "Phone")
            ->setCellValue('E2', "Address");



            // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            // // $format = "Excel2007";
            // // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $format);
            // // $filename = $start.' to '.$end . '.xlsx';

            // // $filename = "Data_".$dataPost['Year']."_".$dataPost['week']. date("Y-m-d-H-i-s") . '.xlsx';
            // // echo FCPATH; die();
            $filename = 'Report_template.xlsx';

            $objWriter->save('upload/Report_template.xlsx');
            // $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);
            //  $objWriter->save('upload/Report_template.xlsx');
            // $objWriter->save($filename); // I guess something with this line here

            $result['message'] = $filename;
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Import extends MY_Controller
{
    // private $tbl_name = 'inventory';
    // private $id = 'id';

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
        
        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
        ini_set('memory_limit', '1024M');
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_PANDASH');
        if (!$role_data['IMFILE']) {
            redirect('Login');
        }
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('import/import_view');
        $this->load->view('share/footer');
    }

    public function upload_file()
    {
        $this->alert_line_notify();
        // print_r($_FILES['file_Pandora_Dashboard']);
        //  print_r('test');die();
        // $uniqid = uniqid();
        // $datapandora['uniqidpandora'] = $uniqid;
        // $datashipment['uniqidshipment'] = $uniqid;
        $date = date('Y-m-d-H-i-s');

        // $this->load->model('HeadpandoraModel', '', true);
        // $this->load->model('HeadShipmentModel', '', true);

        // $uniqidpandora = $this->HeadpandoraModel->insert($datapandora);
        // $uniqidshipment = $this->HeadShipmentModel->insert($datashipment);

        // $this->upload_filepandora($_FILES, $date, $uniqid);
        // $this->upload_fileshipment($_FILES, $date, $uniqid);
        $todayfile = 'Pandora_Dashboard' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/import_excel';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_Pandora_Dashboard']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        

        $status = true;
        if (!$this->upload->do_upload('file_Pandora_Dashboard')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();
            $todayfile = $data['upload_data']['file_name'];
            $this->loadExcelpandoraDashboardToDB($todayfile, $date);
        }
    }


    public function loadExcelpandoraDashboardToDB($file, $date)
    {
        $formattype = 0;
        ini_set('max_execution_time', 300);

        $this->load->library('MyExcel');
        $this->load->model('ImportModel', '', true);
        $this->load->model('ImportStatusCSModel', '', true);
        // print_r($file);
        $objReader = PHPExcel_IOFactory::load('upload/import_excel/' . $file);

        // die();

        $sheetData = $objReader->getActiveSheet()->toArray('', true, false, true);

        // print_r($sheetData);die();

        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        $this->ImportModel->delete_temp_pandora_dashboard();

        // echo 'delete_temp_pandora_dashboard success';die();
        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        // print_r($sheetData[1]['L']);die();
        $format_2_1 = $sheetData[1]['V'];   //format 2
        $format_2_2 = $sheetData[1]['L'];   //format 2

        $format_3_1 = $sheetData[1]['C']; //format 3
        $format_3_2 = $sheetData[1]['L']; //format 3

        $format_4_1 = $sheetData[1]['L']; //format 4
        $format_4_2 = $sheetData[1]['C']; //format 4
        $format_4_3 = $sheetData[1]['X']; //format 4

        $format_5_1 = $sheetData[1]['X']; //format 5

        foreach ($sheetData as $data) {

            try {

                if (
                    $count == 0
                    // and  ("NAME" == trim(strtoupper($data['A'])))

                    and  (strtoupper("Waybill Number") ==  trim(strtoupper($data['A'])))
                    and  (strtoupper("Product Code") ==  trim(strtoupper($data['B'])))
                    and  (strtoupper("Product Group") ==  trim(strtoupper($data['C'])) or (strtoupper("Billing Shipper Reference") ==  trim(strtoupper($data['C']))))
                    // and  (strtoupper("Shipper Reference") ==  trim(strtoupper($data['D'])))
                    and  ((strtoupper("Shipper Reference") ==  trim(strtoupper($data['D']))) or  (strtoupper("Billing Shipper Reference") ==  trim(strtoupper($data['D']))) or (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['D']))))

                    and  (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['E'])) 
                      or (strtoupper("DESTINATION COUNTRY/TERRITORY NAME") ==  trim(strtoupper($data['E'])))
                      or (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['E'])))
                         )

                    and  (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['F'])) 
                    or (strtoupper("DESTINATION COUNTRY/TERRITORY AREA CODE") ==  trim(strtoupper($data['F'])))
                    or (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['F'])))
                         )

                    and  (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['G'])) 
                    or (strtoupper("DESTINATION SERVICE AREA CODE") ==  trim(strtoupper($data['G'])))
                    or (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['G'])))
                    )
                    and  (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['H'])) 
                    or (strtoupper("NUMBER OF DAYS IN CUSTOMS") ==  trim(strtoupper($data['H'])))
                    or (strtoupper(" Number of Days in Customs") ==  trim(strtoupper($data['H'])))
                    )
                    and  (strtoupper("Number of Days in Customs") ==  trim(strtoupper($data['I'])) 
                    or (strtoupper("STARTCLOCK DAY OF WEEK") ==  trim(strtoupper($data['I'])))
                    or (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['I'])))
                    )
                    and  ((strtoupper("Pickup day") ==  trim(strtoupper($data['J']))) 
                    or  (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['J']))) 
                    or (strtoupper("STARTCLOCK DATE") ==  trim(strtoupper($data['J'])))
                    or (strtoupper("Startclock Date") ==  trim(strtoupper($data['J'])))
                    )
                    and  (strtoupper("Startclock Date") ==  trim(strtoupper($data['K'])) 
                    or (strtoupper("STOPCLOCK DATE") ==  trim(strtoupper($data['K'])))
                    or (strtoupper("Stopclock Date") ==  trim(strtoupper($data['K'])))
                    )
                    and  (strtoupper("Stopclock Date") ==  trim(strtoupper($data['L'])) 
                    or (strtoupper("SIGNATORY") ==  trim(strtoupper($data['L']))) 
                    or (strtoupper("Stopclock Day of Week") ==  trim(strtoupper($data['L'])))
                    or (strtoupper("Signatory") ==  trim(strtoupper($data['L'])))
                    )
                    and  (strtoupper("Signatory") ==  trim(strtoupper($data['M'])) 
                    or (strtoupper("ELAPSED ACTUAL TRANSIT DAYS") ==  trim(strtoupper($data['M'])))
                    or (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['M'])))
                    )
                    and  (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['N'])) 
                    or (strtoupper("REPORTING CODE DESCRIPTION") ==  trim(strtoupper($data['N'])))
                    or (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['N'])))
                    )
                    and  (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['O'])) 
                    or (strtoupper("REPORTING CODE CATEGORY DESCRIPTION") ==  trim(strtoupper($data['O'])))
                    or (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['O'])))
                    )
                    and  (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['P'])) 
                    or (strtoupper("CONSIGNEE COMPANY NAME") ==  trim(strtoupper($data['P'])))
                    or (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['P'])))
                    )
                    and  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['Q'])) 
                    or (strtoupper("CONSIGNEE ADDRESS") ==  trim(strtoupper($data['Q'])))
                    or (strtoupper("Consignee Address") ==  trim(strtoupper($data['Q'])))
                    )
                    and  (strtoupper("Consignee Address") ==  trim(strtoupper($data['R'])) 
                    or (strtoupper("CONSIGNEE ZIP") ==  trim(strtoupper($data['R'])))
                    or (strtoupper("Consignee Zip") ==  trim(strtoupper($data['R'])))
                    )
                    and  (strtoupper("Consignee Zip") ==  trim(strtoupper($data['S'])) 
                    or (strtoupper("CONSIGNEE CITY") ==  trim(strtoupper($data['S'])))
                    or (strtoupper("Consignee City") ==  trim(strtoupper($data['S'])))
                    )
                    and  (strtoupper("Consignee City") ==  trim(strtoupper($data['T'])) 
                    or (strtoupper("MANIFESTED NUMBER OF PIECES") ==  trim(strtoupper($data['T'])))
                    or (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['T'])))
                    )
                    and  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U'])) 
                    or (strtoupper("CURRENCY CODE OF DECLARED VALUE") ==  trim(strtoupper($data['U'])))
                    or (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['U'])))
                    )
                    and  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V']))) 
                    or (strtoupper("Billing Account Number") ==  trim(strtoupper($data['V'])) 
                    or (strtoupper("BILLING ACCOUNT NUMBER") ==  trim(strtoupper($data['V'])))
                    or (strtoupper("Billing Account Number") ==  trim(strtoupper($data['V'])))
                    )
                    and  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W']))) 
                    or (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['W'])) 
                    or (strtoupper("CUSTOMS DECLARED VALUE") ==  trim(strtoupper($data['W'])))
                    or (strtoupper("Stopclock Day of Week") ==  trim(strtoupper($data['W'])))
                    )
                    
                    // and  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V'])))
                    // and  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W'])))

                    and  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X'])) 
                    or (strtoupper("OPERATIONS CALCULATED WEIGHT") ==  trim(strtoupper($data['X'])))
                    or (strtoupper("Startclock Week") ==  trim(strtoupper($data['X'])))
                    )

                ) {
                    // echo "found data <br/>";
                    // echo'case 1';
                    $fuondFormat = 1;

                    // $this->JobTypeModel->dropJobType();
                } else if ($count == 0) {
                    // print_r($data);die();
                    // echo'SCAC';
                    // echo trim(strtoupper($data['E']));
                    $ErrorMassage = 'Your file has an out-of-format format in the column ';
                    $FlagFormat = true;

                    $ErrorMassage .=  (strtoupper("Waybill Number") ==  trim(strtoupper($data['A']))) ? "" : " A ";
                    $ErrorMassage .=  (strtoupper("Product Code") ==  trim(strtoupper($data['B']))) ? "" : " B ";
                    $ErrorMassage .=  (strtoupper("Product Group") ==  trim(strtoupper($data['C'])) or (strtoupper("Billing Shipper Reference") ==  trim(strtoupper($data['C'])))) ? "" : " C ";
                    $ErrorMassage .=  ((strtoupper("Shipper Reference") ==  trim(strtoupper($data['D']))) or  (strtoupper("Billing Shipper Reference") ==  trim(strtoupper($data['D']))) or (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['D'])))) ? "" : " D ";

                    $ErrorMassage .=  (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['E']))  or (strtoupper("DESTINATION COUNTRY/TERRITORY NAME") ==  trim(strtoupper($data['E']))) or (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['E'])))) ? "" : " E ";
                    $ErrorMassage .=   (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['F'])) or (strtoupper("DESTINATION COUNTRY/TERRITORY AREA CODE") ==  trim(strtoupper($data['F']))) or (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['F'])))) ? "" : " E ";

                    $ErrorMassage .=  (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['G']))  or (strtoupper("DESTINATION SERVICE AREA CODE") ==  trim(strtoupper($data['G']))) or (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['G'])))) ? "" : " G ";
                    $ErrorMassage .=  (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['H']))  or (strtoupper("NUMBER OF DAYS IN CUSTOMS") ==  trim(strtoupper($data['H']))) or (strtoupper(" Number of Days in Customs") ==  trim(strtoupper($data['H'])))) ? "" : " H ";
                    $ErrorMassage .=  (strtoupper("Number of Days in Customs") ==  trim(strtoupper($data['I']))  or (strtoupper("STARTCLOCK DAY OF WEEK") ==  trim(strtoupper($data['I']))) or (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['I'])))) ? "" : " I ";
                    // $ErrorMassage .=  (strtoupper("Pickup day") ==  trim(strtoupper($data['J']))) ? "" : " J ";
                    $ErrorMassage .=  ((strtoupper("Pickup day") ==  trim(strtoupper($data['J'])))  or  (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['J'])))  or (strtoupper("STARTCLOCK DATE") ==  trim(strtoupper($data['J']))) or (strtoupper("Startclock Date") ==  trim(strtoupper($data['J'])))) ? "" : " J ";
                    $ErrorMassage .=  (strtoupper("Startclock Date") ==  trim(strtoupper($data['K']))  or (strtoupper("STOPCLOCK DATE") ==  trim(strtoupper($data['K']))) or (strtoupper("Stopclock Date") ==  trim(strtoupper($data['K'])))) ? "" : " K ";
                    $ErrorMassage .=  (strtoupper("Stopclock Date") ==  trim(strtoupper($data['L']))  or (strtoupper("SIGNATORY") ==  trim(strtoupper($data['L'])))  or (strtoupper("Stopclock Day of Week") ==  trim(strtoupper($data['L']))) or (strtoupper("Signatory") ==  trim(strtoupper($data['L'])))) ? "" : " L ";
                    $ErrorMassage .=  (strtoupper("Signatory") ==  trim(strtoupper($data['M']))  or (strtoupper("ELAPSED ACTUAL TRANSIT DAYS") ==  trim(strtoupper($data['M']))) or (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['M'])))) ? "" : " M ";
                    $ErrorMassage .=  (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['N']))  or (strtoupper("REPORTING CODE DESCRIPTION") ==  trim(strtoupper($data['N']))) or (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['N'])))) ? "" : " N ";
                    $ErrorMassage .=  (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['O']))  or (strtoupper("REPORTING CODE CATEGORY DESCRIPTION") ==  trim(strtoupper($data['O']))) or (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['O'])))) ? "" : " O ";
                    $ErrorMassage .=  (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['P']))  or (strtoupper("CONSIGNEE COMPANY NAME") ==  trim(strtoupper($data['P']))) or (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['P'])))) ? "" : " P ";
                    $ErrorMassage .=  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['Q']))  or (strtoupper("CONSIGNEE ADDRESS") ==  trim(strtoupper($data['Q']))) or (strtoupper("Consignee Address") ==  trim(strtoupper($data['Q'])))) ? "" : " Q ";
                    $ErrorMassage .=  (strtoupper("Consignee Address") ==  trim(strtoupper($data['R']))  or (strtoupper("CONSIGNEE ZIP") ==  trim(strtoupper($data['R']))) or (strtoupper("Consignee Zip") ==  trim(strtoupper($data['R'])))) ? "" : " R ";
                    $ErrorMassage .=  (strtoupper("Consignee Zip") ==  trim(strtoupper($data['S']))  or (strtoupper("CONSIGNEE CITY") ==  trim(strtoupper($data['S']))) or (strtoupper("Consignee City") ==  trim(strtoupper($data['S'])))) ? "" : " S ";
                    $ErrorMassage .=  (strtoupper("Consignee City") ==  trim(strtoupper($data['T']))  or (strtoupper("MANIFESTED NUMBER OF PIECES") ==  trim(strtoupper($data['T']))) or (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['T'])))) ? "" : " T ";
                    $ErrorMassage .=  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U']))  or (strtoupper("CURRENCY CODE OF DECLARED VALUE") ==  trim(strtoupper($data['U']))) or (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['U'])))) ? "" : " U ";
                    $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V'])))  or (strtoupper("Billing Account Number") ==  trim(strtoupper($data['V']))  or (strtoupper("BILLING ACCOUNT NUMBER") ==  trim(strtoupper($data['V']))) or (strtoupper("Billing Account Number") ==  trim(strtoupper($data['V'])))) ? "" : " V "; 
                    $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W'])))  or (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['W']))  or (strtoupper("CUSTOMS DECLARED VALUE") ==  trim(strtoupper($data['W']))) or (strtoupper("Stopclock Day of Week") ==  trim(strtoupper($data['W'])))) ? "" : " W ";

                    
                    // $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                    // $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W']))) ? "" : " W ";

                    $ErrorMassage .=  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X']))  or (strtoupper("OPERATIONS CALCULATED WEIGHT") ==  trim(strtoupper($data['X']))) or (strtoupper("Startclock Week") ==  trim(strtoupper($data['X'])))) ? "" : " X ";

                    // echo ("LIMIT" ==  trim(strtoupper($data['B']))) ? "T" : "F";
                    // echo "Miss data format ";
                    break;
                    // exit();
                }

                // print_r($data['V']);die();
                if($format_2_1 == "Currency Code of Declared Value" && $format_2_2 == "Stopclock Date"){
                    // print_r(trim(strtoupper($data['V'])));die();
                    // print_r($row_count);
                    print_r("format_2");

                     if ($row_count > 1) {
                        // print_r("CURRENCY");die();

                        // $data_model['title_excel'] = $sheetData[2]['B'];

                        $data_model['Waybill Number'] = isset($data['A']) ? $data['A'] : 0;
                        $data_model['Product Code'] = isset($data['B']) ? $data['B'] : 0;
                        $data_model['Product Name'] = isset($data['C']) ? $data['C'] : 0;
                        $data_model['Shipper Reference'] = isset($data['D']) ? $data['D'] : 0;
                        $data_model['Shipper Company Name'] = isset($data['E']) ? $data['E'] : 0;
                        $data_model['Destination Country/Territory Name'] = isset($data['F']) ? $data['F'] : 0;

                        // $data_model['event_date'] =  $event_date;
                        $data_model['Destination Country/Territory Area Code'] = isset($data['G']) ? $data['G'] : 0;
                        $data_model['Destination Service Area Code'] = isset($data['H']) ? $data['H'] : 0;
                        $data_model['Number of Days in Customs'] = isset($data['I']) ? $data['I'] : 0;
                        $data_model['Startclock Day of Week'] = isset($data['J']) ? $data['J'] : 0;
                        // $data_model['Startclock Date'] = isset($data['K']) ? $data['K'] : 0;
                        $data_model['Startclock Date'] = isset($data['K'])  && $data['K'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['K'])->format('Y-m-d') : 0;

                        // $data_model['Stopclock Date'] = isset($data['L']) ? $data['L'] : 0;
                        $data_model['Stopclock Date'] = isset($data['L'])  && $data['L'] != '' && $data['K'] != 'UNKNOWN' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['L'])->format('Y-m-d') : '';

                        $data_model['Stopclock Day of Week'] = "";
                        $data_model['Signatory'] = isset($data['M']) ? $data['M'] : 0;
                        $data_model['Elapsed Actual Transit Days'] = isset($data['N']) ? $data['N'] : 0;
                        // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                        $data_model['Reporting Code Description'] = isset($data['O']) ? $data['O'] : 0;
                        $data_model['Reporting Code Category Description'] = isset($data['P']) ? $data['P'] : 0;
                        $data_model['Consignee Company Name'] = isset($data['Q']) ? $data['Q'] : 0;
                        $data_model['Consignee Address'] = isset($data['R']) ? $data['R'] : 0;
                        $data_model['Consignee Zip'] = isset($data['S']) ? $data['S'] : 0;
                        $data_model['Consignee City'] = isset($data['T']) ? $data['T'] : 0;
                        $data_model['Manifested Number of Pieces'] = isset($data['U']) ? $data['U'] : 0;
                        $data_model['Billed Weight'] = "";
                        $data_model['Customs Declared Value'] = isset($data['X']) ? $data['X'] : 0;
                        $data_model['Currency Code of Declared Value'] = isset($data['V']) ? $data['V'] : 0;
                        $data_model['Billing Account Number'] = isset($data['W']) ? $data['W'] : 0;

        
                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;

                        // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                        //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                        // }

                        $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);

                    }
                    $row_count++;
                    $count++; 

                } else if($format_3_1 == "Billing Shipper Reference" && $format_3_2 == "SIGNATORY"){
                    if ($row_count > 1) {

                        print_r("format_3");
                        // print_r(trim(strtoupper($data['V'])));die();
                        // $data_model['title_excel'] = $sheetData[2]['B'];
                        // print_r("Billing");

                        $data_model['Waybill Number'] = isset($data['A']) ? $data['A'] : 0; //Waybill Number
                        $data_model['Product Code'] = isset($data['B']) ? $data['B'] : 0; //Product Code
                        $data_model['Product Name'] = "";
                        $data_model['Shipper Reference'] = isset($data['C']) ? $data['C'] : 0; //Billing Shipper Reference
                        $data_model['Shipper Company Name'] = isset($data['D']) ? $data['D'] : 0; //Shipper Company Name
                        $data_model['Destination Country/Territory Name'] = isset($data['E']) ? $data['E'] : 0; //Destination Country/Territory Name

                        // $data_model['event_date'] =  $event_date;
                        $data_model['Destination Country/Territory Area Code'] = isset($data['F']) ? $data['F'] : 0; //Destination Country/Territory Area Code
                        $data_model['Destination Service Area Code'] = isset($data['G']) ? $data['G'] : 0;
                        $data_model['Number of Days in Customs'] = isset($data['H']) ? $data['H'] : 0;
                        $data_model['Startclock Day of Week'] = isset($data['I']) ? $data['I'] : 0;
                        // $data_model['Startclock Date'] = isset($data['K']) ? $data['K'] : 0;
                        $data_model['Startclock Date'] = isset($data['J'])  && $data['J'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['J'])->format('Y-m-d') : 0;

                        // $data_model['Stopclock Date'] = isset($data['L']) ? $data['L'] : 0;
                        $data_model['Stopclock Date'] = isset($data['K'])  && $data['K'] != '' && $data['K'] != 'UNKNOWN' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['K'])->format('Y-m-d') : '';

                        $data_model['Stopclock Day of Week'] = "";
                        $data_model['Signatory'] = isset($data['L']) ? $data['L'] : 0;
                        $data_model['Elapsed Actual Transit Days'] = isset($data['M']) ? $data['M'] : 0;
                        // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                        $data_model['Reporting Code Description'] = isset($data['N']) ? $data['N'] : 0;
                        $data_model['Reporting Code Category Description'] = isset($data['O']) ? $data['O'] : 0;
                        $data_model['Consignee Company Name'] = isset($data['P']) ? $data['P'] : 0;
                        $data_model['Consignee Address'] = isset($data['Q']) ? $data['Q'] : 0;
                        $data_model['Consignee Zip'] = isset($data['R']) ? $data['R'] : 0;
                        $data_model['Consignee City'] = isset($data['S']) ? $data['S'] : 0;
                        $data_model['Manifested Number of Pieces'] = isset($data['T']) ? $data['T'] : 0;
                        $data_model['Billed Weight'] = "";
                        $data_model['Currency Code of Declared Value'] = isset($data['U']) ? $data['U'] : 0;
                        $data_model['Billing Account Number'] = isset($data['V']) ? $data['V'] : 0;
                        $data_model['Customs Declared Value'] = isset($data['W']) ? $data['W'] : 0;

        
                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;

                        // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                        //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                        // }

                        $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);

                        
                    }
                    $row_count++;
                    $count++; 
                } else if ($format_4_1 == "Stopclock Day of Week" && $format_4_2 == "Billing Shipper Reference" && $format_4_3 == "Customs Declared Value"){ //เพิ่มมาวันที่ 26/06/23

                    print_r("format_4");

                    if ($row_count > 1) {
                        $data_model['Waybill Number'] = isset($data['A']) ? $data['A'] : 0; //Waybill Number
                        $data_model['Product Code'] = isset($data['B']) ? $data['B'] : 0; //Product Code
                        $data_model['Product Name'] = "";
                        $data_model['Shipper Reference'] = isset($data['C']) ? $data['C'] : 0; //Billing Shipper Reference
                        $data_model['Shipper Company Name'] = isset($data['D']) ? $data['D'] : 0; //Shipper Company Name
                        $data_model['Destination Country/Territory Name'] = isset($data['E']) ? $data['E'] : 0; //Destination Country/Territory Name

                        // $data_model['event_date'] =  $event_date;
                        $data_model['Destination Country/Territory Area Code'] = isset($data['F']) ? $data['F'] : 0; //Destination Country/Territory Area Code
                        $data_model['Destination Service Area Code'] = isset($data['G']) ? $data['G'] : 0;
                        $data_model['Number of Days in Customs'] = isset($data['H']) ? $data['H'] : 0;
                        $data_model['Startclock Day of Week'] = isset($data['I']) ? $data['I'] : 0;
                        // $data_model['Startclock Date'] = isset($data['K']) ? $data['K'] : 0;
                        $data_model['Startclock Date'] = isset($data['J'])  && $data['J'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['J'])->format('Y-m-d') : 0;

                        // $data_model['Stopclock Date'] = isset($data['L']) ? $data['L'] : 0;
                        
                        if (DateTime::createFromFormat('Y-m-d', $data['K']) !== false) {
                            $data_model['Stopclock Date'] = isset($data['K']) ? $data['K'] : 0;
                            // print_r($data_model['Stopclock Date']);die();
                          }else{
                            $data_model['Stopclock Date'] = isset($data['K'])  && $data['K'] != '' && $data['K'] != 'UNKNOWN' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['K'])->format('Y-m-d') : '';
                          }

                        // $data_model['Stopclock Date'] = isset($data['K'])  && $data['K'] != '' && $data['K'] != 'UNKNOWN' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['K'])->format('Y-m-d') : '';
                        
                        $data_model['Stopclock Day of Week'] = isset($data['L']) ? $data['L'] : 0;
                        $data_model['Signatory'] = isset($data['M']) ? $data['M'] : 0;
                        $data_model['Elapsed Actual Transit Days'] = isset($data['N']) ? $data['N'] : 0;
                        // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                        $data_model['Reporting Code Description'] = isset($data['O']) ? $data['O'] : 0;
                        $data_model['Reporting Code Category Description'] = isset($data['P']) ? $data['P'] : 0;
                        $data_model['Consignee Company Name'] = isset($data['Q']) ? $data['Q'] : 0;
                        $data_model['Consignee Address'] = isset($data['R']) ? $data['R'] : 0;
                        $data_model['Consignee Zip'] = isset($data['S']) ? $data['S'] : 0;
                        $data_model['Consignee City'] = isset($data['T']) ? $data['T'] : 0;
                        $data_model['Manifested Number of Pieces'] = isset($data['U']) ? $data['U'] : 0;
                        $data_model['Billed Weight'] = "";
                        $data_model['Currency Code of Declared Value'] = isset($data['V']) ? $data['V'] : 0;
                        $data_model['Billing Account Number'] = isset($data['W']) ? $data['W'] : 0;
                        $data_model['Customs Declared Value'] = isset($data['X']) ? $data['X'] : 0;

        
                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;

                        // print_r($data_model);
                        // die();
                        // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                        //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                        // }

                        $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                    }
                    $row_count++;
                    $count++; 
                } else if ($format_5_1 == "Startclock Week"){ //เพิ่มมาวันที่ 20/01/25

                    print_r("format_5");

                    if ($row_count > 1) {
                        $data_model['Waybill Number'] = isset($data['A']) ? $data['A'] : 0; //Waybill Number
                        $data_model['Product Code'] = isset($data['B']) ? $data['B'] : 0; //Product Code
                        $data_model['Product Name'] = "";
                        $data_model['Shipper Reference'] = isset($data['C']) ? $data['C'] : 0; //Billing Shipper Reference
                        $data_model['Shipper Company Name'] = isset($data['D']) ? $data['D'] : 0; //Shipper Company Name
                        $data_model['Destination Country/Territory Name'] = isset($data['E']) ? $data['E'] : 0; //Destination Country/Territory Name

                        $data_model['Destination Country/Territory Area Code'] = isset($data['F']) ? $data['F'] : 0; //Destination Country/Territory Area Code
                        $data_model['Destination Service Area Code'] = isset($data['G']) ? $data['G'] : 0;
                        $data_model['Number of Days in Customs'] = isset($data['H']) ? $data['H'] : 0;
                        $data_model['Startclock Day of Week'] = isset($data['I']) ? $data['I'] : 0;
                        $data_model['Startclock Date'] = isset($data['J'])  && $data['J'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['J'])->format('Y-m-d') : 0;

                        if (DateTime::createFromFormat('Y-m-d', $data['K']) !== false) {
                            $data_model['Stopclock Date'] = isset($data['K']) ? $data['K'] : 0;
                            // print_r($data_model['Stopclock Date']);die();
                          }else{
                            $data_model['Stopclock Date'] = isset($data['K'])  && $data['K'] != '' && $data['K'] != 'UNKNOWN' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['K'])->format('Y-m-d') : '';
                          }
                        
                        $data_model['Signatory'] = isset($data['L']) ? $data['L'] : 0;
                        $data_model['Elapsed Actual Transit Days'] = isset($data['M']) ? $data['M'] : 0;
                        $data_model['Reporting Code Description'] = isset($data['N']) ? $data['N'] : 0;
                        $data_model['Reporting Code Category Description'] = isset($data['O']) ? $data['O'] : 0;
                        $data_model['Consignee Company Name'] = isset($data['P']) ? $data['P'] : 0;
                        $data_model['Consignee Address'] = isset($data['Q']) ? $data['Q'] : 0;
                        $data_model['Consignee Zip'] = isset($data['R']) ? $data['R'] : 0;
                        $data_model['Consignee City'] = isset($data['S']) ? $data['S'] : 0;
                        $data_model['Manifested Number of Pieces'] = isset($data['T']) ? $data['T'] : 0;
                        $data_model['Currency Code of Declared Value'] = isset($data['U']) ? $data['U'] : 0;
                        $data_model['Billed Weight'] = "";
                        $data_model['Billing Account Number'] = isset($data['V']) ? $data['V'] : 0;
                        $data_model['Stopclock Day of Week'] = isset($data['W']) ? $data['W'] : 0;
                        $data_model['Startclock Week'] = isset($data['X']) ? $data['X'] : 0; //Startclock Week เพิ่มฟิลด์มาใหม่
                        $data_model['Customs Declared Value'] = isset($data['Y']) ? $data['Y'] : 0;

        
                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;

                        // print_r($data_model);
                        // die();

                        $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                    }
                    $row_count++;
                    $count++; 
                } else {
                    if ($row_count > 1) {

                        // print_r(trim(strtoupper($data['V'])));die();
                        // $data_model['title_excel'] = $sheetData[2]['B'];
                        // print_r("Billing");

                        $data_model['Waybill Number'] = isset($data['A']) ? $data['A'] : 0;
                        $data_model['Product Code'] = isset($data['B']) ? $data['B'] : 0;
                        $data_model['Product Name'] = isset($data['C']) ? $data['C'] : 0;
                        $data_model['Shipper Reference'] = isset($data['D']) ? $data['D'] : 0;
                        $data_model['Shipper Company Name'] = isset($data['E']) ? $data['E'] : 0;
                        $data_model['Destination Country/Territory Name'] = isset($data['F']) ? $data['F'] : 0;

                        // $data_model['event_date'] =  $event_date;
                        $data_model['Destination Country/Territory Area Code'] = isset($data['G']) ? $data['G'] : 0;
                        $data_model['Destination Service Area Code'] = isset($data['H']) ? $data['H'] : 0;
                        $data_model['Number of Days in Customs'] = isset($data['I']) ? $data['I'] : 0;
                        $data_model['Startclock Day of Week'] = isset($data['J']) ? $data['J'] : 0;
                        // $data_model['Startclock Date'] = isset($data['K']) ? $data['K'] : 0;
                        $data_model['Startclock Date'] = isset($data['K'])  && $data['K'] != '' && $data['K'] != 'UNKNOWN' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['K'])->format('Y-m-d') : '';

                        // $data_model['Stopclock Date'] = isset($data['L']) ? $data['L'] : 0;
                        $data_model['Stopclock Date'] = isset($data['L'])  && $data['L'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['L'])->format('Y-m-d') : 0;

                        $data_model['Stopclock Day of Week'] = "";
                        $data_model['Signatory'] = isset($data['M']) ? $data['M'] : 0;
                        $data_model['Elapsed Actual Transit Days'] = isset($data['N']) ? $data['N'] : 0;
                        // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                        $data_model['Reporting Code Description'] = isset($data['O']) ? $data['O'] : 0;
                        $data_model['Reporting Code Category Description'] = isset($data['P']) ? $data['P'] : 0;
                        $data_model['Consignee Company Name'] = isset($data['Q']) ? $data['Q'] : 0;
                        $data_model['Consignee Address'] = isset($data['R']) ? $data['R'] : 0;
                        $data_model['Consignee Zip'] = isset($data['S']) ? $data['S'] : 0;
                        $data_model['Consignee City'] = isset($data['T']) ? $data['T'] : 0;
                        $data_model['Manifested Number of Pieces'] = isset($data['U']) ? $data['U'] : 0;
                        $data_model['Billed Weight'] = "";
                        $data_model['Customs Declared Value'] = isset($data['X']) ? $data['X'] : 0;
                        $data_model['Currency Code of Declared Value'] = isset($data['W']) ? $data['W'] : 0;
                        $data_model['Billing Account Number'] = isset($data['V']) ? $data['V'] : 0;

        
                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;

                        // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                        //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                        // }

                        $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);

                        
                    }
                    $row_count++;
                    $count++; 
                }

               


                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            // $row_count++;
            // $count++; 
            // $row_count=$row_count+1;
            // die();
        }

        $checkpoint_shipment_status_cs = $this->ImportStatusCSModel->checkpoint_shipment_status_cs();

        $updatename_shipment_status_cs = $this->ImportStatusCSModel->updatename_shipment_status_cs();

        // $check_duplicate = $this->ImportModel->check_duplicate();

        // print_r( $check_duplicate);

        if ($FlagFormat == true) {
            $result['status'] = false;
            $result['message'] =  $ErrorMassage;
        } else if (count($check_duplicate) > 0) {
            $result['status'] = false;
            $result['message'] = 'Duplicate data found from file. That has already been uploaded';
            // $this->ImportModel->DeleteTableTemp();
        } else {
            $dataimpot['file_name'] =  $file;
            $dataimpot['type'] =  'Excel Upload';
            $dataimpot['upload_date'] =  $date;
            $this->ImportModel->delete_t_temp_shipment();
            $this->ImportModel->insertto_t_shipment();
            // $this->ImportModel->insertto_customer($file, $date);
            $nResult = $this->ImportModel->insert_log_shipment($dataimpot);
            // $this->ImportModel->DeleteTableTemp();
            $this->ImportModel->update_t_shipment_set_consignee_zip();
            $this->ImportModel->update_consignee_zip();
            $result['status'] = true;
            $result['message'] = $this->lang->line('savesuccess');
        };
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }




    public function getdailyModelList()
    {
        try {
            $this->load->model('ImportModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ImportModel->getdailyModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            $result['totalRecords'] = $this->ImportModel->getTotal($dataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function alert_line_notify()
    {
        // call line api
        $Token = 'UeMJwWbMswxUmnEA08yzzgGhWAuszabSpVT8BtUHvpr';
        $message = 'ImportWeekly'.' '. date('Y-m-d');
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . ''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
        } else {
            $result_ = json_decode($result, true);
        }
        curl_close($chOne);
    }

    public function deleteImport()
    {
        try {
            $this->load->model('ImportModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);die();
            $dataModel = isset($dataPost['file_name']) ? $dataPost['file_name'] : '';
            // print_r($dataModel);die();
            $result['status'] = true;
            $result['message'] = $this->ImportModel->deleteImport($dataModel);
            $result['ShowMessage'] = 'The deletion was successful.';
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

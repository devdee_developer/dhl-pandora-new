<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use  setasign\Fpdi;
class User extends CI_Controller {
	public function __construct() {
        parent::__construct();
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('user/user_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function adduser() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('UserModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			 
	  		$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0; 
			$data['user'] =  isset($dataPost['user'])?$dataPost['user']: "";
			$data['password'] =  isset($dataPost['password'])?md5($dataPost['password']): "";
			$data['update_user'] = $this->session->userdata('user_name');
			$data['update_date'] = date("Y-m-d H:i:s");
			 
	  		// load model 
    		if ($data['id'] == 0) {  
				$data['create_user'] = $this->session->userdata('user_name');
				$data['create_date'] = date("Y-m-d H:i:s");
    			$nResult = $this->UserModel->insert($data);
		    }
		    else {  
		      	$nResult = $this->UserModel->update($data['id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteuser(){
		try{
			$this->load->model('UserModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->UserModel->deleteUsername($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getuserModel(){
	 
		try{
			$this->load->model('UserModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->UserModel->getuserNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->UserModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getUserModelList(){
	 
		try{
			$this->load->model('UserModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->UserModel->getuserNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->UserModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->UserModel->getUserModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getuserComboList(){
	 
		try{ 
			$this->load->model('UserModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->UserModel->getuserComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	 
}

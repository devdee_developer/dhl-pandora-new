<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ImportStatusCS extends MY_Controller
{
    // private $tbl_name = 'inventory';
    // private $id = 'id';

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }

        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
        ini_set('memory_limit', '1024M');
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_PANDASH');
        if (!$role_data['IMFILE']) {
            redirect('Login');
        }
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('import/importStatusCS_view');
        $this->load->view('share/footer');
    }

    public function upload_file()
    {
        $this->alert_line_notify();
        // print_r($_FILES['file_Pandora_Dashboard']);
        //  print_r('test');die();
        // $uniqid = uniqid();
        // $datapandora['uniqidpandora'] = $uniqid;
        // $datashipment['uniqidshipment'] = $uniqid;
        $date = date('Y-m-d-H-i-s');

        // $this->load->model('HeadpandoraModel', '', true);
        // $this->load->model('HeadShipmentModel', '', true);

        // $uniqidpandora = $this->HeadpandoraModel->insert($datapandora);
        // $uniqidshipment = $this->HeadShipmentModel->insert($datashipment);

        // $this->upload_filepandora($_FILES, $date, $uniqid);
        // $this->upload_fileshipment($_FILES, $date, $uniqid);
        $todayfile = 'Pandora_Dashboard' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/import_excel';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_Pandora_Dashboard']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);



        $status = true;
        if (!$this->upload->do_upload('file_Pandora_Dashboard')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();
            $todayfile = $data['upload_data']['file_name'];
            $this->loadExcelpandoraDashboardToDB($todayfile, $date);
        }
    }


    public function loadExcelpandoraDashboardToDB($file, $date)
    {
        $formattype = 0;
        ini_set('max_execution_time', 300);

        $this->load->library('MyExcel');
        $this->load->model('ImportStatusCSModel', '', true);
        // print_r($file);
        $objReader = PHPExcel_IOFactory::load('upload/import_excel/' . $file);

        // die();
        $sheetData = $objReader->setActiveSheetIndex(0)->toArray('', true, false, true);

        // $sheetData = $objReader->getsheet(0);
        // $sheetData = $objReader->getActiveSheet()->toArray('', true, false, true);

        // print_r($sheetData);
        // die();

        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        $this->ImportStatusCSModel->delete_temp_pandora_dashboard();

        // echo 'delete_temp_pandora_dashboard success';die();
        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        // print_r($sheetData);die();
        $format_2_1 = $sheetData[1]['J'];   //format 2
        $format_2_2 = $sheetData[1]['K'];   //format 2
        // if ($sheetData[1]['L'] = " ") {
        //     $test = "test";
        // }
        // print_r($test);die();



        foreach ($sheetData as $data) {

            try {

                if (
                    $count == 0

                    // and  ("Date_Pickup" == trim(strtoupper($data['A'])))

                    // and  (strtoupper("Content.MAWB") ==  trim(strtoupper($data['A'])))
                    // and  (strtoupper("No.") ==  trim(strtoupper($data['B'])))
                    // and  (strtoupper("Waybill") ==  trim(strtoupper($data['C'])))
                    // and  (strtoupper("Consignee") ==  trim(strtoupper($data['D'])))
                    // and  (strtoupper("Status") ==  trim(strtoupper($data['E'])))
                    // and  (strtoupper("Receiver name") ==  trim(strtoupper($data['F'])))
                    // and  (strtoupper("Delivery date") ==  trim(strtoupper($data['G'])))
                    // and  (strtoupper("PC") ==  trim(strtoupper($data['H'])))
                    // and  (strtoupper("Date") ==  trim(strtoupper($data['I'])))
                    // and  (strtoupper("Dest") ==  trim(strtoupper($data['J'])))
                    // and  (strtoupper("Factor Incident") ==  trim(strtoupper($data['K'])))
                    // and  (strtoupper("Factor") ==  trim(strtoupper($data['L'])))

                    and  (strtoupper("Content.MAWB") ==  trim(strtoupper($data['A'])) or (strtoupper("Mother Waybill Number") ==  trim(strtoupper($data['A']))))

                    // and  ((strtoupper("Content.MAWB") ==  trim(strtoupper($data['A']))) or  (strtoupper("Mother Waybill Number") ==  trim(strtoupper($data['A']))))
                    and  ((strtoupper("Content.No.") ==  trim(strtoupper($data['B']))) or  (strtoupper("No.") ==  trim(strtoupper($data['B']))))
                    and  ((strtoupper("Content.Waybill") ==  trim(strtoupper($data['C']))) or  (strtoupper("Waybill Number") ==  trim(strtoupper($data['C']))))
                    and  ((strtoupper("Content.Consignee") ==  trim(strtoupper($data['D']))) or  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['D']))))
                    and  ((strtoupper("Content.Receiver name") ==  trim(strtoupper($data['E']))) or  (strtoupper("Status") ==  trim(strtoupper($data['E']))))
                    and  ((strtoupper("Content.Status") ==  trim(strtoupper($data['F']))) or  (strtoupper("Receiver name") ==  trim(strtoupper($data['F']))))
                    and  ((strtoupper("Content.Delivery date") ==  trim(strtoupper($data['G']))) or  (strtoupper("Delivery date") ==  trim(strtoupper($data['G']))))
                    and  ((strtoupper("Content.PC") ==  trim(strtoupper($data['H']))) or  (strtoupper("Number of Pieces") ==  trim(strtoupper($data['H']))))
                    and  ((strtoupper("Content.Date") ==  trim(strtoupper($data['I']))) or  (strtoupper("Pickup date") ==  trim(strtoupper($data['I']))))
                    and  ((strtoupper("Content.Dest") ==  trim(strtoupper($data['J']))) or  (strtoupper("Destination Country") ==  trim(strtoupper($data['J']))))
                    and  ((strtoupper("Content.Factor Incident") ==  trim(strtoupper($data['K']))) or  (strtoupper("Destination Port") ==  trim(strtoupper($data['K']))))
                    or  (strtoupper("Content.Factor") ==  trim(strtoupper($data['L'])))


                    // and  ((strtoupper("Content.MAWB") ==  trim(strtoupper($data['A']))) or  (strtoupper("MAWB") ==  trim(strtoupper($data['A']))))
                    // and  ((strtoupper("Content.No") ==  trim(strtoupper($data['B']))) or  (strtoupper("No.") ==  trim(strtoupper($data['B']))))
                    // and  ((strtoupper("Content.Waybill") ==  trim(strtoupper($data['C']))) or  (strtoupper("Waybill") ==  trim(strtoupper($data['C']))))
                    // and  ((strtoupper("Content.Consignee") ==  trim(strtoupper($data['D']))) or  (strtoupper("Consignee") ==  trim(strtoupper($data['D']))))
                    // and  ((strtoupper("Content.Status") ==  trim(strtoupper($data['E']))) or  (strtoupper("Status") ==  trim(strtoupper($data['E']))))
                    // and  ((strtoupper("Content.Receiver name") ==  trim(strtoupper($data['F']))) or  (strtoupper("Receiver name") ==  trim(strtoupper($data['F']))))
                    // and  ((strtoupper("Content.Delivery date") ==  trim(strtoupper($data['G']))) or  (strtoupper("Delivery date") ==  trim(strtoupper($data['G']))))
                    // and  ((strtoupper("Content.PC") ==  trim(strtoupper($data['H']))) or  (strtoupper("PC") ==  trim(strtoupper($data['H']))))
                    // and  ((strtoupper("Content.Date") ==  trim(strtoupper($data['I']))) or  (strtoupper("Date") ==  trim(strtoupper($data['I']))))
                    // and  ((strtoupper("Content.Dest") ==  trim(strtoupper($data['J']))) or  (strtoupper("Dest") ==  trim(strtoupper($data['J']))))
                    // and  ((strtoupper("Content.Factor Incident") ==  trim(strtoupper($data['K']))) or  (strtoupper("Factor Incident") ==  trim(strtoupper($data['K']))))
                    // and  ((strtoupper("Content.Factor") ==  trim(strtoupper($data['L']))) or  (strtoupper("Factor") ==  trim(strtoupper($data['L']))))

                ) {
                    // echo "found data <br/>";
                    // echo'case 1';
                    $fuondFormat = 1;

                    // $this->JobTypeModel->dropJobType();
                } else if ($count == 0) {
                    
                    // echo'SCAC';
                    // echo trim(strtoupper($data['E']));
                    $ErrorMassage = 'Your file has an out-of-format format in the column ';
                    $FlagFormat = true;

                    // $ErrorMassage .=  (strtoupper("Content.MAWB") ==  trim(strtoupper($data['A']))) ? "" : " A ";
                    // $ErrorMassage .=  (strtoupper("Content.No.") ==  trim(strtoupper($data['B']))) ? "" : " B ";
                    // $ErrorMassage .=  (strtoupper("Content.Waybill") ==  trim(strtoupper($data['C']))) ? "" : " C ";
                    // $ErrorMassage .=  (strtoupper("Content.Consignee") ==  trim(strtoupper($data['D']))) ? "" : " D ";
                    // $ErrorMassage .=  (strtoupper("Content.Status") ==  trim(strtoupper($data['E']))) ? "" : " E ";
                    // $ErrorMassage .=  (strtoupper("Content.Receiver name") ==  trim(strtoupper($data['F']))) ? "" : " F ";
                    // $ErrorMassage .=  (strtoupper("Content.Delivery date") ==  trim(strtoupper($data['G']))) ? "" : " G ";
                    // $ErrorMassage .=  (strtoupper("Content.PC") ==  trim(strtoupper($data['H']))) ? "" : " H ";
                    // $ErrorMassage .=  (strtoupper("Content.Date") ==  trim(strtoupper($data['I']))) ? "" : " I ";
                    // $ErrorMassage .=  (strtoupper("Content.Dest") ==  trim(strtoupper($data['J']))) ? "" : " J ";
                    // $ErrorMassage .=  (strtoupper("Content.Factor Incident") ==  trim(strtoupper($data['K']))) ? "" : " K ";
                    // $ErrorMassage .=  (strtoupper("Content.Factor") ==  trim(strtoupper($data['L']))) ? "" : " L ";

                    $ErrorMassage .=  ((strtoupper("Content.MAWB") ==  trim(strtoupper($data['A']))) or  (strtoupper("Mother Waybill Number") ==  trim(strtoupper($data['A'])))) ? "" : " A ";
                    $ErrorMassage .=  ((strtoupper("Content.No.") ==  trim(strtoupper($data['B']))) or  (strtoupper("No.") ==  trim(strtoupper($data['B'])))) ? "" : " B ";
                    $ErrorMassage .=  ((strtoupper("Content.Waybill") ==  trim(strtoupper($data['C']))) or  (strtoupper("Waybill Number") ==  trim(strtoupper($data['C'])))) ? "" : " C ";
                    $ErrorMassage .=  ((strtoupper("Content.Consignee") ==  trim(strtoupper($data['D']))) or  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['D'])))) ? "" : " D ";
                    $ErrorMassage .=  ((strtoupper("Content.Status") ==  trim(strtoupper($data['E']))) or  (strtoupper("Status") ==  trim(strtoupper($data['E'])))) ? "" : " E ";
                    $ErrorMassage .=  ((strtoupper("Content.Receiver name") ==  trim(strtoupper($data['F']))) or  (strtoupper("Receiver name") ==  trim(strtoupper($data['F'])))) ? "" : " F ";
                    $ErrorMassage .=  ((strtoupper("Content.Delivery date") ==  trim(strtoupper($data['G']))) or  (strtoupper("Delivery date") ==  trim(strtoupper($data['G'])))) ? "" : " G ";
                    $ErrorMassage .=  ((strtoupper("Content.PC") ==  trim(strtoupper($data['H']))) or  (strtoupper("Number of Pieces") ==  trim(strtoupper($data['H'])))) ? "" : " H ";
                    $ErrorMassage .=  ((strtoupper("Content.Date") ==  trim(strtoupper($data['I']))) or  (strtoupper("Pickup date") ==  trim(strtoupper($data['I'])))) ? "" : " I ";
                    $ErrorMassage .=  ((strtoupper("Content.Dest") ==  trim(strtoupper($data['J']))) or  (strtoupper("Destination Country") ==  trim(strtoupper($data['J'])))) ? "" : " J ";
                    $ErrorMassage .=  ((strtoupper("Content.Factor Incident") ==  trim(strtoupper($data['K']))) or  (strtoupper("Destination Port") ==  trim(strtoupper($data['K'])))) ? "" : " K ";
                    $ErrorMassage .=  (strtoupper("Content.Factor") ==  trim(strtoupper($data['L'])) or $data['L'] == " ") ? "" : " L ";






                    // $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                    // $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W']))) ? "" : " W ";


                    // echo ("LIMIT" ==  trim(strtoupper($data['B']))) ? "T" : "F";
                    // echo "Miss data format ";
                    break;
                    // exit();
                }

                // print_r($FlagFormat);die();
                if ($format_2_1 == "Destination Country" && $format_2_2 == "Destination Port") {

                    if ($row_count > 1) {

                        $data_model['MAWB'] = isset($data['A']) ? $data['A'] : '';
                        $data_model['No'] = isset($data['B']) ? $data['B'] : '';
                        $data_model['Waybill'] = isset($data['C']) ? $data['C'] : '';
                        $data_model['Consignee'] = isset($data['D']) ? $data['D'] : '';
                        $data_model['Status'] = isset($data['E']) ? $data['E'] : '';
                        $data_model['Receiver_name'] = isset($data['F']) ? $data['F'] : '';
                        $data_model['Delivery_date'] = isset($data['G']) && $data['G'] != '' && $data['G'] != 'UNKNOWN' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['G'])->format('d/m/Y') : '';
                        $data_model['PC'] = isset($data['H']) ? $data['H'] : '';
                        $data_model['Date'] = isset($data['I']) && $data['I'] != '' && $data['I'] != 'UNKNOWN' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['I'])->format('d/m/Y') : '';
                        $data_model['Dest'] = isset($data['J']).isset($data['K']) ? $data['J'].'-'.$data['K'] : '';
                        
                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;
    
    
                       
                        $nResult = $this->ImportStatusCSModel->insert_to_temp_shipment_status_cs($data_model);
    
    
                        $dataid = $this->ImportStatusCSModel->check_duplicate($data_model['Waybill']);
    
                        if ($dataid == 0) {
                            $this->ImportStatusCSModel->insert_t_shipment_status_cs($data_model);
                            // $this->ImportStatusCSModel->delete_t_temp_shipment_status_cs();
                        }
    
    
                        // $update_shipment_status_cs = $this->ImportStatusCSModel->update_t_shipment_status_cs();
    
                    } 
                    $row_count++;
                    $count++;
                } else {
                    if ($row_count > 1) {

                        // $data_model['title_excel'] = $sheetData[2]['B'];
    
    
                        $data_model['MAWB'] = isset($data['A']) ? $data['A'] : '';
                        $data_model['No'] = isset($data['B']) ? $data['B'] : '';
                        $data_model['Waybill'] = isset($data['C']) ? $data['C'] : '';
                        $data_model['Consignee'] = isset($data['D']) ? $data['D'] : '';
                        $data_model['Status'] = isset($data['E']) ? $data['E'] : '';
                        $data_model['Receiver_name'] = isset($data['F']) ? $data['F'] : '';
    
                        // $data_model['event_date'] =  $event_date;
                        $data_model['Delivery_date'] = isset($data['G']) && $data['G'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['G'])->format('d/m/Y') : '';
                        // $data_model['Delivery_date'] = isset($data['G']) ? $data['G'] : '';
    
    
                        $data_model['PC'] = isset($data['H']) ? $data['H'] : '';
                        $data_model['Date'] = isset($data['I'])  && $data['I'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['I'])->format('d/m/Y') : '';
                        // $data_model['Date'] = isset($data['I']) ? $data['I'] : '';
                        $data_model['Dest'] = isset($data['J']) ? $data['J'] : '';
    
                        $data_model['Factor_Incident'] = isset($data['K']) ? $data['K'] : '';
                        $data_model['Factor'] = isset($data['L']) ? $data['L'] : '';
                        // $data_model['SLA_C'] = isset($data['M']) ? $data['M'] : 0;
                        // $data_model['SLA_Remark'] = isset($data['N']) ? $data['N'] : 0;
                        // $data_model['check_mapping'] = isset($data['O']) ? $data['O'] : 0;
    
                        // $data_model['Date'] = isset($data['K']) ? $data['K'] : 0;
                        // $data_model['Date'] = isset($data['K']) ? date('Y-m-d', strtotime($data['K'])) : 0;
                        // $data_model['Date'] = isset($data['K']) && $data['K'] != '' ? DateTime::createFromFormat('d/m/Y', $data['K'])->format('Y-m-d') : 0;
                        // $data_model['Date'] = isset($data['K'])  && $data['K'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['K'])->format('Y-m-d') : 0;
    
    
    
    
                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;
    
                        // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                        //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                        // }
    
                        // print_r($data_model);die();
    
    
                        $nResult = $this->ImportStatusCSModel->insert_to_temp_shipment_status_cs($data_model);
    
    
                        // $datastatusid = $this->ImportStatusCSModel->check_status_date($data_model);
                        // print_r($data_model);die();
    
                        $dataid = $this->ImportStatusCSModel->check_duplicate($data_model['Waybill']);
    
                        if ($dataid == 0) {
                            $this->ImportStatusCSModel->insert_t_shipment_status_cs($data_model);
                            // $this->ImportStatusCSModel->delete_t_temp_shipment_status_cs();
                        }
    
    
                        // $update_shipment_status_cs = $this->ImportStatusCSModel->update_t_shipment_status_cs();
    
                    } 
                    $row_count++;
                    $count++;
                }

               



                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            // $row_count++;
            // $count++;
            // $row_count=$row_count+1;
            // die();
        }

        // $check_duplicate = $this->ImportStatusCSModel->check_duplicate();
        // $update_shipment_status_cs = $this->ImportStatusCSModel->update_t_shipment_status_cs();

        // $dataid = $this->ImportStatusCSModel->check_duplicate($data_model['Waybill']);

        // if ($dataid == 0) {
        //     $this->ImportStatusCSModel->insert_t_shipment_status_cs($data_model);
        //     $this->ImportStatusCSModel->delete_t_temp_shipment_status_cs();
        // }


        $update_shipment_status_cs = $this->ImportStatusCSModel->update_t_shipment_status_cs();



        $checkpoint_shipment_status_cs = $this->ImportStatusCSModel->checkpoint_shipment_status_cs();

        $updatename_shipment_status_cs = $this->ImportStatusCSModel->updatename_shipment_status_cs();

        $update_shipment_weekly_revise_to_cs = $this->ImportStatusCSModel->update_t_shipment_weekly_revise_to_cs();


        // $datastatusid = $this->ImportStatusCSModel->check_status_date($data_model);
        // print_r($datastatusid);die();
        // print_r($data_model);die();


        // $delete_temp_shipment_status_cs = $this->ImportStatusCSModel->delete_t_temp_shipment_status_cs();



        // $delete_shipment_daily = $this->ImportStatusCSModel->delete_temp_pandora_dashboard();

        // $dataimpot['file_name'] =  $file;
        // $dataimpot['type'] =  'Excel Shipment Status CS Upload';
        // $dataimpot['upload_date'] =  $date;

        // print_r($dataimpot);die();

        if ($FlagFormat == 1) {
            $result['status'] = false;
            $result['message'] =  $ErrorMassage;
        } else {
            $dataimpot['file_name'] =  $file;
            $dataimpot['type'] =  'Excel Shipment Status CS Upload';
            $dataimpot['upload_date'] =  $date;
            $nResult = $this->ImportStatusCSModel->insert_log_shipment($dataimpot);

            $result['status'] = true;
            $result['message'] = $this->lang->line('savesuccess');
        };

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }


    public function getdailyModelList()
    {
        try {
            $this->load->model('ImportStatusCSModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ImportStatusCSModel->getdailyModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            $result['totalRecords'] = $this->ImportStatusCSModel->getTotal($dataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function alert_line_notify()
    {
        // call line api
        $Token = 'UeMJwWbMswxUmnEA08yzzgGhWAuszabSpVT8BtUHvpr';
        $message = 'ImportCS'.' '. date('Y-m-d');
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . ''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
        } else {
            $result_ = json_decode($result, true);
        }
        curl_close($chOne);
    }

    public function deleteImport()
    {
        try {
            $this->load->model('ImportStatusCSModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);die();
            $dataModel = isset($dataPost['file_name']) ? $dataPost['file_name'] : '';
            // print_r($dataModel);die();
            $result['status'] = true;
            $result['message'] = $this->ImportStatusCSModel->deleteImport($dataModel);
            $result['ShowMessage'] = 'The deletion was successful.';
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

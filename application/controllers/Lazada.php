<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Lazada extends CI_Controller
{
    private $tbl_name = 'inventory';
    private $id = 'id';

    public function __construct()
    {
        // echo'test';
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
        ini_set('memory_limit', '512M');
    }

    public function index()
    {
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('lazada/lazada_view');
        $this->load->view('share/footer');
    }

    public function upload_file()
    {
        //  print_r('test');die();
        // $uniqid = uniqid();
        // $dataapple['uniqidapple'] = $uniqid;
        // $datashipment['uniqidshipment'] = $uniqid;
        $date = date('Y-m-d-H-i-s');

        // $this->load->model('HeadAppleModel', '', true);
        // $this->load->model('HeadShipmentModel', '', true);

        // $uniqidapple = $this->HeadAppleModel->insert($dataapple);
        // $uniqidshipment = $this->HeadShipmentModel->insert($datashipment);

        // $this->upload_fileapple($_FILES, $date, $uniqid);
        // $this->upload_fileshipment($_FILES, $date, $uniqid);
        $todayfile = 'lazada' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/lazada';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_lazada']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $status = true;
        if (!$this->upload->do_upload('file_lazada')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();
            $todayfile = $data['upload_data']['file_name'];
            $this->loadExcelazadaToDB($todayfile, $date);
        }
    }


    public function loadExcelazadaToDB($file, $date)
    {
        $formattype = 0;
        ini_set('max_execution_time', 300);
        $this->load->library('MyExcel');
        $this->load->model('LazadaModel', '', true);
        // print_r($file);
        $objReader = PHPExcel_IOFactory::load('upload/lazada/' . $file);
        // die();

        $sheetData = $objReader->getActiveSheet()->toArray('', true, false, true);

        // print_r($sheetData);die();

        $row_count = 1;
        $i = 'A';
        // $header_true == true;
        $this->LazadaModel->delete_temp_lazada();
        $resuult = array();
        foreach ($sheetData as $data) {

            try {

                if ($row_count > 1) {
                    if ($formattype == 2) {
                        $data['J'] = sprintf('%f', $data['J']);
                        $data['J'] = number_format($data['J'], 0);
                        $data['J'] = str_replace(",", "", $data['J']);

                        $data['A'] = sprintf('%f', $data['A']);
                        $data['A'] = number_format($data['A'], 0);
                        $data['A'] = str_replace(",", "", $data['A']);

                        $data['D'] = sprintf('%f', $data['D']);
                        $data['D'] = number_format($data['D'], 0);
                        $data['D'] = str_replace(",", "", $data['D']);

                        $data['AI'] = sprintf('%f', $data['AI']);
                        $data['AI'] = number_format($data['AI'], 0);
                        $data['AI'] = str_replace(",", "", $data['AI']);

                        $data['AN'] = sprintf('%f', $data['AN']);
                        $data['AN'] = number_format($data['AN'], 0);
                        $data['AN'] = str_replace(",", "", $data['AN']);

                        $data_model['order_item_id'] = isset($data['A']) ? $data['A'] : 0;
                        $data_model['order_type'] = isset($data['B']) ? $data['B'] : 0;
                        $data_model['order_flag'] = isset($data['C']) ? $data['C'] : 0;
                        $data_model['lazada_id'] = isset($data['D']) ? $data['D'] : 0;
                        $data_model['seller_sku'] = isset($data['E']) ? $data['E'] : 0;
                        $data_model['seller_sku_code'] = isset($data['F']) ? $data['F'] : 0;
                        $data_model['lazada_sku'] = isset($data['G']) ? $data['G'] : 0;
                        $data_model['created_at'] = isset($data['H']) ? PHPExcel_Shared_Date::ExcelToPHPObject($data['H'])->format('Y-m-d H:i:s') : 0;
                        $data_model['updated_at'] = isset($data['I']) ? PHPExcel_Shared_Date::ExcelToPHPObject($data['I'])->format('Y-m-d H:i:s') : 0;
                        $data_model['order_number'] = isset($data['J']) ? $data['J'] : 0;
                        $data_model['invioce_required'] = isset($data['K']) ? $data['K'] : 0;
                        $data_model['customer_name'] = isset($data['L']) ? $data['L'] : 0;
                        $data_model['customer_email'] = isset($data['M']) ? $data['M'] : 0;
                        $data_model['national_registration_number'] = isset($data['N']) ? $data['N'] : 0;
                        $data_model['shipping_name'] = isset($data['O']) ? $data['O'] : 0;
                        $data_model['shipping_address'] = isset($data['P']) ? $data['P'] : 0;
                        $data_model['shipping_address2'] = isset($data['Q']) ? $data['Q'] : 0;
                        $data_model['shipping_address3'] = isset($data['R']) ? $data['R'] : 0;
                        $data_model['shipping_address3_eng'] = isset($data['S']) ? $data['S'] : 0;
                        $data_model['shipping_address4'] = isset($data['T']) ? $data['T'] : 0;
                        $data_model['shipping_address4_eng'] = isset($data['U']) ? $data['U'] : 0;
                        $data_model['shipping_address5'] = isset($data['V']) ? $data['V'] : 0;
                        $data_model['shipping_phone_number'] = isset($data['W']) ? $data['W'] : 0;
                        $data_model['shipping_phone_number2'] = isset($data['X']) ? $data['X'] : 0;
                        $data_model['shipping_city'] = isset($data['Y']) ? $data['Y'] : 0;
                        $data_model['shipping_postcode'] = isset($data['Z']) ? $data['Z'] : 0;
                        $data_model['shipping_country'] = isset($data['AA']) ? $data['AA'] : 0;
                        $data_model['shipping_region'] = isset($data['AB']) ? $data['AB'] : 0;
                        $data_model['billing_name'] = isset($data['AC']) ? $data['AC'] : 0;
                        $data_model['billing_address'] = isset($data['AD']) ? $data['AD'] : 0;
                        $data_model['billing_address2'] = isset($data['AE']) ? $data['AE'] : 0;
                        $data_model['billing_address3'] = isset($data['AF']) ? $data['AF'] : 0;
                        $data_model['billing_address4'] = isset($data['AG']) ? $data['AG'] : 0;
                        $data_model['billing_address5'] = isset($data['AH']) ? $data['AH'] : 0;
                        $data_model['billing_phone_number'] = isset($data['AI']) ? $data['AI'] : 0;
                        $data_model['billing_phone_number2'] = isset($data['AJ']) ? $data['AJ'] : 0;
                        $data_model['billing_city'] = isset($data['AK']) ? $data['AK'] : 0;
                        $data_model['billing_postcode'] = isset($data['AL']) ? $data['AL'] : 0;
                        $data_model['billing_country'] = isset($data['AM']) ? $data['AM'] : 0;
                        $data_model['tax_code'] = isset($data['AN']) ? $data['AN'] : 0;
                        $data_model['branch_number'] = isset($data['AO']) ? $data['AO'] : 0;
                        $data_model['tax_invoice_requested'] = isset($data['AP']) ? $data['AP'] : 0;
                        $data_model['payment_method'] = isset($data['AQ']) ? $data['AQ'] : 0;
                        $data_model['paid_price'] = isset($data['AR']) ? $data['AR'] : 0;
                        $data_model['unit_price'] = isset($data['AS']) ? $data['AS'] : 0;
                        $data_model['shipping_fee'] = isset($data['AT']) ? $data['AT'] : 0;
                        $data_model['wallet_credits'] = isset($data['AU']) ? $data['AU'] : 0;
                        $data_model['item_name'] = isset($data['AV']) ? $data['AV'] : 0;
                        $data_model['variation'] = isset($data['AW']) ? $data['AW'] : 0;
                        $data_model['cd_shipping_provider'] = isset($data['AX']) ? $data['AX'] : 0;
                        $data_model['shipping_provider'] = isset($data['AY']) ? $data['AY'] : 0;
                        $data_model['shipment_type_name'] = isset($data['AZ']) ? $data['AZ'] : 0;
                        $data_model['shipping_provider_type'] = isset($data['BA']) ? $data['BA'] : 0;
                        $data_model['cd_tracking_code'] = isset($data['BB']) ? $data['BB'] : 0;
                        $data_model['cd_tracking_code'] = isset($data['BC']) ? $data['BC'] : 0;
                        $data_model['tracking_url'] = isset($data['BD']) ? $data['BD'] : 0;
                        $data_model['shipping_provider_first_mile'] = isset($data['BE']) ? $data['BE'] : 0;
                        $data_model['tracking_code_first_mile'] = isset($data['BF']) ? $data['BF'] : 0;
                        $data_model['tracking_url_first_mile'] = isset($data['BG']) ? $data['BG'] : 0;
                        $data_model['promised_shipping_time'] = isset($data['BH']) ? $data['BH'] : 0;
                        $data_model['premium'] = isset($data['BI']) ? $data['BI'] : 0;
                        $data_model['status'] = isset($data['BJ']) ? $data['BJ'] : 0;
                        $data_model['cancel_return_initiator'] = isset($data['BK']) ? $data['BK'] : 0;
                        $data_model['reason'] = isset($data['BL']) ? $data['BL'] : 0;
                        $data_model['reason_detail'] = isset($data['BM']) ? $data['BM'] : 0;
                        $data_model['editor'] = isset($data['BN']) ? $data['BN'] : 0;
                        $data_model['bundle_id'] = isset($data['BO']) ? $data['BO'] : 0;
                        $data_model['bundle_discount'] = isset($data['BP']) ? $data['BP'] : 0;
                        $data_model['refund_amount'] = isset($data['BQ']) ? $data['BQ'] : 0;

                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;
                    } else {
                        $data['I'] = sprintf('%f', $data['I']);
                        $data['I'] = number_format($data['I'], 0);
                        $data['I'] = str_replace(",", "", $data['I']);

                        $data['A'] = sprintf('%f', $data['A']);
                        $data['A'] = number_format($data['A'], 0);
                        $data['A'] = str_replace(",", "", $data['A']);

                        $data['D'] = sprintf('%f', $data['D']);
                        $data['D'] = number_format($data['D'], 0);
                        $data['D'] = str_replace(",", "", $data['D']);

                        $data['AF'] = sprintf('%f', $data['AF']);
                        $data['AF'] = number_format($data['AF'], 0);
                        $data['AF'] = str_replace(",", "", $data['AF']);

                        $data['AK'] = sprintf('%f', $data['AK']);
                        $data['AK'] = number_format($data['AK'], 0);
                        $data['AK'] = str_replace(",", "", $data['AK']);

                        $data_model['order_item_id'] = isset($data['A']) ? $data['A'] : 0;
                        $data_model['order_type'] = isset($data['B']) ? $data['B'] : 0;
                        $data_model['order_flag'] = isset($data['C']) ? $data['C'] : 0;
                        $data_model['lazada_id'] = isset($data['D']) ? $data['D'] : 0;
                        $data_model['seller_sku'] = isset($data['E']) ? $data['E'] : 0;
                        // $data_model['seller_sku_code'] = isset($data['F']) ? $data['F'] : 0;
                        $data_model['lazada_sku'] = isset($data['F']) ? $data['F'] : 0;
                        $data_model['created_at'] = isset($data['G']) ? PHPExcel_Shared_Date::ExcelToPHPObject($data['G'])->format('Y-m-d H:i:s') : 0;
                        $data_model['updated_at'] = isset($data['H']) ? PHPExcel_Shared_Date::ExcelToPHPObject($data['H'])->format('Y-m-d H:i:s') : 0;
                        $data_model['order_number'] = isset($data['I']) ? $data['I'] : 0;
                        $data_model['invioce_required'] = isset($data['J']) ? $data['J'] : 0;
                        $data_model['customer_name'] = isset($data['K']) ? $data['K'] : 0;
                        $data_model['customer_email'] = isset($data['L']) ? $data['L'] : 0;
                        $data_model['national_registration_number'] = isset($data['M']) ? $data['M'] : 0;
                        $data_model['shipping_name'] = isset($data['N']) ? $data['N'] : 0;
                        $data_model['shipping_address'] = isset($data['O']) ? $data['O'] : 0;
                        $data_model['shipping_address2'] = isset($data['P']) ? $data['P'] : 0;

                        $address3 = explode("/", $data['Q']);
                        $data_model['shipping_address3'] = isset($address3[0]) ? $address3[0] : 0;
                        $data_model['shipping_address3_eng'] = isset($address3[1]) ? $address3[1] : 0;

                        $address4 = explode("/", $data['R']);
                        $data_model['shipping_address4'] = isset($address4[0]) ? $address4[0] : 0;
                        $data_model['shipping_address4_eng'] = isset($address4[1]) ? $address4[1] : 0;

                        $data_model['shipping_address5'] = isset($data['S']) ? $data['S'] : 0;
                        $data_model['shipping_phone_number'] = isset($data['T']) ? $data['T'] : 0;
                        $data_model['shipping_phone_number2'] = isset($data['U']) ? $data['U'] : 0;
                        $data_model['shipping_city'] = isset($data['V']) ? $data['V'] : 0;
                        $data_model['shipping_postcode'] = isset($data['W']) ? $data['W'] : 0;
                        $data_model['shipping_country'] = isset($data['X']) ? $data['X'] : 0;
                        $data_model['shipping_region'] = isset($data['Y']) ? $data['Y'] : 0;
                        $data_model['billing_name'] = isset($data['Z']) ? $data['Z'] : 0;
                        $data_model['billing_address'] = isset($data['AA']) ? $data['AA'] : 0;
                        $data_model['billing_address2'] = isset($data['AB']) ? $data['AB'] : 0;
                        $data_model['billing_address3'] = isset($data['AC']) ? $data['AC'] : 0;
                        $data_model['billing_address4'] = isset($data['AD']) ? $data['AD'] : 0;
                        $data_model['billing_address5'] = isset($data['AE']) ? $data['AE'] : 0;
                        $data_model['billing_phone_number'] = isset($data['AF']) ? $data['AF'] : 0;
                        $data_model['billing_phone_number2'] = isset($data['AG']) ? $data['AG'] : 0;
                        $data_model['billing_city'] = isset($data['AH']) ? $data['AH'] : 0;
                        $data_model['billing_postcode'] = isset($data['AI']) ? $data['AI'] : 0;
                        $data_model['billing_country'] = isset($data['AJ']) ? $data['AJ'] : 0;
                        $data_model['tax_code'] = isset($data['AK']) ? $data['AK'] : 0;
                        $data_model['branch_number'] = isset($data['AL']) ? $data['AL'] : 0;
                        $data_model['tax_invoice_requested'] = isset($data['AM']) ? $data['AM'] : 0;
                        $data_model['payment_method'] = isset($data['AN']) ? $data['AN'] : 0;
                        $data_model['paid_price'] = isset($data['AO']) ? $data['AO'] : 0;
                        $data_model['unit_price'] = isset($data['AP']) ? $data['AP'] : 0;
                        $data_model['shipping_fee'] = isset($data['AQ']) ? $data['AQ'] : 0;
                        $data_model['wallet_credits'] = isset($data['AR']) ? $data['AR'] : 0;
                        $data_model['item_name'] = isset($data['AS']) ? $data['AS'] : 0;
                        $data_model['variation'] = isset($data['AT']) ? $data['AT'] : 0;
                        $data_model['cd_shipping_provider'] = isset($data['AU']) ? $data['AU'] : 0;
                        $data_model['shipping_provider'] = isset($data['AV']) ? $data['AV'] : 0;
                        $data_model['shipment_type_name'] = isset($data['AW']) ? $data['AW'] : 0;
                        $data_model['shipping_provider_type'] = isset($data['AX']) ? $data['AX'] : 0;
                        $data_model['cd_tracking_code'] = isset($data['AY']) ? $data['AY'] : 0;
                        $data_model['cd_tracking_code'] = isset($data['AZ']) ? $data['AZ'] : 0;
                        $data_model['tracking_url'] = isset($data['BA']) ? $data['BA'] : 0;
                        $data_model['shipping_provider_first_mile'] = isset($data['BB']) ? $data['BB'] : 0;
                        $data_model['tracking_code_first_mile'] = isset($data['BC']) ? $data['BC'] : 0;
                        $data_model['tracking_url_first_mile'] = isset($data['BD']) ? $data['BD'] : 0;
                        $data_model['promised_shipping_time'] = isset($data['BE']) ? $data['BE'] : 0;
                        $data_model['premium'] = isset($data['BI']) ? $data['BI'] : 0;
                        $data_model['status'] = isset($data['BJ']) ? $data['BJ'] : 0;
                        $data_model['cancel_return_initiator'] = isset($data['BH']) ? $data['BH'] : 0;
                        $data_model['reason'] = isset($data['BL']) ? $data['BL'] : 0;
                        $data_model['reason_detail'] = isset($data['BJ']) ? $data['BJ'] : 0;
                        $data_model['editor'] = isset($data['BK']) ? $data['BK'] : 0;
                        $data_model['bundle_id'] = isset($data['BL']) ? $data['BL'] : 0;
                        $data_model['bundle_discount'] = isset($data['BM']) ? $data['BM'] : 0;
                        $data_model['refund_amount'] = isset($data['BN']) ? $data['BN'] : 0;

                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;
                    }

                    $data_model['order_number_system'] = $this->LazadaModel->CreateNumberSystem($data_model);
                    // print_r($data_model);die();

                    if ($data_model['order_item_id'] != '') {
                        $nResult = $this->LazadaModel->insert($data_model);
                    }
                } else {
                    if ($data['BO'] == '') {
                        $formattype = 1;
                    } else if ($data['BO'] != '') {
                        $formattype = 2;
                    } else if ($data['A'] != 'Order Item Id') {
                        $result['status'] = false;
                        $result['message'] = 'Wrong file format';
                        echo json_encode($result, JSON_UNESCAPED_UNICODE);
                        exit;
                    }
                    // echo $formattype;
                    // die();
                }



                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            $row_count++;
            // $row_count=$row_count+1;
        }
        $check_duplicate = $this->LazadaModel->check_duplicate();
        // print_r( $check_duplicate);(die);

        if (count($check_duplicate) > 0) {
            $result['status'] = false;
            $result['message'] = 'Duplicate data found from file. That has already been uploaded';
        } else {
            $dataimpot['file_name'] =  $file;
            $dataimpot['type'] =  'lazada';
            $dataimpot['upload_date'] =  $date;
            $this->LazadaModel->insertto_t_lazada();
            $this->LazadaModel->insertto_customer($file, $date);
            $nResult = $this->LazadaModel->insert_log_lazada($dataimpot);
            $result['status'] = true;
            $result['message'] = $this->lang->line('savesuccess');
        };
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }



    public function getdailyModelList()
    {
        try {
            $this->load->model('LazadaModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->LazadaModel->getdailyModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            // $result['totalRecords'] = $this->CashadvanceModel->getTotal($dataModel);
            // $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    // public function downloadapple()
    // {
    //     try {
    //         $this->load->model('DailyimportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);
    //         // print_r($dataPost);die();
    //         $dataModel = isset($dataPost['id']) ? $dataPost['id'] : '';

    //         $result['status'] = true;
    //         $result['message'] = $this->DailyimportModel->downloadapple($dataModel);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }

    // public function downloadshipment()
    // {
    //     try {
    //         $this->load->model('DailyimportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);
    //         // print_r($dataPost);die();
    //         $dataModel = isset($dataPost['id']) ? $dataPost['id'] : '';

    //         $result['status'] = true;
    //         $result['message'] = $this->DailyimportModel->downloadshipment($dataModel);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }
    public function deleteImport()
    {
        try {
            $this->load->model('LazadaModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);die();
            $dataModel = isset($dataPost['file_name']) ? $dataPost['file_name'] : '';

            $result['status'] = true;
            $result['message'] = $this->LazadaModel->deleteImport($dataModel);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }


    public function Exportexcel()
    {
        ini_set('MAX_EXECUTION_TIME', '-1');

        try {
            $this->load->model('LazadaModel', '', TRUE);


            $this->load->library('MyExcel');
            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);
            // die();

            $dataModel['file_name'] = isset($dataPost['file_name']) ? $dataPost['file_name'] : "";
            // print_r($dataModel);
            // die();

            $data = $this->LazadaModel->GetcustomerExport($dataModel);
            // print_r($data);
            // die();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Customer");


            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => '00ffff')
                // )
            );


            $headbody = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => 'ffff00')
                // )
            );
            // //กำหนด style ส่วน body//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_THIN
            //         )
            //     )
            // );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            // if (isset($dataModel['end_date']) && $dataModel['end_date'] != "") {
            //     $EndDateTS = strtotime($dataModel['end_date']);
            //     if ($EndDateTS !== false) {
            //         date('Y-m-d', $EndDateTS);
            //     }
            // }
            // if (isset($dataModel['date']) && $dataModel['date'] != "") {
            //     $StarDateTS = strtotime($dataModel['date']);
            //     if ($StarDateTS !== false) {
            //         date('Y-m-d', $StarDateTS);
            //     }
            // }
            $objPHPExcel->setActiveSheetIndex(0)

                ->setCellValue('A1', 'Customer')
                ->setCellValue('B1', 'Name')
                ->setCellValue('C1', 'Address 1')
                ->setCellValue('D1', '')
                ->setCellValue('E1', '')
                ->setCellValue('F1', '')
                ->setCellValue('G1', 'อำเภอ')
                ->setCellValue('H1', 'จังหวัด')
                ->setCellValue('I1', 'รหัสไปรษณีย์')
                ->setCellValue('J1', 'Bang')
                ->setCellValue('K1', 'Type')
                ->setCellValue('L1', 'End user type')
                ->setCellValue('M1', 'Invoice category')
                ->setCellValue('N1', 'Corporate credit')
                ->setCellValue('O1', 'Corporate cust')
                ->setCellValue('P1', 'Tax Code');

            // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($headbody);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);

            $start_row = 2;
            foreach ($data as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);




                $objPHPExcel->setActiveSheetIndex(0)
                    // ->setCellValue('A' . $start_row, date('d/m/Y', strtotime($row['update_date'])))
                    ->setCellValue('A' . $start_row, $row['customer_no'])
                    ->setCellValue('B' . $start_row, $row['customer_name'])
                    ->setCellValue('C' . $start_row, $row['address1'])
                    ->setCellValue('D' . $start_row, $row['address2'])
                    ->setCellValue('E' . $start_row, $row['address3'])

                    // ->setCellValue('D' . $start_row, $row['account'])
                    // ->setCellValueExplicit('D'.$start_row, $row['account'],PHPExcel_Cell_DataType::TYPE_STRING)

                    ->setCellValue('F' . $start_row, $row['address4'])
                    ->setCellValue('G' . $start_row, $row['district'])
                    ->setCellValue('H' . $start_row, $row['province'])
                    ->setCellValue('I' . $start_row, $row['postal_code'])
                    ->setCellValue('J' . $start_row, $row['bang'])
                    ->setCellValue('K' . $start_row, $row['type'])
                    ->setCellValue('L' . $start_row, $row['end_user_type'])
                    ->setCellValue('M' . $start_row, $row['invoice_category'])
                    ->setCellValue('N' . $start_row, $row['corporate_credit'])
                    ->setCellValue('O' . $start_row, $row['corporate_customer'])
                    ->setCellValueExplicit('P' . $start_row, $row['tax_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $start_row++;
            }

            $dataOrderhead = $this->LazadaModel->getDataOderHeader($dataModel);
            // print_r($dataOrderhead);die();

            $objPHPExcel->createSheet(1);



            $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'order num laz')
                ->setCellValue('B1', 'order num')
                ->setCellValue('C1', 'Customer')
                ->setCellValue('D1', 'order date')
                ->setCellValue('E1', 'warehouse')
                ->setCellValue('F1', 'credit hold');

            $objPHPExcel->getActiveSheet()->setTitle("Order_H");

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);

            $start_row = 2;
            foreach ($dataOrderhead as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);

                $objPHPExcel->setActiveSheetIndex(1)
                    // ->setCellValue('A' . $start_row, date('d/m/Y', strtotime($row['update_date'])))
                    ->setCellValueExplicit('A' . $start_row, $row['order_item_id'], PHPExcel_Cell_DataType::TYPE_STRING)
                     ->setCellValue('B' . $start_row, $row['order_number'])
                    ->setCellValue('C' . $start_row, $row['customer_no'])
                    ->setCellValue('D' . $start_row, $row['order_date'])
                    ->setCellValue('E' . $start_row, $row['warehouse'])
                    ->setCellValue('F' . $start_row, '0');
                $start_row++;
            }

            $dataOrderDetail = $this->LazadaModel->getDataOderDetail($dataModel);
            // print_r($dataOrderDetail );die();

            $objPHPExcel->createSheet(2);

            $objPHPExcel->setActiveSheetIndex(2)
                ->setCellValue('A1', 'order num laz')
                ->setCellValue('B1', 'order')
                ->setCellValue('C1', 'line')
                ->setCellValue('D1', 'item')
                ->setCellValue('E1', 'qty')
                ->setCellValue('F1', 'price')
                ->setCellValue('G1', 'customer');
                
                

            $objPHPExcel->getActiveSheet()->setTitle("Oder_L");

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
           

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
         
            $start_row = 2;
            foreach ($dataOrderDetail as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
            

                $objPHPExcel->setActiveSheetIndex(2)
                    // ->setCellValue('A' . $start_row, date('d/m/Y', strtotime($row['update_date'])))
                    
                    ->setCellValueExplicit('A' . $start_row, $row['order_item_id'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('B' . $start_row, $row['order_number_system'])
                    ->setCellValue('C' . $start_row, $row['line'])
                    ->setCellValue('D' . $start_row, $row['item'])
                    ->setCellValue('E' . $start_row, $row['qty'])
                    ->setCellValue('F' . $start_row, $row['price'])
                    ->setCellValue('G' . $start_row, $row['customer']);
                

                $start_row++;
            }


            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $filename = $start.' to '.$end . '.xlsx';

            $filename = 'Export-' . date("Y-m-d-H-i-s") . '.xlsx';
            // echo FCPATH; die();
            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);
            $result['message'] = $filename;
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ImportDaily extends MY_Controller
{
    // private $tbl_name = 'inventory';
    // private $id = 'id';

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }

        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
        ini_set('memory_limit', '1024M');
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_PANDASH');
        if (!$role_data['IMFILE']) {
            redirect('Login');
        }
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('import/importDaily_view');
        $this->load->view('share/footer');
    }

    public function upload_file()
    {
        $this->alert_line_notify();

        //  print_r('test');die();
        // $uniqid = uniqid();
        // $datapandora['uniqidpandora'] = $uniqid;
        // $datashipment['uniqidshipment'] = $uniqid;
        $date = date('Y-m-d-H-i-s');

        // $this->load->model('HeadpandoraModel', '', true);
        // $this->load->model('HeadShipmentModel', '', true);

        // $uniqidpandora = $this->HeadpandoraModel->insert($datapandora);
        // $uniqidshipment = $this->HeadShipmentModel->insert($datashipment);

        // $this->upload_filepandora($_FILES, $date, $uniqid);
        // $this->upload_fileshipment($_FILES, $date, $uniqid);
        $todayfile = 'Pandora_Dashboard' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/import_excel';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_Pandora_Dashboard']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);



        $status = true;
        if (!$this->upload->do_upload('file_Pandora_Dashboard')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();
            $todayfile = $data['upload_data']['file_name'];
            $this->loadExcelpandoraDashboardToDB($todayfile, $date);
        }
    }


    public function loadExcelpandoraDashboardToDB($file, $date)
    {
        $formattype = 0;
        ini_set('max_execution_time', 300);

        $this->load->library('MyExcel');
        $this->load->model('ImportDailyModel', '', true);
        // print_r($file);die();
        $objReader = PHPExcel_IOFactory::load('upload/import_excel/' . $file);

        // print_r($objReader);die();

        $sheetData = $objReader->setActiveSheetIndex(0)->toArray('', true, false, true);

        // $sheetData = $objReader->getsheet(0);
        // $sheetData = $objReader->getActiveSheet()->toArray('', true, false, true);


        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        $this->ImportDailyModel->delete_temp_pandora_dashboard();

        // echo 'delete_temp_pandora_dashboard success';die();
        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        $row_ErrorMassage_count = 0;
        //  print_r($sheetData);die();
        // print_r($sheetData[4]);die();

        // $ErrorFormatDate = 45801;
        // $ErrorFormatDate = 44705; 

        // $str1 = 'Hello world!';
        // echo strlen($str1); // Outputs: 12
        // $ErrorHAWB_No = [];
        foreach ($sheetData as $row) {

            if ($row_ErrorMassage_count > 0) {
                if (strlen($row['V']) != 17 or strlen($row['X']) != 16) {
                    $ErrorHAWB_No[] = $row['C'];
                    // $FlagFormat = true;
                }
            }
            $row_ErrorMassage_count++;
        }
        // print_r($ErrorHAWB_No);die();
        if (count($ErrorHAWB_No) > 0) {
            // print_r($ErrorHAWB_No);
            // $FlagFormat = true;

            foreach ($ErrorHAWB_No as $row) {
                // print_r($row); 
                $HAWB_No[] = $row;
                // print_r($HAWB_No);
            }
            // echo ($HAWB_No);
            $massage = 'Error column status date wrong format for HAWB No. ' . $HAWB_No[0] . ' ' . $HAWB_No[1] . ' ' . $HAWB_No[2] . ' ' . $HAWB_No[3] . ' ' . $HAWB_No[4];
            $result['message'] =  $massage;
            $result['status'] = false;
            // print_r([$HAWB_No]);die();

        } else {



            // print_r($ErrorMassage);
            // die();
            // print_r($ErrorMassage);
            // die();




            foreach ($sheetData as $data) {

                try {

                    if (
                        $count == 0

                        // and  ("Date_Pickup" == trim(strtoupper($data['A'])))

                        and  (strtoupper("Date_Pickup") ==  trim(strtoupper($data['A'])))
                        and  (strtoupper("Mother_WB") ==  trim(strtoupper($data['B'])))
                        and  (strtoupper("HAWB_No") ==  trim(strtoupper($data['C'])))
                        and  (strtoupper("Shipper_Company") ==  trim(strtoupper($data['D'])))
                        and  (strtoupper("Cust_ID") ==  trim(strtoupper($data['E'])))
                        and  (strtoupper("Consignee_Company") ==  trim(strtoupper($data['F'])))
                        and  (strtoupper("Consignee_Address") ==  trim(strtoupper($data['G'])))
                        and  (strtoupper("Account") ==  trim(strtoupper($data['H'])))
                        and  (strtoupper("Origin") ==  trim(strtoupper($data['I'])))
                        and  (strtoupper("Destination") ==  trim(strtoupper($data['J'])))
                        and  (strtoupper("M_IATA") ==  trim(strtoupper($data['K'])))
                        and  (strtoupper("Destination_Ctry") ==  trim(strtoupper($data['L'])))
                        and  (strtoupper("Destination_City") ==  trim(strtoupper($data['M'])))
                        and  (strtoupper("Postal Code") ==  trim(strtoupper($data['N'])))
                        and  (strtoupper("Peice(s)") ==  trim(strtoupper($data['O'])))
                        and  (strtoupper("A_Weight") ==  trim(strtoupper($data['P'])))
                        and  (strtoupper("V_Weight") ==  trim(strtoupper($data['Q'])))
                        and  (strtoupper("Product") ==  trim(strtoupper($data['R'])))
                        and  (strtoupper("Delivery") ==  trim(strtoupper($data['S'])))
                        and  (strtoupper("Shipment_Status") ==  trim(strtoupper($data['T'])))
                        and  (strtoupper("POD") ==  trim(strtoupper($data['U'])))
                        and  (strtoupper("Status_Date") ==  trim(strtoupper($data['V'])))
                        and  (strtoupper("Status_Time") ==  trim(strtoupper($data['W'])))
                        and  (strtoupper("Pickup_Date") ==  trim(strtoupper($data['X'])))
                        and  (strtoupper("SLA_C") ==  trim(strtoupper($data['Y'])))
                        and  (strtoupper("SLA_Remark") ==  trim(strtoupper($data['Z'])))

                        and  (strtoupper("Incident_Remark") ==  trim(strtoupper($data['AA'])))
                        and  (strtoupper("FD_Remark") ==  trim(strtoupper($data['AB'])))

                        // and  ((strtoupper("Pickup day") ==  trim(strtoupper($data['J']))) or  (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['J']))))

                    ) {
                        // echo "found data <br/>";
                        // echo'case 1';
                        $fuondFormat = 1;

                        // $this->JobTypeModel->dropJobType();
                    } else if ($count == 0) {
                        // print_r($data);
                        // echo'SCAC';
                        // echo trim(strtoupper($data['E']));
                        $ErrorMassage = 'Your file has an out-of-format format in the column ';
                        $FlagFormat = true;

                        $ErrorMassage .=  (strtoupper("Date_Pickup") ==  trim(strtoupper($data['A']))) ? "" : " A ";
                        $ErrorMassage .=  (strtoupper("Mother_WB") ==  trim(strtoupper($data['B']))) ? "" : " B ";
                        $ErrorMassage .=  (strtoupper("HAWB_No") ==  trim(strtoupper($data['C']))) ? "" : " C ";
                        $ErrorMassage .=  (strtoupper("Shipper_Company") ==  trim(strtoupper($data['D']))) ? "" : " D ";
                        $ErrorMassage .=  (strtoupper("Cust_ID") ==  trim(strtoupper($data['E']))) ? "" : " E ";
                        $ErrorMassage .=  (strtoupper("Consignee_Company") ==  trim(strtoupper($data['F']))) ? "" : " F ";
                        $ErrorMassage .=  (strtoupper("Consignee_Address") ==  trim(strtoupper($data['G']))) ? "" : " G ";
                        $ErrorMassage .=  (strtoupper("Account") ==  trim(strtoupper($data['H']))) ? "" : " H ";
                        $ErrorMassage .=  (strtoupper("Origin") ==  trim(strtoupper($data['I']))) ? "" : " I ";
                        $ErrorMassage .=  (strtoupper("Destination") ==  trim(strtoupper($data['J']))) ? "" : " J ";
                        // $ErrorMassage .=  ((strtoupper("Pickup day") ==  trim(strtoupper($data['J']))) or  (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['J'])))) ? "" : " J ";
                        $ErrorMassage .=  (strtoupper("M_IATA") ==  trim(strtoupper($data['K']))) ? "" : " K ";
                        $ErrorMassage .=  (strtoupper("Destination_Ctry") ==  trim(strtoupper($data['L']))) ? "" : " L ";
                        $ErrorMassage .=  (strtoupper("Destination_City") ==  trim(strtoupper($data['M']))) ? "" : " M ";
                        $ErrorMassage .=  (strtoupper("Postal Code") ==  trim(strtoupper($data['N']))) ? "" : " N ";
                        $ErrorMassage .=  (strtoupper("Peice(s)") ==  trim(strtoupper($data['O']))) ? "" : " O ";
                        $ErrorMassage .=  (strtoupper("A_Weight") ==  trim(strtoupper($data['P']))) ? "" : " P ";
                        $ErrorMassage .=  (strtoupper("V_Weight") ==  trim(strtoupper($data['Q']))) ? "" : " Q ";
                        $ErrorMassage .=  (strtoupper("Product") ==  trim(strtoupper($data['R']))) ? "" : " R ";
                        $ErrorMassage .=  (strtoupper("Delivery") ==  trim(strtoupper($data['S']))) ? "" : " S ";
                        $ErrorMassage .=  (strtoupper("Shipment_Status") ==  trim(strtoupper($data['T']))) ? "" : " T ";
                        $ErrorMassage .=  (strtoupper("POD") ==  trim(strtoupper($data['U']))) ? "" : " U ";
                        $ErrorMassage .=  (strtoupper("Status_Date") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                        $ErrorMassage .=  (strtoupper("Status_Time") ==  trim(strtoupper($data['W']))) ? "" : " W ";
                        $ErrorMassage .=  (strtoupper("Pickup_Date") ==  trim(strtoupper($data['X']))) ? "" : " X ";
                        $ErrorMassage .=  (strtoupper("SLA_C") ==  trim(strtoupper($data['Y']))) ? "" : " Y ";
                        $ErrorMassage .=  (strtoupper("SLA_Remark") ==  trim(strtoupper($data['Z']))) ? "" : " Z ";

                        $ErrorMassage .=  (strtoupper("Incident_Remark") ==  trim(strtoupper($data['AA']))) ? "" : " AA ";
                        $ErrorMassage .=  (strtoupper("FD_Remark") ==  trim(strtoupper($data['AB']))) ? "" : " AB ";



                        // $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                        // $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W']))) ? "" : " W ";


                        // echo ("LIMIT" ==  trim(strtoupper($data['B']))) ? "T" : "F";
                        // echo "Miss data format ";
                        break;
                        // exit();
                    }

                    // print_r($FlagFormat);die();

                    if ($row_count > 1) {
                        $StatusDate = substr($data['V'],0,-6);
                        // $data_model['Status_Date'] = isset($StatusDate) ? date('Y-m-d', strtotime($StatusDate)) : 0;
                        $PickupDate = substr($data['X'],0,-6);
                        // $data_model['Pickup_Date'] = isset($PickupDate) && $PickupDate != '' ? DateTime::createFromFormat('d/m/Y', $PickupDate)->format('Y-m-d') : 0;


                        // $data_model['title_excel'] = $sheetData[2]['B'];

                        $data_model['Date_Pickup'] = isset($data['A']) ? $data['A'] : 0;
                        $data_model['Mother_WB'] = isset($data['B']) ? $data['B'] : 0;
                        $data_model['HAWB_No'] = isset($data['C']) ? $data['C'] : 0;
                        $data_model['Shipper_Company'] = isset($data['D']) ? $data['D'] : 0;
                        $data_model['Cust_ID'] = isset($data['E']) ? $data['E'] : 0;
                        $data_model['Consignee_Company'] = isset($data['F']) ? $data['F'] : 0;

                        // $data_model['event_date'] =  $event_date;
                        $data_model['Consignee_Address'] = isset($data['G']) ? $data['G'] : 0;
                        $data_model['Account'] = isset($data['H']) ? $data['H'] : 0;
                        $data_model['Origin'] = isset($data['I']) ? $data['I'] : 0;
                        $data_model['Destination'] = isset($data['J']) ? $data['J'] : 0;
                        $data_model['M_IATA'] = isset($data['K']) ? $data['K'] : 0;
                        // $data_model['Startclock Date'] = isset($data['K'])  && $data['K'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['K'])->format('Y-m-d') : 0;

                        $data_model['Destination_Ctry'] = isset($data['L']) ? $data['L'] : 0;
                        // $data_model['Stopclock Date'] = isset($data['L'])  && $data['L'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['L'])->format('Y-m-d') : 0;

                        // $data_model['Stopclock Day of Week'] = "";
                        $data_model['Destination_City'] = isset($data['M']) ? $data['M'] : 0;
                        $data_model['Postal_Code'] = isset($data['N']) ? $data['N'] : 0;
                        // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                        $data_model['Peice'] = isset($data['O']) ? $data['O'] : 0;
                        $data_model['A_Weight'] = isset($data['P']) ? $data['P'] : 0;
                        $data_model['V_Weight'] = isset($data['Q']) ? $data['Q'] : 0;
                        $data_model['Product'] = isset($data['R']) ? $data['R'] : 0;
                        $data_model['Delivery'] = isset($data['S']) ? $data['S'] : 0;
                        $data_model['Shipment_Status'] = isset($data['T']) ? $data['T'] : 0;
                        $data_model['POD'] = isset($data['U']) ? $data['U'] : 0;
                        // $data_model['Billed Weight'] = "";

                        $data_model['Status_Date'] = isset($StatusDate) ? date('Y-m-d', strtotime($StatusDate)) : 0;
                        // echo $data['V'],date('Y-m-d',strtotime($data['V']));die();
                        // $data_model['Status_Date'] = isset($data['V'])  && $data['V'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['V'])->format('Y-m-d') : 0;
                        // print_r(date('d/m/Y',$data['V']));die();
                        $data_model['Status_Time'] = isset($data['W']) ? $data['W'] : 0;
                        $data_model['Pickup_Date'] = isset($PickupDate) && $PickupDate != '' ? DateTime::createFromFormat('d/m/Y', $PickupDate)->format('Y-m-d') : 0;

                        // $ymd = DateTime::createFromFormat('d/m/Y', $data['X'])->format('Y-m-d');
                        // echo $ymd;die();
                        // echo $data['X'],date('Y-m-d',strtotime($data['X']));die();

                        // $data_model['Pickup_Date'] = isset($data['X'])  && $data['X'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['X'])->format('Y-m-d') : 0;

                        $data_model['SLA_C'] = isset($data['Y']) ? $data['Y'] : 0;
                        $data_model['SLA_Remark'] = isset($data['Z']) ? $data['Z'] : 0;

                        $data_model['Incident_Remark'] = isset($data['AA']) ? $data['AA'] : 0;
                        $data_model['FD_Remark'] = isset($data['AB']) ? $data['AB'] : 0;



                        // $data_model['Currency Code of Declared Value'] = isset($data['V']) ? $data['V'] : 0;
                        // $data_model['Billing Account Number'] = isset($data['W']) ? $data['W'] : 0;
                        // $data_model['column_ad'] = isset($data['AD']) ? $data['AD'] : 0;
                        // $data_model['em_sales_order_id'] = isset($data['AE']) ? $data['AE'] : 0;
                        // $data_model['em_origin'] = isset($data['AF']) ? $data['AF'] : 0;
                        // $data_model['em_current_event_name'] = isset($data['AG']) ? $data['AG'] : 0;
                        // $data_model['em_current_event_datetime'] = isset($data['AH']) && $data['AH'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AH'])->format('Y-m-d H:i:s') : 0;
                        // $data_model['em_current_event_reason_code'] = isset($data['AI']) ? $data['AI'] : 0;
                        // $data_model['em_current_event_location_description'] = isset($data['AJ']) ? $data['AJ'] : 0;
                        // $data_model['dq_postship_carrier'] = isset($data['AK']) ? $data['AK'] : 0;
                        // $data_model['ops_channel_level_2_code'] = isset($data['AL']) ? $data['AL'] : 0;
                        // $data_model['em_ship_condition_code'] = isset($data['AM']) ? $data['AM'] : 0;
                        // $data_model['em_ship_point_type'] = isset($data['AN']) ? $data['AN'] : 0;
                        // $data_model['em_ship_point_code'] = isset($data['AO']) ? $data['AO'] : 0;
                        // $data_model['em_city_name'] = isset($data['AP']) ? $data['AP'] : 0;
                        // $data_model['em_postal_code'] = isset($data['AQ']) ? $data['AQ'] : 0;
                        // $data_model['em_ship_confirmation_last_date'] = isset($data['AR']) && $data['AR'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AR'])->format('Y-m-d') : 0;

                        // $data_model['em_ship_confirmation_last_time'] = isset($data['AS']) && $data['AS'] != '' ? PHPExcel_Style_NumberFormat::toFormattedString($data['AS'], 'hh:mm:ss') : 0;
                        // $data_model['em_shipment_picked_up_last_datetime'] = isset($data['AT']) && $data['AT'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AT'])->format('Y-m-d H:i:s') : 0;
                        // $data_model['em_uplift_last_datetime'] = isset($data['AU']) && $data['AU'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AU'])->format('Y-m-d H:i:s') : null;
                        // $data_model['em_first_attempt_date'] = isset($data['AV']) && $data['AV'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AV'])->format('Y-m-d') : null;
                        // $data_model['em_first_attempt_reason_code'] = isset($data['AW']) ? $data['AW'] : 0;
                        // $data_model['em_first_attempt_reason_code_description'] = isset($data['AX']) ? $data['AX'] : 0;
                        // $data_model['em_delivery_not_completed_last_date'] = isset($data['AY']) && $data['AY'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AY'])->format('Y-m-d') : null;
                        // $data_model['em_delivery_not_completed_last_reason_code_description'] = isset($data['AZ']) ? $data['AZ'] : 0;
                        // $data_model['em_shipment_delay_last_date'] = isset($data['BA']) && $data['BA'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['BA'])->format('Y-m-d') : null;
                        // $data_model['em_shipment_delay_last_reason_code'] = isset($data['BB']) ? $data['BB'] : 0;
                        // $data_model['em_shipment_delay_last_reason_code_description'] = isset($data['BC']) ? $data['BC'] : 0;
                        // $data_model['em_count_of_delivery_attempts'] = isset($data['BD']) ? $data['BD'] : 0;

                        $data_model['create_date'] = $date;
                        $data_model['file_name'] = $file;

                        // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                        //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                        // }

                        $nResult = $this->ImportDailyModel->insert_to_temp_shipment_daily($data_model);

                        // $datastatusid = $this->ImportDailyModel->check_status_date($data_model);


                        $dataid = $this->ImportDailyModel->check_duplicate($data_model['HAWB_No']);

                        if ($dataid == 0) {
                            $this->ImportDailyModel->insert_t_shipment_daily($data_model);
                            $this->ImportDailyModel->delete_t_temp_shipment_daily();
                        }
                    } else {
                        // if ($data['BO'] == '') {
                        //     $formattype = 1;
                        // } else if ($data['BO'] != '') {
                        //     $formattype = 2;
                        // } else if ($data['A'] != 'Order Item Id') {
                        //     $result['status'] = false;
                        //     $result['message'] = 'Wrong file format';
                        //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
                        //     exit;
                        // }

                        // echo $formattype;
                        // die();
                    }



                    // if ($nResult > 0) {
                    //     $result['status'] = true;
                    //     $result['message'] = $this->lang->line('savesuccess');
                    // } else {
                    //     $result['status'] = false;
                    //     $result['message'] = $this->lang->line('error');
                    // }
                    // $result[$row_count] = true;
                } catch (Exception $ex) {
                    $result[$row_count] = false;
                    $result['message' . $row_count] = $ex;
                }

                $row_count++;
                $count++;
                // $row_count=$row_count+1;
                // die();
            }
            // $check_duplicate = $this->ImportDailyModel->check_duplicate();

            $update_shipment_daily = $this->ImportDailyModel->update_t_shipment_daily();

            // $datastatusid = $this->ImportDailyModel->check_status_date($data_model);
            // print_r($datastatusid);die();


            $delete_temp_shipment_daily = $this->ImportDailyModel->delete_t_temp_shipment_daily();



            // $delete_shipment_daily = $this->ImportDailyModel->delete_temp_pandora_dashboard();

            // $dataimpot['file_name'] =  $file;
            // $dataimpot['type'] =  'Excel Shipment Daily Upload';
            // $dataimpot['upload_date'] =  $date;


            if ($FlagFormat == 1) {
                $result['status'] = false;
                $result['message'] =  $ErrorMassage;
            } else {
                $dataimpot['file_name'] =  $file;
                $dataimpot['type'] =  'Excel Shipment Daily Upload';
                $dataimpot['upload_date'] =  $date;
                $nResult = $this->ImportDailyModel->insert_log_shipment($dataimpot);
                $this->ImportDailyModel->update_t_shipment_set_postal_code();

                $result['status'] = true;
                $result['message'] = $this->lang->line('savesuccess');
            };
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }






    public function getdailyModelList()
    {
        try {
            $this->load->model('ImportDailyModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ImportDailyModel->getdailyModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            $result['totalRecords'] = $this->ImportDailyModel->getTotal($dataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function alert_line_notify()
    {
        // call line api
        $Token = 'UeMJwWbMswxUmnEA08yzzgGhWAuszabSpVT8BtUHvpr';
        $message = 'ImportDaily'.' '. date('Y-m-d');
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . ''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
        } else {
            $result_ = json_decode($result, true);
        }
        curl_close($chOne);
    }

    public function deleteImport()
    {
        try {
            $this->load->model('ImportDailyModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);die();
            $dataModel = isset($dataPost['file_name']) ? $dataPost['file_name'] : '';
            // print_r($dataModel);die();
            $result['status'] = true;
            $result['message'] = $this->ImportDailyModel->deleteImport($dataModel);
            $result['ShowMessage'] = 'The deletion was successful.';
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

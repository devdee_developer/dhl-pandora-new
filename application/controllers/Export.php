<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Export extends MY_Controller
{
    private $tbl_name = 'inventory';
    private $id = 'id';

    public function __construct()
    {
        // ini_set('memory_limit', '1024M');
        ini_set('memory_limit', '4095M'); 
        ini_set('MAX_EXECUTION_TIME', 0);

        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
        ini_set('memory_limit', '1024M');
    }

    public function index()
    {
        // echo"hello";die();
        $role_data =  $this->session->userdata('role_PANDASH');
        // if (!$role_data['EFILE']) {
        //     redirect('Login');
        // }
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('export/export_view');
        $this->load->view('share/footer');
    }


    public function upload_file()
    {
        // print_r($_FILES['file_Pandora_Dashboard']);
        //  print_r('test');die();
        // $uniqid = uniqid();
        // $datapandora['uniqidpandora'] = $uniqid;
        // $datashipment['uniqidshipment'] = $uniqid;
        $date = date('Y-m-d-H-i-s');

        // $this->load->model('HeadpandoraModel', '', true);
        // $this->load->model('HeadShipmentModel', '', true);

        // $uniqidpandora = $this->HeadpandoraModel->insert($datapandora);
        // $uniqidshipment = $this->HeadShipmentModel->insert($datashipment);

        // $this->upload_filepandora($_FILES, $date, $uniqid);
        // $this->upload_fileshipment($_FILES, $date, $uniqid);
        $todayfile = 'Pandora_Dashboard' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/import_excel';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_Pandora_Dashboard']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);



        $status = true;
        if (!$this->upload->do_upload('file_Pandora_Dashboard')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();
            $todayfile = $data['upload_data']['file_name'];
            $this->loadExcelpandoraDashboardToDB($todayfile, $date);
        }
    }


    public function loadExcelpandoraDashboardToDB($file, $date)
    {
        $formattype = 0;
        ini_set('max_execution_time', 300);

        $this->load->library('MyExcel');
        $this->load->model('ExportModel', '', true);
        // print_r($file);
        $objReader = PHPExcel_IOFactory::load('upload/import_excel/' . $file);

        // die();

        $sheetData = $objReader->getActiveSheet()->toArray('', true, false, true);

        // print_r($sheetData);die();

        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        $this->ExportModel->delete_temp_pandora_dashboard();

        // echo 'delete_temp_pandora_dashboard success';die();
        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        // print_r($sheetData[4]);die();

        foreach ($sheetData as $data) {

            try {

                if (
                    $count == 0
                    // and  ("NAME" == trim(strtoupper($data['A'])))

                    and  (strtoupper("Waybill Number") ==  trim(strtoupper($data['A'])))
                    and  (strtoupper("Product Code") ==  trim(strtoupper($data['B'])))
                    and  (strtoupper("Product Group") ==  trim(strtoupper($data['C'])))
                    and  (strtoupper("Shipper Reference") ==  trim(strtoupper($data['D'])))
                    and  (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['E'])))
                    and  (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['F'])))

                    and  (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['G'])))
                    and  (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['H'])))
                    and  (strtoupper("Number of Days in Customs") ==  trim(strtoupper($data['I'])))
                    // and  (strtoupper("Pickup day") ==  trim(strtoupper($data['J'])))
                    and  ((strtoupper("Pickup day") ==  trim(strtoupper($data['J']))) or  (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['J']))))
                    and  (strtoupper("Startclock Date") ==  trim(strtoupper($data['K'])))
                    and  (strtoupper("Stopclock Date") ==  trim(strtoupper($data['L'])))
                    and  (strtoupper("Signatory") ==  trim(strtoupper($data['M'])))
                    and  (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['N'])))
                    and  (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['O'])))
                    and  (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['P'])))
                    and  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['Q'])))
                    and  (strtoupper("Consignee Address") ==  trim(strtoupper($data['R'])))
                    and  (strtoupper("Consignee Zip") ==  trim(strtoupper($data['S'])))
                    and  (strtoupper("Consignee City") ==  trim(strtoupper($data['T'])))
                    and  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U'])))
                    and  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['V'])))
                    and  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['W'])))
                    and  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X'])))

                ) {
                    // echo "found data <br/>";
                    // echo'case 1';
                    $fuondFormat = 1;

                    // $this->JobTypeModel->dropJobType();
                } else if ($count == 0) {
                    // print_r($data);
                    // echo'SCAC';
                    // echo trim(strtoupper($data['E']));
                    $ErrorMassage = 'Your file has an out-of-format format in the column ';
                    $FlagFormat = true;

                    $ErrorMassage .=  (strtoupper("Waybill Number") ==  trim(strtoupper($data['A']))) ? "" : " A ";
                    $ErrorMassage .=  (strtoupper("Product Code") ==  trim(strtoupper($data['B']))) ? "" : " B ";
                    $ErrorMassage .=  (strtoupper("Product Group") ==  trim(strtoupper($data['C']))) ? "" : " C ";
                    $ErrorMassage .=  (strtoupper("Shipper Reference") ==  trim(strtoupper($data['D']))) ? "" : " D ";
                    $ErrorMassage .=  (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['E']))) ? "" : " E ";
                    $ErrorMassage .=  (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['F']))) ? "" : " E ";

                    $ErrorMassage .=  (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['G']))) ? "" : " G ";
                    $ErrorMassage .=  (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['H']))) ? "" : " H ";
                    $ErrorMassage .=  (strtoupper("Number of Days in Customs") ==  trim(strtoupper($data['I']))) ? "" : " I ";
                    $ErrorMassage .=  ((strtoupper("Pickup day") ==  trim(strtoupper($data['J']))) or  (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['J'])))) ? "" : " J ";
                    $ErrorMassage .=  (strtoupper("Startclock Date") ==  trim(strtoupper($data['K']))) ? "" : " K ";
                    $ErrorMassage .=  (strtoupper("Stopclock Date") ==  trim(strtoupper($data['L']))) ? "" : " L ";
                    $ErrorMassage .=  (strtoupper("Signatory") ==  trim(strtoupper($data['M']))) ? "" : " M ";
                    $ErrorMassage .=  (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['N']))) ? "" : " N ";
                    $ErrorMassage .=  (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['O']))) ? "" : " O ";
                    $ErrorMassage .=  (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['P']))) ? "" : " P ";
                    $ErrorMassage .=  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['Q']))) ? "" : " Q ";
                    $ErrorMassage .=  (strtoupper("Consignee Address") ==  trim(strtoupper($data['R']))) ? "" : " R ";
                    $ErrorMassage .=  (strtoupper("Consignee Zip") ==  trim(strtoupper($data['S']))) ? "" : " S ";
                    $ErrorMassage .=  (strtoupper("Consignee City") ==  trim(strtoupper($data['T']))) ? "" : " T ";
                    $ErrorMassage .=  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U']))) ? "" : " U ";
                    $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                    $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['W']))) ? "" : " W ";
                    $ErrorMassage .=  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X']))) ? "" : " X ";

                    // echo ("LIMIT" ==  trim(strtoupper($data['B']))) ? "T" : "F";
                    // echo "Miss data format ";
                    break;
                    // exit();
                }

                // print_r($FlagFormat);die();

                if ($row_count > 1) {


                    // $data_model['title_excel'] = $sheetData[2]['B'];

                    $data_model['Waybill Number'] = isset($data['A']) ? $data['A'] : 0;
                    $data_model['Product Code'] = isset($data['B']) ? $data['B'] : 0;
                    $data_model['Product Name'] = isset($data['C']) ? $data['C'] : 0;
                    $data_model['Shipper Reference'] = isset($data['D']) ? $data['D'] : 0;
                    $data_model['Shipper Company Name'] = isset($data['E']) ? $data['E'] : 0;
                    $data_model['Destination Country/Territory Name'] = isset($data['F']) ? $data['F'] : 0;

                    // $data_model['event_date'] =  $event_date;
                    $data_model['Destination Country/Territory Area Code'] = isset($data['G']) ? $data['G'] : 0;
                    $data_model['Destination Service Area Code'] = isset($data['H']) ? $data['H'] : 0;
                    $data_model['Number of Days in Customs'] = isset($data['I']) ? $data['I'] : 0;
                    $data_model['Startclock Day of Week'] = isset($data['J']) ? $data['J'] : 0;
                    $data_model['Startclock Date'] = isset($data['K']) ? $data['K'] : 0;
                    $data_model['Stopclock Date'] = isset($data['L']) ? $data['L'] : 0;
                    $data_model['Stopclock Day of Week'] = "";
                    $data_model['Signatory'] = isset($data['M']) ? $data['M'] : 0;
                    $data_model['Elapsed Actual Transit Days'] = isset($data['N']) ? $data['N'] : 0;
                    // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                    $data_model['Reporting Code Description'] = isset($data['O']) ? $data['O'] : 0;
                    $data_model['Reporting Code Category Description'] = isset($data['P']) ? $data['P'] : 0;
                    $data_model['Consignee Company Name'] = isset($data['Q']) ? $data['Q'] : 0;
                    $data_model['Consignee Address'] = isset($data['R']) ? $data['R'] : 0;
                    $data_model['Consignee Zip'] = isset($data['S']) ? $data['S'] : 0;
                    $data_model['Consignee City'] = isset($data['T']) ? $data['T'] : 0;
                    $data_model['Currency Code of Declared Value'] = isset($data['W']) ? $data['W'] : 0;
                    $data_model['Manifested Number of Pieces'] = isset($data['U']) ? $data['U'] : 0;
                    $data_model['Billed Weight'] = "";
                    $data_model['Customs Declared Value'] = isset($data['X']) ? $data['X'] : 0;
                    $data_model['Billing Account Number'] = isset($data['V']) ? $data['V'] : 0;
                    // $data_model['column_ad'] = isset($data['AD']) ? $data['AD'] : 0;
                    // $data_model['em_sales_order_id'] = isset($data['AE']) ? $data['AE'] : 0;
                    // $data_model['em_origin'] = isset($data['AF']) ? $data['AF'] : 0;
                    // $data_model['em_current_event_name'] = isset($data['AG']) ? $data['AG'] : 0;
                    // $data_model['em_current_event_datetime'] = isset($data['AH']) && $data['AH'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AH'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_current_event_reason_code'] = isset($data['AI']) ? $data['AI'] : 0;
                    // $data_model['em_current_event_location_description'] = isset($data['AJ']) ? $data['AJ'] : 0;
                    // $data_model['dq_postship_carrier'] = isset($data['AK']) ? $data['AK'] : 0;
                    // $data_model['ops_channel_level_2_code'] = isset($data['AL']) ? $data['AL'] : 0;
                    // $data_model['em_ship_condition_code'] = isset($data['AM']) ? $data['AM'] : 0;
                    // $data_model['em_ship_point_type'] = isset($data['AN']) ? $data['AN'] : 0;
                    // $data_model['em_ship_point_code'] = isset($data['AO']) ? $data['AO'] : 0;
                    // $data_model['em_city_name'] = isset($data['AP']) ? $data['AP'] : 0;
                    // $data_model['em_postal_code'] = isset($data['AQ']) ? $data['AQ'] : 0;
                    // $data_model['em_ship_confirmation_last_date'] = isset($data['AR']) && $data['AR'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AR'])->format('Y-m-d') : 0;

                    // $data_model['em_ship_confirmation_last_time'] = isset($data['AS']) && $data['AS'] != '' ? PHPExcel_Style_NumberFormat::toFormattedString($data['AS'], 'hh:mm:ss') : 0;
                    // $data_model['em_shipment_picked_up_last_datetime'] = isset($data['AT']) && $data['AT'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AT'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_uplift_last_datetime'] = isset($data['AU']) && $data['AU'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AU'])->format('Y-m-d H:i:s') : null;
                    // $data_model['em_first_attempt_date'] = isset($data['AV']) && $data['AV'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AV'])->format('Y-m-d') : null;
                    // $data_model['em_first_attempt_reason_code'] = isset($data['AW']) ? $data['AW'] : 0;
                    // $data_model['em_first_attempt_reason_code_description'] = isset($data['AX']) ? $data['AX'] : 0;
                    // $data_model['em_delivery_not_completed_last_date'] = isset($data['AY']) && $data['AY'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AY'])->format('Y-m-d') : null;
                    // $data_model['em_delivery_not_completed_last_reason_code_description'] = isset($data['AZ']) ? $data['AZ'] : 0;
                    // $data_model['em_shipment_delay_last_date'] = isset($data['BA']) && $data['BA'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['BA'])->format('Y-m-d') : null;
                    // $data_model['em_shipment_delay_last_reason_code'] = isset($data['BB']) ? $data['BB'] : 0;
                    // $data_model['em_shipment_delay_last_reason_code_description'] = isset($data['BC']) ? $data['BC'] : 0;
                    // $data_model['em_count_of_delivery_attempts'] = isset($data['BD']) ? $data['BD'] : 0;

                    $data_model['create_date'] = $date;
                    $data_model['file_name'] = $file;

                    // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                    //     $nResult = $this->ExportModel->insert_to_temp_shipment($data_model);
                    // }

                    $nResult = $this->ExportModel->insert_to_temp_shipment($data_model);
                } else {
                    // if ($data['BO'] == '') {
                    //     $formattype = 1;
                    // } else if ($data['BO'] != '') {
                    //     $formattype = 2;
                    // } else if ($data['A'] != 'Order Item Id') {
                    //     $result['status'] = false;
                    //     $result['message'] = 'Wrong file format';
                    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    //     exit;
                    // }

                    // echo $formattype;
                    // die();
                }



                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            $row_count++;
            $count++;
            // $row_count=$row_count+1;
            // die();
        }

        $check_duplicate = $this->ExportModel->check_duplicate();

        // print_r( $check_duplicate);

        if ($FlagFormat == true) {
            $result['status'] = false;
            $result['message'] =  $ErrorMassage;
        } else if (count($check_duplicate) > 0) {
            $result['status'] = false;
            $result['message'] = 'Duplicate data found from file. That has already been uploaded';
            // $this->ExportModel->DeleteTableTemp();
        } else {
            $dataimpot['file_name'] =  $file;
            $dataimpot['type'] =  'Excel Upload';
            $dataimpot['upload_date'] =  $date;
            $this->ExportModel->insertto_t_shipment();
            // $this->ExportModel->insertto_customer($file, $date);
            $nResult = $this->ExportModel->insert_log_shipment($dataimpot);
            // $this->ExportModel->DeleteTableTemp();
            $result['status'] = true;
            $result['message'] = $this->lang->line('savesuccess');
        };
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }






    // public function getdailyModelList()
    // {
    //     try {
    //         $this->load->model('ExportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);

    //         $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
    //         $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
    //         $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
    //         $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
    //         $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

    //         $offset = ($PageIndex - 1) * $PageSize;

    //         $result['status'] = true;
    //         $result['message'] = $this->ExportModel->getdailyModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
    //         // $result['totalRecords'] = $this->CashadvanceModel->getTotal($dataModel);
    //         // $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }


    public function getWeekComboList()
    {
        try {
            $this->load->model('ExportModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 70;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ExportModel->getWeekModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            // $result['totalRecords'] = $this->CashadvanceModel->getTotal($dataModel);
            // $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }


    public function getYearComboList()
    {
        try {
            $this->load->model('ExportModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ExportModel->getYearModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            // $result['totalRecords'] = $this->CashadvanceModel->getTotal($dataModel);
            // $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getMonthComboList()
    {
        try {
            $this->load->model('ExportModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ExportModel->getMonthModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            // $result['totalRecords'] = $this->CashadvanceModel->getTotal($dataModel);
            // $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }


    // public function downloadpandora()
    // {
    //     try {
    //         $this->load->model('DailyimportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);
    //         // print_r($dataPost);die();
    //         $dataModel = isset($dataPost['id']) ? $dataPost['id'] : '';

    //         $result['status'] = true;
    //         $result['message'] = $this->DailyimportModel->downloadpandora($dataModel);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }

    // public function downloadshipment()
    // {
    //     try {
    //         $this->load->model('DailyimportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);
    //         // print_r($dataPost);die();
    //         $dataModel = isset($dataPost['id']) ? $dataPost['id'] : '';

    //         $result['status'] = true;
    //         $result['message'] = $this->DailyimportModel->downloadshipment($dataModel);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }
    // public function deleteExport()
    // {
    //     try {
    //         $this->load->model('ExportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);
    //         // print_r($dataPost);die();
    //         $dataModel = isset($dataPost['file_name']) ? $dataPost['file_name'] : '';
    //         // print_r($dataModel);die();
    //         $result['status'] = true;
    //         $result['message'] = $this->ExportModel->deleteExport($dataModel);
    //         $result['ShowMessage'] = 'The deletion was successful.';
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }



    public function Exportexcel()
    {
        $this->alert_line_notify_export_Weekly_report();
        // log_message('error', 'Exportexcel');
        // ini_set("memory_limit", "10000M");
        // ini_set('MAX_EXECUTION_TIME', 0);

        try {
            $this->load->model('ExportModel', '', true);

            $this->load->library('MyExcel');

            $dataPost = json_decode($this->input->raw_input_stream, true);

            // print_r($dataPost);
            // die();

            // $date = date('Y-m-d-H-i-s');
            // print_r($date);
            // die();

            // $date = '2021-07-28';

            // log_message('error', 'Can get date');
            // $data = [];
            $data = $this->ExportModel->ExportExcel($dataPost);
            // log_message('error', 'Can get data');
            // print_r($data);
            // die();

            //2021-07-29//
            // ini_set('max_execution_time', 300); //300 seconds = 5 minutes
            // set_time_limit(300);
            //2021-07-29//

            // print_r($data[0]['file_name']);die();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Sheet1");

            // die();

            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ]

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => '00ffff')
                // )
            );

            $headcolor = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'CBFFB0')
                )
            );

            $headcoloryellow = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'F6FFB0')
                )
            );

            $headcoloryellow2 = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E4FB32')
                )
            );


            $headbody = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => 'ffff00')
                // )
            );
            // //กำหนด style ส่วน body//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_THIN
            //         )
            //     )
            // );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);

            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setAutoSize(TRUE);



            // if (isset($dataModel['end_date']) && $dataModel['end_date'] != "") {
            //     $EndDateTS = strtotime($dataModel['end_date']);
            //     if ($EndDateTS !== false) {
            //         date('Y-m-d', $EndDateTS);
            //     }
            // }
            // if (isset($dataModel['date']) && $dataModel['date'] != "") {
            //     $StarDateTS = strtotime($dataModel['date']);
            //     if ($StarDateTS !== false) {
            //         date('Y-m-d', $StarDateTS);
            //     }
            // }
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'A')
                // ->setCellValue('B1', 'Group Over SLA ( Exclude weekend )')
                // ->setCellValue('C1', 'Group Over SLA ( Include weekend )')
                ->setCellValue('B1', 'Group Diff SLA ( Exclude weekend )')
                ->setCellValue('C1', 'Group Diff SLA ( Include weekend )')
                ->setCellValue('D1', 'Reporting Code Description')
                ->setCellValue('E1', 'factor_incident')
                ->setCellValue('F1', 'On time status')
                ->setCellValue('G1', 'Group T/T Include weekend range')
                ->setCellValue('H1', 'factor_incident_cal')
                ->setCellValue('I1', 'factor')
                ->setCellValue('J1', 'lane')
                ->setCellValue('K1', 'Diff SLA ( Include weekend )')
                ->setCellValue('L1', 'Diff SLA ( Exclude weekend )')
                ->setCellValue('M1', 'T/T included weekend by_pickup_day')
                ->setCellValue('N1', 'T/T excluded weekend')
                ->setCellValue('O1', 'T/T included weekend')
                ->setCellValue('P1', 'delivery_provider ')
                ->setCellValue('Q1', 'gateway')
                ->setCellValue('R1', 'shipmonth')
                ->setCellValue('S1', 'Quater')
                ->setCellValue('T1', 'Year')
                ->setCellValue('U1', 'week_number')
                ->setCellValue('V1', 'Waybill Number')
                ->setCellValue('W1', 'Product Code')
                ->setCellValue('X1', 'Product Name')
                ->setCellValue('Y1', 'Shipper Reference')
                ->setCellValue('Z1', 'Shipper Company Name')
                ->setCellValue('AA1', 'Destination Country/Territory Name')
                ->setCellValue('AB1', 'Destination Country/Territory Area Code')
                ->setCellValue('AC1', 'Destination Service Area Code')
                ->setCellValue('AD1', 'Number of Days in Customs')
                // ->setCellValue('AE1', 'Pickup day')Startclock Day of Week
                ->setCellValue('AE1', 'Startclock Day of Week')

                ->setCellValue('AF1', 'Startclock Date')
                ->setCellValue('AG1', 'Stopclock Date')
                ->setCellValue('AH1', 'Stopclock Day of Week')
                ->setCellValue('AI1', 'Signatory')
                ->setCellValue('AJ1', 'Elapsed Actual Transit Days')
                ->setCellValue('AK1', 'Reporting Code Description')
                ->setCellValue('AL1', 'Reporting Code Category Description')
                ->setCellValue('AM1', 'Consignee Company Name')
                ->setCellValue('AN1', 'Consignee Address')
                ->setCellValue('AO1', 'Consignee Zip')
                ->setCellValue('AP1', 'Consignee City ')
                ->setCellValue('AQ1', 'Billing Account Number')
                ->setCellValue('AR1', 'Manifested Number of Pieces')
                ->setCellValue('AS1', 'Billed Weight')
                ->setCellValue('AT1', 'Customs Declared Value')
                ->setCellValue('AU1', 'Currency Code of Declared Value')
                ->setCellValue('AV1', 'Unknown POD Status')
                ->setCellValue('Aw1', 'State Region');


            // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($headbody);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headcoloryellow2);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headcoloryellow2);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($headtable);

            $objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AN1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AO1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AP1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AQ1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AR1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AS1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AT1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AU1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AV1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AW1')->applyFromArray($headtable);


            $start_row = 2;
            foreach ($data as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Q' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('R' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('S' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('T' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('U' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('V' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('W' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('X' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Y' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Z' . $start_row)->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->getStyle('AA' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AB' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AC' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AD' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AE' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AP' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AQ' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AR' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AS' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AT' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AU' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AV' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AW' . $start_row)->applyFromArray($styleArray);



                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $start_row, 0)
                    ->setCellValue('B' . $start_row, $row['Group Diff SLA ( Exclude weekend )'])
                    // ->setCellValue('A' . $start_row, "Test");
                    ->setCellValue('C' . $start_row, $row['Group Diff SLA ( Include weekend )'])
                    ->setCellValue('D' . $start_row, $row['Reporting Code Description'])
                    ->setCellValue('E' . $start_row, $row['factor_incident'])
                    ->setCellValue('F' . $start_row, $row['On time status'])
                    ->setCellValueExplicit("G" . $start_row, $row['Group T/T Include weekend range'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('H' . $start_row, $row['factor_incident_cal'])
                    ->setCellValue('I' . $start_row, $row['factor'])
                    ->setCellValue('J' . $start_row, $row['lane'])
                    ->setCellValue('K' . $start_row, $row['Diff SLA ( Include weekend )'])
                    ->setCellValue('L' . $start_row, $row['Diff SLA ( Exclude weekend )'])
                    ->setCellValue('M' . $start_row, $row['T/T included weekend by_pickup_day'])
                    ->setCellValue('N' . $start_row, $row['T/T excluded weekend'])
                    ->setCellValue('O' . $start_row, $row['T/T included weekend'])
                    ->setCellValue('P' . $start_row, $row['delivery_provider'])
                    ->setCellValue('Q' . $start_row, $row['gateway'])
                    ->setCellValue('R' . $start_row, $row['shipmonth'])
                    ->setCellValue('S' . $start_row, $row['Quater'])
                    ->setCellValue('T' . $start_row, $row['Year'])
                    ->setCellValue('U' . $start_row, $row['week_number'])
                    ->setCellValue('V' . $start_row, $row['Waybill Number'])
                    ->setCellValue('W' . $start_row, $row['Product Code'])
                    ->setCellValue('X' . $start_row, $row['Product Name'])
                    ->setCellValue('Y' . $start_row, $row['Shipper Reference'])
                    ->setCellValue('Z' . $start_row, $row['Shipper Company Name'])
                    ->setCellValue('AA' . $start_row, $row['Destination Country/Territory Name'])
                    ->setCellValue('AB' . $start_row, $row['Destination Country/Territory Area Code'])
                    ->setCellValue('AC' . $start_row, $row['Destination Service Area Code'])
                    ->setCellValue('AD' . $start_row, $row['Number of Days in Customs'])
                    ->setCellValue('AE' . $start_row, $row['Startclock Day of Week'])
                    ->setCellValue('AF' . $start_row, $row['Startclock Date'])
                    ->setCellValue('AG' . $start_row, $row['Stopclock Date'])
                    ->setCellValue('AH' . $start_row, $row['Stopclock Day of Week'])
                    ->setCellValue('AI' . $start_row, $row['Signatory'])
                    ->setCellValue('AJ' . $start_row, $row['Elapsed Actual Transit Days'])
                    ->setCellValue('AK' . $start_row, $row['Reporting Code Description'])
                    ->setCellValue('AL' . $start_row, $row['Reporting Code Category Description'])
                    ->setCellValue('AM' . $start_row, $row['Consignee Company Name'])
                    ->setCellValue('AN' . $start_row, $row['Consignee Address'])
                    ->setCellValue('AO' . $start_row, $row['Consignee Zip'])
                    ->setCellValue('AP' . $start_row, $row['Consignee City'])
                    ->setCellValue('AQ' . $start_row, $row['Billing Account Number'])
                    ->setCellValue('AR' . $start_row, $row['Manifested Number of Pieces'])
                    ->setCellValue('AS' . $start_row, $row['Billed Weight'])
                    ->setCellValue('AT' . $start_row, $row['Customs Declared Value'])
                    ->setCellValue('AU' . $start_row, $row['Currency Code of Declared Value'])
                    ->setCellValue('AV' . $start_row, $row['Unknown POD Status'])
                    ->setCellValue('AW' . $start_row, $row['state_region']);





                $start_row++;
            }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $filename = $start.' to '.$end . '.xlsx';

            $filename = 'Sample Table.xlsx';

            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);

            $urlShipment = FCPATH . 'upload/export_excel/' . $filename;
            $url = FCPATH . 'upload/PandoraWeeklyReport.xlsm';

            $file_name = basename($url);
            if (file_put_contents($file_name, file_get_contents($url))) {
                $result['status'] = true;
                $result['message'] = $file_name;
            } else {
                $result['status'] = false;
                $result['message'] = "File downloading failed.";
            }

            $excelShipment = basename($urlShipment);

            if (file_put_contents($excelShipment, file_get_contents($urlShipment))) {
                $result['status'] = true;
                $result['message'] = $excelShipment;
            } else {
                $result['status'] = false;
                $result['message'] = "File downloading failed.";
            }

            $this->load->library('zip');
            $fileZip = "PandoraWeeklyReport" . date("Y-m-d-H-i-s") . '.zip';

            $this->zip->read_file($excelShipment);
            $this->zip->read_file($file_name);
            $this->zip->archive(FCPATH . '/upload/pivot/' . $fileZip);

            $result['message'] = $fileZip;
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function ExportexcelMonthly()
    {
        // log_message('error', 'Exportexcel');
        ini_set("memory_limit", "10000M");
        ini_set('MAX_EXECUTION_TIME', 0);

        try {
            $this->load->model('ExportModel', '', true);

            $this->load->library('MyExcel');

            $dataPost = json_decode($this->input->raw_input_stream, true);

            // print_r($dataPost);
            // die();

            // $date = date('Y-m-d-H-i-s');
            // print_r($date);
            // die();

            // $date = '2021-07-28';

            // log_message('error', 'Can get date');
            // $data = [];
            $data = $this->ExportModel->ExportExcelMonthly($dataPost);
            // log_message('error', 'Can get data');
            // print_r($data);
            // die();

            //2021-07-29//
            // ini_set('max_execution_time', 300); //300 seconds = 5 minutes
            // set_time_limit(300);
            //2021-07-29//

            // print_r($data[0]['file_name']);die();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Sheet1");

            // die();

            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ]

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => '00ffff')
                // )
            );

            $headcolor = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'CBFFB0')
                )
            );

            $headcoloryellow = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'F6FFB0')
                )
            );

            $headcoloryellow2 = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E4FB32')
                )
            );


            $headbody = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => 'ffff00')
                // )
            );
            // //กำหนด style ส่วน body//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_THIN
            //         )
            //     )
            // );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);

            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(TRUE);


            // if (isset($dataModel['end_date']) && $dataModel['end_date'] != "") {
            //     $EndDateTS = strtotime($dataModel['end_date']);
            //     if ($EndDateTS !== false) {
            //         date('Y-m-d', $EndDateTS);
            //     }
            // }
            // if (isset($dataModel['date']) && $dataModel['date'] != "") {
            //     $StarDateTS = strtotime($dataModel['date']);
            //     if ($StarDateTS !== false) {
            //         date('Y-m-d', $StarDateTS);
            //     }
            // }
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'A')
                // ->setCellValue('B1', 'Group Over SLA ( Exclude weekend )')
                // ->setCellValue('C1', 'Group Over SLA ( Include weekend )')
                ->setCellValue('B1', 'Group Diff SLA ( Exclude weekend )')
                ->setCellValue('C1', 'Group Diff SLA ( Include weekend )')
                ->setCellValue('D1', 'Reporting Code Description')
                ->setCellValue('E1', 'factor_incident')
                ->setCellValue('F1', 'On time status')
                ->setCellValue('G1', 'Group T/T Include weekend range')
                ->setCellValue('H1', 'factor_incident_cal')
                ->setCellValue('I1', 'factor')
                ->setCellValue('J1', 'lane')
                ->setCellValue('K1', 'Diff SLA ( Include weekend )')
                ->setCellValue('L1', 'Diff SLA ( Exclude weekend )')
                ->setCellValue('M1', 'T/T included weekend by_pickup_day')
                ->setCellValue('N1', 'T/T excluded weekend')
                ->setCellValue('O1', 'T/T included weekend')
                ->setCellValue('P1', 'delivery_provider ')
                ->setCellValue('Q1', 'gateway')
                ->setCellValue('R1', 'shipmonth')
                ->setCellValue('S1', 'Quater')
                ->setCellValue('T1', 'Year')
                ->setCellValue('U1', 'week_number')
                ->setCellValue('V1', 'Waybill Number')
                ->setCellValue('W1', 'Product Code')
                ->setCellValue('X1', 'Product Name')
                ->setCellValue('Y1', 'Shipper Reference')
                ->setCellValue('Z1', 'Shipper Company Name')
                ->setCellValue('AA1', 'Destination Country/Territory Name')
                ->setCellValue('AB1', 'Destination Country/Territory Area Code')
                ->setCellValue('AC1', 'Destination Service Area Code')
                ->setCellValue('AD1', 'Number of Days in Customs')
                // ->setCellValue('AE1', 'Pickup day')Startclock Day of Week
                ->setCellValue('AE1', 'Startclock Day of Week')

                ->setCellValue('AF1', 'Startclock Date')
                ->setCellValue('AG1', 'Stopclock Date')
                ->setCellValue('AH1', 'Stopclock Day of Week')
                ->setCellValue('AI1', 'Signatory')
                ->setCellValue('AJ1', 'Elapsed Actual Transit Days')
                ->setCellValue('AK1', 'Reporting Code Description')
                ->setCellValue('AL1', 'Reporting Code Category Description')
                ->setCellValue('AM1', 'Consignee Company Name')
                ->setCellValue('AN1', 'Consignee Address')
                ->setCellValue('AO1', 'Consignee Zip')
                ->setCellValue('AP1', 'Consignee City ')
                ->setCellValue('AQ1', 'Billing Account Number')
                ->setCellValue('AR1', 'Manifested Number of Pieces')
                ->setCellValue('AS1', 'Billed Weight')
                ->setCellValue('AT1', 'Customs Declared Value')
                ->setCellValue('AU1', 'Currency Code of Declared Value')
                ->setCellValue('AV1', 'Unknown POD Status');

            // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($headbody);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headcoloryellow2);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headcoloryellow2);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($headtable);

            $objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AN1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AO1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AP1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AQ1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AR1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AS1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AT1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AU1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AV1')->applyFromArray($headtable);


            $start_row = 2;
            foreach ($data as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Q' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('R' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('S' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('T' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('U' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('V' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('W' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('X' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Y' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Z' . $start_row)->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->getStyle('AA' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AB' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AC' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AD' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AE' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AP' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AQ' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AR' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AS' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AT' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AU' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AV' . $start_row)->applyFromArray($styleArray);


                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $start_row)
                    ->setCellValue('B' . $start_row, $row['Group Over SLA ( Exclude weekend )'])
                    // ->setCellValue('A' . $start_row, "Test");
                    ->setCellValue('C' . $start_row, $row['Group Over SLA ( Include weekend )'])
                    ->setCellValue('D' . $start_row, $row['Reporting Code Description'])
                    ->setCellValue('E' . $start_row, $row['factor_incident'])
                    ->setCellValue('F' . $start_row, $row['On time status'])
                    ->setCellValueExplicit("G" . $start_row, $row['Group T/T Include weekend range'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('H' . $start_row, $row['factor_incident_cal'])
                    ->setCellValue('I' . $start_row, $row['factor'])
                    ->setCellValue('J' . $start_row, $row['lane'])
                    ->setCellValue('K' . $start_row, $row['Diff SLA ( Include weekend )'])
                    ->setCellValue('L' . $start_row, $row['Diff SLA ( Exclude weekend )'])
                    ->setCellValue('M' . $start_row, $row['T/T included weekend by_pickup_day'])
                    ->setCellValue('N' . $start_row, $row['T/T excluded weekend'])
                    ->setCellValue('O' . $start_row, $row['T/T included weekend'])
                    ->setCellValue('P' . $start_row, $row['delivery_provider'])
                    ->setCellValue('Q' . $start_row, $row['gateway'])
                    ->setCellValue('R' . $start_row, $row['shipmonth'])
                    ->setCellValue('S' . $start_row, $row['Quater'])
                    ->setCellValue('T' . $start_row, $row['Year'])
                    ->setCellValue('U' . $start_row, $row['week_number'])
                    ->setCellValue('V' . $start_row, $row['Waybill Number'])
                    ->setCellValue('W' . $start_row, $row['Product Code'])
                    ->setCellValue('X' . $start_row, $row['Product Name'])
                    ->setCellValue('Y' . $start_row, $row['Shipper Reference'])
                    ->setCellValue('Z' . $start_row, $row['Shipper Company Name'])
                    ->setCellValue('AA' . $start_row, $row['Destination Country/Territory Name'])
                    ->setCellValue('AB' . $start_row, $row['Destination Country/Territory Area Code'])
                    ->setCellValue('AC' . $start_row, $row['Destination Service Area Code'])
                    ->setCellValue('AD' . $start_row, $row['Number of Days in Customs'])
                    ->setCellValue('AE' . $start_row, $row['Startclock Day of Week'])
                    ->setCellValue('AF' . $start_row, $row['Startclock Date'])
                    ->setCellValue('AG' . $start_row, $row['Stopclock Date'])
                    ->setCellValue('AH' . $start_row, $row['Stopclock Day of Week'])
                    ->setCellValue('AI' . $start_row, $row['Signatory'])
                    ->setCellValue('AJ' . $start_row, $row['Elapsed Actual Transit Days'])
                    ->setCellValue('AK' . $start_row, $row['Reporting Code Description'])
                    ->setCellValue('AL' . $start_row, $row['Reporting Code Category Description'])
                    ->setCellValue('AM' . $start_row, $row['Consignee Company Name'])
                    ->setCellValue('AN' . $start_row, $row['Consignee Address'])
                    ->setCellValue('AO' . $start_row, $row['Consignee Zip'])
                    ->setCellValue('AP' . $start_row, $row['Consignee City'])
                    ->setCellValue('AQ' . $start_row, $row['Billing Account Number'])
                    ->setCellValue('AR' . $start_row, $row['Manifested Number of Pieces'])
                    ->setCellValue('AS' . $start_row, $row['Billed Weight'])
                    ->setCellValue('AT' . $start_row, $row['Customs Declared Value'])
                    ->setCellValue('AU' . $start_row, $row['Currency Code of Declared Value'])
                    ->setCellValue('AV' . $start_row, $row['Unknown POD Status']);




                $start_row++;
            }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $filename = $start.' to '.$end . '.xlsx';

            $filename = 'Sample Table.xlsx';

            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);

            $urlShipment = FCPATH . 'upload/export_excel/' . $filename;
            $url = FCPATH . 'upload/PandoraMonthlyReport.xlsm';

            $file_name = basename($url);
            if (file_put_contents($file_name, file_get_contents($url))) {
                $result['status'] = true;
                $result['message'] = $file_name;
            } else {
                $result['status'] = false;
                $result['message'] = "File downloading failed.";
            }

            $excelShipment = basename($urlShipment);

            if (file_put_contents($excelShipment, file_get_contents($urlShipment))) {
                $result['status'] = true;
                $result['message'] = $excelShipment;
            } else {
                $result['status'] = false;
                $result['message'] = "File downloading failed.";
            }

            $this->load->library('zip');
            $fileZip = "PandoraMonthlyReport" . date("Y-m-d-H-i-s") . '.zip';

            $this->zip->read_file($excelShipment);
            $this->zip->read_file($file_name);
            $this->zip->archive(FCPATH . '/upload/pivot/' . $fileZip);

            $result['message'] = $fileZip;
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function ExportexcelDaily()
    {
        // $this->alert_line_notify_export_Pandora_Dashboard();
        // log_message('error', 'Exportexcel');
        // ini_set("memory_limit", "10000M");
        // ini_set('MAX_EXECUTION_TIME', 0);
        // ini_set('memory_limit', '-1');
        // ini_set('max_execution_time', '300');
        set_time_limit(300);

        try {
            $this->load->model('ExportModel', '', true);

            $this->load->library('MyExcel');

            $dataPost = json_decode($this->input->raw_input_stream, true);

            // print_r($dataPost);
            // die();

            // $date = date('Y-m-d-H-i-s');
            // print_r($date);
            // die();

            // $date = '2021-07-28';

            // log_message('error', 'Can get date');
            // $data = [];
            $data = $this->ExportModel->ExportExcelDaily($dataPost);
            // log_message('error', 'Can get data');
            // print_r($data);
            // die();

            //2021-07-29//
            // ini_set('max_execution_time', 300); //300 seconds = 5 minutes
            // set_time_limit(300);
            //2021-07-29//

            // print_r($data[0]['file_name']);die();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Sheet1");

            // die();

            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ]

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => '00ffff')
                // )
            );

            $headcolor = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'CBFFB0')
                )
            );

            $headcoloryellow = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'F6FFB0')
                )
            );

            $headcoloryellow2 = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E4FB32')
                )
            );


            $headbody = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => 'ffff00')
                // )
            );
            // //กำหนด style ส่วน body//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_THIN
            //         )
            //     )
            // );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);

            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(TRUE);


            // if (isset($dataModel['end_date']) && $dataModel['end_date'] != "") {
            //     $EndDateTS = strtotime($dataModel['end_date']);
            //     if ($EndDateTS !== false) {
            //         date('Y-m-d', $EndDateTS);
            //     }
            // }
            // if (isset($dataModel['date']) && $dataModel['date'] != "") {
            //     $StarDateTS = strtotime($dataModel['date']);
            //     if ($StarDateTS !== false) {
            //         date('Y-m-d', $StarDateTS);
            //     }
            // }
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'A')
                ->setCellValue('B1', 'Date_Pickup')
                // ->setCellValue('B1', 'Group Over SLA ( Exclude weekend )')
                // ->setCellValue('C1', 'Group Over SLA ( Include weekend )')
                ->setCellValue('C1', 'Mother_WB')
                ->setCellValue('D1', 'HAWB_No')
                ->setCellValue('E1', 'Shipper_Company')
                ->setCellValue('F1', 'Cust_ID')
                ->setCellValue('G1', 'Consignee_Company')
                ->setCellValue('H1', 'Consignee_Address')
                ->setCellValue('I1', 'Account')
                ->setCellValue('J1', 'Origin')
                ->setCellValue('K1', 'Destination')
                ->setCellValue('L1', 'M_IATA')
                ->setCellValue('M1', 'Destination_Ctry')
                ->setCellValue('N1', 'Destination_City')
                ->setCellValue('O1', 'Postal_Code')
                ->setCellValue('P1', 'Peice')
                ->setCellValue('Q1', 'A_Weight')
                ->setCellValue('R1', 'V_Weight')
                ->setCellValue('S1', 'Product')
                ->setCellValue('T1', 'Delivery')
                ->setCellValue('U1', 'Shipment_Status')
                ->setCellValue('V1', 'POD')
                ->setCellValue('W1', 'Status_Date')
                ->setCellValue('X1', 'Status_Time')
                ->setCellValue('Y1', 'Pickup_Date')
                ->setCellValue('Z1', 'SLA_C')
                ->setCellValue('AA1', 'SLA_Remark')
                ->setCellValue('AB1', 'incident_Remark')
                ->setCellValue('AC1', 'FD_Remark')
                ->setCellValue('AD1', 'week')
                ->setCellValue('AE1', 'Pick_up_day')
                // ->setCellValue('AE1', 'Pickup day')Startclock Day of Week
                ->setCellValue('AF1', 'Month')
                ->setCellValue('AG1', 'Year')

                ->setCellValue('AH1', 'Region')
                ->setCellValue('AI1', 'Gateway')
                ->setCellValue('AJ1', 'delivery_provider')
                ->setCellValue('AK1', 'Actual_T/T')
                ->setCellValue('AL1', 'Group_Diff_SLA')
                ->setCellValue('AM1', 'status')
                ->setCellValue('AN1', 'Reporting_Code_Description')
                ->setCellValue('AO1', 'Reporting_Code_Category')
                ->setCellValue('AP1', 'State Region');
            // ->setCellValue('AO1', 'Consignee Zip')
            // ->setCellValue('AP1', 'Consignee City ')
            // ->setCellValue('AQ1', 'Billing Account Number')
            // ->setCellValue('AR1', 'Manifested Number of Pieces')
            // ->setCellValue('AS1', 'Billed Weight')
            // ->setCellValue('AT1', 'Customs Declared Value')
            // ->setCellValue('AU1', 'Currency Code of Declared Value')
            // ->setCellValue('AV1', 'Unknown POD Status');    

            // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($headbody);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headcoloryellow2);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headcoloryellow2);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($headtable);

            $objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AN1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AO1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AP1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AQ1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AR1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AS1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AT1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AU1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AV1')->applyFromArray($headtable);


            $start_row = 2;
            foreach ($data as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Q' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('R' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('S' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('T' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('U' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('V' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('W' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('X' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Y' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Z' . $start_row)->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->getStyle('AA' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AB' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AC' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AD' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AE' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AP' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AQ' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AR' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AS' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AT' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AU' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AV' . $start_row)->applyFromArray($styleArray);


                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $start_row, 0)
                    ->setCellValue('B' . $start_row, $row['Date_Pickup'])

                    ->setCellValue('C' . $start_row, $row['Mother_WB'])
                    // ->setCellValue('A' . $start_row, "Test");
                    ->setCellValue('D' . $start_row, $row['HAWB_No'])
                    ->setCellValue('E' . $start_row, $row['Shipper_Company'])
                    ->setCellValue('F' . $start_row, $row['Cust_ID'])
                    ->setCellValue('G' . $start_row, $row['Consignee_Company'])
                    // ->setCellValueExplicit("G". $start_row, $row['Group T/T Include weekend range'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('H' . $start_row, $row['Consignee_Address'])
                    ->setCellValue('I' . $start_row, $row['Account'])
                    ->setCellValue('J' . $start_row, $row['Origin'])
                    ->setCellValue('K' . $start_row, $row['Destination'])
                    ->setCellValue('L' . $start_row, $row['M_IATA'])
                    ->setCellValue('M' . $start_row, $row['Destination_Ctry'])
                    ->setCellValue('N' . $start_row, $row['Destination_City'])
                    ->setCellValue('O' . $start_row, $row['Postal_Code'])
                    ->setCellValue('P' . $start_row, $row['Peice'])
                    ->setCellValue('Q' . $start_row, $row['A_Weight'])
                    ->setCellValue('R' . $start_row, $row['V_Weight'])
                    ->setCellValue('S' . $start_row, $row['Product'])
                    ->setCellValue('T' . $start_row, $row['Delivery'])
                    ->setCellValue('U' . $start_row, $row['Shipment_Status'])
                    ->setCellValue('V' . $start_row, $row['POD'])
                    ->setCellValue('W' . $start_row, $row['Status_Date'])
                    ->setCellValue('X' . $start_row, $row['Status_Time'])
                    ->setCellValue('Y' . $start_row, $row['Pickup_Date'])
                    ->setCellValue('Z' . $start_row, $row['SLA_C'])
                    ->setCellValue('AA' . $start_row, $row['SLA_Remark'])
                    ->setCellValue('AB' . $start_row, $row['incident_Remark'])
                    ->setCellValue('AC' . $start_row, $row['FD_Remark'])
                    ->setCellValue('AD' . $start_row, $row['week'])
                    ->setCellValue('AE' . $start_row, $row['Pick_up_day'])
                    ->setCellValue('AF' . $start_row, $row['Month'])
                    ->setCellValue('AG' . $start_row, $row['Year'])
                    ->setCellValue('AH' . $start_row, $row['Region'])
                    ->setCellValue('AI' . $start_row, $row['Gateway'])
                    ->setCellValue('AJ' . $start_row, $row['delivery_provider'])
                    ->setCellValue('AK' . $start_row, $row['Actual_T/T'])
                    ->setCellValue('AL' . $start_row, $row['Group_Diff_SLA'])
                    ->setCellValue('AM' . $start_row, $row['status'])
                    ->setCellValue('AN' . $start_row, $row['Reporting_Code_Description'])
                    ->setCellValue('AO' . $start_row, $row['Reporting_Code_Category'])
                    ->setCellValue('AP' . $start_row, $row['state_region']);
                // ->setCellValue('AQ' . $start_row, $row['Billing Account Number'])
                // ->setCellValue('AR' . $start_row, $row['Manifested Number of Pieces'])
                // ->setCellValue('AS' . $start_row, $row['Billed Weight'])
                // ->setCellValue('AT' . $start_row, $row['Customs Declared Value'])
                // ->setCellValue('AU' . $start_row, $row['Currency Code of Declared Value'])
                // ->setCellValue('AV' . $start_row, $row['Unknown POD Status']);




                $start_row++;
            }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $filename = $start.' to '.$end . '.xlsx';

            $filename = 'Sample Table.xlsx';

            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);

            $urlShipment = FCPATH . 'upload/export_excel/' . $filename;
            $url = FCPATH . 'upload/PandoraProActiveReport.xlsm';

            $file_name = basename($url);
            if (file_put_contents($file_name, file_get_contents($url))) {
                $result['status'] = true;
                $result['message'] = $file_name;
            } else {
                $result['status'] = false;
                $result['message'] = "File downloading failed.";
            }

            $excelShipment = basename($urlShipment);

            if (file_put_contents($excelShipment, file_get_contents($urlShipment))) {
                $result['status'] = true;
                $result['message'] = $excelShipment;
            } else {
                $result['status'] = false;
                $result['message'] = "File downloading failed.";
            }

            $this->load->library('zip');
            $fileZip = "PandoraProActiveReport" . date("Y-m-d-H-i-s") . '.zip';

            $this->zip->read_file($excelShipment);
            $this->zip->read_file($file_name);
            $this->zip->archive(FCPATH . '/upload/pivot/' . $fileZip);

            $result['message'] = $fileZip;
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function ExportexcelStatus()
    {
        $this->alert_line_notify_export_CS_hipment_Status();
        // log_message('error', 'Exportexcel');
        ini_set("memory_limit", "100000M");
        ini_set('MAX_EXECUTION_TIME', 0);

        try {
            $this->load->model('ExportModel', '', true);

            $this->load->library('MyExcel');

            $dataPost = json_decode($this->input->raw_input_stream, true);

            // print_r($dataPost);
            // die();

            // $date = date('Y-m-d-H-i-s');
            // print_r($date);
            // die();

            // $date = '2021-07-28';

            // log_message('error', 'Can get date');
            // $data = [];
            $data = $this->ExportModel->ExportexcelStatus($dataPost);
            // log_message('error', 'Can get data');
            // print_r($data);
            // die();

            //2021-07-29//
            // ini_set('max_execution_time', 300); //300 seconds = 5 minutes
            // set_time_limit(300);
            //2021-07-29//

            // print_r($data[0]['file_name']);die();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Sheet1");

            // die();

            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ]

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => '00ffff')
                // )
            );

            $headcolor = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'CBFFB0')
                )
            );

            $headcoloryellow = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    'size' => 10,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FFFF00')
                )
            );

            $headcoloryellow2 = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E4FB32')
                )
            );


            $headbody = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => 'ffff00')
                // )
            );
            // //กำหนด style ส่วน body//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_THIN
            //         )
            //     )
            // );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);


            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'MAWB')
                ->setCellValue('B1', 'No.')
                // ->setCellValue('B1', 'Group Over SLA ( Exclude weekend )')
                // ->setCellValue('C1', 'Group Over SLA ( Include weekend )')
                ->setCellValue('C1', 'Waybill')
                ->setCellValue('D1', 'Consignee')
                ->setCellValue('E1', 'Status')
                ->setCellValue('F1', 'Receiver name')
                ->setCellValue('G1', 'Delivery date')
                ->setCellValue('H1', 'PC')
                ->setCellValue('I1', 'Date')
                ->setCellValue('J1', 'Dest')
                ->setCellValue('K1', 'Factor Incident')
                ->setCellValue('L1', 'Factor')
                ->setCellValue('M1', 'SLA_C')
                ->setCellValue('N1', 'SLA_Remark')
                ->setCellValue('O1', 'check_mapping');
                // ->setCellValue('P1', 'Peice')
                // ->setCellValue('Q1', 'A_Weight')
                // ->setCellValue('R1', 'V_Weight')
                // ->setCellValue('S1', 'Product')
                // ->setCellValue('T1', 'Delivery')
                // ->setCellValue('U1', 'Shipment_Status')
                // ->setCellValue('V1', 'POD')
                // ->setCellValue('W1', 'Status_Date')
                // ->setCellValue('X1', 'Status_Time')
                // ->setCellValue('Y1', 'Pickup_Date')
                // ->setCellValue('Z1', 'SLA_C')
             
            

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headcoloryellow);
            // $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($headtable);



            $start_row = 2;
            foreach ($data as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('Q' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('R' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('S' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('T' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('U' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('V' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('W' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('X' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('Y' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('Z' . $start_row)->applyFromArray($styleArray);



                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $start_row, $row['MAWB'])
                    ->setCellValue('B' . $start_row, $row['No'])

                    ->setCellValue('C' . $start_row, $row['Waybill'])
                    // ->setCellValue('A' . $start_row, "Test");
                    ->setCellValue('D' . $start_row, $row['Consignee'])
                    ->setCellValue('E' . $start_row, $row['Status'])
                    ->setCellValue('F' . $start_row, $row['Receiver_name'])
                    ->setCellValue('G' . $start_row, $row['Delivery_date'])

                    // ->setCellValue('G' . $start_row, $row['Delivery_date'] = isset($data['G']) && $data['G'] != '' ? DateTime::createFromFormat('Y-m-d', $data['G'])->format('d/m/Y') : 0)
                    // ->setCellValue('G' . $start_row, $row['Delivery_date'] = isset($data['G']) && $data['G'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['G'])->format('Y-m-d') : 0)


                    
                    // ->setCellValueExplicit("G". $start_row, $row['Group T/T Include weekend range'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('H' . $start_row, $row['PC'])
                    ->setCellValue('I' . $start_row, $row['Date'])
                    // ->setCellValue('I' . $start_row, $row['Date'] = isset($data['I']) && $data['I'] != '' ? DateTime::createFromFormat('Y-m-d', $data['I'])->format('d/m/Y') : 0)
                    ->setCellValue('J' . $start_row, $row['Dest'])
                    ->setCellValue('K' . $start_row, $row['Factor_Incident'])
                    ->setCellValue('L' . $start_row, $row['Factor'])
                    ->setCellValue('M' . $start_row, $row['SLA_C'])
                    ->setCellValue('N' . $start_row, $row['SLA_Remark'])
                    ->setCellValue('O' . $start_row, $row['check_mapping']);
                    // ->setCellValue('P' . $start_row, $row['Peice'])
                    // ->setCellValue('Q' . $start_row, $row['A_Weight'])
                    // ->setCellValue('R' . $start_row, $row['V_Weight'])
                    // ->setCellValue('S' . $start_row, $row['Product'])
                    // ->setCellValue('T' . $start_row, $row['Delivery'])
                    // ->setCellValue('U' . $start_row, $row['Shipment_Status'])
                    // ->setCellValue('V' . $start_row, $row['POD'])
                    // ->setCellValue('W' . $start_row, $row['Status_Date'])
                    // ->setCellValue('X' . $start_row, $row['Status_Time'])
                    // ->setCellValue('Y' . $start_row, $row['Pickup_Date'])
                    // ->setCellValue('Z' . $start_row, $row['SLA_C'])

                $start_row++;
            }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $filename = $start.' to '.$end . '.xlsx';

            $filename = "CS_Shipment_Status_" . date("Y-m-d-H-i-s") . '.xlsx';
            

            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);

            $urlShipment = FCPATH . 'upload/export_excel/' . $filename;
            // $url = FCPATH . 'upload/PandoraProActiveReport.xlsm';

            // $file_name = basename($url);
            // if (file_put_contents($file_name, file_get_contents($url))) {
            //     $result['status'] = true;
            //     $result['message'] = $file_name;
            // } else {
            //     $result['status'] = false;
            //     $result['message'] = "File downloading failed.";
            // }

            $excelShipment = basename($urlShipment);

            if ((file_get_contents($urlShipment))) {
                $result['status'] = true;
                $result['message'] = $excelShipment;
            } else {
                $result['status'] = false;
                $result['message'] = "File downloading failed.";
            }
            

            // $this->load->library('zip');
            // $fileZip = "PandoraProActiveReport" . date("Y-m-d-H-i-s") . '.zip';

            // $this->zip->read_file($excelShipment);
            // $this->zip->read_file($file_name);
            // $this->zip->archive(FCPATH . '/upload/pivot/' . $fileZip);

            // $result['message'] = $fileZip;
            // $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function ExportWaitCheckpoint()
    {
        // log_message('error', 'Exportexcel');
        ini_set("memory_limit", "10000M");
        ini_set('MAX_EXECUTION_TIME', 0);

        try {
            $this->load->model('ExportModel', '', true);

            $this->load->library('MyExcel');

            $dataPost = json_decode($this->input->raw_input_stream, true);

            // print_r($dataPost);
            // die();

            // $date = date('Y-m-d-H-i-s');
            // print_r($date);
            // die();

            // $date = '2021-07-28';

            // log_message('error', 'Can get date');
            // $data = [];
            $dataPost['factor_incident'] = "Wait for check checkpoint event";
            $data = $this->ExportModel->ExportExcel($dataPost);
            // log_message('error', 'Can get data');
            // print_r($data);
            // die();

            //2021-07-29//
            // ini_set('max_execution_time', 300); //300 seconds = 5 minutes
            // set_time_limit(300);
            //2021-07-29//

            // print_r($data[0]['file_name']);die();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Shipment Wait for checkpoint");

            // die();

            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ]

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => '00ffff')
                // )
            );


            $headbody = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => 'ffff00')
                // )
            );
            // //กำหนด style ส่วน body//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_THIN
            //         )
            //     )
            // );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);

            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(TRUE);

            // if (isset($dataModel['end_date']) && $dataModel['end_date'] != "") {
            //     $EndDateTS = strtotime($dataModel['end_date']);
            //     if ($EndDateTS !== false) {
            //         date('Y-m-d', $EndDateTS);
            //     }
            // }
            // if (isset($dataModel['date']) && $dataModel['date'] != "") {
            //     $StarDateTS = strtotime($dataModel['date']);
            //     if ($StarDateTS !== false) {
            //         date('Y-m-d', $StarDateTS);
            //     }
            // }
            $objPHPExcel->setActiveSheetIndex(0)

                ->setCellValue('A1', 'Group Over SLA ( Exclude weekend )')
                ->setCellValue('B1', 'Group Over SLA ( Include weekend )')
                ->setCellValue('C1', 'Reporting Code Description')
                ->setCellValue('D1', 'factor_incident')
                ->setCellValue('E1', 'On time status')
                ->setCellValue('F1', 'Group T/T Include weekend range')
                ->setCellValue('G1', 'factor_incident_cal')
                ->setCellValue('H1', 'factor')
                ->setCellValue('I1', 'lane')
                ->setCellValue('J1', 'Diff SLA ( Include weekend )')
                ->setCellValue('K1', 'Diff SLA ( Exclude weekend )')
                ->setCellValue('L1', 'T/T included weekend by_pickup_day')
                ->setCellValue('M1', 'T/T excluded weekend')
                ->setCellValue('N1', 'T/T included weekend')
                ->setCellValue('O1', 'delivery_provider ')
                ->setCellValue('P1', 'gateway')
                ->setCellValue('Q1', 'shipmonth')
                ->setCellValue('R1', 'Quater')
                ->setCellValue('S1', 'Year')
                ->setCellValue('T1', 'week_number')
                ->setCellValue('U1', 'Waybill Number')
                ->setCellValue('V1', 'Product Code')
                ->setCellValue('W1', 'Product Name')
                ->setCellValue('X1', 'Shipper Reference')
                ->setCellValue('Y1', 'Shipper Company Name')
                ->setCellValue('Z1', 'Destination Country/Territory Name')

                ->setCellValue('AA1', 'Destination Country/Territory Area Code')
                ->setCellValue('AB1', 'Destination Service Area Code')
                ->setCellValue('AC1', 'Number of Days in Customs')
                ->setCellValue('AD1', 'Pickup day')
                ->setCellValue('AE1', 'Startclock Date')
                ->setCellValue('AF1', 'Stopclock Date')
                ->setCellValue('AG1', 'Stopclock Day of Week')
                ->setCellValue('AH1', 'Signatory')
                ->setCellValue('AI1', 'Elapsed Actual Transit Days')
                ->setCellValue('AJ1', 'Reporting Code Description')
                ->setCellValue('AK1', 'Reporting Code Category Description')
                ->setCellValue('AL1', 'Consignee Company Name')
                ->setCellValue('AM1', 'Consignee Address')
                ->setCellValue('AN1', 'Consignee Zip')
                ->setCellValue('AO1', 'Consignee City ')
                ->setCellValue('AP1', 'Billing Account Number')
                ->setCellValue('AQ1', 'Manifested Number of Pieces')
                ->setCellValue('AR1', 'Billed Weight')
                ->setCellValue('AS1', 'Customs Declared Value')
                ->setCellValue('AT1', 'Currency Code of Declared Value')
                ->setCellValue('AU1', 'Unknown POD Status');

            // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($headbody);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($headtable);

            $objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AN1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AO1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AP1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AQ1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AR1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AS1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AT1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AU1')->applyFromArray($headtable);

            $start_row = 2;
            foreach ($data as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Q' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('R' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('S' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('T' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('U' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('V' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('W' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('X' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Y' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Z' . $start_row)->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->getStyle('AA' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AB' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AC' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AD' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AE' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AP' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AQ' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AR' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AS' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AT' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AU' . $start_row)->applyFromArray($styleArray);

                $objPHPExcel->setActiveSheetIndex(0)
                    // ->setCellValue('A' . $start_row, date('d/m/Y', strtotime($row['update_date'])))
                    ->setCellValue('A' . $start_row, $row['Group Over SLA ( Exclude weekend )'])
                    ->setCellValue('B' . $start_row, $row['Group Over SLA ( Include weekend )'])
                    ->setCellValue('C' . $start_row, $row['Reporting Code Description'])
                    ->setCellValue('D' . $start_row, $row['factor_incident'])
                    ->setCellValue('E' . $start_row, $row['On time status'])
                    // ->setCellValue('F' . $start_row, $row['Group T/T Include weekend range'])
                    ->setCellValueExplicit("F" . $start_row, $row['Group T/T Include weekend range'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('G' . $start_row, $row['factor_incident_cal'])
                    ->setCellValue('H' . $start_row, $row['factor'])
                    ->setCellValue('I' . $start_row, $row['lane'])
                    ->setCellValue('J' . $start_row, $row['Diff SLA ( Include weekend )'])
                    ->setCellValue('K' . $start_row, $row['Diff SLA ( Exclude weekend )'])
                    ->setCellValue('L' . $start_row, $row['T/T included weekend by_pickup_day'])
                    ->setCellValue('M' . $start_row, $row['T/T excluded weekend'])
                    ->setCellValue('N' . $start_row, $row['T/T included weekend'])
                    ->setCellValue('O' . $start_row, $row['delivery_provider'])
                    ->setCellValue('P' . $start_row, $row['gateway'])
                    ->setCellValue('Q' . $start_row, $row['shipmonth'])
                    ->setCellValue('R' . $start_row, $row['Quater'])
                    ->setCellValue('S' . $start_row, $row['Year'])
                    ->setCellValue('T' . $start_row, $row['week_number'])
                    ->setCellValue('U' . $start_row, $row['Waybill Number'])
                    ->setCellValue('V' . $start_row, $row['Product Code'])
                    ->setCellValue('W' . $start_row, $row['Product Name'])
                    ->setCellValue('X' . $start_row, $row['Shipper Reference'])
                    ->setCellValue('Y' . $start_row, $row['Shipper Company Name'])
                    ->setCellValue('Z' . $start_row, $row['Destination Country/Territory Name'])

                    ->setCellValue('AA' . $start_row, $row['Destination Country/Territory Area Code'])
                    ->setCellValue('AB' . $start_row, $row['Destination Service Area Code'])
                    ->setCellValue('AC' . $start_row, $row['Number of Days in Customs'])
                    ->setCellValue('AD' . $start_row, $row['Startclock Day of Week'])
                    ->setCellValue('AE' . $start_row, $row['Startclock Date'])
                    ->setCellValue('AF' . $start_row, $row['Stopclock Date'])
                    ->setCellValue('AG' . $start_row, $row['Stopclock Day of Week'])
                    ->setCellValue('AH' . $start_row, $row['Signatory'])
                    ->setCellValue('AI' . $start_row, $row['Elapsed Actual Transit Days'])
                    ->setCellValue('AJ' . $start_row, $row['Reporting Code Description'])
                    ->setCellValue('AK' . $start_row, $row['Reporting Code Category Description'])
                    ->setCellValue('AL' . $start_row, $row['Consignee Company Name'])
                    ->setCellValue('AM' . $start_row, $row['Consignee Address'])
                    ->setCellValue('AN' . $start_row, $row['Consignee Zip'])
                    ->setCellValue('AO' . $start_row, $row['Consignee City'])
                    ->setCellValue('AP' . $start_row, $row['Billing Account Number'])
                    ->setCellValue('AQ' . $start_row, $row['Manifested Number of Pieces'])
                    ->setCellValue('AR' . $start_row, $row['Billed Weight'])
                    ->setCellValue('AS' . $start_row, $row['Customs Declared Value'])
                    ->setCellValue('AT' . $start_row, $row['Currency Code of Declared Value'])
                    ->setCellValue('AU' . $start_row, $row['Unknown POD Status']);
                // ->setCellValueExplicit('P' . $start_row, $row['tax_code'], PHPExcel_Cell_DataType::TYPE_STRING);




                $start_row++;
            }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $filename = $start.' to '.$end . '.xlsx';

            $filename = "Data_" . $dataPost['Year'] . "_" . $dataPost['week'] . date("Y-m-d-H-i-s") . '.xlsx';
            // echo FCPATH; die();
            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);
            $result['message'] = $filename;
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function alert_line_notify_export_Weekly_report()
    {
        // call line api
        $Token = 'UeMJwWbMswxUmnEA08yzzgGhWAuszabSpVT8BtUHvpr';
        $message = 'Export Weekly report'.' '. date('Y-m-d');
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . ''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
        } else {
            $result_ = json_decode($result, true);
        }
        curl_close($chOne);
    }

    public function alert_line_notify_export_Pandora_Dashboard()
    {
        // call line api
        $Token = 'UeMJwWbMswxUmnEA08yzzgGhWAuszabSpVT8BtUHvpr';
        $message = 'Export Pandora Dashboard (Daily)'.' '. date('Y-m-d');
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . ''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
        } else {
            $result_ = json_decode($result, true);
        }
        curl_close($chOne);
    }

    public function alert_line_notify_export_CS_hipment_Status()
    {
        // call line api
        $Token = 'UeMJwWbMswxUmnEA08yzzgGhWAuszabSpVT8BtUHvpr';
        $message = 'Export CS Shipment Status'.' '. date('Y-m-d');
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . ''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
        } else {
            $result_ = json_decode($result, true);
        }
        curl_close($chOne);
    }
}



<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ImportEvent extends MY_Controller
{
    // private $tbl_name = 'inventory';
    // private $id = 'id';

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
        
        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
        ini_set('memory_limit', '1024M');
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_PANDASH');
        if (!$role_data['IMFILE']) {
            redirect('Login');
        }
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('import/importEvent_view');
        $this->load->view('share/footer');
    }

    public function upload_file()
    {
        // print_r($_FILES['file_Pandora_Dashboard']);
        //  print_r('test');
        // $uniqid = uniqid();
        // $datapandora['uniqidpandora'] = $uniqid;
        // $datashipment['uniqidshipment'] = $uniqid;
        $date = date('Y-m-d-H-i-s');

        // $this->load->model('HeadpandoraModel', '', true);
        // $this->load->model('HeadShipmentModel', '', true);

        // $uniqidpandora = $this->HeadpandoraModel->insert($datapandora);
        // $uniqidshipment = $this->HeadShipmentModel->insert($datashipment);

        // $this->upload_filepandora($_FILES, $date, $uniqid);
        // $this->upload_fileshipment($_FILES, $date, $uniqid);
        $todayfile = 'Checkpoint_Event' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/import_excel';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_Pandora_Dashboard']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        

        $status = true;
        if (!$this->upload->do_upload('file_Pandora_Dashboard')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();

            $todayfile = $data['upload_data']['file_name'];

            $this->loadExcelpandoraDashboardToDB($todayfile, $date);
        }
    }


    public function loadExcelpandoraDashboardToDB($file, $date)
    {
        // print_r($file);

        $formattype = 0;
        ini_set('max_execution_time', 300);

        $this->load->library('MyExcel');
        $this->load->model('ImportEventModel', '', true);
        // print_r($file);
        $objReader = PHPExcel_IOFactory::load('upload/import_excel/' . $file);

        // die();




        //sheet1
        // $sheetData = $objReader->setActiveSheetIndex(0);
        $sheetData = $objReader->getSheet(0)->toArray('', true, false, true);
        // print_r("test");
        // $CurrentWorkSheetIndex = 0;

        // print_r($sheetData);die();

        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        $this->ImportEventModel->delete_temp_checkpoint_baby();

        // echo 'delete_temp_checkpoint_baby success';die();
        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        // print_r('test');
        // print_r($sheetData);die();

        foreach ($sheetData as $datababy) {
        // print_r($datababy['A']);

            try {

                if (
                    $count == 0
                    // and  ("NAME" == trim(strtoupper($datababy['A'])))

                    and  (strtoupper("Waybill Number") ==  trim(strtoupper($datababy['A'])))
                    and  (strtoupper("Piece ID") ==  trim(strtoupper($datababy['B'])))
                    and  (strtoupper("Event Stn") ==  trim(strtoupper($datababy['C'])))
                    and  (strtoupper("Event Fac") ==  trim(strtoupper($datababy['D'])))
                    and  (strtoupper("Event Cd") ==  trim(strtoupper($datababy['E'])))
                    and  (strtoupper("Route/Cycle") ==  trim(strtoupper($datababy['F'])))

                    and  (strtoupper("Event Dtm") ==  trim(strtoupper($datababy['G'])))
                    and  (strtoupper("Event Dtm GMT") ==  trim(strtoupper($datababy['H'])))
                    and  (strtoupper("Event Remark") ==  trim(strtoupper($datababy['I'])))
                    and  (strtoupper("MvmtNo") ==  trim(strtoupper($datababy['J'])))
                    and  (strtoupper("Mvmt Lane") ==  trim(strtoupper($datababy['K'])))
                    and  (strtoupper("Mvmt Dep Dt") ==  trim(strtoupper($datababy['L'])))
                    and  (strtoupper("HU") ==  trim(strtoupper($datababy['M'])))
                    and  (strtoupper("MAWB") ==  trim(strtoupper($datababy['N'])))
                    and  (strtoupper("Other Info") ==  trim(strtoupper($datababy['O'])))
                    and  (strtoupper("Event Class") ==  trim(strtoupper($datababy['P'])))
                    and  (strtoupper("UserID") ==  trim(strtoupper($datababy['Q'])))
                    and  (strtoupper("Originator") ==  trim(strtoupper($datababy['R'])))
                    and  (strtoupper("Data Delay >15mins") ==  trim(strtoupper($datababy['S'])))
                    // and  (strtoupper("Consignee City") ==  trim(strtoupper($datababy['T'])))
                    // and  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($datababy['U'])))
                    // and  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($datababy['V'])))
                    // and  (strtoupper("Billing Account Number") ==  trim(strtoupper($datababy['W'])))
                    // and  (strtoupper("Customs Declared Value") ==  trim(strtoupper($datababy['X'])))

                ) {
                    // echo "found datababy <br/>";
                    // echo'case 1';
                    $fuondFormat = 1;

                    // $this->JobTypeModel->dropJobType();
                } else if ($count == 0) {
                    // print_r($datababy);
                    // echo'SCAC';
                    // echo trim(strtoupper($datababy['E']));
                    $ErrorMassage = 'Your file has an out-of-format format in the column ';
                    $FlagFormat = true;

                    $ErrorMassage .=  (strtoupper("Waybill Number") ==  trim(strtoupper($datababy['A']))) ? "" : " A ";
                    $ErrorMassage .=  (strtoupper("Piece ID") ==  trim(strtoupper($datababy['B']))) ? "" : " B ";
                    $ErrorMassage .=  (strtoupper("Event Stn") ==  trim(strtoupper($datababy['C']))) ? "" : " C ";
                    $ErrorMassage .=  (strtoupper("Event Fac") ==  trim(strtoupper($datababy['D']))) ? "" : " D ";
                    $ErrorMassage .=  (strtoupper("Event Cd") ==  trim(strtoupper($datababy['E']))) ? "" : " E ";
                    $ErrorMassage .=  (strtoupper("Route/Cycle") ==  trim(strtoupper($datababy['F']))) ? "" : " E ";

                    $ErrorMassage .=  (strtoupper("Event Dtm") ==  trim(strtoupper($datababy['G']))) ? "" : " G ";
                    $ErrorMassage .=  (strtoupper("Event Dtm GMT") ==  trim(strtoupper($datababy['H']))) ? "" : " H ";
                    $ErrorMassage .=  (strtoupper("Event Remark") ==  trim(strtoupper($datababy['I']))) ? "" : " I ";
                    $ErrorMassage .=  (strtoupper("MvmtNo") ==  trim(strtoupper($datababy['J']))) ? "" : " J ";
                    $ErrorMassage .=  (strtoupper("Mvmt Lane") ==  trim(strtoupper($datababy['K']))) ? "" : " K ";
                    $ErrorMassage .=  (strtoupper("Mvmt Dep Dt") ==  trim(strtoupper($datababy['L']))) ? "" : " L ";
                    $ErrorMassage .=  (strtoupper("HU") ==  trim(strtoupper($datababy['M']))) ? "" : " M ";
                    $ErrorMassage .=  (strtoupper("MAWB") ==  trim(strtoupper($datababy['N']))) ? "" : " N ";
                    $ErrorMassage .=  (strtoupper("Other Info") ==  trim(strtoupper($datababy['O']))) ? "" : " O ";
                    $ErrorMassage .=  (strtoupper("Event Class") ==  trim(strtoupper($datababy['P']))) ? "" : " P ";
                    $ErrorMassage .=  (strtoupper("UserID") ==  trim(strtoupper($datababy['Q']))) ? "" : " Q ";
                    $ErrorMassage .=  (strtoupper("Originator") ==  trim(strtoupper($datababy['R']))) ? "" : " R ";
                    $ErrorMassage .=  (strtoupper("Data Delay >15mins") ==  trim(strtoupper($datababy['S']))) ? "" : " S ";
                    // $ErrorMassage .=  (strtoupper("Consignee City") ==  trim(strtoupper($datababy['T']))) ? "" : " T ";
                    // $ErrorMassage .=  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($datababy['U']))) ? "" : " U ";
                    // $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($datababy['V']))) ? "" : " V ";
                    // $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($datababy['W']))) ? "" : " W ";
                    // $ErrorMassage .=  (strtoupper("Customs Declared Value") ==  trim(strtoupper($datababy['X']))) ? "" : " X ";

                    // echo ("LIMIT" ==  trim(strtoupper($datababy['B']))) ? "T" : "F";
                    // echo "Miss datababy format ";
                    break;
                    // exit();
                }

                // print_r($FlagFormat);die();

                if ($row_count > 1) {

                    // print_r($datababy);die();

                    // $data_model['title_excel'] = $sheetData[2]['B'];

                    $data_baby['Waybill Number'] = isset($datababy['A']) ? $datababy['A'] : 0;
                    $data_baby['Piece ID'] = isset($datababy['B']) ? $datababy['B'] : 0;
                    $data_baby['Event Stn'] = isset($datababy['C']) ? $datababy['C'] : 0;
                    $data_baby['Event Fac'] = isset($datababy['D']) ? $datababy['D'] : 0;
                    $data_baby['Event Cd'] = isset($datababy['E']) ? $datababy['E'] : 0;
                    $data_baby['Route/Cycle'] = isset($datababy['F']) ? $datababy['F'] : 0;

                    // $data_baby['event_date'] =  $event_date;
                    $data_baby['Event Dtm'] = isset($datababy['G']) ? $datababy['G'] : 0;
                    $data_baby['Event Dtm GMT'] = isset($datababy['H']) ? $datababy['H'] : 0;
                    $data_baby['Event Remark'] = isset($datababy['I']) ? $datababy['I'] : 0;
                    $data_baby['MvmtNo'] = isset($datababy['J']) ? $datababy['J'] : 0;
                    $data_baby['Mvmt Lane'] = isset($datababy['K']) ? $datababy['K'] : 0;
                    $data_baby['Mvmt Dep Dt'] = isset($datababy['L']) ? $datababy['L'] : 0;
                    // $data_baby['HU'] = "";
                    $data_baby['HU'] = isset($datababy['M']) ? $datababy['M'] : 0;
                    $data_baby['MAWB'] = isset($datababy['N']) ? $datababy['N'] : 0;
                    // $data_baby['em_scheduled_delivery_date'] = isset($datababy['R'])  && $datababy['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($datababy['R'])->format('Y-m-d') : 0;
                    $data_baby['Other Info'] = isset($datababy['O']) ? $datababy['O'] : 0;
                    $data_baby['Event Class'] = isset($datababy['P']) ? $datababy['P'] : 0;
                    $data_baby['UserID'] = isset($datababy['Q']) ? $datababy['Q'] : 0;
                    $data_baby['Originator'] = isset($datababy['R']) ? $datababy['R'] : 0;
                    $data_baby['Data Delay >15mins'] = isset($datababy['S']) ? $datababy['S'] : 0;
                    // $data_baby['Consignee City'] = isset($data['T']) ? $data['T'] : 0;
                    // $data_baby['Billing Account Number'] = isset($data['W']) ? $data['W'] : 0;
                    // $data_baby['Manifested Number of Pieces'] = isset($data['U']) ? $data['U'] : 0;
                    // $data_baby['Billed Weight'] = "";
                    // $data_baby['Customs Declared Value'] = isset($data['X']) ? $data['X'] : 0;
                    // $data_baby['Currency Code of Declared Value'] = isset($data['V']) ? $data['V'] : 0;
                    // $data_baby['column_ad'] = isset($data['AD']) ? $data['AD'] : 0;
                    // $data_baby['em_sales_order_id'] = isset($data['AE']) ? $data['AE'] : 0;
                    // $data_baby['em_origin'] = isset($data['AF']) ? $data['AF'] : 0;
                    // $data_baby['em_current_event_name'] = isset($data['AG']) ? $data['AG'] : 0;
                    // $data_baby['em_current_event_datetime'] = isset($data['AH']) && $data['AH'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AH'])->format('Y-m-d H:i:s') : 0;
                    // $data_baby['em_current_event_reason_code'] = isset($data['AI']) ? $data['AI'] : 0;
                    // $data_baby['em_current_event_location_description'] = isset($data['AJ']) ? $data['AJ'] : 0;
                    // $data_baby['dq_postship_carrier'] = isset($data['AK']) ? $data['AK'] : 0;
                    // $data_baby['ops_channel_level_2_code'] = isset($data['AL']) ? $data['AL'] : 0;
                    // $data_baby['em_ship_condition_code'] = isset($data['AM']) ? $data['AM'] : 0;
                    // $data_baby['em_ship_point_type'] = isset($data['AN']) ? $data['AN'] : 0;
                    // $data_baby['em_ship_point_code'] = isset($data['AO']) ? $data['AO'] : 0;
                    // $data_baby['em_city_name'] = isset($data['AP']) ? $data['AP'] : 0;
                    // $data_baby['em_postal_code'] = isset($data['AQ']) ? $data['AQ'] : 0;
                    // $data_baby['em_ship_confirmation_last_date'] = isset($data['AR']) && $data['AR'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AR'])->format('Y-m-d') : 0;

                    // $data_baby['em_ship_confirmation_last_time'] = isset($data['AS']) && $data['AS'] != '' ? PHPExcel_Style_NumberFormat::toFormattedString($data['AS'], 'hh:mm:ss') : 0;
                    // $data_baby['em_shipment_picked_up_last_datetime'] = isset($data['AT']) && $data['AT'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AT'])->format('Y-m-d H:i:s') : 0;
                    // $data_baby['em_uplift_last_datetime'] = isset($data['AU']) && $data['AU'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AU'])->format('Y-m-d H:i:s') : null;
                    // $data_baby['em_first_attempt_date'] = isset($data['AV']) && $data['AV'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AV'])->format('Y-m-d') : null;
                    // $data_baby['em_first_attempt_reason_code'] = isset($data['AW']) ? $data['AW'] : 0;
                    // $data_baby['em_first_attempt_reason_code_description'] = isset($data['AX']) ? $data['AX'] : 0;
                    // $data_baby['em_delivery_not_completed_last_date'] = isset($data['AY']) && $data['AY'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AY'])->format('Y-m-d') : null;
                    // $data_baby['em_delivery_not_completed_last_reason_code_description'] = isset($data['AZ']) ? $data['AZ'] : 0;
                    // $data_baby['em_shipment_delay_last_date'] = isset($data['BA']) && $data['BA'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['BA'])->format('Y-m-d') : null;
                    // $data_baby['em_shipment_delay_last_reason_code'] = isset($data['BB']) ? $data['BB'] : 0;
                    // $data_baby['em_shipment_delay_last_reason_code_description'] = isset($data['BC']) ? $data['BC'] : 0;
                    // $data_baby['em_count_of_delivery_attempts'] = isset($data['BD']) ? $data['BD'] : 0;
                    
                    $data_baby['create_date'] = $date;
                    $data_baby['create_user'] = $this->session->userdata('user_name');
                    $data_baby['file_name'] = $file;

                    // if ($data_baby['ssd_status'] == 'Due Today' || $data_baby['ssd_status'] == 'Overdue' || $data_baby['ssd_status'] == 'Future Shipments') {
                    //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_baby);
                    // }
                    // print_r($data_baby);
                    $nResult = $this->ImportEventModel->insert_to_temp_checkpoint_baby($data_baby);

                } else {
                    // if ($data['BO'] == '') {
                    //     $formattype = 1;
                    // } else if ($data['BO'] != '') {
                    //     $formattype = 2;
                    // } else if ($data['A'] != 'Order Item Id') {
                    //     $result['status'] = false;
                    //     $result['message'] = 'Wrong file format';
                    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    //     exit;
                    // }

                    // echo $formattype;
                    // die();
                }



                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            $row_count++;
            $count++;
        }

        // $check_duplicate = $this->ImportEventModel->check_duplicate();


        //sheet 2
        $sheetData2 = $objReader->getSheet(1)->toArray('', true, false, true);

        // print_r($sheetData2);die();

        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        $this->ImportEventModel->delete_temp_checkpoint_mother();

        // echo 'delete_temp_pandora_dashboard success';die();
        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        // print_r($sheetData[4]);die();

        foreach ($sheetData2 as $data) {

            try {

                if (
                    $count == 0
                    // and  ("NAME" == trim(strtoupper($data['A'])))

                    and  (strtoupper("Shipper Reference") ==  trim(strtoupper($data['A'])))
                    and  (strtoupper("Piece ID") ==  trim(strtoupper($data['B'])))
                    and  (strtoupper("Event Stn") ==  trim(strtoupper($data['C'])))
                    and  (strtoupper("Event Fac") ==  trim(strtoupper($data['D'])))
                    and  (strtoupper("Event Cd") ==  trim(strtoupper($data['E'])))
                    and  (strtoupper("Route/Cycle") ==  trim(strtoupper($data['F'])))

                    and  (strtoupper("Event Dtm") ==  trim(strtoupper($data['G'])))
                    and  (strtoupper("Event Dtm GMT") ==  trim(strtoupper($data['H'])))
                    and  (strtoupper("Event Remark") ==  trim(strtoupper($data['I'])))
                    and  (strtoupper("MvmtNo") ==  trim(strtoupper($data['J'])))
                    and  (strtoupper("Mvmt Lane") ==  trim(strtoupper($data['K'])))
                    and  (strtoupper("Mvmt Dep Dt") ==  trim(strtoupper($data['L'])))
                    and  (strtoupper("HU") ==  trim(strtoupper($data['M'])))
                    and  (strtoupper("MAWB") ==  trim(strtoupper($data['N'])))
                    and  (strtoupper("Other Info") ==  trim(strtoupper($data['O'])))
                    and  (strtoupper("Event Class") ==  trim(strtoupper($data['P'])))
                    and  (strtoupper("UserID") ==  trim(strtoupper($data['Q'])))
                    and  (strtoupper("Originator") ==  trim(strtoupper($data['R'])))
                    and  (strtoupper("Data Delay >15mins") ==  trim(strtoupper($data['S'])))
                    // and  (strtoupper("Consignee City") ==  trim(strtoupper($data['T'])))
                    // and  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U'])))
                    // and  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V'])))
                    // and  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W'])))
                    // and  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X'])))

                ) {
                    // echo "found data <br/>";
                    // echo'case 1';
                    $fuondFormat = 1;

                    // $this->JobTypeModel->dropJobType();
                } else if ($count == 0) {
                    // print_r($data);
                    // echo'SCAC';
                    // echo trim(strtoupper($data['E']));
                    $ErrorMassage = 'Your file has an out-of-format format in the column ';
                    $FlagFormat = true;

                    $ErrorMassage .=  (strtoupper("Shipper Reference") ==  trim(strtoupper($data['A']))) ? "" : " A ";
                    $ErrorMassage .=  (strtoupper("Piece ID") ==  trim(strtoupper($data['B']))) ? "" : " B ";
                    $ErrorMassage .=  (strtoupper("Event Stn") ==  trim(strtoupper($data['C']))) ? "" : " C ";
                    $ErrorMassage .=  (strtoupper("Event Fac") ==  trim(strtoupper($data['D']))) ? "" : " D ";
                    $ErrorMassage .=  (strtoupper("Event Cd") ==  trim(strtoupper($data['E']))) ? "" : " E ";
                    $ErrorMassage .=  (strtoupper("Route/Cycle") ==  trim(strtoupper($data['F']))) ? "" : " E ";

                    $ErrorMassage .=  (strtoupper("Event Dtm") ==  trim(strtoupper($data['G']))) ? "" : " G ";
                    $ErrorMassage .=  (strtoupper("Event Dtm GMT") ==  trim(strtoupper($data['H']))) ? "" : " H ";
                    $ErrorMassage .=  (strtoupper("Event Remark") ==  trim(strtoupper($data['I']))) ? "" : " I ";
                    $ErrorMassage .=  (strtoupper("MvmtNo") ==  trim(strtoupper($data['J']))) ? "" : " J ";
                    $ErrorMassage .=  (strtoupper("Mvmt Lane") ==  trim(strtoupper($data['K']))) ? "" : " K ";
                    $ErrorMassage .=  (strtoupper("Mvmt Dep Dt") ==  trim(strtoupper($data['L']))) ? "" : " L ";
                    $ErrorMassage .=  (strtoupper("HU") ==  trim(strtoupper($data['M']))) ? "" : " M ";
                    $ErrorMassage .=  (strtoupper("MAWB") ==  trim(strtoupper($data['N']))) ? "" : " N ";
                    $ErrorMassage .=  (strtoupper("Other Info") ==  trim(strtoupper($data['O']))) ? "" : " O ";
                    $ErrorMassage .=  (strtoupper("Event Class") ==  trim(strtoupper($data['P']))) ? "" : " P ";
                    $ErrorMassage .=  (strtoupper("UserID") ==  trim(strtoupper($data['Q']))) ? "" : " Q ";
                    $ErrorMassage .=  (strtoupper("Originator") ==  trim(strtoupper($data['R']))) ? "" : " R ";
                    $ErrorMassage .=  (strtoupper("Data Delay >15mins") ==  trim(strtoupper($data['S']))) ? "" : " S ";
                    // $ErrorMassage .=  (strtoupper("Consignee City") ==  trim(strtoupper($data['T']))) ? "" : " T ";
                    // $ErrorMassage .=  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U']))) ? "" : " U ";
                    // $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                    // $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W']))) ? "" : " W ";
                    // $ErrorMassage .=  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X']))) ? "" : " X ";

                    // echo ("LIMIT" ==  trim(strtoupper($data['B']))) ? "T" : "F";
                    // echo "Miss data format ";
                    break;
                    // exit();
                }

                // print_r($FlagFormat);die();

                if ($row_count > 1) {


                    // $data_model['title_excel'] = $sheetData[2]['B'];

                    $data_model['Shipper Reference'] = isset($data['A']) ? $data['A'] : 0;
                    $data_model['Piece ID'] = isset($data['B']) ? $data['B'] : 0;
                    $data_model['Event Stn'] = isset($data['C']) ? $data['C'] : 0;
                    $data_model['Event Fac'] = isset($data['D']) ? $data['D'] : 0;
                    $data_model['Event Cd'] = isset($data['E']) ? $data['E'] : 0;
                    $data_model['Route/Cycle'] = isset($data['F']) ? $data['F'] : 0;

                    // $data_model['event_date'] =  $event_date;
                    $data_model['Event Dtm'] = isset($data['G']) ? $data['G'] : 0;
                    $data_model['Event Dtm GMT'] = isset($data['H']) ? $data['H'] : 0;
                    $data_model['Event Remark'] = isset($data['I']) ? str_replace('"',"'",$data['I']) : 0;
                    $data_model['MvmtNo'] = isset($data['J']) ? $data['J'] : 0;
                    $data_model['Mvmt Lane'] = isset($data['K']) ? $data['K'] : 0;
                    $data_model['Mvmt Dep Dt'] = isset($data['L']) ? $data['L'] : 0;
                    // $data_model['HU'] = "";
                    $data_model['HU'] = isset($data['M']) ? $data['M'] : 0;
                    $data_model['MAWB'] = isset($data['N']) ? $data['N'] : 0;
                    // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                    $data_model['Other Info'] = isset($data['O']) ? $data['O'] : 0;
                    $data_model['Event Class'] = isset($data['P']) ? $data['P'] : 0;
                    $data_model['UserID'] = isset($data['Q']) ? $data['Q'] : 0;
                    $data_model['Originator'] = isset($data['R']) ? $data['R'] : 0;
                    $data_model['Data Delay >15mins'] = isset($data['S']) ? $data['S'] : 0;
                    // $data_model['Consignee City'] = isset($data['T']) ? $data['T'] : 0;
                    // $data_model['Billing Account Number'] = isset($data['W']) ? $data['W'] : 0;
                    // $data_model['Manifested Number of Pieces'] = isset($data['U']) ? $data['U'] : 0;
                    // $data_model['Billed Weight'] = "";
                    // $data_model['Customs Declared Value'] = isset($data['X']) ? $data['X'] : 0;
                    // $data_model['Currency Code of Declared Value'] = isset($data['V']) ? $data['V'] : 0;
                    // $data_model['column_ad'] = isset($data['AD']) ? $data['AD'] : 0;
                    // $data_model['em_sales_order_id'] = isset($data['AE']) ? $data['AE'] : 0;
                    // $data_model['em_origin'] = isset($data['AF']) ? $data['AF'] : 0;
                    // $data_model['em_current_event_name'] = isset($data['AG']) ? $data['AG'] : 0;
                    // $data_model['em_current_event_datetime'] = isset($data['AH']) && $data['AH'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AH'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_current_event_reason_code'] = isset($data['AI']) ? $data['AI'] : 0;
                    // $data_model['em_current_event_location_description'] = isset($data['AJ']) ? $data['AJ'] : 0;
                    // $data_model['dq_postship_carrier'] = isset($data['AK']) ? $data['AK'] : 0;
                    // $data_model['ops_channel_level_2_code'] = isset($data['AL']) ? $data['AL'] : 0;
                    // $data_model['em_ship_condition_code'] = isset($data['AM']) ? $data['AM'] : 0;
                    // $data_model['em_ship_point_type'] = isset($data['AN']) ? $data['AN'] : 0;
                    // $data_model['em_ship_point_code'] = isset($data['AO']) ? $data['AO'] : 0;
                    // $data_model['em_city_name'] = isset($data['AP']) ? $data['AP'] : 0;
                    // $data_model['em_postal_code'] = isset($data['AQ']) ? $data['AQ'] : 0;
                    // $data_model['em_ship_confirmation_last_date'] = isset($data['AR']) && $data['AR'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AR'])->format('Y-m-d') : 0;

                    // $data_model['em_ship_confirmation_last_time'] = isset($data['AS']) && $data['AS'] != '' ? PHPExcel_Style_NumberFormat::toFormattedString($data['AS'], 'hh:mm:ss') : 0;
                    // $data_model['em_shipment_picked_up_last_datetime'] = isset($data['AT']) && $data['AT'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AT'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_uplift_last_datetime'] = isset($data['AU']) && $data['AU'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AU'])->format('Y-m-d H:i:s') : null;
                    // $data_model['em_first_attempt_date'] = isset($data['AV']) && $data['AV'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AV'])->format('Y-m-d') : null;
                    // $data_model['em_first_attempt_reason_code'] = isset($data['AW']) ? $data['AW'] : 0;
                    // $data_model['em_first_attempt_reason_code_description'] = isset($data['AX']) ? $data['AX'] : 0;
                    // $data_model['em_delivery_not_completed_last_date'] = isset($data['AY']) && $data['AY'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AY'])->format('Y-m-d') : null;
                    // $data_model['em_delivery_not_completed_last_reason_code_description'] = isset($data['AZ']) ? $data['AZ'] : 0;
                    // $data_model['em_shipment_delay_last_date'] = isset($data['BA']) && $data['BA'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['BA'])->format('Y-m-d') : null;
                    // $data_model['em_shipment_delay_last_reason_code'] = isset($data['BB']) ? $data['BB'] : 0;
                    // $data_model['em_shipment_delay_last_reason_code_description'] = isset($data['BC']) ? $data['BC'] : 0;
                    // $data_model['em_count_of_delivery_attempts'] = isset($data['BD']) ? $data['BD'] : 0;
                    
                    $data_model['create_date'] = $date;
                    $data_model['create_user'] = $this->session->userdata('user_name');
                    $data_model['file_name'] = $file;

                    // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                    //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                    // }
                    // print_r($data_model);

                    $nResult = $this->ImportEventModel->insert_to_temp_checkpoint_mother($data_model);

                } else {
                    // if ($data['BO'] == '') {
                    //     $formattype = 1;
                    // } else if ($data['BO'] != '') {
                    //     $formattype = 2;
                    // } else if ($data['A'] != 'Order Item Id') {
                    //     $result['status'] = false;
                    //     $result['message'] = 'Wrong file format';
                    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    //     exit;
                    // }

                    // echo $formattype;
                    // die();
                }



                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            $row_count++;
            $count++;
            // $row_count=$row_count+1;
            // die();
        }

        // $check_duplicate = $this->ImportEventModel->check_duplicate();


        // print_r( $check_duplicate);

        if ($FlagFormat == true) {
            $result['status'] = false;
            $result['message'] =  $ErrorMassage;
        }
        //  else if (count($check_duplicate) > 0) {
        //     $result['status'] = false;
        //     $result['message'] = 'Duplicate data found from file. That has already been uploaded';
        //     // $this->ImportModel->DeleteTableTemp();
        // } 
        else {
            // die();

            $dataCheckBaby = $this->ImportEventModel->getCheckBabyModel();
            $dataCheckMother = $this->ImportEventModel->getCheckMotherModel();

            if(count($dataCheckBaby) > 0 || count($dataCheckMother) > 0){

                $this->ImportEventModel->delete_temp_checkpoint_baby();
                $this->ImportEventModel->delete_temp_checkpoint_mother();
                $result['status'] = false;
                $result['message'] = "Duplicate Data";
            }else{
                // die();
                $this->ImportEventModel->delete_checkpoint_baby();
                $this->ImportEventModel->delete_checkpoint_mother();

                // print_r("Update3");
                $dataimpot['file_name'] =  $file;
                $dataimpot['type'] =  'Excel Checkpoint Upload';
                $dataimpot['upload_date'] =  $date;

                $this->ImportEventModel->insertto_t_checkpoint_event_baby();
                $this->ImportEventModel->insertto_t_checkpoint_event_mother();

                // $this->ImportModel->insertto_customer($file, $date);
                $nResult = $this->ImportEventModel->insert_log_shipment($dataimpot);
                // $this->ImportModel->DeleteTableTemp();
                $result['status'] = true;
                $result['message'] = $this->lang->line('savesuccess');
            }


            
        };
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }






    public function getdailyModelList()
    {
        try {
            $this->load->model('ImportEventModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ImportEventModel->getdailyModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            // $result['totalRecords'] = $this->CashadvanceModel->getTotal($dataModel);
            // $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    // public function downloadpandora()
    // {
    //     try {
    //         $this->load->model('DailyimportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);
    //         // print_r($dataPost);die();
    //         $dataModel = isset($dataPost['id']) ? $dataPost['id'] : '';

    //         $result['status'] = true;
    //         $result['message'] = $this->DailyimportModel->downloadpandora($dataModel);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }

    // public function downloadshipment()
    // {
    //     try {
    //         $this->load->model('DailyimportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);
    //         // print_r($dataPost);die();
    //         $dataModel = isset($dataPost['id']) ? $dataPost['id'] : '';

    //         $result['status'] = true;
    //         $result['message'] = $this->DailyimportModel->downloadshipment($dataModel);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }
    public function deleteImport()
    {
        try {
            $this->load->model('ImportEventModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);die();
            $dataModel = isset($dataPost['file_name']) ? $dataPost['file_name'] : '';
            // print_r($dataModel);die();
            $result['status'] = true;
            $result['message'] = $this->ImportEventModel->deleteImport($dataModel);
            $result['ShowMessage'] = 'The deletion was successful.';
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

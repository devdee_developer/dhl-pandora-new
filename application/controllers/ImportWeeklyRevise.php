<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ImportWeeklyRevise extends MY_Controller
{
    // private $tbl_name = 'inventory';
    // private $id = 'id';

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }

        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
        ini_set('memory_limit', '1024M');
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_PANDASH');
        if (!$role_data['IMFILE']) {
            redirect('Login');
        }
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('import/importWeeklyRevise_view');
        $this->load->view('share/footer');
    }

    public function upload_file()
    {
        $this->alert_line_notify(); //Line Notify

        // print_r($_FILES['file_Pandora_Dashboard']);
        //  print_r('test');die();
        // $uniqid = uniqid();
        // $datapandora['uniqidpandora'] = $uniqid;
        // $datashipment['uniqidshipment'] = $uniqid;
        $date = date('Y-m-d-H-i-s');

        // $this->load->model('HeadpandoraModel', '', true);
        // $this->load->model('HeadShipmentModel', '', true);

        // $uniqidpandora = $this->HeadpandoraModel->insert($datapandora);
        // $uniqidshipment = $this->HeadShipmentModel->insert($datashipment);

        // $this->upload_filepandora($_FILES, $date, $uniqid);
        // $this->upload_fileshipment($_FILES, $date, $uniqid);
        $todayfile = 'Updated_Factor_Incident' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/import_excel';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_Updated_Factor_Incident']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);



        $status = true;
        if (!$this->upload->do_upload('file_Updated_Factor_Incident')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();
            $todayfile = $data['upload_data']['file_name'];
            $this->loadExcelpandoraDashboardToDB($todayfile, $date);
        }
    }


    public function loadExcelpandoraDashboardToDB($file, $date)
    {
        $formattype = 0;
        ini_set('max_execution_time', 300);

        $this->load->library('MyExcel');
        $this->load->model('ImportWeeklyReviseModel', '', true);
        // print_r($file);
        $objReader = PHPExcel_IOFactory::load('upload/import_excel/' . $file);

        // die();
        $sheetData = $objReader->setActiveSheetIndex(0)->toArray('', true, false, true);

        // $sheetData = $objReader->getsheet(0);
        // $sheetData = $objReader->getActiveSheet()->toArray('', true, false, true);

        // print_r($sheetData);
        // die();

        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        $this->ImportWeeklyReviseModel->delete_temp_weekly_revise();

        // echo 'delete_temp_pandora_dashboard success';die();
        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        // print_r($sheetData);die();




        foreach ($sheetData as $data) {

            try {

                if (
                    $count == 0
                   
                    and  (strtoupper("Waybill Number") ==  trim(strtoupper($data['A'])))
                    and  (strtoupper("Group Diff SLA ( Include weekend )") ==  trim(strtoupper($data['B'])))
                    and  (strtoupper("factor_incident") ==  trim(strtoupper($data['C'])))
                    and  (strtoupper("On time status") ==  trim(strtoupper($data['D'])))

                ) {
                    // echo "found data <br/>";
                    // echo'case 1';
                    $fuondFormat = 1;

                    // $this->JobTypeModel->dropJobType();
                } else if ($count == 0) {
                    // print_r($data);die();
                    // echo'SCAC';
                    // echo trim(strtoupper($data['E']));
                    $ErrorMassage = 'Your file has an out-of-format format in the column ';
                    $FlagFormat = true;


                    $ErrorMassage .=  (strtoupper("Waybill Number") ==  trim(strtoupper($data['A']))) ? "" : " A ";
                    $ErrorMassage .=  (strtoupper("Group Diff SLA ( Include weekend )") ==  trim(strtoupper($data['B']))) ? "" : " B ";
                    $ErrorMassage .=  (strtoupper("factor_incident") ==  trim(strtoupper($data['C']))) ? "" : " C ";
                    $ErrorMassage .=  (strtoupper("On time status") ==  trim(strtoupper($data['D']))) ? "" : " D ";

                    // $ErrorMassage .=  (strtoupper("factor_incident") ==  trim(strtoupper($data['E']))) ? "" : " E ";
                    // $ErrorMassage .=  (strtoupper("On time status") ==  trim(strtoupper($data['F']))) ? "" : " F ";
                    // $ErrorMassage .=  (strtoupper("Content.Delivery date") ==  trim(strtoupper($data['G']))) ? "" : " G ";
                    // $ErrorMassage .=  (strtoupper("Content.PC") ==  trim(strtoupper($data['H']))) ? "" : " H ";
                    // $ErrorMassage .=  (strtoupper("Content.Date") ==  trim(strtoupper($data['I']))) ? "" : " I ";
                    // $ErrorMassage .=  (strtoupper("Content.Dest") ==  trim(strtoupper($data['J']))) ? "" : " J ";
                    // $ErrorMassage .=  (strtoupper("Content.Factor Incident") ==  trim(strtoupper($data['K']))) ? "" : " K ";
                    // $ErrorMassage .=  (strtoupper("Content.Factor") ==  trim(strtoupper($data['L']))) ? "" : " L ";


                    // $ErrorMassage .=  (strtoupper("Waybill Number") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                    // $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W']))) ? "" : " W ";


                    // echo ("LIMIT" ==  trim(strtoupper($data['B']))) ? "T" : "F";
                    // echo "Miss data format ";
                    break;
                    // exit();
                }

                // print_r($FlagFormat);die();

                if ($row_count > 1) {

                    // $data_model['title_excel'] = $sheetData[2]['B'];


                    $data_model['awb_no'] = isset($data['A']) ? $data['A'] : 0;
                    $data_model['`Group_Diff_SLA_(Include_weekend)`'] = isset($data['B']) ? $data['B'] : 0;
                    $data_model['factor_incident'] = isset($data['C']) ? $data['C'] : 0;
                    $data_model['factor'] = isset($data['D']) ? $data['D'] : 0;

                    // $data_model['factor_incident'] = isset($data['E']) ? $data['E'] : 0;
                    // $data_model['factor'] = isset($data['F']) ? $data['F'] : 0;

                    // $data_model['Delivery_date'] = isset($data['G']) && $data['G'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['G'])->format('d/m/Y') : 0;


                    // $data_model['PC'] = isset($data['H']) ? $data['H'] : 0;
                    // $data_model['Date'] = isset($data['I'])  && $data['I'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['I'])->format('d/m/Y') : 0;
                    // $data_model['Dest'] = isset($data['J']) ? $data['J'] : 0;

                    // $data_model['Factor_Incident'] = isset($data['K']) ? $data['K'] : 0;
                    // $data_model['Factor'] = isset($data['L']) ? $data['L'] : 0;

                    // $data_model['awb_no'] = isset($data['V']) ? $data['V'] : 0;
                  




                    $data_model['create_date'] = $date;
                    $data_model['file_name'] = $file;

                    // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                    //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                    // }

                    // print_r($data_model);die();


                    $nResult = $this->ImportWeeklyReviseModel->insert_to_temp_weekly_revise($data_model);


                    // $datastatusid = $this->ImportWeeklyReviseModel->check_status_date($data_model);
                    // print_r($data_model);die();

                    $dataid = $this->ImportWeeklyReviseModel->check_duplicate($data_model['awb_no']);

                    if ($dataid == 0) {
                        $this->ImportWeeklyReviseModel->insert_t_shipment_weekly_revise($data_model);
                        // $this->ImportStatusCSModel->delete_t_temp_shipment_status_cs();
                    }


                    // $update_shipment_status_cs = $this->ImportStatusCSModel->update_t_shipment_status_cs();

                } else {
                    // if ($data['BO'] == '') {
                    //     $formattype = 1;
                    // } else if ($data['BO'] != '') {
                    //     $formattype = 2;
                    // } else if ($data['A'] != 'Order Item Id') {
                    //     $result['status'] = false;
                    //     $result['message'] = 'Wrong file format';
                    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    //     exit;
                    // }

                    // echo $formattype;
                    // die();
                }



                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            $row_count++;
            $count++;
            // $row_count=$row_count+1;
            // die();
        }

        // $check_duplicate = $this->ImportStatusCSModel->check_duplicate();
        // $update_shipment_status_cs = $this->ImportStatusCSModel->update_t_shipment_status_cs();

        // $dataid = $this->ImportStatusCSModel->check_duplicate($data_model['Waybill']);

        // if ($dataid == 0) {
        //     $this->ImportStatusCSModel->insert_t_shipment_status_cs($data_model);
        //     $this->ImportStatusCSModel->delete_t_temp_shipment_status_cs();
        // }


        $update_shipment_weekly_revise = $this->ImportWeeklyReviseModel->update_t_shipment_weekly_revise();

        $update_shipment_weekly_revise_to_cs = $this->ImportWeeklyReviseModel->update_t_shipment_weekly_revise_to_cs();

        // $checkpoint_shipment_status_cs = $this->ImportStatusCSModel->checkpoint_shipment_status_cs();

        // $updatename_shipment_status_cs = $this->ImportStatusCSModel->updatename_shipment_status_cs();


        // $datastatusid = $this->ImportStatusCSModel->check_status_date($data_model);
        // print_r($datastatusid);die();
        // print_r($data_model);die();


        // $delete_temp_shipment_status_cs = $this->ImportStatusCSModel->delete_t_temp_shipment_status_cs();



        // $delete_shipment_daily = $this->ImportStatusCSModel->delete_temp_pandora_dashboard();

        // $dataimpot['file_name'] =  $file;
        // $dataimpot['type'] =  'Excel Shipment Status CS Upload';
        // $dataimpot['upload_date'] =  $date;

        // print_r($dataimpot);die();

        if ($FlagFormat == 1) {
            $result['status'] = false;
            $result['message'] =  $ErrorMassage;
        } else {
            $dataimpot['file_name'] =  $file;
            $dataimpot['type'] =  'Excel Shipment Weekly Revise Upload';
            $dataimpot['upload_date'] =  $date;
            $nResult = $this->ImportWeeklyReviseModel->insert_log_shipment($dataimpot);

            $result['status'] = true;
            $result['message'] = $this->lang->line('savesuccess');
        };

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }


    public function getdailyModelList()
    {
        try {
            $this->load->model('ImportWeeklyReviseModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ImportWeeklyReviseModel->getdailyModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            $result['totalRecords'] = $this->ImportWeeklyReviseModel->getTotal($dataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function alert_line_notify()
    {
        // call line api
        $Token = 'UeMJwWbMswxUmnEA08yzzgGhWAuszabSpVT8BtUHvpr';
        $message = 'ImportUpdatedFactorIncident'.' '. date('Y-m-d');
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . ''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
        } else {
            $result_ = json_decode($result, true);
        }
        curl_close($chOne);
    }

    public function deleteImport()
    {
        try {
            $this->load->model('ImportWeeklyReviseModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);die();
            $dataModel = isset($dataPost['file_name']) ? $dataPost['file_name'] : '';
            // print_r($dataModel);die();
            $result['status'] = true;
            $result['message'] = $this->ImportWeeklyReviseModel->deleteImport($dataModel);
            $result['ShowMessage'] = 'The deletion was successful.';
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

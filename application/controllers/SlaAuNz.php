<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SlaAuNz extends CI_Controller
{
    public function __construct()
    {
        ini_set('memory_limit', '6144M'); 
        ini_set('MAX_EXECUTION_TIME', 0);

        parent::__construct();
        error_reporting(0);
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {

            redirect('https://th-logis.com/dhlsso/Login');
        }
        $this->session->mark_as_temp('validated', 1200);

        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
        ini_set('memory_limit', '2048M');
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_WLHOG');
        // check หน้า//
        // if( !$role_data ['AGNT'] ) {
        // 	redirect('Login');
        // }
        // check หน้า//
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('sla_au_nz/sla_au_nz_view');
        $this->load->view('share/footer');
    }

    public function getSlaAuNzList()
    {
        try {
            $this->load->model('SlaAuNzModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->SlaAuNzModel->getSlaAuNzModel($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function SaveSlaAuNz()
    {
        try {
            $this->load->model('SlaAuNzModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->SlaAuNzModel->SaveSlaAuNz($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function DeleteSlaAuNz()
    {
        try {
            $this->load->model('SlaAuNzModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->SlaAuNzModel->DeleteSlaAuNz($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function Exportexcel()
    {
        // log_message('error', 'Exportexcel');
        // ini_set("memory_limit", "10000M");
        // ini_set('MAX_EXECUTION_TIME', 0);
        set_time_limit(5000);

        try {
            $this->load->model('SlaAuNzModel', '', true);

            $this->load->library('MyExcel');

            $dataPost = json_decode($this->input->raw_input_stream, true);

            // print_r("test");
            // die();

            // $date = date('Y-m-d-H-i-s');
            // print_r($date);
            // die();

            // $date = '2021-07-28';

            // log_message('error', 'Can get date');
            // $data = [];
            $data = $this->SlaAuNzModel->ExportExcel($dataPost);
            // log_message('error', 'Can get data');
            // print_r($data);
            // echo"test";
            // die();

            //2021-07-29//
            // ini_set('max_execution_time', 300); //300 seconds = 5 minutes
            // set_time_limit(300);
            //2021-07-29//

            // print_r($data[0]['file_name']);die();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Sla Au Nz");

            // die();

            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ]

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => '00ffff')
                // )
            );


            $headbody = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => 'ffff00')
                // )
            );
            // //กำหนด style ส่วน body//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_THIN
            //         )
            //     )
            // );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);

            // $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(TRUE);

            $objPHPExcel->setActiveSheetIndex(0)

                ->setCellValue('A1', 'Destination City')
                ->setCellValue('B1', 'Destination Postcode')
                ->setCellValue('C1', 'Destination Svc Area Code')
                ->setCellValue('D1', 'Destination Country Code')
                ->setCellValue('E1', 'Lane')
                ->setCellValue('F1', 'Delivery Provider')
                ->setCellValue('G1', 'Gateway')
                ->setCellValue('H1', 'T T Sla')
                ->setCellValue('I1', 'Mon')
                ->setCellValue('J1', 'Tue')
                ->setCellValue('K1', 'Wed')
                ->setCellValue('L1', 'Thu')
                ->setCellValue('M1', 'Fri')
                ->setCellValue('N1', 'Sat')
                ->setCellValue('O1', 'T T Pandemic')
                ->setCellValue('P1', 'Product')
                ->setCellValue('Q1', 'Type')
                ->setCellValue('R1', 'State Region');
            // ->setCellValue('S1', 'Year')
            // ->setCellValue('T1', 'week_number')
            // ->setCellValue('U1', 'Waybill Number')
            // ->setCellValue('V1', 'Product Code')
            // ->setCellValue('W1', 'Product Name')
            // ->setCellValue('X1', 'Shipper Reference')
            // ->setCellValue('Y1', 'Shipper Company Name')
            // ->setCellValue('Z1', 'Destination Country/Territory Name')

            // ->setCellValue('AA1', 'Destination Country/Territory Area Code')
            // ->setCellValue('AB1', 'Destination Service Area Code')
            // ->setCellValue('AC1', 'Number of Days in Customs')
            // ->setCellValue('AD1', 'Startclock Day of Week')
            // ->setCellValue('AE1', 'Startclock Date')
            // ->setCellValue('AF1', 'Stopclock Date')
            // ->setCellValue('AG1', 'Stopclock Day of Week')
            // ->setCellValue('AH1', 'Signatory')
            // ->setCellValue('AI1', 'Elapsed Actual Transit Days')
            // ->setCellValue('AJ1', 'Reporting Code Description')
            // ->setCellValue('AK1', 'Reporting Code Category Description')
            // ->setCellValue('AL1', 'Consignee Company Name')
            // ->setCellValue('AM1', 'Consignee Address')
            // ->setCellValue('AN1', 'Consignee Zip')
            // ->setCellValue('AO1', 'Consignee City ')
            // ->setCellValue('AP1', 'Billing Account Number')
            // ->setCellValue('AQ1', 'Manifested Number of Pieces')
            // ->setCellValue('AR1', 'Billed Weight')
            // ->setCellValue('AS1', 'Customs Declared Value')
            // ->setCellValue('AT1', 'Currency Code of Declared Value')
            // ->setCellValue('AU1', 'Unknown POD Status');

            // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($headbody);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($headtable);

            // $objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AN1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AO1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AP1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AQ1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AR1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AS1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AT1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AU1')->applyFromArray($headtable);


            $start_row = 2;
            foreach ($data as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Q' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('R' . $start_row)->applyFromArray($styleArray);

                
                $objPHPExcel->setActiveSheetIndex(0)
                    // ->setCellValue('A' . $start_row, date('d/m/Y', strtotime($row['update_date'])))
                    ->setCellValue('A' . $start_row, $row['destination_city'])
                    ->setCellValue('B' . $start_row, $row['destination_postcode'])
                    ->setCellValue('C' . $start_row, $row['destination_svc_area_code'])
                    ->setCellValue('D' . $start_row, $row['destination_country_code'])
                    ->setCellValue('E' . $start_row, $row['lane'])
                    ->setCellValue('F' . $start_row, $row['delivery_provider'])
                    // ->setCellValueExplicit("F". $start_row, $row['Group T/T Include weekend range'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('G' . $start_row, $row['gateway'])
                    ->setCellValue('H' . $start_row, $row['t_t_sla'])
                    ->setCellValue('I' . $start_row, $row['1_mon'])
                    ->setCellValue('J' . $start_row, $row['2_tue'])
                    ->setCellValue('K' . $start_row, $row['3_wed'])
                    ->setCellValue('L' . $start_row, $row['4_thu'])
                    ->setCellValue('M' . $start_row, $row['5_fri'])
                    ->setCellValue('N' . $start_row, $row['6_sat'])
                    ->setCellValue('O' . $start_row, $row['t_t_pandemic'])
                    ->setCellValue('P' . $start_row, $row['product'])
                    ->setCellValue('Q' . $start_row, $row['type'])
                    ->setCellValue('R' . $start_row, $row['state_region']);


                $start_row++;
            }
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $filename = $start.' to '.$end . '.xlsx';

            $filename = "Sla_Au_Nz" . date("Y-m-d-H-i-s") . '.xlsx';
            // echo FCPATH; die();
            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);
            $result['message'] = $filename;
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function upload_file()
    {
        // print_r($_FILES['file_Pandora_SlaAuNz']);die();

        $date = date('Y-m-d-H-i-s');


        $todayfile = 'Pandora_SlaAuNz' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/import_excel';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_Pandora_SlaAuNz']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $status = true;
        if (!$this->upload->do_upload('file_Pandora_SlaAuNz')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();
            $todayfile = $data['upload_data']['file_name'];
            $this->loadExcelpandoraDashboardToDB($todayfile, $date);
        }
    }


    public function loadExcelpandoraDashboardToDB($file, $date)
    {
        //  print_r($date);die();
        $formattype = 0;
        ini_set('max_execution_time', 300);

        $this->load->library('MyExcel');
        $this->load->model('SlaAuNzModel', '', true);
        // print_r($file);die();
        $objReader = PHPExcel_IOFactory::load('upload/import_excel/' . $file);
        

        // print_r($objReader);die();

        $sheetData = $objReader->getActiveSheet()->toArray('', true, false, true);

        // print_r($sheetData);die();

        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        // $this->ImportModel->delete_temp_pandora_dashboard();

        // echo 'delete_temp_pandora_dashboard success';die();

        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        // print_r($sheetData[4]);die();

        foreach ($sheetData as $data) {

            try {

                if (
                    $count == 0
                    // and  ("NAME" == trim(strtoupper($data['A'])))

                    and  (strtoupper("Destination City") ==  trim(strtoupper($data['A'])))
                    and  (strtoupper("Destination Postcode") ==  trim(strtoupper($data['B'])))
                    and  (strtoupper("Destination Svc Area Code") ==  trim(strtoupper($data['C'])))
                    and  (strtoupper("Destination Country Code") ==  trim(strtoupper($data['D'])))
                    and  (strtoupper("Lane") ==  trim(strtoupper($data['E'])))
                    and  (strtoupper("Delivery Provider") ==  trim(strtoupper($data['F'])))
                    and  (strtoupper("Gateway") ==  trim(strtoupper($data['G'])))
                    and  (strtoupper("T T Sla") ==  trim(strtoupper($data['H'])))
                    and  (strtoupper("Mon") ==  trim(strtoupper($data['I'])))
                    and  (strtoupper("Tue") ==  trim(strtoupper($data['J'])))
                    and  (strtoupper("Wed") ==  trim(strtoupper($data['K'])))
                    and  (strtoupper("Thu") ==  trim(strtoupper($data['L'])))
                    and  (strtoupper("Fri") ==  trim(strtoupper($data['M'])))
                    and  (strtoupper("Sat") ==  trim(strtoupper($data['N'])))
                    and  (strtoupper("T T Pandemic") ==  trim(strtoupper($data['O'])))
                    and  (strtoupper("Product") ==  trim(strtoupper($data['P'])))
                    and  (strtoupper("Type") ==  trim(strtoupper($data['Q'])))
                    and  (strtoupper("State Region") ==  trim(strtoupper($data['R'])))

                

                ) {
                    // print_r($count);
                    $this->SlaAuNzModel->delete_slaAuNz();

                    // echo "found data <br/>";
                    $fuondFormat = 1;
                    // $this->JobTypeModel->dropJobType();
                } else if ($count == 0) {

                    // print_r($data);
                    // echo'SCAC';
                    // echo trim(strtoupper($data['E']));
                    $ErrorMassage = 'Your file has an out-of-format format in the column ';
                    $FlagFormat = true;

                    $ErrorMassage .=  (strtoupper("Destination City") ==  trim(strtoupper($data['A']))) ? "" : " A ";
                    $ErrorMassage .=  (strtoupper("Destination Postcode") ==  trim(strtoupper($data['B']))) ? "" : " B ";
                    $ErrorMassage .=  (strtoupper("Destination Svc Area Code") ==  trim(strtoupper($data['C']))) ? "" : " C ";
                    $ErrorMassage .=  (strtoupper("Destination Country Code") ==  trim(strtoupper($data['D']))) ? "" : " D ";
                    $ErrorMassage .=  (strtoupper("Lane") ==  trim(strtoupper($data['E']))) ? "" : " E ";
                    $ErrorMassage .=  (strtoupper("Delivery Provider") ==  trim(strtoupper($data['F']))) ? "" : " F ";

                    $ErrorMassage .=  (strtoupper("Gateway") ==  trim(strtoupper($data['G']))) ? "" : " G ";
                    $ErrorMassage .=  (strtoupper("T T Sla") ==  trim(strtoupper($data['H']))) ? "" : " H ";
                    $ErrorMassage .=  (strtoupper("Mon") ==  trim(strtoupper($data['I']))) ? "" : " I ";
                    $ErrorMassage .=  (strtoupper("Tue") ==  trim(strtoupper($data['J']))) ? "" : " J ";
                    $ErrorMassage .=  (strtoupper("Wed") ==  trim(strtoupper($data['K']))) ? "" : " K ";
                    $ErrorMassage .=  (strtoupper("Thu") ==  trim(strtoupper($data['L']))) ? "" : " L ";
                    $ErrorMassage .=  (strtoupper("Fri") ==  trim(strtoupper($data['M']))) ? "" : " M ";
                    $ErrorMassage .=  (strtoupper("Sat") ==  trim(strtoupper($data['N']))) ? "" : " N ";
                    $ErrorMassage .=  (strtoupper("T T Pandemic") ==  trim(strtoupper($data['O']))) ? "" : " O ";
                    $ErrorMassage .=  (strtoupper("Product") ==  trim(strtoupper($data['P']))) ? "" : " P ";
                    $ErrorMassage .=  (strtoupper("Type") ==  trim(strtoupper($data['Q']))) ? "" : " Q ";
                    $ErrorMassage .=  (strtoupper("State Region") ==  trim(strtoupper($data['R']))) ? "" : " R ";
                    // $ErrorMassage .=  (strtoupper("Consignee Zip") ==  trim(strtoupper($data['S']))) ? "" : " S ";
                    // $ErrorMassage .=  (strtoupper("Consignee City") ==  trim(strtoupper($data['T']))) ? "" : " T ";
                    // $ErrorMassage .=  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U']))) ? "" : " U ";
                    // $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                    // $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W']))) ? "" : " W ";
                    // $ErrorMassage .=  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X']))) ? "" : " X ";

                    // echo ("LIMIT" ==  trim(strtoupper($data['B']))) ? "T" : "F";
                    // echo "Miss data format ";
                    break;
                    // exit();
                }

                // print_r($FlagFormat);die();

                if ($row_count > 1) {


                    // $data_model['title_excel'] = $sheetData[2]['B'];

                    $data_model['destination_city'] = isset($data['A']) ? $data['A'] : 0;
                    $data_model['destination_postcode'] = isset($data['B']) ? $data['B'] : 0;
                    $data_model['destination_svc_area_code'] = isset($data['C']) ? $data['C'] : 0;
                    $data_model['destination_country_code'] = isset($data['D']) ? $data['D'] : 0;
                    $data_model['lane'] = isset($data['E']) ? $data['E'] : 0;
                    $data_model['delivery_provider'] = isset($data['F']) ? $data['F'] : 0;

                    // $data_model['event_date'] =  $event_date;
                    $data_model['gateway'] = isset($data['G']) ? $data['G'] : 0;
                    $data_model['t_t_sla'] = isset($data['H']) ? $data['H'] : 0;
                    $data_model['1_mon'] = isset($data['I']) ? $data['I'] : 0;
                    $data_model['2_tue'] = isset($data['J']) ? $data['J'] : 0;
                    $data_model['3_wed'] = isset($data['K']) ? $data['K'] : 0;
                    $data_model['4_thu'] = isset($data['L']) ? $data['L'] : 0;
                    // $data_model['5_fri'] = "";
                    $data_model['5_fri'] = isset($data['M']) ? $data['M'] : 0;
                    $data_model['6_sat'] = isset($data['N']) ? $data['N'] : 0;
                    // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                    $data_model['t_t_pandemic'] = isset($data['O']) ? $data['O'] : 0;
                    $data_model['product'] = isset($data['P']) ? $data['P'] : 0;
                    $data_model['type'] = isset($data['Q']) ? $data['Q'] : 0;
                    $data_model['state_region'] = isset($data['R']) ? $data['R'] : 0;
                    // $data_model['Consignee Zip'] = isset($data['S']) ? $data['S'] : 0;
                    // $data_model['Consignee City'] = isset($data['T']) ? $data['T'] : 0;
                    // $data_model['Billing Account Number'] = isset($data['W']) ? $data['W'] : 0;
                    // $data_model['Manifested Number of Pieces'] = isset($data['U']) ? $data['U'] : 0;
                    // $data_model['Billed Weight'] = "";
                    // $data_model['Customs Declared Value'] = isset($data['X']) ? $data['X'] : 0;
                    // $data_model['Currency Code of Declared Value'] = isset($data['V']) ? $data['V'] : 0;
                    // $data_model['column_ad'] = isset($data['AD']) ? $data['AD'] : 0;
                    // $data_model['em_sales_order_id'] = isset($data['AE']) ? $data['AE'] : 0;
                    // $data_model['em_origin'] = isset($data['AF']) ? $data['AF'] : 0;
                    // $data_model['em_current_event_name'] = isset($data['AG']) ? $data['AG'] : 0;
                    // $data_model['em_current_event_datetime'] = isset($data['AH']) && $data['AH'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AH'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_current_event_reason_code'] = isset($data['AI']) ? $data['AI'] : 0;
                    // $data_model['em_current_event_location_description'] = isset($data['AJ']) ? $data['AJ'] : 0;
                    // $data_model['dq_postship_carrier'] = isset($data['AK']) ? $data['AK'] : 0;
                    // $data_model['ops_channel_level_2_code'] = isset($data['AL']) ? $data['AL'] : 0;
                    // $data_model['em_ship_condition_code'] = isset($data['AM']) ? $data['AM'] : 0;
                    // $data_model['em_ship_point_type'] = isset($data['AN']) ? $data['AN'] : 0;
                    // $data_model['em_ship_point_code'] = isset($data['AO']) ? $data['AO'] : 0;
                    // $data_model['em_city_name'] = isset($data['AP']) ? $data['AP'] : 0;
                    // $data_model['em_postal_code'] = isset($data['AQ']) ? $data['AQ'] : 0;
                    // $data_model['em_ship_confirmation_last_date'] = isset($data['AR']) && $data['AR'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AR'])->format('Y-m-d') : 0;

                    // $data_model['em_ship_confirmation_last_time'] = isset($data['AS']) && $data['AS'] != '' ? PHPExcel_Style_NumberFormat::toFormattedString($data['AS'], 'hh:mm:ss') : 0;
                    // $data_model['em_shipment_picked_up_last_datetime'] = isset($data['AT']) && $data['AT'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AT'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_uplift_last_datetime'] = isset($data['AU']) && $data['AU'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AU'])->format('Y-m-d H:i:s') : null;
                    // $data_model['em_first_attempt_date'] = isset($data['AV']) && $data['AV'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AV'])->format('Y-m-d') : null;
                    // $data_model['em_first_attempt_reason_code'] = isset($data['AW']) ? $data['AW'] : 0;
                    // $data_model['em_first_attempt_reason_code_description'] = isset($data['AX']) ? $data['AX'] : 0;
                    // $data_model['em_delivery_not_completed_last_date'] = isset($data['AY']) && $data['AY'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AY'])->format('Y-m-d') : null;
                    // $data_model['em_delivery_not_completed_last_reason_code_description'] = isset($data['AZ']) ? $data['AZ'] : 0;
                    // $data_model['em_shipment_delay_last_date'] = isset($data['BA']) && $data['BA'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['BA'])->format('Y-m-d') : null;
                    // $data_model['em_shipment_delay_last_reason_code'] = isset($data['BB']) ? $data['BB'] : 0;
                    // $data_model['em_shipment_delay_last_reason_code_description'] = isset($data['BC']) ? $data['BC'] : 0;
                    // $data_model['em_count_of_delivery_attempts'] = isset($data['BD']) ? $data['BD'] : 0;

                    // $data_model['create_date'] = $date;
                    // $data_model['create_user'] = $this->session->userdata('user_name');
                    // $data_model['new_flag'] = 1;

                    // $data_model['create_date'] = $date;
                    // $data_model['file_name'] = $file;

                    // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                    //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                    // }

                    $nResult = $this->SlaAuNzModel->insert_to_slaAuNz($data_model);
                }
                $row_count++;
                $count++;


                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            // $row_count++;
            // $count++;
            // $row_count=$row_count+1;
            // die();
        }

        $update_destination_postcode = $this->SlaAuNzModel->update_destination_postcode();

        // $check_duplicate = $this->ImportModel->check_duplicate();

        // print_r($FlagFormat);
        // die();
        if ($FlagFormat == true) {
            $result['status'] = false;
            $result['message'] =  $ErrorMassage;
        } else {

            // $this->SlaAuNzModel->delete_slaAuNz();
            // print_r("Update3");
            $dataUpdate['update_date'] =  date('Y-m-d H:i:s');
            $dataUpdate['update_user'] = $this->session->userdata('user_name');
            // $return = $this->SlaAuNzModel->update_slaAuNz($dataUpdate);
            // print_r($return);
            // die();
            $result['status'] = true;
            $result['message'] = $this->lang->line('savesuccess');
        };
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }
}

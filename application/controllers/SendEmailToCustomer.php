<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SendEmailToCustomer extends CI_Controller
{
    // private $tbl_name = 'inventory';
    // private $id = 'id';

    public function __construct()
    {
        ini_set('memory_limit', '4095M'); 
        ini_set('MAX_EXECUTION_TIME', 0);

        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
    }

    public function index()
    {
        echo"hello";die();
        // $role_data =  $this->session->userdata('role_PANDASH');
        // if (!$role_data['EFILE']) {
        //     redirect('Login');
        // }
        // $this->load->view('share/head');
        // $this->load->view('share/sidebar');
        // $this->load->view('export/export_view');
        // $this->load->view('share/footer');

    }

    public function ExportexcelDaily()
    {
        $this->alert_line_notify_SentEmail_Pandora_Dashboard(); 
        // log_message('error', 'Exportexcel');
        // ini_set("memory_limit", "10000M");
        // ini_set('MAX_EXECUTION_TIME', 0);
        // ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '300');
        set_time_limit(300);

        try {
            $this->load->model('SendEmailToCustomerModel', '', true);

            $this->load->library('MyExcel');

            // $dataPost = json_decode($this->input->raw_input_stream, true);

            // $dataPost['start_date'] = date('Y-m-d', strtotime($dataPost['start_date']));

            // $dataPost['end_date'] = date('Y-m-d', strtotime($dataPost['end_date']));
            

            // $dateStartDate = date_create($dataPost['start_date']);
            // $dateEndDate = date_create($dataPost['end_date']);
            // $dateDiff = date_diff($dateStartDate,$dateEndDate)->format("%a");  
            // // print_r($dateDiff);

            $data = $this->SendEmailToCustomerModel->ExportExcelDailyToEmail();
            

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Sheet1");

            // die();

            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ]

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => '00ffff')
                // )
            );

            $headcolor = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'CBFFB0')
                )
            );

            $headcoloryellow = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'F6FFB0')
                )
            );

            $headcoloryellow2 = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ],

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E4FB32')
                )
            );


            $headbody = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => 'ffff00')
                // )
            );
            // //กำหนด style ส่วน body//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_THIN
            //         )
            //     )
            // );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);

            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(TRUE);


            // if (isset($dataModel['end_date']) && $dataModel['end_date'] != "") {
            //     $EndDateTS = strtotime($dataModel['end_date']);
            //     if ($EndDateTS !== false) {
            //         date('Y-m-d', $EndDateTS);
            //     }
            // }
            // if (isset($dataModel['date']) && $dataModel['date'] != "") {
            //     $StarDateTS = strtotime($dataModel['date']);
            //     if ($StarDateTS !== false) {
            //         date('Y-m-d', $StarDateTS);
            //     }
            // }
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'A')
                ->setCellValue('B1', 'Date_Pickup')
                // ->setCellValue('B1', 'Group Over SLA ( Exclude weekend )')
                // ->setCellValue('C1', 'Group Over SLA ( Include weekend )')
                ->setCellValue('C1', 'Mother_WB')
                ->setCellValue('D1', 'HAWB_No')
                ->setCellValue('E1', 'Shipper_Company')
                ->setCellValue('F1', 'Cust_ID')
                ->setCellValue('G1', 'Consignee_Company')
                ->setCellValue('H1', 'Consignee_Address')
                ->setCellValue('I1', 'Account')
                ->setCellValue('J1', 'Origin')
                ->setCellValue('K1', 'Destination')
                ->setCellValue('L1', 'M_IATA')
                ->setCellValue('M1', 'Destination_Ctry')
                ->setCellValue('N1', 'Destination_City')
                ->setCellValue('O1', 'Postal_Code')
                ->setCellValue('P1', 'Peice')
                ->setCellValue('Q1', 'A_Weight')
                ->setCellValue('R1', 'V_Weight')
                ->setCellValue('S1', 'Product')
                ->setCellValue('T1', 'Delivery')
                ->setCellValue('U1', 'Shipment_Status')
                ->setCellValue('V1', 'POD')
                ->setCellValue('W1', 'Status_Date')
                ->setCellValue('X1', 'Status_Time')
                ->setCellValue('Y1', 'Pickup_Date')
                ->setCellValue('Z1', 'SLA_C')
                ->setCellValue('AA1', 'SLA_Remark')
                ->setCellValue('AB1', 'incident_Remark')
                ->setCellValue('AC1', 'FD_Remark')
                ->setCellValue('AD1', 'week')
                ->setCellValue('AE1', 'Pick_up_day')
                // ->setCellValue('AE1', 'Pickup day')Startclock Day of Week
                ->setCellValue('AF1', 'Month')
                ->setCellValue('AG1', 'Year')

                ->setCellValue('AH1', 'Region')
                ->setCellValue('AI1', 'Gateway')
                ->setCellValue('AJ1', 'delivery_provider')
                ->setCellValue('AK1', 'Actual_T/T')
                ->setCellValue('AL1', 'Group_Diff_SLA')
                ->setCellValue('AM1', 'status')
                ->setCellValue('AN1', 'Reporting_Code_Description')
                ->setCellValue('AO1', 'Reporting_Code_Category')
                ->setCellValue('AP1', 'State Region');
            // ->setCellValue('AO1', 'Consignee Zip')
            // ->setCellValue('AP1', 'Consignee City ')
            // ->setCellValue('AQ1', 'Billing Account Number')
            // ->setCellValue('AR1', 'Manifested Number of Pieces')
            // ->setCellValue('AS1', 'Billed Weight')
            // ->setCellValue('AT1', 'Customs Declared Value')
            // ->setCellValue('AU1', 'Currency Code of Declared Value')
            // ->setCellValue('AV1', 'Unknown POD Status');    

            // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($headbody);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headcoloryellow2);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headcoloryellow2);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headcolor);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headcoloryellow);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($headtable);

            $objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AN1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AO1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('AP1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AQ1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AR1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AS1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AT1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AU1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AV1')->applyFromArray($headtable);


            $start_row = 2;
            foreach ($data as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Q' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('R' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('S' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('T' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('U' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('V' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('W' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('X' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Y' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('Z' . $start_row)->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->getStyle('AA' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AB' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AC' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AD' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AE' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('AP' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AQ' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AR' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AS' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AT' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AU' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AV' . $start_row)->applyFromArray($styleArray);


                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $start_row, 0)
                    ->setCellValue('B' . $start_row, $row['Date_Pickup'])

                    ->setCellValue('C' . $start_row, $row['Mother_WB'])
                    // ->setCellValue('A' . $start_row, "Test");
                    ->setCellValue('D' . $start_row, $row['HAWB_No'])
                    ->setCellValue('E' . $start_row, $row['Shipper_Company'])
                    ->setCellValue('F' . $start_row, $row['Cust_ID'])
                    ->setCellValue('G' . $start_row, $row['Consignee_Company'])
                    // ->setCellValueExplicit("G". $start_row, $row['Group T/T Include weekend range'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue('H' . $start_row, $row['Consignee_Address'])
                    ->setCellValue('I' . $start_row, $row['Account'])
                    ->setCellValue('J' . $start_row, $row['Origin'])
                    ->setCellValue('K' . $start_row, $row['Destination'])
                    ->setCellValue('L' . $start_row, $row['M_IATA'])
                    ->setCellValue('M' . $start_row, $row['Destination_Ctry'])
                    ->setCellValue('N' . $start_row, $row['Destination_City'])
                    ->setCellValue('O' . $start_row, $row['Postal_Code'])
                    ->setCellValue('P' . $start_row, $row['Peice'])
                    ->setCellValue('Q' . $start_row, $row['A_Weight'])
                    ->setCellValue('R' . $start_row, $row['V_Weight'])
                    ->setCellValue('S' . $start_row, $row['Product'])
                    ->setCellValue('T' . $start_row, $row['Delivery'])
                    ->setCellValue('U' . $start_row, $row['Shipment_Status'])
                    ->setCellValue('V' . $start_row, $row['POD'])
                    ->setCellValue('W' . $start_row, $row['Status_Date'])
                    ->setCellValue('X' . $start_row, $row['Status_Time'])
                    ->setCellValue('Y' . $start_row, $row['Pickup_Date'])
                    ->setCellValue('Z' . $start_row, $row['SLA_C'])
                    ->setCellValue('AA' . $start_row, $row['SLA_Remark'])
                    ->setCellValue('AB' . $start_row, $row['incident_Remark'])
                    ->setCellValue('AC' . $start_row, $row['FD_Remark'])
                    ->setCellValue('AD' . $start_row, $row['week'])
                    ->setCellValue('AE' . $start_row, $row['Pick_up_day'])
                    ->setCellValue('AF' . $start_row, $row['Month'])
                    ->setCellValue('AG' . $start_row, $row['Year'])
                    ->setCellValue('AH' . $start_row, $row['Region'])
                    ->setCellValue('AI' . $start_row, $row['Gateway'])
                    ->setCellValue('AJ' . $start_row, $row['delivery_provider'])
                    ->setCellValue('AK' . $start_row, $row['Actual_T/T'])
                    ->setCellValue('AL' . $start_row, $row['Group_Diff_SLA'])
                    ->setCellValue('AM' . $start_row, $row['status'])
                    ->setCellValue('AN' . $start_row, $row['Reporting_Code_Description'])
                    ->setCellValue('AO' . $start_row, $row['Reporting_Code_Category'])
                    ->setCellValue('AP' . $start_row, $row['state_region']);
                // ->setCellValue('AQ' . $start_row, $row['Billing Account Number'])
                // ->setCellValue('AR' . $start_row, $row['Manifested Number of Pieces'])
                // ->setCellValue('AS' . $start_row, $row['Billed Weight'])
                // ->setCellValue('AT' . $start_row, $row['Customs Declared Value'])
                // ->setCellValue('AU' . $start_row, $row['Currency Code of Declared Value'])
                // ->setCellValue('AV' . $start_row, $row['Unknown POD Status']);




                $start_row++;
            }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $filename = $start.' to '.$end . '.xlsx';

            $filename = 'Sample Table.xlsx';

            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);

            $urlShipment = FCPATH . 'upload/export_excel/' . $filename;
            $url = FCPATH . 'upload/PandoraProActiveReport.xlsm';

            $file_name = basename($url);
            if (file_put_contents($file_name, file_get_contents($url))) {
                $result['status'] = true;
                $result['message'] = $file_name;
            } else {
                $result['status'] = false;
                $result['message'] = "File downloading failed.";
            }

            $excelShipment = basename($urlShipment);

            if (file_put_contents($excelShipment, file_get_contents($urlShipment))) {
                $result['status'] = true;
                $result['message'] = $excelShipment;
            } else {
                $result['status'] = false;
                $result['message'] = "File downloading failed.";
            }

            $this->load->library('zip');
            $fileZip = "PandoraProActiveReport" . date("Y-m-d-H-i-s") . '.zip';

            $this->zip->read_file($excelShipment);
            $this->zip->read_file($file_name);
            $this->zip->archive(FCPATH . '/upload/pivot/' . $fileZip);


            $result['message'] = $fileZip;
            $result['status'] = true;

            $this->PadoraDashboardSentEmail($fileZip);

        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
    
    public function PadoraDashboardSentEmail($fileZip)
    {
        // print_r($fileZip);die();
        $subject = "Email Information";
        //ของใหม่

        $config = array(
            'protocol'  => 'smtp',

            'smtp_host' => 'mail.th-logistic.net',

            // 'smtp_port' =>  25,//ของเดิม//
            'smtp_port' =>  587,

            'smtp_user' => 'pandora_dashboard@th-logistic.net',
            // ของเดิม
            // 'smtp_pass' => '1234567',
            // ของเดิม
            // ของใหม่
            'smtp_pass' => 'P@sswOrd@Dhl',
            // ของใหม่
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            // 'smtp_crypto' => "tls"
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->from('pandora_dashboard@th-logistic.net');

        $subject = "[ทดสอบ Auto Daily Dashboard Email]";

        $ccMail = "akarapon@devdeeth.com,paradontou11@gmail.com";
        // $email = "Sunita.Sojaroentham@dhl.com,montira.s@pandora.net";
        $email = "paradontou11@gmail.com";
        
    
        $this->email->to($email);
        $this->email->cc($ccMail);
        $this->email->subject($subject);
        // $this->email->attach(FCPATH . 'upload/GenFile_Excel_AndSentEmailToMontransport/' . $filename);
        // $this->email->attach(FCPATH . '/upload/pivot/' . $fileZip);

        $FileZiip = 'https://th-logistic.net/dhl-pandora-new/upload/pivot/' . $fileZip;
        // $tester = FCPATH . '/upload/pivot/' . $fileZip;
        // print_r($tester);die();

        $month = date("m");

		if ($month == "0") {
			$StartDate = (date("Y")-1) ."-". "12";
		}else{
			$StartDate = (date("Y")) ."-". (date("m")-1);
		}

		$EndtDate = date("Y-m-d");

        $SentDate = date('Y-m-d');
        $body = "Daily Pro Active Dashboard / " . $SentDate . "<br> Start pickup date : " . $StartDate ."-01"." End pickup date : " . $EndtDate .
                "<br> please click link for donwload report : " . $FileZiip;

        $this->email->message($body);
        // $this->email->send();
        if (!$this->email->send()) {
            show_error($this->email->print_debugger());
        } else {
            echo true;
        }

    }

    public function alert_line_notify_SentEmail_Pandora_Dashboard()
    {
        // call line api
        $Token = 'UeMJwWbMswxUmnEA08yzzgGhWAuszabSpVT8BtUHvpr';
        $message = 'SentEmail Pandora Dashboard (Daily)'.' '. date('Y-m-d');
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . ''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
        } else {
            $result_ = json_decode($result, true);
        }
        curl_close($chOne);
    }

}



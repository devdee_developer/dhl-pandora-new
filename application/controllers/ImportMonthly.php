<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ImportMonthly extends MY_Controller
{
    // private $tbl_name = 'inventory';
    // private $id = 'id';

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
        
        $this->load->library('MyExcel');
        $this->styleError = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
            ),
        );
        $this->styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );

        $this->styleBg = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'font' => array(
                //'name' => 'Verdana',
                'color' => array('rgb' => '000000'),
                //'size' => 11
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF99'),
            ),
        );
        ini_set('memory_limit', '1024M');
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_PANDASH');
        if (!$role_data['IMFILE']) {
            redirect('Login');
        }
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('import/importMonthly_view');
        $this->load->view('share/footer');
    }

    public function upload_file()
    {
        // print_r($_FILES['file_Pandora_Dashboard']);
        //  print_r('test');die();
        // $uniqid = uniqid();
        // $datapandora['uniqidpandora'] = $uniqid;
        // $datashipment['uniqidshipment'] = $uniqid;
        $date = date('Y-m-d-H-i-s');

        // $this->load->model('HeadpandoraModel', '', true);
        // $this->load->model('HeadShipmentModel', '', true);

        // $uniqidpandora = $this->HeadpandoraModel->insert($datapandora);
        // $uniqidshipment = $this->HeadShipmentModel->insert($datashipment);

        // $this->upload_filepandora($_FILES, $date, $uniqid);
        // $this->upload_fileshipment($_FILES, $date, $uniqid);
        $todayfile = 'Pandora_Dashboard' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/import_excel';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_Pandora_Dashboard']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        

        $status = true;
        if (!$this->upload->do_upload('file_Pandora_Dashboard')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();
            $todayfile = $data['upload_data']['file_name'];
            $this->loadExcelpandoraDashboardToDB($todayfile, $date);
        }
    }


    public function loadExcelpandoraDashboardToDB($file, $date)
    {
        $formattype = 0;
        ini_set('max_execution_time', 300);

        $this->load->library('MyExcel');
        $this->load->model('ImportMonthlyModel', '', true);
        // print_r($file);
        $objReader = PHPExcel_IOFactory::load('upload/import_excel/' . $file);

        // die();


        $sheetData = $objReader->getSheet(1)->toArray('', true, false, true);

        // print_r($sheetData);die();

        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        $this->ImportMonthlyModel->delete_temp_pandora_dashboard();

        // echo 'delete_temp_pandora_dashboard success';die();
        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        // print_r($sheetData[4]);die();

        foreach ($sheetData as $data) {

            try {

                if (
                    $count == 0
                    // and  ("NAME" == trim(strtoupper($data['A'])))

                    and  (strtoupper("Group Diff SLA ( Exclude weekend )") ==  trim(strtoupper($data['A'])))
                    and  (strtoupper("Group Diff SLA ( Include weekend )") ==  trim(strtoupper($data['B'])))
                    and  (strtoupper("factor_incident") ==  trim(strtoupper($data['C'])))
                    and  (strtoupper("On time status") ==  trim(strtoupper($data['D'])))
                    and  (strtoupper("Group T/T Include weekend range") ==  trim(strtoupper($data['E'])))
                    and  (strtoupper("lane") ==  trim(strtoupper($data['F'])))

                    and  (strtoupper("Diff SLA ( Include weekend )") ==  trim(strtoupper($data['G'])))
                    and  (strtoupper("Diff SLA ( Exclude weekend )") ==  trim(strtoupper($data['H'])))
                    and  (strtoupper("T/T included weekend by_pickup_day") ==  trim(strtoupper($data['I'])))
                    and  (strtoupper("T/T excluded weekend") ==  trim(strtoupper($data['J'])))
                    and  (strtoupper("T/T included weekend") ==  trim(strtoupper($data['K'])))
                    and  (strtoupper("delivery_provider") ==  trim(strtoupper($data['L'])))
                    and  (strtoupper("gateway") ==  trim(strtoupper($data['M'])))
                    and  (strtoupper("shipmonth") ==  trim(strtoupper($data['N'])))
                    and  (strtoupper("Quater") ==  trim(strtoupper($data['O'])))
                    and  (strtoupper("Year") ==  trim(strtoupper($data['P'])))
                    and  (strtoupper("week_number") ==  trim(strtoupper($data['Q'])))
                    and  (strtoupper("Waybill Number") ==  trim(strtoupper($data['R'])))
                    and  (strtoupper("Product Code") ==  trim(strtoupper($data['S'])))
                    and  (strtoupper("Product Name") ==  trim(strtoupper($data['T'])))
                    and  (strtoupper("Shipper Reference") ==  trim(strtoupper($data['U'])))
                    and  (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['V'])))
                    and  (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['W'])))
                    and  (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['X'])))
                    and  (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['Y'])))
                    and  (strtoupper("Number of Days in Customs") ==  trim(strtoupper($data['Z'])))
                    and  (strtoupper("Pickup day") ==  trim(strtoupper($data['AA'])))
                    and  (strtoupper("Startclock Date") ==  trim(strtoupper($data['AB'])))
                    and  (strtoupper("Stopclock Date") ==  trim(strtoupper($data['AC'])))
                    and  (strtoupper("Stopclock Day of Week") ==  trim(strtoupper($data['AD'])))
                    and  (strtoupper("Signatory") ==  trim(strtoupper($data['AE'])))
                    and  (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['AF'])))
                    and  (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['AG'])))
                    and  (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['AH'])))
                    and  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['AI'])))
                    and  (strtoupper("Consignee Address") ==  trim(strtoupper($data['AJ'])))
                    and  (strtoupper("Consignee Zip") ==  trim(strtoupper($data['AK'])))
                    and  (strtoupper("Consignee City") ==  trim(strtoupper($data['AL'])))
                    and  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['AM'])))
                    and  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['AN'])))
                    and  (strtoupper("Billed Weight") ==  trim(strtoupper($data['AO'])))
                    and  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['AP'])))
                    and  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['AQ'])))
                    and  (strtoupper("Unknown POD Status") ==  trim(strtoupper($data['AR'])))
                    // and  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['AS'])))
                    // and  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['AT'])))
                    // and  (strtoupper("Unknown POD Status") ==  trim(strtoupper($data['AU'])))

                ) {
                    // echo "found data <br/>";
                    // echo'case 1';
                    $fuondFormat = 1;

                    // $this->JobTypeModel->dropJobType();
                } else if ($count == 0) {
                    // print_r($data);
                    // echo'SCAC';
                    // echo trim(strtoupper($data['E']));
                    $ErrorMassage = 'Your file has an out-of-format format in the column ';
                    $FlagFormat = true;

                    $ErrorMassage .=  (strtoupper("Group Diff SLA ( Exclude weekend )") ==  trim(strtoupper($data['A']))) ? "" : " A ";
                    $ErrorMassage .=  (strtoupper("Group Diff SLA ( Include weekend )") ==  trim(strtoupper($data['B']))) ? "" : " B ";
                    $ErrorMassage .=  (strtoupper("Product Group") ==  trim(strtoupper($data['C']))) ? "" : " C ";
                    $ErrorMassage .=  (strtoupper("Shipper Reference") ==  trim(strtoupper($data['D']))) ? "" : " D ";
                    $ErrorMassage .=  (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['E']))) ? "" : " E ";
                    $ErrorMassage .=  (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['F']))) ? "" : " E ";

                    $ErrorMassage .=  (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['G']))) ? "" : " G ";
                    $ErrorMassage .=  (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['H']))) ? "" : " H ";
                    $ErrorMassage .=  (strtoupper("Number of Days in Customs") ==  trim(strtoupper($data['I']))) ? "" : " I ";
                    $ErrorMassage .=  (strtoupper("Pickup day") ==  trim(strtoupper($data['J']))) ? "" : " J ";
                    $ErrorMassage .=  (strtoupper("Startclock Date") ==  trim(strtoupper($data['K']))) ? "" : " K ";
                    $ErrorMassage .=  (strtoupper("Stopclock Date") ==  trim(strtoupper($data['L']))) ? "" : " L ";
                    $ErrorMassage .=  (strtoupper("Signatory") ==  trim(strtoupper($data['M']))) ? "" : " M ";
                    $ErrorMassage .=  (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['N']))) ? "" : " N ";
                    $ErrorMassage .=  (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['O']))) ? "" : " O ";
                    $ErrorMassage .=  (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['P']))) ? "" : " P ";
                    $ErrorMassage .=  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['Q']))) ? "" : " Q ";
                    $ErrorMassage .=  (strtoupper("Consignee Address") ==  trim(strtoupper($data['R']))) ? "" : " R ";
                    $ErrorMassage .=  (strtoupper("Consignee Zip") ==  trim(strtoupper($data['S']))) ? "" : " S ";
                    $ErrorMassage .=  (strtoupper("Consignee City") ==  trim(strtoupper($data['T']))) ? "" : " T ";
                    $ErrorMassage .=  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U']))) ? "" : " U ";
                    $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                    $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['W']))) ? "" : " W ";
                    $ErrorMassage .=  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X']))) ? "" : " X ";

                    // echo ("LIMIT" ==  trim(strtoupper($data['B']))) ? "T" : "F";
                    // echo "Miss data format ";
                    break;
                    // exit();
                }

                // print_r($FlagFormat);die();

                if ($row_count > 1) {


                    // $data_model['title_excel'] = $sheetData[2]['B'];

                    $data_model['Group Over SLA ( Exclude weekend )'] = isset($data['A']) ? $data['A'] : 0;
                    $data_model['Group Over SLA ( Include weekend )'] = isset($data['B']) ? $data['B'] : 0;
                    $data_model['factor_incident'] = isset($data['C']) ? $data['C'] : 0;
                    $data_model['On time status'] = isset($data['D']) ? $data['D'] : 0;
                    $data_model['Group T/T Include weekend range'] = isset($data['E']) ? $data['E'] : 0;
                    $data_model['lane'] = isset($data['F']) ? $data['F'] : 0;

                    // $data_model['event_date'] =  $event_date;
                    $data_model['Diff SLA ( Include weekend )'] = isset($data['G']) ? $data['G'] : 0;
                    $data_model['Diff SLA ( Exclude weekend )'] = isset($data['H']) ? $data['H'] : 0;
                    $data_model['T/T included weekend by_pickup_day'] = isset($data['I']) ? $data['I'] : 0;
                    $data_model['T/T excluded weekend'] = isset($data['J']) ? $data['J'] : 0;
                    $data_model['T/T included weekend'] = isset($data['K']) ? $data['K'] : 0;
                    $data_model['delivery_provider'] = isset($data['L']) ? $data['L'] : 0;
                    // $data_model['Stopclock Day of Week'] = "";
                    $data_model['gateway'] = isset($data['M']) ? $data['M'] : 0;
                    $data_model['shipmonth'] = isset($data['N']) ? $data['N'] : 0;
                    // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                    $data_model['Quater'] = isset($data['O']) ? $data['O'] : 0;
                    $data_model['Year'] = isset($data['P']) ? $data['P'] : 0;
                    $data_model['week_number'] = isset($data['Q']) ? $data['Q'] : 0;
                    $data_model['Waybill Number'] = isset($data['R']) ? $data['R'] : 0;
                    $data_model['Product Code'] = isset($data['S']) ? $data['S'] : 0;
                    $data_model['Product Name'] = isset($data['T']) ? $data['T'] : 0;
                    $data_model['Shipper Reference'] = isset($data['U']) ? $data['U'] : 0;
                    $data_model['Shipper Company Name'] = isset($data['V']) ? $data['V'] : 0;
                    // $data_model['Billed Weight'] = "";
                    $data_model['Destination Country/Territory Name'] = isset($data['W']) ? $data['W'] : 0;
                    $data_model['Destination Country/Territory Area Code'] = isset($data['X']) ? $data['X'] : 0;
                    $data_model['Destination Service Area Code'] = isset($data['Y']) ? $data['Y'] : 0;
                    $data_model['Number of Days in Customs'] = isset($data['Z']) ? $data['Z'] : 0;
                    $data_model['Pickup day'] = isset($data['AA']) ? $data['AA'] : 0;
                    $data_model['Startclock Date'] = isset($data['AB']) ? $data['AB'] : 0;
                    $data_model['Stopclock Date'] = isset($data['AC']) ? $data['AC'] : 0;
                    $data_model['Stopclock Day of Week'] = isset($data['AD']) ? $data['AD'] : 0;
                    $data_model['Signatory'] = isset($data['AE']) ? $data['AE'] : 0;
                    $data_model['Elapsed Actual Transit Days'] = isset($data['AF']) ? $data['AF'] : 0;
                    $data_model['Reporting Code Description'] = isset($data['AG']) ? $data['AG'] : 0;
                    $data_model['Reporting Code Category Description'] = isset($data['AH']) ? $data['AH'] : 0;
                    $data_model['Consignee Company Name'] = isset($data['AI']) ? $data['AI'] : 0;
                    $data_model['Consignee Address'] = isset($data['AJ']) ? $data['AJ'] : 0;
                    $data_model['Consignee Zip'] = isset($data['AK']) ? $data['AK'] : 0;
                    $data_model['Consignee City'] = isset($data['AL']) ? $data['AL'] : 0;
                    $data_model['Billing Account Number'] = isset($data['AM']) ? $data['AM'] : 0;
                    $data_model['Manifested Number of Pieces'] = isset($data['AN']) ? $data['AN'] : 0;
                    $data_model['Billed Weight'] = isset($data['AO']) ? $data['AO'] : 0;
                    $data_model['Customs Declared Value'] = isset($data['AP']) ? $data['AP'] : 0;
                    $data_model['Currency Code of Declared Value'] = isset($data['AQ']) ? $data['AQ'] : 0;
                    $data_model['Unknown POD Status'] = isset($data['AR']) ? $data['AR'] : 0;
                    // $data_model['Customs Declared Value'] = isset($data['AS']) ? $data['AS'] : 0;
                    // $data_model['Currency Code of Declared Value'] = isset($data['AT']) ? $data['AT'] : 0;
                    // $data_model['Unknown POD Status'] = isset($data['AU']) ? $data['AU'] : 0;
                    
                    // $data_model['column_ad'] = isset($data['AD']) ? $data['AD'] : 0;
                    // $data_model['em_sales_order_id'] = isset($data['AE']) ? $data['AE'] : 0;
                    // $data_model['em_origin'] = isset($data['AF']) ? $data['AF'] : 0;
                    // $data_model['em_current_event_name'] = isset($data['AG']) ? $data['AG'] : 0;
                    // $data_model['em_current_event_datetime'] = isset($data['AH']) && $data['AH'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AH'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_current_event_reason_code'] = isset($data['AI']) ? $data['AI'] : 0;
                    // $data_model['em_current_event_location_description'] = isset($data['AJ']) ? $data['AJ'] : 0;
                    // $data_model['dq_postship_carrier'] = isset($data['AK']) ? $data['AK'] : 0;
                    // $data_model['ops_channel_level_2_code'] = isset($data['AL']) ? $data['AL'] : 0;
                    // $data_model['em_ship_condition_code'] = isset($data['AM']) ? $data['AM'] : 0;
                    // $data_model['em_ship_point_type'] = isset($data['AN']) ? $data['AN'] : 0;
                    // $data_model['em_ship_point_code'] = isset($data['AO']) ? $data['AO'] : 0;
                    // $data_model['em_city_name'] = isset($data['AP']) ? $data['AP'] : 0;
                    // $data_model['em_postal_code'] = isset($data['AQ']) ? $data['AQ'] : 0;
                    // $data_model['em_ship_confirmation_last_date'] = isset($data['AR']) && $data['AR'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AR'])->format('Y-m-d') : 0;

                    // $data_model['em_ship_confirmation_last_time'] = isset($data['AS']) && $data['AS'] != '' ? PHPExcel_Style_NumberFormat::toFormattedString($data['AS'], 'hh:mm:ss') : 0;
                    // $data_model['em_shipment_picked_up_last_datetime'] = isset($data['AT']) && $data['AT'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AT'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_uplift_last_datetime'] = isset($data['AU']) && $data['AU'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AU'])->format('Y-m-d H:i:s') : null;
                    // $data_model['em_first_attempt_date'] = isset($data['AV']) && $data['AV'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AV'])->format('Y-m-d') : null;
                    // $data_model['em_first_attempt_reason_code'] = isset($data['AW']) ? $data['AW'] : 0;
                    // $data_model['em_first_attempt_reason_code_description'] = isset($data['AX']) ? $data['AX'] : 0;
                    // $data_model['em_delivery_not_completed_last_date'] = isset($data['AY']) && $data['AY'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AY'])->format('Y-m-d') : null;
                    // $data_model['em_delivery_not_completed_last_reason_code_description'] = isset($data['AZ']) ? $data['AZ'] : 0;
                    // $data_model['em_shipment_delay_last_date'] = isset($data['BA']) && $data['BA'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['BA'])->format('Y-m-d') : null;
                    // $data_model['em_shipment_delay_last_reason_code'] = isset($data['BB']) ? $data['BB'] : 0;
                    // $data_model['em_shipment_delay_last_reason_code_description'] = isset($data['BC']) ? $data['BC'] : 0;
                    // $data_model['em_count_of_delivery_attempts'] = isset($data['BD']) ? $data['BD'] : 0;
                    
                    $data_model['create_date'] = $date;
                    $data_model['file_name'] = $file;

                    // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                    //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                    // }

                    $nResult = $this->ImportMonthlyModel->insert_to_temp_shipment($data_model);

                } else {
                    // if ($data['BO'] == '') {
                    //     $formattype = 1;
                    // } else if ($data['BO'] != '') {
                    //     $formattype = 2;
                    // } else if ($data['A'] != 'Order Item Id') {
                    //     $result['status'] = false;
                    //     $result['message'] = 'Wrong file format';
                    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    //     exit;
                    // }

                    // echo $formattype;
                    // die();
                }



                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            $row_count++;
            $count++;
            // $row_count=$row_count+1;
            // die();
        }

        $check_duplicate = $this->ImportMonthlyModel->check_duplicate();

        // print_r($check_duplicate);

        if ($FlagFormat == true) {
            $result['status'] = false;
            $result['message'] =  $ErrorMassage;
        } else if (count($check_duplicate) > 0) {
            $result['status'] = false;
            $result['message'] = 'Duplicate data found from file. That has already been uploaded';
            // $this->ImportModel->DeleteTableTemp();
        } else {
            $dataimpot['file_name'] =  $file;
            $dataimpot['type'] =  'Excel Upload';
            $dataimpot['upload_date'] =  $date;
            $this->ImportMonthlyModel->insertto_t_shipment();
            // $this->ImportModel->insertto_customer($file, $date);
            $nResult = $this->ImportMonthlyModel->insert_log_shipment($dataimpot);
            // $this->ImportModel->DeleteTableTemp();
            $result['status'] = true;
            $result['message'] = $this->lang->line('savesuccess');
        };
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }






    public function getdailyModelList()
    {
        try {
            $this->load->model('ImportMonthlyModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'desc';
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : '';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->ImportMonthlyModel->getdailyModelList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            // $result['totalRecords'] = $this->CashadvanceModel->getTotal($dataModel);
            // $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    // public function downloadpandora()
    // {
    //     try {
    //         $this->load->model('DailyimportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);
    //         // print_r($dataPost);die();
    //         $dataModel = isset($dataPost['id']) ? $dataPost['id'] : '';

    //         $result['status'] = true;
    //         $result['message'] = $this->DailyimportModel->downloadpandora($dataModel);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }

    // public function downloadshipment()
    // {
    //     try {
    //         $this->load->model('DailyimportModel', '', true);

    //         $dataPost = json_decode($this->input->raw_input_stream, true);
    //         // print_r($dataPost);die();
    //         $dataModel = isset($dataPost['id']) ? $dataPost['id'] : '';

    //         $result['status'] = true;
    //         $result['message'] = $this->DailyimportModel->downloadshipment($dataModel);
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = 'exception: ' . $ex;
    //     }
    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }
    public function deleteImport()
    {
        try {
            $this->load->model('ImportMonthlyModel', '', true);

            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);die();
            $dataModel = isset($dataPost['file_name']) ? $dataPost['file_name'] : '';
            // print_r($dataModel);die();
            $result['status'] = true;
            $result['message'] = $this->ImportMonthlyModel->deleteImport($dataModel);
            $result['ShowMessage'] = 'The deletion was successful.';
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('MyEnDecode');
		//$this->lang->load("message","thai");


		/*if(!isset($_SESSION['u_user'])){
			redirect('/login', 'refresh');
		}*/
		$role_data =  $this->session->userdata('role_PANDASH');

		if ($role_data['REP']) {
			redirect('Import');
		} else if ($role_data['IMFILE']) {
			redirect('Import');
		} else if ($role_data['EFILE']) {
			redirect('Export');
		} else if ($role_data['MAS']) {
			redirect('SlaAuNz');
		}  else {
  
			// echo '55555';die();
			//host//
			// redirect('https://th-logis.com/dhl_sso/Login');

			//local//
			redirect('http://localhost/dhl_sso/Login');


			// header("location:http://localhost/dhl_sso/#/accountmanagement");
			// exit(0);
			// echo 'ตกเคส else ใน __construct';die();
		};
		// }
	}

	public function index()
	{

		$this->loginSSO($this->input->post('uname'), $this->input->post('pswsss'));

		// $data['msgError'] = '';
		// $username = $this->security->xss_clean($this->input->post('uname'));
		// $password = $this->security->xss_clean($this->input->post('pswsss'));

		// //echo $username. " :  " .  $password. " : " . md5($password);

		// if ($username != '' && $password  != '') {

		// 	$this->load->model('UserModel');
		// 	// Validate the user can login
		// 	$result = $this->UserModel->validate($username, $password);
		// 	//echo $result;
		// 	// Now we verify the result
		// 	if (!$result) {
		// 		// If user did not validate, then show them login page again
		// 		$data['msgError'] =  "User or Password incorrect!";
		// 	} else {
		// 		// If user did validate, 
		// 		// Send them to members area

		// 		$data = array("user_name" => $username);
		// 		$this->session->set_userdata($data);
		// 		redirect('Dashboard');
		// 	}
		// }

		// $this->load->view('login/login_view', $data);
	}

	public function loginApi()
	{
		try {

			print_r($_COOKIE);

			$value = 'something from somewhere';
			setcookie("TestCookie", $value, time() + 3600);
			setcookie("gg", "Hello world");
			print_r($_COOKIE);

			$result['status'] = false;
			$result['message'] = "Login fail!";

			$data['msgError'] = '';
			$username = $this->security->xss_clean($this->input->post('uname'));
			$password = $this->security->xss_clean($this->input->post('pswsss'));

			if ($username != '' && $password  != '') {
				$this->load->model('UserModel');
				$result = $this->UserModel->validate($username, $password);
			}

			$result['username'] = $username;
			$result['password'] = $password;
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}


	public function loginSSO($username, $password)
	{

		$data['msgError'] = '';
		//$username = "admin";
		//$password = "9i8u7y6t";
		// print_r($username);die();
		$clean_username = $this->security->xss_clean($username);
		$clean_password = $this->security->xss_clean($password);

		if ($username != '' && $password  != '') {
			$this->load->model('UserModel');

			$result = $this->UserModel->validate_with_sso($clean_username, $clean_password);

			// print_r($result);die();

			if (!$result) {
				// If user did not validate, then show them login page again
				$data['msgError'] =  "User or Password incorrect!";
			} else {
				// If user did validate, 
				// Send them to members area 
				$data = array("user_name" => $username);
				$this->session->set_userdata($data);


				// $role_data =  $this->session->userdata('role_PANDASH');
				// if ($role_data['DASH']) {
				// 	redirect('Dashboard');
				// } else if ($role_data['IMFILE']) {
				// 	redirect('Import');
				// } else if ($role_data['MAPC']) {
				// 	redirect('CodeAppleAndDhl');
				// } else {
				// 	redirect('Login');
				// };
				// redirect('Dashboard');
			}
		}


		$this->load->view('login/login_view', $data);
	}

	public function logout()
	{
		$data = array(
			'id' => 0,
			'user' => "",
			'user_name' =>  "",
			'validated' => false
		);
		$this->session->set_userdata($data);

		$this->load->view('login/login_view', $data);
	}

	public function loginToken()
	{
		$date_now = date("Y-m-d");
		//  print_r($date_now);die();
		$token = base64_decode($this->input->get('token', TRUE));

		$tokenUserAndPass = $this->myendecode->aes_decrypt($token, 'DevdeeThailand');

		$UserAndPass = explode(":", $tokenUserAndPass);

		// print_r($UserAndPass);die();

		$User = $UserAndPass[0];
		$Password = $UserAndPass[1];
		$datelogin = $UserAndPass[2];

		if ($datelogin != $date_now) {
			$this->index();
		} else {
			$this->loginSSO($User, $Password);
		}
		// print_r($UserAndPass);die();
	}
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class WeekNumber extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        date_default_timezone_set('Asia/Bangkok');
        if (!$this->session->userdata('validated')) {

            redirect('https://th-logis.com/dhlsso/Login');
        }
        $this->session->mark_as_temp('validated', 1200);
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_WLHOG');
        // check หน้า//
        // if( !$role_data ['AGNT'] ) {
        // 	redirect('Login');
        // }
        // check หน้า//
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('weekNumber/weekNumber_view');
        $this->load->view('share/footer');
    }

    public function getWeekNumberList()
    {
        try {
            $this->load->model('WeekNumberModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->WeekNumberModel->getWeekNumberModel($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function SaveWeekNumber()
    {
        try {
            $this->load->model('WeekNumberModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->WeekNumberModel->SaveWeekNumber($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function DeleteWeekNumber()
    {
        try {
            $this->load->model('WeekNumberModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->WeekNumberModel->DeleteWeekNumber($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }


    public function Exportexcel()
    {
        // log_message('error', 'Exportexcel');
        ini_set("memory_limit", "10000M");
        ini_set('MAX_EXECUTION_TIME', 0);

        try {
            $this->load->model('WeekNumberModel', '', true);

            $this->load->library('MyExcel');

            $dataPost = json_decode($this->input->raw_input_stream, true);

            // print_r("test");
            // die();

            // $date = date('Y-m-d-H-i-s');
            // print_r($date);
            // die();

            // $date = '2021-07-28';

            // log_message('error', 'Can get date');
            // $data = [];
            $data = $this->WeekNumberModel->ExportExcel($dataPost);
            // log_message('error', 'Can get data');
            // print_r($data);
            // die();

            //2021-07-29//
            // ini_set('max_execution_time', 300); //300 seconds = 5 minutes
            // set_time_limit(300);
            //2021-07-29//

            // print_r($data[0]['file_name']);die();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Week Number");

            // die();

            //กำหนด style ส่วนหัว//
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                // )
            );

            $headtable = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'font' => [
                    // 'size' => 9,
                    'bold'  => true,
                    // 'name'  => 'Arial',
                ]

                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => '00ffff')
                // )
            );


            $headbody = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                // 'alignment' => array(
                //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,

                // ),
                // 'fill' => array(
                //     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                //     'color' => array('rgb' => 'ffff00')
                // )
            );
            // //กำหนด style ส่วน body//
            // $styleArray = array(
            //     'borders' => array(
            //         'allborders' => array(
            //             'style' => PHPExcel_Style_Border::BORDER_THIN
            //         )
            //     )
            // );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(TRUE);

            // $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(TRUE);
            // $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(TRUE);

            $objPHPExcel->setActiveSheetIndex(0)

                ->setCellValue('A1', 'Week')
                ->setCellValue('B1', 'Start')
                ->setCellValue('C1', 'End');
            // ->setCellValue('D1', 'factor_incident')
            // ->setCellValue('E1', 'On time status')
            // ->setCellValue('F1', 'Group T/T Include weekend range')
            // ->setCellValue('G1', 'factor_incident_cal')
            // ->setCellValue('H1', 'factor')
            // ->setCellValue('I1', 'lane')
            // ->setCellValue('J1', 'Diff SLA ( Include weekend )')
            // ->setCellValue('K1', 'Diff SLA ( Exclude weekend )')
            // ->setCellValue('L1', 'T/T included weekend by_pickup_day')
            // ->setCellValue('M1', 'T/T excluded weekend')
            // ->setCellValue('N1', 'T/T included weekend')
            // ->setCellValue('O1', 'delivery_provider ')
            // ->setCellValue('P1', 'gateway')
            // ->setCellValue('Q1', 'shipmonth')
            // ->setCellValue('R1', 'Quater')
            // ->setCellValue('S1', 'Year')
            // ->setCellValue('T1', 'week_number')
            // ->setCellValue('U1', 'Waybill Number')
            // ->setCellValue('V1', 'Product Code')
            // ->setCellValue('W1', 'Product Name')
            // ->setCellValue('X1', 'Shipper Reference')
            // ->setCellValue('Y1', 'Shipper Company Name')
            // ->setCellValue('Z1', 'Destination Country/Territory Name')

            // ->setCellValue('AA1', 'Destination Country/Territory Area Code')
            // ->setCellValue('AB1', 'Destination Service Area Code')
            // ->setCellValue('AC1', 'Number of Days in Customs')
            // ->setCellValue('AD1', 'Startclock Day of Week')
            // ->setCellValue('AE1', 'Startclock Date')
            // ->setCellValue('AF1', 'Stopclock Date')
            // ->setCellValue('AG1', 'Stopclock Day of Week')
            // ->setCellValue('AH1', 'Signatory')
            // ->setCellValue('AI1', 'Elapsed Actual Transit Days')
            // ->setCellValue('AJ1', 'Reporting Code Description')
            // ->setCellValue('AK1', 'Reporting Code Category Description')
            // ->setCellValue('AL1', 'Consignee Company Name')
            // ->setCellValue('AM1', 'Consignee Address')
            // ->setCellValue('AN1', 'Consignee Zip')
            // ->setCellValue('AO1', 'Consignee City ')
            // ->setCellValue('AP1', 'Billing Account Number')
            // ->setCellValue('AQ1', 'Manifested Number of Pieces')
            // ->setCellValue('AR1', 'Billed Weight')
            // ->setCellValue('AS1', 'Customs Declared Value')
            // ->setCellValue('AT1', 'Currency Code of Declared Value')
            // ->setCellValue('AU1', 'Unknown POD Status');

            // $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($headbody);
            // $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($headbody);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($headtable);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($headtable);

            // $objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AN1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AO1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AP1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AQ1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AR1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AS1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AT1')->applyFromArray($headtable);
            // $objPHPExcel->getActiveSheet()->getStyle('AU1')->applyFromArray($headtable);

            $start_row = 2;
            foreach ($data as $row) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $start_row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('D' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('E' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('F' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('G' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('H' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('I' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('J' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('K' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('L' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('M' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('N' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('O' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('P' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('Q' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('R' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('S' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('T' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('U' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('V' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('W' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('X' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('Y' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('Z' . $start_row)->applyFromArray($styleArray);

                // $objPHPExcel->getActiveSheet()->getStyle('AA' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AB' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AC' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AD' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AE' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AF' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AG' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AH' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AI' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AJ' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AK' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AL' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AM' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AN' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AO' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AP' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AQ' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AR' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AS' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AT' . $start_row)->applyFromArray($styleArray);
                // $objPHPExcel->getActiveSheet()->getStyle('AU' . $start_row)->applyFromArray($styleArray);

                $objPHPExcel->setActiveSheetIndex(0)
                    // ->setCellValue('A' . $start_row, date('d/m/Y', strtotime($row['update_date'])))
                    ->setCellValue('A' . $start_row, $row['week'])
                    ->setCellValue('B' . $start_row, $row['start'])
                    ->setCellValue('C' . $start_row, $row['end']);
                // ->setCellValue('D' . $start_row, $row['factor_incident'])
                // ->setCellValue('E' . $start_row, $row['On time status'])
                // // ->setCellValue('F' . $start_row, $row['Group T/T Include weekend range'])
                // ->setCellValueExplicit("F". $start_row, $row['Group T/T Include weekend range'], PHPExcel_Cell_DataType::TYPE_STRING)
                // ->setCellValue('G' . $start_row, $row['factor_incident_cal'])
                // ->setCellValue('H' . $start_row, $row['factor'])
                // ->setCellValue('I' . $start_row, $row['lane'])
                // ->setCellValue('J' . $start_row, $row['Diff SLA ( Include weekend )'])
                // ->setCellValue('K' . $start_row, $row['Diff SLA ( Exclude weekend )'])
                // ->setCellValue('L' . $start_row, $row['T/T included weekend by_pickup_day'])
                // ->setCellValue('M' . $start_row, $row['T/T excluded weekend'])
                // ->setCellValue('N' . $start_row, $row['T/T included weekend'])
                // ->setCellValue('O' . $start_row, $row['delivery_provider'])
                // ->setCellValue('P' . $start_row, $row['gateway'])
                // ->setCellValue('Q' . $start_row, $row['shipmonth'])
                // ->setCellValue('R' . $start_row, $row['Quater'])
                // ->setCellValue('S' . $start_row, $row['Year'])
                // ->setCellValue('T' . $start_row, $row['week_number'])
                // ->setCellValue('U' . $start_row, $row['Waybill Number'])
                // ->setCellValue('V' . $start_row, $row['Product Code'])
                // ->setCellValue('W' . $start_row, $row['Product Name'])
                // ->setCellValue('X' . $start_row, $row['Shipper Reference'])
                // ->setCellValue('Y' . $start_row, $row['Shipper Company Name'])
                // ->setCellValue('Z' . $start_row, $row['Destination Country/Territory Name'])

                // ->setCellValue('AA' . $start_row, $row['Destination Country/Territory Area Code'])
                // ->setCellValue('AB' . $start_row, $row['Destination Service Area Code'])
                // ->setCellValue('AC' . $start_row, $row['Number of Days in Customs'])
                // ->setCellValue('AD' . $start_row, $row['Startclock Day of Week'])
                // ->setCellValue('AE' . $start_row, $row['Startclock Date'])
                // ->setCellValue('AF' . $start_row, $row['Stopclock Date'])
                // ->setCellValue('AG' . $start_row, $row['Stopclock Day of Week'])
                // ->setCellValue('AH' . $start_row, $row['Signatory'])
                // ->setCellValue('AI' . $start_row, $row['Elapsed Actual Transit Days'])
                // ->setCellValue('AJ' . $start_row, $row['Reporting Code Description'])
                // ->setCellValue('AK' . $start_row, $row['Reporting Code Category Description'])
                // ->setCellValue('AL' . $start_row, $row['Consignee Company Name'])
                // ->setCellValue('AM' . $start_row, $row['Consignee Address'])
                // ->setCellValue('AN' . $start_row, $row['Consignee Zip'])
                // ->setCellValue('AO' . $start_row, $row['Consignee City'])
                // ->setCellValue('AP' . $start_row, $row['Billing Account Number'])
                // ->setCellValue('AQ' . $start_row, $row['Manifested Number of Pieces'])
                // ->setCellValue('AR' . $start_row, $row['Billed Weight'])
                // ->setCellValue('AS' . $start_row, $row['Customs Declared Value'])
                // ->setCellValue('AT' . $start_row, $row['Currency Code of Declared Value'])
                // ->setCellValue('AU' . $start_row, $row['Unknown POD Status']);
                // ->setCellValueExplicit('P' . $start_row, $row['tax_code'], PHPExcel_Cell_DataType::TYPE_STRING);




                $start_row++;
            }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            // $filename = $start.' to '.$end . '.xlsx';

            $filename = "Week_Number" . date("Y-m-d-H-i-s") . '.xlsx';
            // echo FCPATH; die();
            $objWriter->save(FCPATH . 'upload/export_excel/' . $filename);
            $result['message'] = $filename;
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }


    public function upload_file()
    {
        // print_r($_FILES['file_Pandora_Week']);
        //  print_r('test');die();
        // $uniqid = uniqid();
        // $datapandora['uniqidpandora'] = $uniqid;
        // $datashipment['uniqidshipment'] = $uniqid;
        $date = date('Y-m-d-H-i-s');

        // $this->load->model('HeadpandoraModel', '', true);
        // $this->load->model('HeadShipmentModel', '', true);

        // $uniqidpandora = $this->HeadpandoraModel->insert($datapandora);
        // $uniqidshipment = $this->HeadShipmentModel->insert($datashipment);

        // $this->upload_filepandora($_FILES, $date, $uniqid);
        // $this->upload_fileshipment($_FILES, $date, $uniqid);
        $todayfile = 'Pandora_Week' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/import_excel';
        // $config['upload_path']          = base_url('/upload');
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['file_Pandora_Week']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);



        $status = true;
        if (!$this->upload->do_upload('file_Pandora_Week')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
            $data = array('upload_data' => $this->upload->data());
            // print_r($data['upload_data']['file_name']);die();
            $todayfile = $data['upload_data']['file_name'];
            $this->loadExcelpandoraDashboardToDB($todayfile, $date);
        }
    }


    public function loadExcelpandoraDashboardToDB($file, $date)
    {
        $formattype = 0;
        ini_set('max_execution_time', 300);

        $this->load->library('MyExcel');
        $this->load->model('WeekNumberModel', '', true);
        // print_r($file);
        $objReader = PHPExcel_IOFactory::load('upload/import_excel/' . $file);

        // die();

        $sheetData = $objReader->getActiveSheet()->toArray('', true, false, true);

        // print_r($sheetData);die();

        $row_count = 1;
        // $i = 'B';
        // $header_true == true;
        // $this->ImportModel->delete_temp_pandora_dashboard();

        // echo 'delete_temp_pandora_dashboard success';die();
        $resuult = array();

        $event_date = date('Y-m-d');
        $count = 0;
        // print_r($sheetData[4]);die();

        foreach ($sheetData as $data) {

            try {

                if (
                    $count == 0
                    // and  ("NAME" == trim(strtoupper($data['A'])))

                    and  (strtoupper("Week") ==  trim(strtoupper($data['A'])))
                    and  (strtoupper("Start") ==  trim(strtoupper($data['B'])))
                    and  (strtoupper("End") ==  trim(strtoupper($data['C'])))
                    // and  (strtoupper("Shipper Reference") ==  trim(strtoupper($data['D'])))
                    // and  (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['E'])))
                    // and  (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['F'])))

                    // and  (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['G'])))
                    // and  (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['H'])))
                    // and  (strtoupper("Number of Days in Customs") ==  trim(strtoupper($data['I'])))
                    // and  (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['J'])))
                    // and  (strtoupper("Startclock Date") ==  trim(strtoupper($data['K'])))
                    // and  (strtoupper("Stopclock Date") ==  trim(strtoupper($data['L'])))
                    // and  (strtoupper("Signatory") ==  trim(strtoupper($data['M'])))
                    // and  (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['N'])))
                    // and  (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['O'])))
                    // and  (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['P'])))
                    // and  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['Q'])))
                    // and  (strtoupper("Consignee Address") ==  trim(strtoupper($data['R'])))
                    // and  (strtoupper("Consignee Zip") ==  trim(strtoupper($data['S'])))
                    // and  (strtoupper("Consignee City") ==  trim(strtoupper($data['T'])))
                    // and  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U'])))
                    // and  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V'])))
                    // and  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W'])))
                    // and  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X'])))

                ) {
                    $this->WeekNumberModel->delete_wk();
                    // echo "found data <br/>";
                    // echo'case 1';
                    $fuondFormat = 1;

                    // $this->JobTypeModel->dropJobType();
                } else if ($count == 0) {
                    // print_r($data);
                    // echo'SCAC';
                    // echo trim(strtoupper($data['E']));
                    $ErrorMassage = 'Your file has an out-of-format format in the column ';
                    $FlagFormat = true;

                    $ErrorMassage .=  (strtoupper("Week") ==  trim(strtoupper($data['A']))) ? "" : " A ";
                    $ErrorMassage .=  (strtoupper("Start") ==  trim(strtoupper($data['B']))) ? "" : " B ";
                    $ErrorMassage .=  (strtoupper("End") ==  trim(strtoupper($data['C']))) ? "" : " C ";
                    // $ErrorMassage .=  (strtoupper("Shipper Reference") ==  trim(strtoupper($data['D']))) ? "" : " D ";
                    // $ErrorMassage .=  (strtoupper("Shipper Company Name") ==  trim(strtoupper($data['E']))) ? "" : " E ";
                    // $ErrorMassage .=  (strtoupper("Destination Country/Territory Name") ==  trim(strtoupper($data['F']))) ? "" : " E ";

                    // $ErrorMassage .=  (strtoupper("Destination Country/Territory Area Code") ==  trim(strtoupper($data['G']))) ? "" : " G ";
                    // $ErrorMassage .=  (strtoupper("Destination Service Area Code") ==  trim(strtoupper($data['H']))) ? "" : " H ";
                    // $ErrorMassage .=  (strtoupper("Number of Days in Customs") ==  trim(strtoupper($data['I']))) ? "" : " I ";
                    // $ErrorMassage .=  (strtoupper("Startclock Day of Week") ==  trim(strtoupper($data['J']))) ? "" : " J ";
                    // $ErrorMassage .=  (strtoupper("Startclock Date") ==  trim(strtoupper($data['K']))) ? "" : " K ";
                    // $ErrorMassage .=  (strtoupper("Stopclock Date") ==  trim(strtoupper($data['L']))) ? "" : " L ";
                    // $ErrorMassage .=  (strtoupper("Signatory") ==  trim(strtoupper($data['M']))) ? "" : " M ";
                    // $ErrorMassage .=  (strtoupper("Elapsed Actual Transit Days") ==  trim(strtoupper($data['N']))) ? "" : " N ";
                    // $ErrorMassage .=  (strtoupper("Reporting Code Description") ==  trim(strtoupper($data['O']))) ? "" : " O ";
                    // $ErrorMassage .=  (strtoupper("Reporting Code Category Description") ==  trim(strtoupper($data['P']))) ? "" : " P ";
                    // $ErrorMassage .=  (strtoupper("Consignee Company Name") ==  trim(strtoupper($data['Q']))) ? "" : " Q ";
                    // $ErrorMassage .=  (strtoupper("Consignee Address") ==  trim(strtoupper($data['R']))) ? "" : " R ";
                    // $ErrorMassage .=  (strtoupper("Consignee Zip") ==  trim(strtoupper($data['S']))) ? "" : " S ";
                    // $ErrorMassage .=  (strtoupper("Consignee City") ==  trim(strtoupper($data['T']))) ? "" : " T ";
                    // $ErrorMassage .=  (strtoupper("Manifested Number of Pieces") ==  trim(strtoupper($data['U']))) ? "" : " U ";
                    // $ErrorMassage .=  (strtoupper("Currency Code of Declared Value") ==  trim(strtoupper($data['V']))) ? "" : " V ";
                    // $ErrorMassage .=  (strtoupper("Billing Account Number") ==  trim(strtoupper($data['W']))) ? "" : " W ";
                    // $ErrorMassage .=  (strtoupper("Customs Declared Value") ==  trim(strtoupper($data['X']))) ? "" : " X ";

                    // echo ("LIMIT" ==  trim(strtoupper($data['B']))) ? "T" : "F";
                    // echo "Miss data format ";
                    break;
                    // exit();
                }

                // print_r($FlagFormat);die();

                if ($row_count > 1) {


                    // $data_model['title_excel'] = $sheetData[2]['B'];

                    $data_model['Week'] = isset($data['A']) ? $data['A'] : 0;
                    $data_model['Start'] = isset($data['B']) ? $data['B'] : 0;
                    $data_model['End'] = isset($data['C']) ? $data['C'] : 0;
                    // $data_model['Shipper Reference'] = isset($data['D']) ? $data['D'] : 0;
                    // $data_model['Shipper Company Name'] = isset($data['E']) ? $data['E'] : 0;
                    // $data_model['Destination Country/Territory Name'] = isset($data['F']) ? $data['F'] : 0;

                    // // $data_model['event_date'] =  $event_date;
                    // $data_model['Destination Country/Territory Area Code'] = isset($data['G']) ? $data['G'] : 0;
                    // $data_model['Destination Service Area Code'] = isset($data['H']) ? $data['H'] : 0;
                    // $data_model['Number of Days in Customs'] = isset($data['I']) ? $data['I'] : 0;
                    // $data_model['Startclock Day of Week'] = isset($data['J']) ? $data['J'] : 0;
                    // $data_model['Startclock Date'] = isset($data['K']) ? $data['K'] : 0;
                    // $data_model['Stopclock Date'] = isset($data['L']) ? $data['L'] : 0;
                    // $data_model['Startclock Day of Week'] = "";
                    // $data_model['Signatory'] = isset($data['M']) ? $data['M'] : 0;
                    // $data_model['Elapsed Actual Transit Days'] = isset($data['N']) ? $data['N'] : 0;
                    // // $data_model['em_scheduled_delivery_date'] = isset($data['R'])  && $data['R'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['R'])->format('Y-m-d') : 0;
                    // $data_model['Reporting Code Description'] = isset($data['O']) ? $data['O'] : 0;
                    // $data_model['Reporting Code Category Description'] = isset($data['P']) ? $data['P'] : 0;
                    // $data_model['Consignee Company Name'] = isset($data['Q']) ? $data['Q'] : 0;
                    // $data_model['Consignee Address'] = isset($data['R']) ? $data['R'] : 0;
                    // $data_model['Consignee Zip'] = isset($data['S']) ? $data['S'] : 0;
                    // $data_model['Consignee City'] = isset($data['T']) ? $data['T'] : 0;
                    // $data_model['Billing Account Number'] = isset($data['W']) ? $data['W'] : 0;
                    // $data_model['Manifested Number of Pieces'] = isset($data['U']) ? $data['U'] : 0;
                    // $data_model['Billed Weight'] = "";
                    // $data_model['Customs Declared Value'] = isset($data['X']) ? $data['X'] : 0;
                    // $data_model['Currency Code of Declared Value'] = isset($data['V']) ? $data['V'] : 0;
                    // $data_model['column_ad'] = isset($data['AD']) ? $data['AD'] : 0;
                    // $data_model['em_sales_order_id'] = isset($data['AE']) ? $data['AE'] : 0;
                    // $data_model['em_origin'] = isset($data['AF']) ? $data['AF'] : 0;
                    // $data_model['em_current_event_name'] = isset($data['AG']) ? $data['AG'] : 0;
                    // $data_model['em_current_event_datetime'] = isset($data['AH']) && $data['AH'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AH'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_current_event_reason_code'] = isset($data['AI']) ? $data['AI'] : 0;
                    // $data_model['em_current_event_location_description'] = isset($data['AJ']) ? $data['AJ'] : 0;
                    // $data_model['dq_postship_carrier'] = isset($data['AK']) ? $data['AK'] : 0;
                    // $data_model['ops_channel_level_2_code'] = isset($data['AL']) ? $data['AL'] : 0;
                    // $data_model['em_ship_condition_code'] = isset($data['AM']) ? $data['AM'] : 0;
                    // $data_model['em_ship_point_type'] = isset($data['AN']) ? $data['AN'] : 0;
                    // $data_model['em_ship_point_code'] = isset($data['AO']) ? $data['AO'] : 0;
                    // $data_model['em_city_name'] = isset($data['AP']) ? $data['AP'] : 0;
                    // $data_model['em_postal_code'] = isset($data['AQ']) ? $data['AQ'] : 0;
                    // $data_model['em_ship_confirmation_last_date'] = isset($data['AR']) && $data['AR'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AR'])->format('Y-m-d') : 0;

                    // $data_model['em_ship_confirmation_last_time'] = isset($data['AS']) && $data['AS'] != '' ? PHPExcel_Style_NumberFormat::toFormattedString($data['AS'], 'hh:mm:ss') : 0;
                    // $data_model['em_shipment_picked_up_last_datetime'] = isset($data['AT']) && $data['AT'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AT'])->format('Y-m-d H:i:s') : 0;
                    // $data_model['em_uplift_last_datetime'] = isset($data['AU']) && $data['AU'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AU'])->format('Y-m-d H:i:s') : null;
                    // $data_model['em_first_attempt_date'] = isset($data['AV']) && $data['AV'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AV'])->format('Y-m-d') : null;
                    // $data_model['em_first_attempt_reason_code'] = isset($data['AW']) ? $data['AW'] : 0;
                    // $data_model['em_first_attempt_reason_code_description'] = isset($data['AX']) ? $data['AX'] : 0;
                    // $data_model['em_delivery_not_completed_last_date'] = isset($data['AY']) && $data['AY'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['AY'])->format('Y-m-d') : null;
                    // $data_model['em_delivery_not_completed_last_reason_code_description'] = isset($data['AZ']) ? $data['AZ'] : 0;
                    // $data_model['em_shipment_delay_last_date'] = isset($data['BA']) && $data['BA'] != '' ? PHPExcel_Shared_Date::ExcelToPHPObject($data['BA'])->format('Y-m-d') : null;
                    // $data_model['em_shipment_delay_last_reason_code'] = isset($data['BB']) ? $data['BB'] : 0;
                    // $data_model['em_shipment_delay_last_reason_code_description'] = isset($data['BC']) ? $data['BC'] : 0;
                    // $data_model['em_count_of_delivery_attempts'] = isset($data['BD']) ? $data['BD'] : 0;

                    // $data_model['create_date'] = $date;
                    // $data_model['create_user'] = $this->session->userdata('user_name');
                    // $data_model['new_flag'] = 1;

                    // if ($data_model['ssd_status'] == 'Due Today' || $data_model['ssd_status'] == 'Overdue' || $data_model['ssd_status'] == 'Future Shipments') {
                    //     $nResult = $this->ImportModel->insert_to_temp_shipment($data_model);
                    // }

                    $nResult = $this->WeekNumberModel->insert_to_wk($data_model);
                } else {
                    // if ($data['BO'] == '') {
                    //     $formattype = 1;
                    // } else if ($data['BO'] != '') {
                    //     $formattype = 2;
                    // } else if ($data['A'] != 'Order Item Id') {
                    //     $result['status'] = false;
                    //     $result['message'] = 'Wrong file format';
                    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    //     exit;
                    // }

                    // echo $formattype;
                    // die();
                }



                // if ($nResult > 0) {
                //     $result['status'] = true;
                //     $result['message'] = $this->lang->line('savesuccess');
                // } else {
                //     $result['status'] = false;
                //     $result['message'] = $this->lang->line('error');
                // }
                // $result[$row_count] = true;
            } catch (Exception $ex) {
                $result[$row_count] = false;
                $result['message' . $row_count] = $ex;
            }

            $row_count++;
            $count++;
            // $row_count=$row_count+1;
            // die();
        }

        // $check_duplicate = $this->ImportModel->check_duplicate();

        // print_r( $check_duplicate);
        // die();
        if ($FlagFormat == true) {
            $result['status'] = false;
            $result['message'] =  $ErrorMassage;
        } else {

            // print_r("Update3");
            $dataUpdate['update_date'] =  date('Y-m-d H:i:s');
            $dataUpdate['update_user'] = $this->session->userdata('user_name');
            // $return = $this->WeekNumberModel->update_wk($dataUpdate);
            $result['status'] = true;
            $result['message'] = $this->lang->line('savesuccess');
        };
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        // $dataimpot['file_name'] =  $file;
        //     $dataimpot['type'] =  'pnd53';

        //     $nResult = $this->Pnd53Model->insert_pnd53($dataimpot);
    }
}

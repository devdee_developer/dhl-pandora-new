<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CodeAppleAndDhl extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
        $role_data =  $this->session->userdata('role_PANDASH');
		if (!$role_data['MAPC']) {
			redirect('Login');
		}
        $this->load->view('share/head');
        $this->load->view('share/sidebar');
        $this->load->view('codeapple_dhl/codeapple_dhl_view');
        $this->load->view('share/footer');
    }

    public function callView()
    {
        $this->load->view('test');
    }
    public function addCodeAppleAndDhl()
    {
        // $this->output->set_content_type('application/json');
        $nResult = 0;

        try {

            $this->load->model('CodeAppleAndDhlModel', '', TRUE);

            $dataPost = json_decode($this->input->raw_input_stream, true);

            /*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/

            //$dateRecord = date("Y-m-d H:i:s"); 
            $data['id'] =  isset($dataPost['id']) ? $dataPost['id'] : 0;
            $data['reason_code'] =  isset($dataPost['reason_code']) ? $dataPost['reason_code'] : "";
            $data['code_description'] =  isset($dataPost['code_description']) ? $dataPost['code_description'] : "";
            $data['check_point'] =  isset($dataPost['check_point']) ? $dataPost['check_point'] : "";
            $data['remark'] =  isset($dataPost['remark']) ? $dataPost['remark'] : "";
            //$data['IsActive'] = isset($dataPost['IsActive'])?$dataPost['IsActive']: "1";
            //$data['website'] =  isset($dataPost['website'])?$dataPost['website']: "";

            //print_r($data);

            //$data['update_date'] = $dateRecord;
            //$data['update_user'] = $this->session->userdata('user_name'); 
            // load model 
            $check_duplicate = $this->CodeAppleAndDhlModel->check_duplicate($data);
            // print_r($check_duplicate);die();

            if ($data['id'] == 0) {
                if (count($check_duplicate) > 0) {
                    $result['status'] = false;
                    $result['message'] = 'Duplicate data checkpiont. Please add new';
                } else {
                    $nResult = $this->CodeAppleAndDhlModel->insert($data);
                }
            } else {
                if (count($check_duplicate) > 0) {
                    $result['status'] = false;
                    $result['message'] = 'Duplicate data checkpiont. Please add new';
                } else {
                    $nResult = $this->CodeAppleAndDhlModel->update($data['id'], $data);
                }
            }
            if ($nResult > 0) {
                $result['status'] = true;
                $result['message'] = $this->lang->line("savesuccess");
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function deleteCodeAppleAndDhl()
    {
        try {
            $this->load->model('CodeAppleAndDhlModel', '', TRUE);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $id =  isset($dataPost['id']) ? $dataPost['id'] : 0; // $this->input->post('ap_id');

            $bResult = $this->CodeAppleAndDhlModel->deleteCodeAppleAndDhlBy($id);

            if ($bResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line("savesuccess");
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line("error_faliure");
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    // public function getcodeappleanddhlModelList()
    // {

    //     try {
    //         $this->load->model('CodeAppleAndDhlModel', '', TRUE);

    //         /*$data['m_id'] =  $this->input->post('mab_m_id');

    // 		$limit =  $this->input->post('limit');
    // 		$offset =  $this->input->post('offset');
    // 		$order = $this->input->post('order');
    // 		$direction = $this->input->post('direction');

    // 		$result = $this->AdminModel->getAdminNameAllList($data, $limit , $offset, $order, $direction); */

    //         $result['status'] = true;
    //         $result['message'] = $this->lang->line("savesuccess");
    //     } catch (Exception $ex) {
    //         $result['status'] = false;
    //         $result['message'] = "exception: " . $ex;
    //     }

    //     echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }


    public function getCodeAppleAndDhlModel()
    {

        try {
            $this->load->model('CodeAppleAndDhlModel', '', TRUE);
            $dataPost = json_decode($this->input->raw_input_stream, true);

            //print_r($_POST);
            //print_r($this->input->post()); 
            //echo $this->input->raw_input_stream;  
            //print_r($dataPost);die();
            //$dateRecord = date("Y-m-d H:i:s"); 
            $PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
            $direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
            $dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->CodeAppleAndDhlModel->getCodeAppleAndDhlNameList($dataModel, $PageSize, $offset, $direction, $SortOrder);
            $result['totalRecords'] = $this->CodeAppleAndDhlModel->getTotal($dataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

            //$result['message'] = $this->AdminModel->getAdminModel(); 

        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getCodeAppleAndDhlComboList()
    {

        try {
            $this->load->model('CodeAppleAndDhlModel', '', TRUE);
            $result['status'] = true;
            $result['message'] = $this->CodeAppleAndDhlModel->getCodeAppleAndDhlComboList();
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = "exception: " . $ex;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

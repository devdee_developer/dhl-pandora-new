<?
    function numtothaistring($num)
    {
        $return_str = "";
        $txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า');
        $txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน');
        $num_arr = str_split($num);
        $count = count($num_arr);
    
        if($count == 1){
            foreach($num_arr as $key=>$val){
                $return_str .= $txtnum1[$val];
            }    
        }
        else if($count == 2){
            foreach($num_arr as $key=>$val){
                if($count > 1 && $val == 2 && $key ==($count-2))
                {
                    $return_str .= "ยี่สิบ";
                }
                else if($count > 1 && $val == 1 && $key ==($count-2)){
                    $return_str .= "สิบ";
                }
                else if($count > 1 && $val == 1 && $key ==($count-1))
                {
                    $return_str .= "เอ็ด";
                }
                else
                {
                    if($count > 1 && $val != 0 )
                    {
                        $return_str .= $txtnum1[$val].$txtnum2[$count-$key-1]; 
                    }
                     
                }
            }
            
        }
        else if($count > 2){
            foreach($num_arr as $key=>$val)
            {
                if($count > 1 && $val == 1 && $key ==($count-1))
                {
                    $return_str .= "เอ็ด";
                }
                else if($count > 1 && $val == 2 && $key ==($count-2))
                {
                    $return_str .= "ยี่สิบ";
                }
                else
                {
                    if($count > 1 && $val == 1&& $key ==($count-2)){
                        $return_str .= $txtnum2[$count-$key-1]; 
                    }
                    else if($count > 1 && $val != 0)
                    {
                        $return_str .= $txtnum1[$val].$txtnum2[$count-$key-1]; 
                    }
                     
                }       
                         
            }
        }  
    return $return_str ;
    }
    function numtothai($num)
    {
        $return = "";
        $num = str_replace(",","",$num);
        $number = explode(".",$num);
        if(sizeof($number)>2){
        return 'รูปแบบข้อมุลไม่ถูกต้อง';
        exit;
        }
        $return .= numtothaistring($number[0])."บาท";
        $stang = intval($number[1]);
        if($stang > 0)
        $return.= numtothaistring($stang)."สตางค์";
        else
        $return .= "ถ้วน";
        return $return ;
    }
    function DateThai($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strMonthCut = array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
        //test
    }
    function DateThaiFull($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strMonthCut = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
        return "$strDay เดือน $strMonthThai พ.ศ. $strYear";
        //test
    }
    function DateThaiEng($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strMonthCut = array("","01","02","03","04","05","06","07","08","09","10","11","12");
		$strMonthThai=$strMonthCut[$strMonth];
        return "$strDay/$strMonthThai/$strYear";
        //test
    }
    function StyDateRuning($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strMonthCut = array("","01","02","03","04","05","06","07","08","09","10","11","12");
		$strMonthThai=$strMonthCut[$strMonth];
        return "$strYear$strMonthThai";
        //test
    }
    function getMonth($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strMonthCut = array("","01","02","03","04","05","06","07","08","09","10","11","12");
		$strMonthThai=$strMonthCut[$strMonth];
        return "$strMonthThai";
        //test
    }
    function GetDateTH($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
        return "$strDay";
        //test
    }
    function GetMonthTH($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strMonthCut = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
        return "$strMonthThai";
        //test
    }
    function GetYearTH($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
        return "$strYear";
        //test
    }
    // Get string length for Character Thai
    function getStrLenTH($string)
    {
        $array = getMBStrSplit($string);
        $count = 0;
        
        foreach($array as $value)
        {
            $ascii = ord(iconv("UTF-8", "TIS-620", $value ));
            
            if( !( $ascii == 209 ||  ($ascii >= 212 && $ascii <= 218 ) || ($ascii >= 231 && $ascii <= 238 )) )
            {
                $count += 1;
            }
        }
        return $count;
    }
    // Convert a string to an array with multibyte string
    function getMBStrSplit($string, $split_length = 1)
    {
        mb_internal_encoding('UTF-8');
        mb_regex_encoding('UTF-8'); 
        
        $split_length = ($split_length <= 0) ? 1 : $split_length;
        $mb_strlen = mb_strlen($string, 'utf-8');
        $array = array();
        $i = 0; 
        
        while($i < $mb_strlen)
        {
            $array[] = mb_substr($string, $i, $split_length);
            $i = $i+$split_length;
        }
        
        return $array;
    }
    function utf8_strlen($s) 
    {
        $c = strlen($s); $l = 0;
        for ($i = 0; $i < $c; ++$i)
        if ((ord($s[$i]) & 0xC0) != 0x80) ++$l;
        return $l;
    }
?>
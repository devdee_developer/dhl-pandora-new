<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class MySSO
{
    public function __construct()
    {
        //parent::__construct();
    }

    // บน Host
    // public  $servername = "localhost";
	// public	$username = "dhlops01_dba";
	// public	$password = "P@ssw0rd";
	// public	$dbname = "dhlops01_dhlsso";
    
     // บน เครื่อง
	public  $servername = "localhost";
	public	$username = "devdee_dba";
	public	$password = "P@ssw0rd";
	public	$dbname = "dhl_sso";
	
	// public	$username = "root";
	// public	$password = "1234";
	// public	$dbname = "dhl_sso";
	
	public function Login($userlogin , $passwd)
	{ 
		$userData = [];
		 
		
		// Create connection
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

		// Check connection
		if ($conn->connect_error) {
		  die("Connection failed: " . $conn->connect_error);
		}
		 
		$sql = "SELECT * FROM t_user_account WHERE user_name = '" . $userlogin . "' and delete_flag = 0";//and password = '".$passwd ."'";
		$result = $conn->query($sql);
		 

		if ($result->num_rows > 0) {
		  // output data of each row
		  while($row = $result->fetch_assoc()) {
			//echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
			$userData = $row;
			//print_r($row);
		  }
		} else {
		//   echo "0 results";
		}
		$conn->close();
		
		return $userData;
	}
	
	public function ssoLogin($userlogin , $passwd)
	{ 
		$userData = [];
		 
		
		// Create connection
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

		// Check connection
		if ($conn->connect_error) {
		  die("Connection failed: " . $conn->connect_error);
		}
		 
		$sql = "SELECT * FROM t_user_account WHERE user_name = '" . $userlogin . "' ";//and password = '".$passwd ."'";
		$result = $conn->query($sql);
		 

		if ($result->num_rows > 0) {
		  // output data of each row
		  while($row = $result->fetch_assoc()) {
			//echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
			$userData = $row;
			//print_r($row);
		  }
		} else {
		//   echo "0 results";
		}
		$conn->close();
		
		return $userData;
	}
	
	public function getRoleData($id){
		
		$roleData = [];
		// Create connection
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

		// Check connection
		if ($conn->connect_error) {
		  die("Connection failed: " . $conn->connect_error);
		}
		 
		$sql = "SELECT * FROM t_user_role WHERE id = '" . $id . "' ";
		$result = $conn->query($sql);
		 
		if ($result->num_rows > 0) {
		  // output data of each row
		  while($row = $result->fetch_assoc()) {
			$tempData = $row["role_data"];
			$appData['APP'] = json_decode($tempData, true);
			$roleData['TPARTY'] = $appData['APP']['TPARTY'];
			$roleData['SUPPLY'] = $appData['APP']['SUPPLY'];
			$roleData['HOLIDAY'] = $appData['APP']['HOLIDAY'];
			$roleData['SMARTCOU'] = $appData['APP']['SMARTCOU'];
			$roleData['SMARTDIR'] = $appData['APP']['SMARTDIR'];
			$roleData['WLHOG'] = $appData['APP']['WLHOG'];
			$roleData['WLHOGAP'] = $appData['APP']['WLHOGAP'];
			$roleData['APPDASH'] = $appData['APP']['APPDASH'];
		
			break;
		  }
		} else {
		//   echo "0 results";
		}
		$conn->close(); 
		
		 
		return $roleData ;
	}

    public function mysql_aes_key($string, $key)
    {
        // echo 1;

        try {
           
			/*$iv = mcrypt_create_iv(
                mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
                MCRYPT_DEV_URANDOM
            );

            $encrypted = base64_encode(
                $iv .
                    mcrypt_encrypt(
                        MCRYPT_RIJNDAEL_128,
                        hash('sha256', $key, true),
                        $string,
                        MCRYPT_MODE_CBC,
                        $iv
                    )
            );
            return $encrypted;*/
			
			$plaintext = $string;
			$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
			$iv = openssl_random_pseudo_bytes($ivlen);
			$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
			$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
			$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
			
			return $ciphertext;
			 
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return $string;
        }
    }

    public function aes_decrypt($ciphertext, $key)
    {
        try {
           
			/*$data = base64_decode($ciphertext);
            $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

            $decrypted = rtrim(
                mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_128,
                    hash('sha256', $key, true),
                    substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
                    MCRYPT_MODE_CBC,
                    $iv
                ),
                "\0"
            );
            return $decrypted;
			*/
			
			$c = base64_decode($ciphertext);
			$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
			$iv = substr($c, 0, $ivlen);
			$hmac = substr($c, $ivlen, $sha2len=32);
			$ciphertext_raw = substr($c, $ivlen+$sha2len);
			$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
			$calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
			if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
			{
				//echo $original_plaintext."\n";
			} 
			return $original_plaintext;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return $string;
        }
    }
    // $key = 'DevDeeThailand';
    // $string = '1234';
    // echo "Key value : $key / String value : $string <br/>";
    // ​

    // $encyrpt = mysql_aes_key($string, $key);
    // echo "Encrypt Key value : ". $encyrpt. " <br/>";
    // $decyrpt = aes_decrypt($encyrpt, $key);
    // echo "Decrypt Key value : ". $decyrpt. " <br/>";

}
?>
